--[[ effect_es2.lua
]]

local ffi = require("ffi")
local fbf = require 'util.fbofunctions2'
local Filter = require 'filter2'


local effect_es2 = {}
effect_es2.__index = effect_es2

function effect_es2.new(...)
    local self = setmetatable({}, effect_es2)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function effect_es2:init(params)
    self.vbos = {}
    self.filters = {}
end

local glIntv   = ffi.typeof('GLint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')


function effect_es2:insert_effect(w,h)
    local params = {}

    params.name = "invert"
    params.source = [[
    void main()
    {
        gl_FragColor = vec4(1.) - texture2D(tex, uv); // Invert color
        gl_FragColor.a = .5;
    }
    ]]

    local filt = Filter.new(params)
    filt:initGL()

    -- Get w,h from the first fbo in the list if not specified.
    if not w then
        local first = self.filters[1].fbo
        w,h = first.w, first.h
    end
    filt:resize(w,h)

    -- Re-use the VBO for each program
    local vpos_loc = gl.glGetAttribLocation(filt.prog, "vPosition")
    gl.glVertexAttribPointer(vpos_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vpos_loc)

    table.insert(self.filters, filt)
end

function effect_es2:make_quad_vbos()
    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    table.insert(self.vbos, vvbo)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    local verts = glFloatv(4*2, {
        -1,-1,
        1,-1,
        1,1,
        -1,1,
        })
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
end

function effect_es2:bindVBO()
    -- if list empty, do nothing
    local filter = self.filters[#self.filters]
    if not filter then return end

    local vl = gl.glGetAttribLocation(filter.prog, "vPosition")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function effect_es2:unbindVBO()
    -- if list empty, do nothing
    local filter = self.filters[#self.filters]
    if not filter then return end

    local vl = gl.glGetAttribLocation(filter.prog, "vPosition")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function effect_es2:initGL(w,h)
    self:make_quad_vbos()
    self:insert_effect(w,h)
end

function effect_es2:exitGL()
    for k,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    for _,f in pairs(self.filters) do
        f:exitGL()
    end
end

function effect_es2:resize_fbo(w,h)
    for _,f in pairs(self.filters) do
        f:resize(w,h)
    end
end

function effect_es2:bind_fbo()
    local filter = self.filters[1]
    if not filter then return end
    if filter.fbo then
        fbf.bind_fbo(filter.fbo)
        gl.glViewport(0,0, filter.fbo.w, filter.fbo.h)
    end
end

function effect_es2:draw(prog, w, h, srctex)
    gl.glUseProgram(prog)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, srctex)
    local tx_loc = gl.glGetUniformLocation(prog, "tex")
    gl.glUniform1i(tx_loc, 0)

    self:bindVBO()
    gl.glDrawArrays(GL.GL_TRIANGLE_FAN, 0, 4)
    self:unbindVBO()

    gl.glUseProgram(0)
end

function effect_es2:unbind_fbo()
    fbf.unbind_fbo()
end

function effect_es2:present()
    -- if list empty, do nothing
    local filter = self.filters[#self.filters]
    if not filter then return end

    -- Display last effect's output to screen(bind fbo 0)
    local f = filter.fbo
    self:draw(filter.prog, f.w, f.h, f.tex)
end

return effect_es2
