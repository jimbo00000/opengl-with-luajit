--[[ crt_effect.lua

]]

crt_effect = {}

crt_effect.__index = crt_effect

local PostFXLibrary = require("effect.effect_chain")

function crt_effect.new(...)
    local params = {}
    params.filter_names = {
        "scanline",
        "scanline2",
        "monowarp",
    }
    return PostFXLibrary.new(params)
end

return crt_effect
