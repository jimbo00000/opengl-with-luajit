# imagetoraw.py
# Take an image filename on the cmd line,
# resize it to 128x128 and save out a file
# of raw bytes image data.
# Requires `pip install pillow`

from __future__ import print_function
import sys
from PIL import Image

def main(argv=None):
	im = Image.open(argv[0])
	print(im.format, im.size, im.mode)
	im = im.resize((128, 128))
	rawFile = open("out.raw", "wb")
	rawFile.write(im.tobytes())

if __name__ == "__main__":
	sys.exit(main(sys.argv[1:]))
