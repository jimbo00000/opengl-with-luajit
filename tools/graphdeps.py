# graphdeps.py
# python tools/graphdeps.py > graphdeps.dot
# dot graphdeps.dot -Tpng > graphdeps.png

import os
from glob import glob
import re

print("digraph {")
PATH="."
result = [y for x in os.walk(PATH) for y in glob(os.path.join(x[0], '*.lua'))]
for f in result:
	if '\\jit\\' not in f:
		module = f[2:].replace('\\','.').replace('.lua','')
		#print(module)
		with open(f) as src:
			for line in src:
				line = line.rstrip()
				if re.search("require", line):
					l = line.split('"')[1::2]
					if len(l) > 0:
						if l[0] != 'opengl' and l[0] != 'ffi':
							if l[0] != 'util.shaderfunctions' and l[0] != 'util.matrixmath':
								print("    "+'"'+module+'"'+" -> "+'"'+l[0]+'"')
print("}")
