# changeglconvention.py
# Changes from gl.Function to gl.glFunction convention.

from __future__ import print_function
import sys
import os, fnmatch

def find_files(directory, pattern):
	for root, dirs, files in os.walk(directory):
		for basename in files:
			if fnmatch.fnmatch(basename, pattern):
				filename = os.path.join(root, basename)
				yield filename

def replaceInFile(filepath, replacements):
	if not filepath.endswith(".lua"):
		return
	lines = []
	with open(filepath) as infile:
		for line in infile:
			for src, target in replacements.iteritems():
				line = line.replace(src, target)
			lines.append(line)
	with open(filepath, 'w') as outfile:
		for line in lines:
			outfile.write(line)

#
# Main: enter here
#
def main(argv=None):
	files = []
	for filename in find_files('.', '*'):
		files.append(filename)
	replacements = {
		"gl." : "gl.gl",
		"GL." : "GL.GL_",
	}
	print(replacements)
	for f in files:
		replaceInFile(f, replacements)


if __name__ == "__main__":
	sys.exit(main(sys.argv[1:]))
