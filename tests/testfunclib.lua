-- testfunclib.lua

local ffi = require 'ffi'
local bit = require("bit")

ffi.cdef[[
void incrementFloats(float* inout, const float* in, int num);
void printFloats(float* f, int num);
]]
-- Remember to run with LD_LIBRARY_PATH set
--package.cpath = package.cpath .. ';./?.so'
local funclib = ffi.load('libfunclib.so')

function printFloats(farr, num)
    --print(num)
    for i=0,num-1 do
        print('', i, farr[i])
    end
end

function main()
    local num = 8
    print(num)

    local farr = ffi.typeof('float[?]')(num)
    local varr = ffi.typeof('float[?]')(num)

    -- Populate arrays
    for i=0,num-1 do
        farr[i] = i -- - .1
        varr[i] = 3./(i+1)
    end
    funclib.printFloats(farr, num)

    for i=1,13 do
        funclib.incrementFloats(farr, varr, num)
    end

    funclib.printFloats(farr, num)
end

main()
