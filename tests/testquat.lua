-- testquat.lua

local mm = require("util.matrixmath")

function print_vec(v)
    local str = ""
    for k,v in pairs(v) do str = str .. tostring(v).." " end
    print(str)
end

function make_quat(theta, vec)
    local q = {
        math.cos(.5 * theta),
        vec[1] * math.sin(.5 * theta),
        vec[2] * math.sin(.5 * theta),
        vec[3] * math.sin(.5 * theta),
    }
    return q
end

function test_quat(q)
    local m = mm.quat_to_matrix(q)
    --print_vec(q)
    --print_vec(m)
    local x = {1,0,0,1}
    local y = {0,1,0,1}
    local z = {0,0,1,1}
    local tx = mm.transform(x, m)
    local dx = mm.length(tx) - 1
    local ty = mm.transform(y, m)
    local dy = mm.length(ty) - 1
    local tz = mm.transform(z, m)
    local dz = mm.length(tz) - 1
    local eps = .0000000001

    if dx > eps then
        print("!="..dx)
    end
    if dy > eps then
        print("!="..dy)
    end
    if dz > eps then
        print("!="..dz)
    end
end

function test_quat2(q)
    local m = mm.quat_to_matrix(q)
    local qq = mm.matrix_to_quat(m)
    print_vec(q)
    print_vec(qq)
    print("=======")
end

function main()
    for k,v in pairs(arg) do
    	--print(k,v)
    end

    local q = {1,0,0,0}
    test_quat(q)

    local v = {.5, .8, -.2}
    mm.normalize(v)
    q2 = make_quat(15, v)
    test_quat(q2)

    for i=0,100 do
        local v = {
            math.random(-1,1),
            math.random(-1,1),
            math.random(-1,1),
        }
        local t = math.random(-360,360)
        mm.normalize(v)
        q2 = make_quat(15, v)
        test_quat2(q2)
    end
end

main()
