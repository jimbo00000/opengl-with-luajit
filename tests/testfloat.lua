-- testfloat.lua

local ffi = require 'ffi'
local bit = require("bit")


function printx(x)
  print("0x"..bit.tohex(x))
end

function printFloat(x)
    print('', x, "0x"..bit.tohex(x))
end

function tobinary(x)
    str = '0b['
    for i=31,0,-1 do
        local bitmask = bit.lshift(1, i)
        local set = bit.band(bitmask, x)
        --print(',i',i, bitmask, bit.tohex(bitmask), set)
        --printx(bitmask)
        char = '1'
        if set == 0 then char = '0' end
        str = str..char
        if i == 8 
        or i == 16
        or i == 24 then
            str = str..' '
        end
    end
    str = str .. ']'
    return str
end

function main()
    local farr = ffi.typeof('float[?]')(8)
    local iarr = ffi.cast('uint32_t*', farr)
    --print(farr, iarr)

    local emask = 0x7f800000 -- 8 bits
    local mmask = 0x007fffff -- 23 bits
    local smask = 0x80000000

    farr[0] = 1.0
    farr[1] = -1
    farr[2] = .125
    farr[3] = -10
    farr[4] = -345678.90876856454
    farr[5] = 7.7
    farr[6] = 3.14159262979
    farr[7] = .0142857

    for i=0,6 do
        print(i,farr[i], bit.tohex(iarr[i]), tobinary(iarr[i]))
        local sign = bit.band(iarr[i], smask)
            --bit.rshift(iarr[i], 31)
        local exponent = bit.band(iarr[i], emask)
        local mantissa = bit.band(iarr[i], mmask)
        -- 32-bit float: 1,8,23
        -- 16-bit float: 1,5,10
        local shifted_mantissa = bit.rshift(mantissa, 23-10)
        local shifted_exponent = bit.rshift(exponent, (23+8)-(10+5))
        local shifted_sign     = bit.rshift(sign    , 32-16)
        if false then
            print('', '32sign bit=', "0x"..bit.tohex(sign), tobinary(sign))
            print('', '32exponent=', "0x"..bit.tohex(exponent), tobinary(exponent))
            print('', '32mantissa=', "0x"..bit.tohex(mantissa), tobinary(mantissa))
            print('', '16sign bit=', "0x"..bit.tohex(shifted_sign), tobinary(shifted_sign))
            print('', '16exponent=', "0x"..bit.tohex(shifted_exponent), tobinary(shifted_exponent))
            print('', '16mantissa=', "0x"..bit.tohex(shifted_mantissa), tobinary(shifted_mantissa))
        end
        local halffloat = bit.bor(
            shifted_sign,
            shifted_exponent,
            shifted_mantissa)
        print('', 'Half=', "0x"..bit.tohex(halffloat), tobinary(halffloat))
        print()
    end
end

main()
