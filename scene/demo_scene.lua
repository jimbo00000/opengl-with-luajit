-- demo_scene.lua

demo_scene = {}
demo_scene.__index = demo_scene

package.path = './scene/demo3/?.lua;' .. package.path
package.path = package.path .. ';scene/demo3/lib/?.lua'

local gfx = require("scene.demo3.scene.graphics")
local rk = require("rocket")

-- http://stackoverflow.com/questions/17877224/how-to-prevent-a-lua-script-from-failing-when-a-require-fails-to-find-the-scri
local function prequire(m)
  local ok, err = pcall(require, m)
  if not ok then return nil, err end
  return err
end

local bass, err = prequire("bass")
if bass == nil then
    print("main_demo.lua: Could not load Bass library: "..err)
end


function demo_scene.new(...)
    local self = setmetatable({}, demo_scene)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function demo_scene:init()
    self.firstTime = true

    --print("Load tracks from file")
    local tracks_module = 'data.tracks'
    local status, module = pcall(require, tracks_module)
    if not status then
        print('Tracks file ['..tracks_module ..'.lua] not found.')
        print('Re-launch with argument sync to connect to editor.')
        print('')
        print('Press any key to exit...')
        io.read()
        os.exit(1)
    end
    if rk then rk.sync_tracks = module end
end

function demo_scene:setDataDirectory(dir)
    self.data_dir = dir
end

function demo_scene:initGL()
    gfx.initGL()

    local init_ret = bass.BASS_Init(-1, 44100, 0, 0, nil)
    stream = bass.BASS_StreamCreateFile(false, "scene/demo3/data/queen2.mp3", 0, 0, bass.BASS_STREAM_PRESCAN)
    print("str",stream)
    --local streamlen_bytes = bass.BASS_ChannelGetLength(stream, bass.BASS_POS_BYTE)
    --local streamlen_sec = bass.BASS_ChannelBytes2Seconds(stream, streamlen_bytes)
    --print("Stream len: ",streamlen_sec)

   -- gfx.setbpm(bpm)

    bass.BASS_Start()
    bass.BASS_ChannelPlay(stream, false)
end

function demo_scene:exitGL()
    bass.BASS_StreamFree(stream)
    bass.BASS_Free()
end

function demo_scene:renderEye(model, view, proj)
    gfx.display()
end


--[[
    Music attributes:
    queen2.mp3 - 16 beats in 6.013 seconds
    16 / 6.013 == 2.66 beats.second *60 == 159 bpm
]]
local bpm = (16/6.013)*60
local rpb = 16 -- rows per beat
local rps = (bpm / 60) * rpb
local curtime_ms = 0.0
local song_end = 2*60 + 17

local function ms_to_row_f(time_ms, rps) 
    local row = rps * time_ms/1000
    return row
end

local curtime_ms = 0
function get_current_param_value_by_name(pname)
    if not rk then return end
    return rk.get_value(pname, ms_to_row_f(curtime_ms, rps))
end

function demo_scene:timestep(absTime, dt)
    if self.firstTime == true then
        self.firstTime = false
        self.startTime = absTime
    end

    local time = absTime - self.startTime
    curtime_ms = time * 1000
    --print(time)
    gfx.sync_params(get_current_param_value_by_name)
end

function demo_scene:keypressed(key, scancode, action, mods)
end

return demo_scene
