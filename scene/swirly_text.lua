--[[ swirly_text.lua

    Includes 2 scenes:
        - Poisson multigrid solver
        - Text render to image
]]
swirly_text = {}

swirly_text.__index = swirly_text

function swirly_text.new(...)
    local self = setmetatable({}, swirly_text)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local TextImageLib = require("scene.text_to_image")
local PoissonLib = require("scene.poisson_multigrid")
local mm = require("util.matrixmath")
local fbf = require("util.fbofunctions")
local ffi = require("ffi")

function swirly_text:init()
    self.dataDir = nil
    self.TextImage = TextImageLib.new()
    self.Poisson = PoissonLib.new()
    self.textbuf = ''
    self.lineh = .2
end
function swirly_text:setDataDirectory(dir)
    self.TextImage:setDataDirectory(dir)
    self.Poisson:setDataDirectory(dir)
    self.dataDir = dir
end

function swirly_text:init_rtt()
    local v = "fs"
    local InnerScene = require("scene."..v).new()
    local texFBO = require("scene.textured_fbo").new(InnerScene)
    self.InnerScene = InnerScene
    self.texFBO = texFBO
    self.texFBO:initGL()

    --self.texFBO.fbo.tex

    function readAll(file)
        local f = io.open(file, "rb")
        local content = f:read("*all")
        f:close()
        return content
    end
    --self.Frag:buildShader(hsv)

    local fn = self.dataDir..'/shaders/fs/@party2'
    InnerScene:buildShader(readAll(fn))

    local id = {}
    mm.make_identity_matrix(id)
    self:rtt_pass(id, id, id)
end

function swirly_text:rtt_pass(model, view, proj)
    -- Save viewport dimensions
    -- TODO: this is very inefficient; store them manually, restore at caller
    local vp = ffi.new("GLuint[4]", 0,0,0,0)
    gl.glGetIntegerv(GL.GL_VIEWPORT, vp)
    -- Save bound FBO
    local boundfbo = ffi.new("int[1]")
    gl.glGetIntegerv(GL.GL_FRAMEBUFFER_BINDING, boundfbo)


    local fbo = self.texFBO.fbo
    local p = {}
    local aspect = fbo.w / fbo.h
    mm.glh_perspective_rh(p, 60 * math.pi/180, aspect, .004, 500)

    -- Render pre-pass to fbo
    fbf.bind_fbo(fbo)
    gl.glViewport(0,0, fbo.w, fbo.h)

    local g = .5
    local c = {g,g,g,0}--self.clearColor
    gl.glClearColor(c[1],c[2],c[3],c[4])
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    local model2 = {}
    mm.make_identity_matrix(model2)
    local view2 = {}
    for i=1,16 do view2[i] = view[i] end
    mm.glh_translate(view2, 0,0,-15)

    if self.InnerScene.renderEye then
        self.InnerScene:renderEye(model2, view2, p)
    else
        self.InnerScene:render_for_one_eye(view2, p)
    end


    fbf.unbind_fbo(fbo)
    -- TODO: restore VAO?


    -- Restore viewport
    gl.glViewport(vp[0],vp[1],vp[2],vp[3])
    -- Restore FBO binding
    if boundfbo[0] ~= 0 then
        gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, boundfbo[0])
    end
end

function swirly_text:initGL()
    self.TextImage:initGL()
    self.Poisson:initGL()
    self:init_rtt()
end

function swirly_text:exitGL()
    self.TextImage:exitGL()
    self.Poisson:exitGL()
end

function swirly_text:renderEye(model, view, proj)
    self.Poisson:render_for_one_eye(view, proj)
end

function swirly_text:timestep(absTime, dt)
    self.Poisson:timestep(absTime, dt)
end

function swirly_text:print_hello()
    local textItems = {
        {
            "Hello", -- string
            {.2,.2}, -- xy pos
            16, -- size
            {0,1,0,} -- color
        },
        {
            "@party", -- string
            {.1,.5}, -- xy pos
            16, -- size
            {1,1,0,} -- color
        },
    }
    self.TextImage:renderPrePass(textItems)

    self.Poisson:loadTextureIntoColor(self.TextImage.fbo.tex)
end

function swirly_text:print_next_string()
        local stringlist = {
            "This is",
            "A 2D",
            "semi-",
            "Lagrangian",
            "vortex",
            "method",
        }
        self.stringidx = self.stringidx or 0
        self.stringidx = self.stringidx + 1
        if self.stringidx > #stringlist then self.stringidx = 1 end
        local strdata = {
            {
                stringlist[self.stringidx],
                {.03,.5},
                11,
                {
                    math.random(),
                    math.random(),
                    math.random(),
                }
            }
        }
        self.TextImage:renderPrePass(strdata)
        self.Poisson:loadTextureIntoColor(self.TextImage.fbo.tex)
end

function swirly_text:charkeypressed(ch)
    self.textbuf = self.textbuf..ch
end

function swirly_text:keypressed(key, scancode, action, mods)
    --local ret = self.Poisson:keypressed(key, scancode, action, mods)
    if key == string.byte('=') then
        self:print_hello()
    elseif key >= 290 and key <= 301 then -- F keys
        self:print_next_string()

    elseif key == 259 then -- bksp
        self.Poisson:loadTextureIntoColor(self.texFBO.fbo.tex)

    elseif key == 257 then -- enter
        local textItems = {
            {
                self.textbuf, -- string
                {.2, self.lineh}, -- xy pos
                6, -- size
                {
                    math.random(),
                    math.random(),
                    math.random(),
                },                
            },
        }
        self.TextImage:renderPrePass(textItems)
        self.Poisson:loadTextureIntoColor(self.TextImage.fbo.tex)
        self.textbuf = ''
        self.lineh = self.lineh + .12
        if self.lineh > .75 then
            self.lineh = .2
        end
    end
end

return swirly_text
