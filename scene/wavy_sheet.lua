--[[ wavy_sheet.lua

    Generate a subdivided quad mesh using compute shader and
    animate vertex positions, also with compute.
]]
wavy_sheet = {}

wavy_sheet.__index = wavy_sheet

function wavy_sheet.new(...)
    local self = setmetatable({}, wavy_sheet)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function wavy_sheet:init()
    self.vao = 0
    self.vbos = {}
    self.progs = {}
    self.num_verts = 0
    self.num_tri_idxs = 0
    self.subdivs = 256
    self.global_time = 0
end

--local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv   = ffi.typeof('GLint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

--[[
    Display the grid with basic lighting.
]]
local basic_vert = [[
#version 330

in vec4 vPosition;
in vec4 vNormal;

out vec3 vfPos;
out vec3 vfNormal;
out vec2 vfTex;

uniform mat4 mmtx;
uniform mat4 vmtx;
uniform mat4 prmtx;

void main()
{
    mat4 mvmtx = vmtx * mmtx;
    vfPos = (mvmtx * vPosition).xyz;
    vfTex = vPosition.xz;
    vfNormal = normalize(mat3(mvmtx) * vNormal.xyz);
    gl_Position = prmtx * mvmtx * vPosition;
}
]]

local basic_frag = [[
#version 330

in vec3 vfPos;
in vec3 vfNormal;
in vec2 vfTex;
out vec4 fragColor;

#line 69
uniform float iGlobalTime;

// https://www.shadertoy.com/view/4tXSDf
// Voronoi Water
// Created by clayjohn
vec2 hash2(vec2 p ) {
   return fract(sin(vec2(dot(p, vec2(123.4, 748.6)), dot(p, vec2(547.3, 659.3))))*5232.85324);   
}
float hash(vec2 p) {
  return fract(sin(dot(p, vec2(43.232, 75.876)))*4526.3257);   
}

//Based off of iq's described here: http://www.iquilezles.org/www/articles/voronoilin
float voronoi(vec2 p) {
    vec2 n = floor(p);
    vec2 f = fract(p);
    float md = 5.0;
    vec2 m = vec2(0.0);
    for (int i = -1;i<=1;i++) {
        for (int j = -1;j<=1;j++) {
            vec2 g = vec2(i, j);
            vec2 o = hash2(n+g);
            o = 0.5+0.5*sin(iGlobalTime+5.038*o);
            vec2 r = g + o - f;
            float d = dot(r, r);
            if (d<md) {
              md = d;
              m = n+g+o;
            }
        }
    }
    return md;
}

float ov(vec2 p) {
    float v = 0.0;
    float a = 0.4;
    for (int i = 0;i<3;i++) {
        v+= voronoi(p)*a;
        p*=2.0;
        a*=0.5;
    }
    return v;
}

vec3 getColor(in vec2 uv)
{
    vec3 a = vec3(0.2, 0.4, 1.0);
    vec3 b = vec3(0.85, 0.9, 1.0);
    return mix(a, b, smoothstep(0.2, 0.25, ov(uv*5.0)));
}

vec3 lightPos = vec3(0., 20., -4.);
float shininess = 125.;
void main()
{
    //fragColor = vec4(normalize(abs(vfNormal)), 1.0);

    vec3 N = normalize(vfNormal);
    vec3 L = normalize(lightPos - vfPos); // direction *to* light
    vec3 E = -vfPos;

    // One-sided lighting
    vec3 spec = vec3(0.);
    float bright = max(dot(N,L), 0.);

    if (bright > 0.)
    {
        // Specular lighting
        vec3 H = normalize(L + E);
        float specBr = max(dot(H,N), 0.);
        spec = vec3(1.) * pow(specBr, shininess);
    }

    //fragColor = vec4(egNormal, 0.);
    vec3 basecol = getColor(vfTex);
    fragColor = vec4(abs(basecol) * bright + spec, 1.);
}
]]



--[[
    Set up initial vertex values - a subdivided square grid.
]]
local initverts_comp_src = [[
#version 430

#define THREADS_PER_BLOCK 256
layout(local_size_x=256) in;
layout(std430, binding=0) buffer nblock { vec4 positions[]; };
uniform int uFacets;

void main()
{
    uint index = gl_GlobalInvocationID.x;
    uint f_1 = uFacets + 1;
    uvec2 gridIdx = uvec2(index % f_1, index / f_1);
    vec4 v = vec4(
        float(gridIdx.x)/float(f_1),
        0.,
        float(gridIdx.y)/float(f_1),
        1.);
    positions[index] = v;
}
]]

function wavy_sheet:initVertexPositions()
    local vvbo = self.vbos.vertices
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vvbo[0])
    local prog = self.progs.initverts
    gl.glUseProgram(prog)

    local uf_loc = gl.glGetUniformLocation(prog, "uFacets")
    gl.glUniform1i(uf_loc, self.subdivs)

    gl.glDispatchCompute(self.num_verts/256+1, 1, 1)
    gl.glUseProgram(0)
end

--[[
    Set up initial face values - a subdivided square grid.
]]
local initfaces_comp_src = [[
#version 430
#line 115
#define THREADS_PER_BLOCK 256
layout(local_size_x=256) in;

layout(std430, binding=0) buffer iblock { uint indices[]; };

uniform int uFacets;

uint getFaceFromXY(uint i, uint j)
{
    return (uFacets+1) * j + i;
}

void main()
{
    uint index = gl_GlobalInvocationID.x;
    uint gridIdx = index / 6;
    uint i = gridIdx % uFacets;
    uint j = gridIdx / uFacets;

    uint triIdx = index % 6;
    uint tris[] = {
        getFaceFromXY(i,j),
        getFaceFromXY(i+1,j),
        getFaceFromXY(i,j+1),
        getFaceFromXY(i,j+1),
        getFaceFromXY(i+1,j),
        getFaceFromXY(i+1,j+1),
    };

    indices[index] = tris[triIdx];
}
]]

function wavy_sheet:setFaceIndices()
    local ivbo = self.vbos.elements
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, ivbo[0])
    local prog = self.progs.initfaces
    gl.glUseProgram(prog)

    local uf_loc = gl.glGetUniformLocation(prog, "uFacets")
    gl.glUniform1i(uf_loc, self.subdivs)

    gl.glDispatchCompute(self.num_tri_idxs/256+1, 1, 1)
    gl.glUseProgram(0)
    gl.glMemoryBarrier(GL.GL_SHADER_STORAGE_BARRIER_BIT)
end

--[[
    Perturb all vertices with an animated function.
]]
local perturbverts_comp_src = [[
#version 430

#define THREADS_PER_BLOCK 256
layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
uniform float uGlobalTime;

// by 4rknova
// https://www.shadertoy.com/view/XdXGRS
//  Adopted from Noise - value noise / iq
//  https://www.shadertoy.com/view/lsf3WH

#ifdef GL_ES
precision highp float;
#endif

float hash(in vec2 p)
{
    return fract(sin(dot(p,vec2(127.1,311.7)))*43758.5453123);
}

float noise(in vec2 p)
{
    vec2 i = floor(p);
    vec2 f = fract(p); 
    f *= f*(3.0-2.0*f);
    
    vec2 c = vec2(0,1);
    
    return mix(mix(hash(i + c.xx), 
                   hash(i + c.yx), f.x),
               mix(hash(i + c.xy), 
                   hash(i + c.yy), f.x), f.y);
}

float fbm(in vec2 p)
{
    float f = 0.5;
//    f += 0.50000 * noise(1.0 * p + uGlobalTime);
    f += 0.25000 * noise(2.0 * p + uGlobalTime);
    f += 0.12500 * noise(4.0 * p + uGlobalTime);
    f += 0.06250 * noise(8.0 * p + uGlobalTime);
    f += 0.03115 * noise(16.0 * p + uGlobalTime);
    return f;
}

void main()
{
    uint index = gl_GlobalInvocationID.x;
    vec4 p = positions[index];
    float t = uGlobalTime * .1;
    p.y = .4 * (fbm(p.xz)-.5);
    positions[index] = p;
}
]]

function wavy_sheet:perturbVertexPositions()
    local vvbo = self.vbos.vertices
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vvbo[0])
    local prog = self.progs.perturbverts
    gl.glUseProgram(prog)

    local ut_loc = gl.glGetUniformLocation(prog, "uGlobalTime")
    gl.glUniform1f(ut_loc, self.global_time)

    gl.glDispatchCompute(self.num_verts/256+1, 1, 1)
    gl.glUseProgram(0)
end

--[[
    Clear normals using compute shader to prepare for normal calculation
    which accumulates, averaging normals from each face adjacent to
    each vertex.
]]
local clear_normals_comp_src = [[
#version 430

#define THREADS_PER_BLOCK 256
layout(local_size_x=256) in;
layout(std430, binding=0) buffer nblock { vec4 normals[]; };

void main()
{
    uint index = gl_GlobalInvocationID.x;
    vec4 n = normals[index];
    n.xyz = vec3(0.);
    normals[index] = n;
}
]]

function wavy_sheet:clearNormals()
    local nvbo = self.vbos.normals
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, nvbo[0])
    local prog = self.progs.clearnorms
    gl.glUseProgram(prog)
    gl.glDispatchCompute(self.num_verts/256+1, 1, 1)
    gl.glUseProgram(0)
end

--[[
    Calculate normals in compute shader
    Trigger shader on element indices, indexing into positions and
    normals attribute buffers. Sum up the contributions from each
    face including each vertex indexed.
]]
local calc_normals_comp_src = [[
#version 430

layout(local_size_x=256) in;
layout(std430, binding=0) buffer vblock { vec4 positions[]; };
layout(std430, binding=1) coherent buffer nblock { vec4 normals[]; };

struct faceData {
    int a;
    int b;
    int c;
};
layout(std430, binding=2) coherent buffer iblock { faceData indices[]; };

uniform int numTris;
uniform int triidx;
uniform int trimod;

void main()
{
    uint index = gl_GlobalInvocationID.x;
    if (index >= numTris)
        return;

    // Handle only one triangle per quad at a time to avoid data races
    if ((index % 2) != trimod)
        return;

    faceData fd = indices[index];

    vec3 pos = positions[fd.a].xyz;
    vec3 posx = positions[fd.b].xyz;
    vec3 posy = positions[fd.c].xyz;

    vec3 v1 = posx - pos;
    vec3 v2 = posy - pos;
    vec3 norm = cross(v1, v2);

    if (triidx == 0)
    {
        normals[fd.a].xyz += norm;
    }
    else if (triidx == 1)
    {
        normals[fd.b].xyz += norm;
    }
    else
    {
        normals[fd.c].xyz += norm;
    }
}
]]
function wavy_sheet:recalculateNormals()
    self:clearNormals()

    local vvbo = self.vbos.vertices
    local nvbo = self.vbos.normals
    local fvbo = self.vbos.elements
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 1, nvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 2, fvbo[0])
    local prog = self.progs.calcnorms
    gl.glUseProgram(prog)

    local unt_loc = gl.glGetUniformLocation(prog, "numTris")
    local uti_loc = gl.glGetUniformLocation(prog, "triidx")
    local utm_loc = gl.glGetUniformLocation(prog, "trimod")
    gl.glUniform1i(unt_loc, self.subdivs*self.subdivs*2)
    for m=0,1 do
        gl.glUniform1i(utm_loc, m)
        for i=0,2 do
            gl.glUniform1i(uti_loc, i)
            gl.glDispatchCompute(self.num_tri_idxs/256+1, 1, 1)
            gl.glMemoryBarrier(GL.GL_ALL_BARRIER_BITS)
        end
    end
    gl.glUseProgram(0)
end

function wavy_sheet:init_gridmesh_verts(prog, facets)
    self.num_verts = (self.subdivs+1)*(self.subdivs+1)
    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    local vnorm_loc = gl.glGetAttribLocation(prog, "vNormal")

    local sz = self.num_verts*4*ffi.sizeof('float')
    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(vpos_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    self.vbos.vertices = vvbo

    local nvbo = glIntv(0)
    gl.glGenBuffers(1, nvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, nvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(vnorm_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    self.vbos.normals = nvbo

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vnorm_loc)

    self:initVertexPositions()
end

function wavy_sheet:init_gridmesh_faces(facets)
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)

    self.num_tri_idxs = 6 * facets * facets
    local sz = 4 * self.num_tri_idxs * ffi.sizeof('GLuint')

    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_COPY)
    self.vbos.elements = qvbo

    self:setFaceIndices()
end

function wavy_sheet:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.progs.meshvsfs = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    self.progs.initverts = sf.make_shader_from_source({
        compsrc = initverts_comp_src,
        })

    self.progs.initfaces = sf.make_shader_from_source({
        compsrc = initfaces_comp_src,
        })

    self.progs.clearnorms = sf.make_shader_from_source({
        compsrc = clear_normals_comp_src,
        })

    self.progs.calcnorms= sf.make_shader_from_source({
        compsrc = calc_normals_comp_src,
        })

    self.progs.perturbverts= sf.make_shader_from_source({
        compsrc = perturbverts_comp_src,
        })

    self:init_gridmesh_verts(self.progs.meshvsfs, self.subdivs)
    self:init_gridmesh_faces(self.subdivs)

    gl.glBindVertexArray(0)
end

function wavy_sheet:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    for _,p in pairs(self.progs) do
        gl.glDeleteProgram(p)
    end
    self.progs = {}

    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

function wavy_sheet:renderEye(model, view, proj)
    gl.glDisable(GL.GL_CULL_FACE)

    local prog = self.progs.meshvsfs
    local um_loc = gl.glGetUniformLocation(prog, "mmtx")
    local uv_loc = gl.glGetUniformLocation(prog, "vmtx")
    local upr_loc = gl.glGetUniformLocation(prog, "prmtx")
    gl.glUseProgram(prog)
    gl.glUniformMatrix4fv(uv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    local ut_loc = gl.glGetUniformLocation(prog, "iGlobalTime")
    gl.glUniform1f(ut_loc, self.global_time)

    local m = {}
    for i=1,16 do m[i] = model[i] end
    mm.glh_rotate(m,180,0,0,1)
    mm.glh_scale(m,15,15,15)
    mm.glh_translate(m,-.5,0,-.5)
    gl.glUniformMatrix4fv(um_loc, 1, GL.GL_FALSE, glFloatv(16, m))
    gl.glBindVertexArray(self.vao)
    gl.glDrawElements(GL.GL_TRIANGLES,
        self.num_tri_idxs,
        GL.GL_UNSIGNED_INT, nil)
    gl.glBindVertexArray(0)

    gl.glUseProgram(0)
end

function wavy_sheet:timestep(absTime, dt)
    self.global_time = absTime

    self:perturbVertexPositions()
    gl.glMemoryBarrier(GL.GL_ALL_BARRIER_BITS)
    self:recalculateNormals()
end

function wavy_sheet:keypressed(ch)
    if ch == 'f' then return end
    if ch == 'l' then return end

    if ch ~= 'n' then
        self:perturbVertexPositions()
    end
    gl.glMemoryBarrier(GL.GL_ALL_BARRIER_BITS)

    self:recalculateNormals()
end

return wavy_sheet
