local simple_collide = {
    _DESCRIPTION = [[
        A simple example of CPU movement and collision detection.
        Mover quads are checked for hits against world quads, and removed if hit.
        Target VBO is (optionally) updated on each removal.
    ]],
    _URL = 'https://bitbucket.org/jimbo00000/opengl-with-luajit/src/master/'
}
simple_collide.__index = simple_collide

local mm = require 'util.matrixmath'
local lri = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'
local bump = require 'thirdparty.bump'
local players = require 'player_sprites'
local blocks = require 'world_sprites'
local background = require 'parallax'


-- Serialization - simple load from lua file.
-- Corresponding save function resides in level_editor.
local function loadTargetsFromFile(filename)
    local function prequire(m)
        local ok, err = pcall(require, m)
        if not ok then return nil, err end
        return err
    end

    local r = prequire(filename) -- load happens here
    if r then
        return rects
    else
        print('Error loading targets file:', filename)
        return nil
    end
end

--
-- World functions
--

function simple_collide:removeTarget(other)
    --print(other.name, other.idx)
    -- Don't take out other players
    if other.score then return end

    self.world:remove(other)
    -- This call shuffles all items above it down to close the array.
    table.remove(self.targets, other.idx)
    -- Adjust idxs in world tables to match new position in array.
    for i=other.idx, #self.targets do
        --print(i, self.targets[i].box.idx, self.targets[i].box.name)
        self.targets[i].box.idx = i
    end

    if self.refresh_vbo then
        self:populateVBO()
    end
end

function simple_collide:repopulateBumpWorld()
    self.world = bump.newWorld(self.cellsz)

    for i=1,#self.movers do
        local mov = self.movers[i]
        local x,y = mov.pos[1], mov.pos[2]
        local w,h = mov.sz[1], mov.sz[2]
        self.world:add(mov,  x,y,  w,h)
    end

    self:populateBumpWorld()
end

-- Inject target data into draw object
function simple_collide:populateVBO()
    self.blocks:populateVBOFromTargetData(self.targets)
end

-- Inject target data into bump world
function simple_collide:populateBumpWorld()
    for i=1,#self.targets do
        local t = self.targets[i]
        local x = t.pos[1]
        local y = t.pos[2]
        local w = t.sz[1]
        local h = t.sz[2]
        local blockType = t.which or 1

        local box = {
            name='obj'..i,
            idx=i,
            type=blockType,
        }
        self.world:add(box,  x,y,  w,h)
        t.box = box
    end
end


--
-- Avatar functions
--

function simple_collide:AddNewMoverAtIndex(idx)
    local mov = self.players:createNewMover(idx)
    self.movers[idx] = mov

    local x,y = mov.pos[1], mov.pos[2]
    local sz = mov.sz
    local w,h = sz[1], sz[2]

    mov.jumpVel = self.jumpVelocity

    self.world:add(mov,  x,y,  w,h)
    print('mover added:', self.world:getRect(mov))
end

function simple_collide:resizeMover(mov, factor)
    for i=1,2 do
        mov.sz[i] = mov.sz[i] * factor
    end
    mov.jumpVel = mov.jumpVel * math.sqrt(factor)
    local x,y = mov.pos[1], mov.pos[2]
    local w,h = mov.sz[1], mov.sz[2]
    self.world:update(mov,  x,y,  w,h)
end



--
-- Module functions
--
function simple_collide.new(...)
    local self = setmetatable({}, simple_collide)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end
    return self
end

function simple_collide:setDataDirectory(dir)
    self.dataDir = dir
    if snd.setDataDirectory then snd.setDataDirectory(dir) end

    self.players:setDataDirectory(dir)
    self.blocks:setDataDirectory(dir)
    self.background:setDataDirectory(dir)
end

function simple_collide:resizeViewport(win_w,win_h)
    self.win_w,self.win_h = win_w,win_h
end

function simple_collide:init()
    self.t = 0
    self.t_last = 0

    self.draw_font = true
    self.refresh_vbo = true
    self.debug_mode = false

    self.n_quads = 16
    self.cellsz = 64

    self.movspeed = .01
    self.jumpVelocity = .03

    self.movers = {}
    self.world = bump.newWorld(self.cellsz)

    -- TODO: wrap in pcall and return error
    self.players = players.new(0)
    self.blocks = blocks.new()
    self.background = background.new()
    
    self.background.draw_font = false
end

function simple_collide:initGL()
    local t = loadTargetsFromFile('rect_data')
    self.targets = t or makeRandomTableOfNTargets(self.n_quads)
    self:populateBumpWorld()
    

    local fontname = 'segoe_ui128'
    self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
    self.glfont:setDataDirectory(self.dataDir.."/fonts")
    self.glfont:initGL()

    self.players:initGL()
    self.blocks:initGL()
    self.background:initGL()
    self:populateVBO()
end

function simple_collide:exitGL()
    self.players:exitGL()
    self.blocks:exitGL()
    self.background:exitGL()
end

function simple_collide:render(viewMatInput)
    gl.glClearColor(0.1, 0.0, 0.0, 0.0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    local viewMatrix = {}
    if viewMatInput == nil then
        mm.make_identity_matrix(viewMatrix)
        local mov = self.movers[1]
        if mov then
            local zoom = math.pow(mov.sz[1]/.15, -.5)
            mm.glh_scale(viewMatrix, zoom, zoom, zoom)
            mm.glh_translate(viewMatrix, -mov.pos[1], -mov.pos[2], 0)
        end
    else
        viewMatrix = viewMatInput
    end

    self.background:render(viewMatrix)
    self.blocks:render(viewMatrix)
    self.players:renderSprites(self.movers, viewMatrix)

    if self.draw_font then
        self:drawOverlay()
    end
end

function simple_collide:drawOverlay()
    if not self.glfont then return end

    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .35
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    local str = 'bump/attribs: '
    str = str .. 'n='..tostring(#self.targets)
    str = str .. '/'..tostring(self.n_quads)
    str = str .. '  ['

    for i=1,#self.movers do
        str = str..tostring(self.movers[i].score)..'  '
    end
    str = str..']'
    self.glfont:render_string(m, p, col, str)

    if self.debug_mode then
        mm.glh_translate(m, 0, 120, 0)
        self.glfont:render_string(m, p, col,
            "DBG cellsz="..tostring(self.cellsz)
            ..' refr='..tostring(self.refresh_vbo)
            )
    end
end

function simple_collide:timestep(absTime, dt)
    self.t = absTime
    for j=1,#self.movers do
        local mov = self.movers[j]
        if not mov then return end
        self:moveInWorld(mov, dt)
        mov.time = mov.time + dt
    end


    local mov = self.movers[1]
    if mov then
        local backgroundSpeed = .2
        self.background.xoffset = mov.pos[1] * backgroundSpeed
    end
end

------------------------------------------
-- Collision detection
------------------------------------------
function simple_collide:moveInWorld(mov, dt)
    self.t = absTime

    local acc = {0,-.09} -- gravity

    local pos = mov.pos
    local vel = mov.vel
    for i=1,2 do
        -- Apply acceleration
        vel[i] = vel[i] + acc[i] * dt
    end

    mov.touchingFloor = false

    -- Floor check(y only)
    local miny = -1
    if pos[2] < miny then
        pos[2] = miny
        vel[2] = 0
        mov.touchingFloor = true
    end



    local s = mov.sz
    local goalPos = {}
    for i=1,2 do
        local newpos = mov.pos[i] + mov.vel[i]
        -- VBO holds quad in [0,s] (centered at x+s/2)
        goalPos[i] = newpos --math.min(math.max(-1, newpos), 1-s[i])
    end

    --
    -- Bump physics
    --
    local filters = {
        'touch',
        'cross',
        'slide',
        'bounce',
    }
    local playerFilter = function(item, other)
        if other.type and other.type < 8 then
            return 'slide'
        end
        return 'cross'
    end

    -- Hit Test
    local actualX, actualY, collisions, len =
        self.world:move(
            mov, goalPos[1], goalPos[2], playerFilter)

    -- Apply allowed movement
    mov.pos = {actualX, actualY}

    -- Normal force: Stop gravity when standing
    if goalPos[2] < mov.pos[2] then
        mov.vel[2] = 0
        mov.touchingFloor = true
    end

    -- Ceiling check to stop upward velocity in case of hit
    if goalPos[2] > mov.pos[2] then
        mov.vel[2] = 0
    end

    -- Consequences
    for i=1,len do
        local col = collisions[i]

        if col.other.type and col.other.type > 8 then
            -- Mover collects target
            snd.playSound("Pickup_Coin7.wav")
            self:removeTarget(col.other)
            local mov = self.movers[col.item.idx]
            mov.score = mov.score + 1
        end
    end
end

------------------------------------------
-- Player input
------------------------------------------

local function setVelocityFromInput(mov, curstate, v)
    local vel = mov.vel
    local velInput = v * mov.jumpVel * 30

    local moving = false
    if curstate[lri.JOYPAD_LEFT] ~= 0 then
        vel[1] = -velInput
        mov.dir = 2
        mov.moving = true
    elseif curstate[lri.JOYPAD_RIGHT] ~= 0 then
        vel[1] = velInput
        mov.dir = 1
        mov.moving = true
    else
        vel[1] = 0
        mov.moving = false
    end
--[[
    if curstate[lri.JOYPAD_UP] ~= 0 then
        vel[2] = v
        mov.dir = 0
        moving = true
    elseif curstate[lri.JOYPAD_DOWN] ~= 0 then
        vel[2] = -v
        mov.dir = 3
        moving = true
    else
        vel[2] = 0
    end]]
end

function simple_collide:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:on_button_pressed(j,b)
                end
            end

            -- Create mover if not already there
            if not self.movers[j] then
                self:AddNewMoverAtIndex(j)
            end

            setVelocityFromInput(self.movers[j], curstate, self.movspeed)
            self.players:animateSprite(self.movers[j])
        end
    end
end

function simple_collide:on_button_pressed(padIndex, button)
    local mov = self.movers[padIndex]
    if not mov then return end

    local sz_factor = 1.5

    -- Use this to swap tables
    if button == lri.JOYPAD_START then
        self.debug_mode = not self.debug_mode
    elseif button == lri.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    end

    local func_table_normal = {

        [lri.JOYPAD_A] = function (x)
            -- jump!
            if mov.touchingFloor then
                snd.playSound("test.wav")
                mov.t_last = self.t
                local pos = mov.pos
                local vel = mov.vel
                vel[2] = mov.jumpVel
            end
        end,

        [lri.JOYPAD_X] = function (x)
            snd.playSound("test.wav")
            self.t_last = self.t
            self:resizeMover(mov, 1 * sz_factor)
        end,
        [lri.JOYPAD_Y] = function (x)
            snd.playSound("Pickup_Coin7.wav")
            self.t_last = self.t
            self:resizeMover(mov, 1 / sz_factor)
        end,
        --[[
        [lri.JOYPAD_L] = function (x)
            self.movspeed = self.movspeed / sz_factor
            snd.playSound("Pickup_Coin7.wav")
            self.t_last = self.t
        end,

        [lri.JOYPAD_R] = function (x)
            self.movspeed = self.movspeed * sz_factor
            snd.playSound("Pickup_Coin7.wav")
            self.t_last = self.t
        end,
        ]]
    }

    local func_table_debug = {
        [lri.JOYPAD_A] = function (x)
            snd.playSound("test.wav")
            self.refresh_vbo = not self.refresh_vbo
        end,

        [lri.JOYPAD_Y] = function (x)
            self.cellsz =  self.cellsz / 2
            self.cellsz = math.max(1,self.cellsz)
            self:repopulateBumpWorld()
        end,

        [lri.JOYPAD_X] = function (x)
            self.cellsz =  self.cellsz * 2
            self:repopulateBumpWorld()
        end,
    }

    local func_table = func_table_normal
    if self.debug_mode then
        func_table = func_table_debug
    end
    local f = func_table[button]
    if f then f() end
end

return simple_collide
