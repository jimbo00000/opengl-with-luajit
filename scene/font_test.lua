--[[ font_test.lua

    A font rendering example, drawing 139 lines of lorem ipsum text.

    Uses the glfont class in util, which uses the bmfont module 
    to load font texture and glyphs. glfont handles rendering,
    trying to cache as much data as possible for rendering speed.
]]
local font_test = {}
font_test.__index = font_test

function font_test.new(...)
    local self = setmetatable({}, font_test)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function font_test:init()
    self.glfont = nil
    self.dataDir = nil
    self.lines = {}
end


local GLFontLibrary = require("util.glfont")
local mm = require("util.matrixmath")

-- Since data files must be loaded from disk, we have to know
-- where to find them. Set the directory with this standard entry point.
function font_test:setDataDirectory(dir)
    self.dataDir = dir
end

function font_test:initGL()
    -- Load text file
    local filename = "loremipsumbreaks.txt"
    if self.dataDir then filename = self.dataDir .. "/" .. filename end
    local file = io.open(filename)
    if file then
        for line in file:lines() do
            table.insert(self.lines, line)
        end
    end

    local dir = "fonts"
    if self.dataDir then dir = self.dataDir .. "/" .. dir end

    local fontChoices = {
        { 'courier_512.fnt', 'courier_512_0.raw' },
        { 'segoe_ui128.fnt', 'segoe_ui128_0.raw' },
        { 'vcr_512.fnt', 'vcr_512_0.raw' },
        { 'press_start_512.fnt', 'press_start_512_0.raw' },
        { 'sdf/abc.fnt', 'sdf/abc_0.raw', 2048, 2048 },
        { 'sdf/abc.fnt', 'sdf/abc_0sdf.raw', 2048, 2048, 2, GL.GL_RG },
    }
    local choice = fontChoices[1]

    self.glfont = GLFontLibrary.new(unpack(choice))
    self.glfont:setDataDirectory(dir)
    self.glfont:initGL()
end

function font_test:exitGL()
    if not self.glfont then return end
    self.glfont:exitGL()
end

function font_test:renderEye(model, view, proj)
    local m = {}
    for i=1,16 do m[i] = model[i] end
    mm.glh_translate(m, -3, 1.2, 0)
    local s = .002
    mm.glh_scale(m, s, -s, s)
    mm.pre_multiply(m, view)

    local col = {1, 1, 1}
    local lineh = self.glfont.font.common.lineHeight
    for k,v in pairs(self.lines) do
        self.glfont:render_string(m, proj, col, v)
        mm.glh_translate(m, 0, lineh, 0)
    end
end

return font_test
