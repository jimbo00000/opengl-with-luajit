local player_sprites = {
    _DESCRIPTION = [[
    	Part of the mouse_rects scene - draws a quad for each moving player sprite.
    ]],
    _URL = 'https://bitbucket.org/jimbo00000/opengl-with-luajit/src/master/'
}
player_sprites.__index = player_sprites

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local lri = require 'util.libretro_input'

function player_sprites.new(...)
    local self = setmetatable({}, player_sprites)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function player_sprites:setDataDirectory(dir)
    self.dataDir = dir
end

function player_sprites:createNewMover(idx)
    return {
        pos = {.1, .1*idx},
        vel = {0,0},
        sz = {.15, .15},
        texOffset = {0,0,1,1},
        name = 'player '..idx,
        idx = idx,
        score = 0,
        dir = 1,
        time = 0,
    }
end

-- Sprite sheet logic
-- Manipulate mover's texture
function player_sprites:animateSprite(mov)
    local texYoff = mov.dir / 4
    local loopTime = .8
    local numFrames = 4
    local t01 = math.fmod(mov.time, loopTime) / loopTime
    local whichFrame = math.floor(t01 * numFrames)
    if whichFrame == 3 then whichFrame = 1 end -- bounce animation

    if not mov.moving then
        whichFrame = 1
    end

    -- 7 unused pixels on left, 6 on right and 6 on top (32x32)
    mov.texOffset[1] = (whichFrame + 7/32) / 3
    mov.texOffset[2] = texYoff
    mov.texOffset[3] = (1/3) * (19/32)
    mov.texOffset[4] = (1/4) * (26/32)
end

function player_sprites:init(createTestMovers)
    createTestMovers = createTestMovers or 1
    self.vbos = {}
    self.prog = 0

    if createTestMovers > 0 then
        self.movers = {}
        for i=1,createTestMovers do
            local mov = self:createNewMover(i)
            mov.sz = {.3,.3}
            table.insert(self.movers, mov)
        end
    end
end

local vs = [[
#version 100

uniform vec4 uOffsetSz;
uniform vec4 uTexOffsetSz;
uniform mat4 uViewMatrix;

attribute vec2 aPosition;
varying vec2 vfC;

void main()
{
    vfC = uTexOffsetSz.zw * aPosition + uTexOffsetSz.xy;
    vfC.y = 1. - vfC.y;

    vec2 pt = aPosition;
    vec2 worldPt = uOffsetSz.zw*pt + uOffsetSz.xy;

    gl_Position = uViewMatrix * vec4(worldPt,0.,1.);
}
]]

local fs = [[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec2 vfC;
uniform float uBlue;
uniform sampler2D uSampler;

void main()
{
    vec4 col = texture2D(uSampler, vfC);
    col.b = uBlue;
    gl_FragColor = col;
}
]]

function player_sprites:bindVBO()
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(self.attributes.aPosition, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(self.attributes.aPosition)
end

function player_sprites:unbindVBO()
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(self.attributes.aPosition)
end

function player_sprites:generateVBO()
    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
end

function player_sprites:populateVBO()
    local mvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,mvbo)
    table.insert(self.vbos, mvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,mvbo[0])
    local s = 1
    local v = ffi.typeof('GLfloat[?]')(12,{0,0, s,0, s,s,    0,0, s,s, 0,s,})
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
end

function player_sprites:loadTextures(texfilename, w, h)
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end
    local inp = assert(io.open(texfilename, "rb"))
    local data = inp:read("*all")
    assert(inp:close())
    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    self.texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
                  w, h, 0,
                  GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end

function player_sprites:initGL()
    self.prog, err = sf.make_shader_from_source({vsrc=vs,fsrc=fs})
    if self.prog == 0 then
        local dbg = debug.getinfo(1)
        local tag = dbg.short_src..':'..dbg.currentline
        err = err:sub(1,#err-1) -- strip newline
        err = tag..'\n    '..err
        print(err)
        return err
    end

    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})
    local unis = {
        'uBlue',
        'uOffsetSz',
        'uTexOffsetSz',
        'uViewMatrix',
        'uSampler',
    }
    self.uniforms = {}
    local function uni(var) return gl.glGetUniformLocation(self.prog, var) end
    for k,v in pairs(unis) do
        self.uniforms[v] = uni(v)
    end

    local attrs = {
        'aPosition',
    }
    self.attributes = {}
    local function attr(var) return gl.glGetAttribLocation(self.prog, var) end
    for k,v in pairs(attrs) do
        self.attributes[v] = attr(v)
    end

    self:generateVBO()
    self:populateVBO()

    self.texID = 0
    self:loadTextures('pipo-nekonin002-3x4-96x128.rgba', 96, 128)
end

function player_sprites:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
end

function player_sprites:renderSprites(movers, viewMatrix)
    gl.glEnable(GL.GL_BLEND)

    gl.glUseProgram(self.prog)
    gl.glUniform1f(self.uniforms.uBlue, 0)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glUniform1i(self.uniforms.uSampler, 0)
    local glFloatv = ffi.typeof('GLfloat[?]')
    gl.glUniformMatrix4fv(self.uniforms.uViewMatrix, 1, GL.GL_FALSE, glFloatv(16,viewMatrix))

    self:bindVBO()
    local vvbo = self.vbos[2]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glVertexAttribPointer(self.attributes.aPosition, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(self.attributes.aPosition)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glUniform1i(self.uniforms.uSampler, 0)

    -- TODO: coalesce draw calls
    for i=1,#movers do
        local mov = movers[i]
        gl.glUniform4f(self.uniforms.uOffsetSz, mov.pos[1], mov.pos[2], mov.sz[1], mov.sz[2])
        local txoff = mov.texOffset
        gl.glUniform4f(self.uniforms.uTexOffsetSz, txoff[1], txoff[2], txoff[3], txoff[4])

        gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    end
    
    self:unbindVBO()
end

function player_sprites:render()
    self:renderSprites(self.movers)
end

function player_sprites:timestep(absTime, dt)
    self.t = absTime
    self.movers[1].time = absTime
    --self:animateSprite(self.movers[1])
end

-- TODO: controller functions for test

function player_sprites:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local mov = self.movers[1]
        local vel = mov.vel

        local moving = false
        if curstate[lri.JOYPAD_LEFT] ~= 0 then
            mov.dir = 2
            mov.moving = true
        elseif curstate[lri.JOYPAD_RIGHT] ~= 0 then
            mov.dir = 1
            mov.moving = true
        else
            vel[1] = 0
            mov.moving = false
        end
    end
end

return player_sprites
