-- boids.lua
-- A second-order particle motion system, reads particle positions
-- and computes their evolution in time with a compute shader,
-- writing new positions back to the next in a ring buffer.

local ffi = require("ffi")
local sf = require("util.shaderfunctions2")

boids = {}
boids.__index = boids

local movepoints_comp = [[
#version 310 es
layout(local_size_x=128) in;
layout(location = 0) uniform float time;
layout(location = 1) uniform float dt;
layout(location = 2) uniform int numParticles;
layout(std430, binding=0) buffer piblock { vec4 positionsIn[]; };
layout(std430, binding=1) buffer poblock { vec4 positionsOut[]; };
layout(std430, binding=2) buffer oiblock { vec4 orientationsIn[]; };
layout(std430, binding=3) buffer ooblock { vec4 orientationsOut[]; };

vec3 getAveragePosition()
{
    vec3 tot = vec3(0.);
    for (int i=0; i<numParticles; ++i)
    {
        tot += positionsIn[i].xyz;
    }
    return tot / float(numParticles);
}

// http://www.geeks3d.com/20140205/glsl-simple-morph-target-animation-opengl-glslhacker-demo/
vec4 Slerp(vec4 p0, vec4 p1, float t)
{
  float dotp = dot(normalize(p0), normalize(p1));
  if ((dotp > 0.9999) || (dotp<-0.9999))
  {
    if (t<=0.5)
      return p0;
    return p1;
  }
  float theta = acos(dotp * 3.14159/180.0);
  vec4 P = ((p0*sin((1.-t)*theta) + p1*sin(t*theta)) / sin(theta));
  P.w = 1.;
  return P;
}

void main() {
   int index = int(gl_GlobalInvocationID);
   float t = float(index) / float(numParticles);
   
   // Simply calculate a position in time
   vec4 position = positionsIn[index];
   vec4 ori = orientationsIn[index];

   vec3 p0 = vec3(0.);
   vec3 p1 = vec3(1.);
   //position.xyz = mix(p0, p1, t);

   vec3 vel = normalize(getAveragePosition() - position.xyz);
   position.xyz += dt * vel;
   //position.x += dt * 0.03 * sin(time + 32.*t);

   vec4 target = vec4(vec3(0.,1.,0.), 0.);
   ori = Slerp(ori, target, .5);

   positionsOut[index] = position;
   orientationsOut[index] = ori;
}
]]

function boids.new(...)
    local self = setmetatable({}, boids)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function boids:init()
    self.vao = 0
    self.vbos = {}
    self.prog = 0
    self.prog_move = 0
    self.npts = 128

    self.ringSz = 2
    self.ringPtr = 1
    self.buffers = {}
    self.ori_buffers = {}

    self.src = movepoints_comp
    self.descr = "Incremental particle system"
    self.shadertype = "compute"
end

function boids:stateString()
    return self.npts..' points, ringSz='..self.ringSz
end

local glFloatv = ffi.typeof('GLfloat[?]')

local basic_vert = [[
#version 310 es

in vec4 vPosition;
in vec4 vColor;
in vec4 instancePosition;
in vec4 instanceOrientation;

uniform mat4 mvmtx;
uniform mat4 prmtx;

out vec3 vfColor;

// Quaternion rotation of a vector
vec3 qtransform(vec4 q, vec3 v)
{
    return v + 2.0*cross(cross(v, q.xyz) + q.w*v, q.xyz);
}

void main()
{
    vfColor = vColor.xyz;
    vec3 pos = vPosition.xyz;
    pos -= vec3(.5); // center rotation on cube center
    vec4 q = normalize(instanceOrientation);
    pos = qtransform(q, pos);
    pos += vec3(.5);
    pos *= .035;
    pos += instancePosition.xyz;
    gl_Position = prmtx * mvmtx * vec4(pos, 1.);
}
]]

local basic_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfColor, 1.0);
}
]]

function boids:initSingleInstanceAttributes()
    local glIntv = ffi.typeof('GLint[?]')
    local glUintv = ffi.typeof('GLuint[?]')

    local verts = glFloatv(4*3, {
        0,2.598,0,
        0,0,1.732,
        -1.5,0,-.866,
        1.5,0,-.866,
        })

    local cols = glFloatv(4*3, {
        1,1,1,
        1,0,0,
        0,1,0,
        0,0,1,
        })

    local vpos_loc = gl.glGetAttribLocation(self.prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(self.prog, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(cols), cols, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)

    local tris = glUintv(4*3, {
        0,1,2,
        0,2,3,
        0,1,3,
        1,3,2,
    })
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(tris), tris, GL.GL_STATIC_DRAW)
    table.insert(self.vbos, qvbo)
end

function boids:allocatePerInstanceAttributes()
    local glIntv = ffi.typeof('GLint[?]')
    local glUintv = ffi.typeof('GLuint[?]')

    local vpos_loc = gl.glGetAttribLocation(self.prog, "instancePosition")
    local vcol_loc = gl.glGetAttribLocation(self.prog, "instanceOrientation")

    local sz = self.npts * 4 * ffi.sizeof('float')
    for i=1,2 do
        local vvbo = glIntv(0)
        gl.glGenBuffers(1, vvbo)
        gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
        gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_DRAW)
        gl.glVertexAttribPointer(vpos_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
        gl.glVertexAttribDivisor(vpos_loc, 1)
        table.insert(self.vbos, vvbo)

        table.insert(self.buffers, vvbo[0])

        local cvbo = glIntv(0)
        gl.glGenBuffers(1, cvbo)
        gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
        gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_DRAW)
        gl.glVertexAttribPointer(vcol_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
        gl.glVertexAttribDivisor(vcol_loc, 1)
        table.insert(self.vbos, cvbo)

        table.insert(self.ori_buffers, cvbo[0])
    end

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)
end

function boids:buildShader(src)
    -- TODO: Why does binding VAO here cause compilation to fail?
    --gl.glBindVertexArray(self.vao)
    gl.glDeleteProgram(self.prog_move)
    self.prog_move, err = sf.make_shader_from_source({
        compsrc = src,
        })
    --gl.glBindVertexArray(0)
    return err
end

local clear_instance_positions_comp_src = [[
#version 310 es
layout(local_size_x=128) in;
layout(location = 0) uniform int initType;
layout(location = 1) uniform int numParticles;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
layout(std430, binding=1) buffer oblock { vec4 orientations[]; };

float hash( float n ) { return fract(sin(n)*43758.5453); }

void main()
{
    int index = int(gl_GlobalInvocationID);
    if (index >= numParticles)
        return;
    float fi = float(index+1) / float(numParticles);

    vec3 randp = vec3(hash(fi), hash(fi*3.), hash(fi*17.));
    positions[index] = vec4(2.*randp-vec3(1.), 1.);
    orientations[index] = vec4(hash(fi*11.), hash(fi*5.), hash(fi*7.), hash(fi*19.));
}
]]
function boids:clearInstanceBuffers(initType)
    self.ringPtr = 1
    initType = initType or 0
    if self.prog_clear == nil then
        self.prog_clear, err = sf.make_shader_from_source({
            compsrc = clear_instance_positions_comp_src,
            })
        if err then print(err) end
    end

    gl.glUseProgram(self.prog_clear)
    gl.glUniform1i(0, initType)
    gl.glUniform1i(1, self.npts)

    for i=1,self.ringSz do
        gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, self.buffers[i])
        gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 1, self.ori_buffers[i])
        gl.glDispatchCompute(math.ceil(self.npts/128), 1, 1)
    end
    gl.glUseProgram(0)
    gl.glMemoryBarrier(GL.GL_ALL_BARRIER_BITS)
end


function boids:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.prog = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    self:initSingleInstanceAttributes()
    self:allocatePerInstanceAttributes()
    self:clearInstanceBuffers()
    gl.glBindVertexArray(0)

    self:buildShader(movepoints_comp)
end

function boids:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
    gl.glDeleteProgram(self.prog_move)
    gl.glDeleteProgram(self.prog_clear)

    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)
end

function boids:render_for_one_eye(view, proj)
    if self.prog == 0 then return end

    gl.glUseProgram(self.prog)
    local umv_loc = gl.glGetUniformLocation(self.prog, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(self.prog, "prmtx")
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))
    
    gl.glBindVertexArray(self.vao)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.buffers[self.ringPtr])
    local vpos_loc = gl.glGetAttribLocation(self.prog, "instancePosition")
    gl.glVertexAttribPointer(vpos_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.ori_buffers[self.ringPtr])
    local vori_loc = gl.glGetAttribLocation(self.prog, "instanceOrientation")
    gl.glVertexAttribPointer(vori_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)

    gl.glDrawElementsInstanced(GL.GL_TRIANGLES, 4*3, GL.GL_UNSIGNED_INT, nil, self.npts)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end

function boids:timestep(absTime, dt)
    -- Bind current fbo as tex, render to next
    local nextFbo = 1 + self.ringPtr
    if nextFbo > self.ringSz then nextFbo = 1 end

    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, self.buffers[self.ringPtr])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 1, self.buffers[nextFbo])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 2, self.ori_buffers[self.ringPtr])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 3, self.ori_buffers[nextFbo])

    gl.glUseProgram(self.prog_move)
    gl.glUniform1f(0, absTime)
    gl.glUniform1f(1, dt)
    gl.glUniform1i(2, self.npts)
    gl.glDispatchCompute(math.ceil(self.npts/128), 1, 1)
    gl.glUseProgram(0)
    
    -- Advance ring buf ptr
    self.ringPtr = nextFbo
end

function boids:reallocateBuffers()
    -- Delete all old
    -- TODO: keep single instance buffers around
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    self.ringPtr = 1
    self:initSingleInstanceAttributes()
    self:allocatePerInstanceAttributes()
    self:clearInstanceBuffers()
    gl.glBindVertexArray(0)
end

function boids:scaleBufferSize(k)
    self.npts = self.npts * k
    self:reallocateBuffers()
end
function boids:keypressed(key, scancode, action, mods)
    local func_table = {
        [298] = function (x) self:scaleBufferSize(.5*.5) end, -- F9
        [299] = function (x) self:scaleBufferSize(.5) end, -- F10
        [300] = function (x) self:scaleBufferSize(2) end, -- F11
        [301] = function (x) self:scaleBufferSize(2*2) end, -- F12
    }
    local f = func_table[key]
    if f then f() handled = true end
end

function boids:charkeypressed(key)
    if key == '-' then
        self:scaleBufferSize(.5)
    elseif key == '=' then
        self:scaleBufferSize(2)
    elseif key == '0' then
        self:clearInstanceBuffers()
    end
end

function boids:onSingleTouch(pointerid, action, x, y)
    --print("boids.onSingleTouch",pointerid, action, x, y)
end

return boids
