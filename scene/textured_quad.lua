--[[ textured_quad.lua

    A simple texturing example.

    Loads a texture image from raw data to eliminate the image format
    parsing process. The data can be saved out from Gimp or ImageMagick.
    We have to know the pixel dimensions and color depth ahead of time
    to get the right image.

    We also have to know the directory where the image file resides.
    The standard entry point setDataDirectory takes a directory string
    to be used as the path when opening the raw image file.
]]
textured_quad = {}

textured_quad.__index = textured_quad

function textured_quad.new(...)
    local self = setmetatable({}, textured_quad)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end
    return self
end

function textured_quad:init()
    -- Object-internal state: hold a list of VBOs for deletion on exitGL
    self.vbos = {}
    self.vao = 0
    self.prog = 0
    self.texID = 0
    self.dataDir = nil
end

local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions2")

local glIntv   = ffi.typeof('GLint[?]')
local glUintv  = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local basic_vert = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vPosition;
in vec4 vColor;

out vec3 vfColor;

uniform mat4 mmtx;
uniform mat4 vmtx;
uniform mat4 prmtx;

void main()
{
    vfColor = vColor.xyz;
    gl_Position = prmtx * vmtx * mmtx * vec4(vPosition,1.);
}
]]

local basic_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

uniform sampler2D sTex;

void main()
{
    vec4 tc = texture(sTex, vfColor.xy);
    fragColor = vec4(tc.xyz, 1.);
}
]]

function textured_quad:setDataDirectory(dir)
    self.dataDir = dir
end

function textured_quad:loadtextures()
    --local texfilename, w, h, bpp, format = "images/stone_128x128.raw", 128,128, 3, GL.GL_RGB
    --local texfilename, w, h, bpp, format = "fonts/sdf/abc_0sdf.raw", 2048, 2048, 2, GL.GL_RG
    local texfilename, w, h, bpp, format = "fonts/sdf/abc_0.raw", 2048, 2048, 4, GL.GL_RGBA
    --local texfilename, w, h, bpp, format = "fonts/segoe_ui128_0.raw", 512, 512, 4, GL.GL_RGBA

    if self.dataDir then texfilename = self.dataDir .. "/" .. texfilename end
    local inp = io.open(texfilename, "rb")
    local data = nil
    if not inp then
        print("File ", texfilename, "not found!")
        return
    end

    data = inp:read("*all")
    assert(inp:close())
    assert(#data/(w*h) == bpp)
    
    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    self.texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)

    --gl.glPixelStorei(GL.GL_UNPACK_ALIGNMENT, 1)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)

    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, format,
                  w, h, 0,
                  format, GL.GL_UNSIGNED_BYTE, data)

    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end

function textured_quad:init_cube_attributes()
    local v = {
        0,0,0,
        1,0,0,
        1,1,0,
        0,1,0,
    }
    local verts = glFloatv(#v,v)

    local c = {
        0,1, 1,1,
        1,0, 0,0,
    }
    local cols = glFloatv(#c,c)

    local vpos_loc = gl.glGetAttribLocation(self.prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(self.prog, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(cols), cols, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)

    local q = { -- Vertex indices for drawing triangles of faces
        0,2,3, 0,1,2,
    }
    local quads = glUintv(#q,q)
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(quads), quads, GL.GL_STATIC_DRAW)
    table.insert(self.vbos, qvbo)
end

function textured_quad:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.prog, err = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })
    if self.prog == 0 then
        error("Shader error: ".. err)
    end

    self:init_cube_attributes()
    self:loadtextures()
    gl.glBindVertexArray(0)
end

function textured_quad:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    gl.glDeleteProgram(self.prog)
    local texdel = ffi.new("GLuint[1]", self.texID)
    gl.glDeleteTextures(1,texdel)

    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)
end

function textured_quad:renderEye(model, view, proj)
    local um_loc = gl.glGetUniformLocation(self.prog, "mmtx")
    local uv_loc = gl.glGetUniformLocation(self.prog, "vmtx")
    local upr_loc = gl.glGetUniformLocation(self.prog, "prmtx")
    gl.glUseProgram(self.prog)
    gl.glUniformMatrix4fv(uv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    --gl.glEnable(GL.GL_BLEND)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    local stex_loc = gl.glGetUniformLocation(self.prog, "sTex")
    gl.glUniform1i(stex_loc, 0)

    gl.glUniformMatrix4fv(um_loc, 1, GL.GL_FALSE, glFloatv(16, model))
    gl.glBindVertexArray(self.vao)
    gl.glDrawElements(GL.GL_TRIANGLES, 3*2, GL.GL_UNSIGNED_INT, nil)
    gl.glBindVertexArray(0)

    gl.glUseProgram(0)
end

return textured_quad
