-- build a bridge
-- Fill up with fuel and building materials

-- https://github.com/dan200/ComputerCraft/blob/914df8b0c7cc28451ea56a53808024511ff52642/src/main/resources/assets/computercraft/lua/rom/programs/turtle/tunnel.lua

local function refuel()
    local fuelLevel = turtle.getFuelLevel()
    if fuelLevel == "unlimited" or fuelLevel > 0 then
        return
    end
    
    local function tryRefuel()
        for n=1,16 do
            if turtle.getItemCount(n) > 0 then
                turtle.select(n)
                if turtle.refuel(1) then
                    turtle.select(1)
                    return true
                end
            end
        end
        turtle.select(1)
        return false
    end
    
    if not tryRefuel() then
        print( "Add more fuel to continue." )
        while not tryRefuel() do
            os.pullEvent( "turtle_inventory" )
        end
        print( "Resuming Tunnel." )
    end
end


local function tryForward()
    refuel()
    while not turtle.forward() do
        if turtle.detect() then
            if not tryDig() then
                return false
            end
        elseif turtle.attack() then
            collect()
        else
            sleep( 0.5 )
        end
    end
    return true
end

function selectBuildingMaterial()
    -- todo: find building materials
    turtle.select(2)
end

function buildSection()
    local width = 2
    local b = width-1
    local guards = true
    selectBuildingMaterial()

    turtle.placeDown()

    if guards then
        turtle.turnLeft()
        turtle.place()
        turtle.turnRight()
    end

    turtle.turnRight()

    for i=1,b do
        tryForward()
        turtle.placeDown()
    end

    if guards then
        turtle.place()
    end

    turtle.turnLeft()
    -- Now facing fwd on right

    -- todo: alternate sides to save steps
    -- Back to start pos, on left facing fwd
    do
        turtle.turnLeft()
        for i=1,b do
            tryForward()
        end
        turtle.turnRight()
    end
end


print( "Bridging..." )
length = 3

for n=1,length do
    if n<length then
        buildSection()

        if false then
            turtle.up()
        end

        if not tryForward() then
            print( "Aborting Bridge." )
            break
        end
    else
        print( "Bridge complete." )
    end
end

-- todo: return to start?