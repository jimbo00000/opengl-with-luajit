--[[ particles2.lua

]]
local particles2 = {}
particles2.__index = particles2

local ParticlesLib = require("scene.particles")
local sf = require("util.shaderfunctions")
local mm = require("util.matrixmath")

function particles2.new(...)
    local self = setmetatable({}, particles2)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function particles2:init()
    self.particles = ParticlesLib.new()
    self.progs = {}

    self.scale = 1.008 --tweaked to match raymarch
    self.zoff = 0
    self.lightspeed = 1
end

function particles2:setDataDirectory(dir)
    self.particles:setDataDirectory(dir)
end

function particles2:initGL()
    self.particles:initGL()
    self:linearize_instance_positions()
end

function particles2:exitGL()
    self.particles:exitGL()
end

function particles2:renderEye(model, view, proj)
    local m = {}
    for i=1,16 do m[i] = model[i] end
    mm.glh_translate(m,0,0,self.zoff)

    self.particles:renderEye(m, view, proj)
end

function particles2:timestep(absTime, dt)
    self.particles:timestep(absTime, dt)

    self.particles.scale = self.scale
    self.particles.lightspeed = self.lightspeed
end

function particles2:keypressed(key, scancode, action, mods)
    self.particles:keypressed(key, scancode, action, mods)
    print(key)
    if key == 257 then
        self:linearize_instance_positions()
    end
end

--[[
    Scatter instance positions randomly in particles array.
]]
local randomize_instance_positions_comp_src = [[
#version 430

layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
layout(std430, binding=1) buffer oblock { vec4 orientations[]; };
layout(std430, binding=2) buffer vblock { vec4 velocities[]; };
layout(std430, binding=3) buffer avblock { vec4 angularVelocities[]; };
uniform int numPoints;

float hash( float n ) { return fract(sin(n)*43758.5453); }

void main()
{
    uint index = gl_GlobalInvocationID.x;
    if (index >= numPoints)
        return;

    const uint perRow = 16;
    const uint perCol = 16;
    const uint perLayer = perRow * perCol;

    uint rowIdx = index % perRow;
    uint colIdx = (index / perRow) % (perCol);
    uint layerIdx = index / (perRow * perCol);

    float x = float(rowIdx) / float(perRow);
    float y = float(colIdx) / float(perCol);
    float z = float(layerIdx) / float(perRow);

    vec3 xyz = vec3(x, y, z);

    // perturb
    float fi = float(index+1) / float(numPoints);
    //xyz += .025 * vec3(hash(fi*2.3), hash(fi*4.1), hash(fi*5.3));

    positions[index] = vec4(xyz, 1.);
}
]]
function particles2:linearize_instance_positions()
    if self.progs.scatter_instances == nil then
        self.progs.scatter_instances = sf.make_shader_from_source({
            compsrc = randomize_instance_positions_comp_src,
            })
    end
    local pvbo = self.particles.vbos.inst_positions
    local ovbo = self.particles.vbos.inst_orientations
    local vvbo = self.particles.vbos.inst_velocities
    local avvbo = self.particles.vbos.inst_angularvelocities
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, pvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 1, ovbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 2, vvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 3, avvbo[0])
    local prog = self.progs.scatter_instances
    local function uni(name) return gl.glGetUniformLocation(prog, name) end
    gl.glUseProgram(prog)
    print(self.particles.numPoints)
    gl.glUniform1i(uni("numPoints"), self.particles.numPoints)
    gl.glDispatchCompute(self.particles.numPoints/256+1, 1, 1)
    gl.glUseProgram(0)
end



return particles2
