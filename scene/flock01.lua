-- flock01.lua
--
-- smarter flocking algorithm
--
-- began as comp_scene.lua (c) 2015 James Susinno
-- then nbody07.lua (c) 2016 Mark J. Stock
-- portions (c) 2018 Mark Stock (markjstock@gmail.com)

--local openGL = require("opengl")
local ffi = require("ffi")
local sf = require("util.shaderfunctions")
local mm = require("util.matrixmath")

require("util.timeseries")

flock01 = {}

flock01.__index = flock01

function flock01.new(...)
    local self = setmetatable({}, flock01)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end
    return self
end

function flock01:init()
    self.vbos = {}
    self.vao = 0
    self.prog_display = 0
    self.prog_findvels = 0
    self.prog_integrate = 0
    self.npts = 8*1024
    self.dt = 1/20
    self.time = 0

    -- uncommenting this crashes this scene
    --self.minuteramp = timeseries.new("uniform ramp", 60)

    self.descr = "Flocking/boids particle system"
end

-- Types from:
-- https://github.com/nanoant/glua/blob/master/init.lua
local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glCharv    = ffi.typeof('GLchar[?]')
local glSizeiv   = ffi.typeof('GLsizei[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

local pt_vert = [[
#version 310 es
layout(location = 0) uniform mat4 Model;
layout(location = 1) uniform mat4 View;
layout(location = 2) uniform mat4 Projection;
layout(location = 0) in vec4 vposition;
layout(location = 1) in vec4 vattribute;
layout(location = 2) in vec4 quadAttr;
out vec2 radbrite;
out vec2 txcoord;
void main() {
   radbrite = vattribute.wz;
   float rad = radbrite.x;
   float brite = radbrite.y;

   vec4 pos = View*vposition;
   vec4 ppos = Projection*pos;
   // their apparent radius
   //float fudge = rad * 0.02 * ppos.z;
   // minimum radius
   float fudge = 0.003 * ppos.z;
   float newrad = max(rad, fudge);
   brite = brite * rad / newrad;

   txcoord = quadAttr.xy;
   gl_Position = Projection * vec4(((View * Model) * vposition).xyz + newrad*quadAttr.xyz, 1.);
}
]]

local rad_frag = [[
#version 310 es
#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif
in vec2 txcoord;
in vec2 radbrite;
layout(location = 0) out vec4 FragColor;
void main() {
   float rs = dot(txcoord, txcoord);
   float s = 1./(1.+16.*rs*rs*rs) - 0.06;
   float brite = radbrite.y;
   FragColor = s*vec4(brite, brite, brite, 1.0);
}
]]

--
-- pos for position (x, y, z, 1)
-- vel for velocity (x, y, z, 1)
-- att for attributes (mass, rad squared, brightness, radius)
--
local findvels_comp = [[
#version 310 es
layout(local_size_x=128) in;

layout(location = 0) uniform float dt;
layout(location = 1) uniform int numParticles;
layout(std430, binding=0) restrict readonly buffer pblock { vec4 positions[]; };
layout(std430, binding=1) restrict readonly buffer mblock { vec4 attributes[]; };
layout(std430, binding=2) restrict readonly buffer vblock { vec4 velocities[]; };
layout(std430, binding=3) restrict          buffer nblock { vec4 newvels[]; };

shared vec4 spos[gl_WorkGroupSize.x];
shared vec4 svel[gl_WorkGroupSize.x];
shared vec4 satt[gl_WorkGroupSize.x];
void main() {
   int N = int(gl_NumWorkGroups.x*gl_WorkGroupSize.x);
   int index = int(gl_GlobalInvocationID);
   vec3 mypos = positions[index].xyz;
   vec3 myvel = velocities[index].xyz;
   vec3 newvel = myvel;
   vec3 neibavgpos = vec3(0,0,0);
   vec3 neibavgvel = vec3(0,0,0);
   float numneibs = 1.0;
   for(int tile = 0;tile<N;tile+=int(gl_WorkGroupSize.x)) {
       spos[gl_LocalInvocationIndex] = positions[tile + int(gl_LocalInvocationIndex)];
       svel[gl_LocalInvocationIndex] = velocities[tile + int(gl_LocalInvocationIndex)];
       satt[gl_LocalInvocationIndex] = attributes[tile + int(gl_LocalInvocationIndex)];
       groupMemoryBarrier();
       barrier();

       int wsz = int(gl_WorkGroupSize.x);
       for(int i = 0;i<wsz;++i) {
           vec3 otherpos = spos[i].xyz;
           vec3 othervel = svel[i].xyz;
           vec2 otheratt = satt[i].xy;
           vec3 diff = mypos - otherpos;
           float dist = length(diff) + 0.1;
           //if too close
           if (dist < 0.2) {
               //push away
               newvel += (0.2/dist)*diff;
           }
           //if medium distance
           if (dist < 0.5) {
               neibavgpos += otherpos;
               neibavgvel += othervel;
               numneibs += 1.0;
               // attract
               //newvel -= 0.01*diff;
               // borrow velocity
               //newvel += 0.01*(othervel-myvel);
           }
           //float invdist = 1.0/(length(diff)+otheratt.y);
           //newvel += diff * (invdist*invdist*invdist);
       }
       groupMemoryBarrier();
       barrier();
   }

   //move toward avg position of neibs
   newvel += 0.25 * (neibavgpos/numneibs - mypos);
   //aim toward average vel of neibs
   newvel += 0.25 * (neibavgvel/numneibs - myvel);

   //normalize and save
   newvel = normalize(newvel);
   newvels[index] = vec4(newvel,0);
}
]]

--
-- should we normalize the velocity here?
--
local integ_comp = [[
#version 310 es
layout(local_size_x=128) in;

layout(location = 0) uniform float dt;
layout(location = 1) uniform int numParticles;
layout(std430, binding=0) restrict          buffer pblock { vec4 positions[]; };
layout(std430, binding=1) restrict readonly buffer mblock { vec4 attributes[]; };
layout(std430, binding=2) restrict          buffer vblock { vec4 velocities[]; };
layout(std430, binding=3) restrict readonly buffer nblock { vec4 newvels[]; };

float hash( float n ) { return fract(sin(n)*43758.5453); }

void main() {
   int index = int(gl_GlobalInvocationID);
   float fi = float(index+1) / float(100000.);
   vec4 position = positions[index];
   vec4 oldvel = velocities[index];
   vec4 newvel = newvels[index];
   vec3 randp = 2.0*vec3(hash(fi), hash(fi*3.), hash(fi*17.)) - vec3(1.0);
   vec3 meanvel = normalize(0.5*(oldvel.xyz + newvel.xyz) + 0.1*randp);
   position.xyz += dt*meanvel;
   positions[index] = position;
   velocities[index] = newvel;
}
]]

function flock01:set_point_attributes(pos, att, vel, newvel, nold, nnew)

    -- set boid positions and vels for new parts of the arrays
    local flocksize = 10.0

    -- first attributes
    for i=nold,nnew-1 do
        if math.random() < -0.1 then
            -- dark matter
            -- mass from 1 to 10
            att[4*i+0] = 1. + 9. * math.random()
            -- brite is always zero
            att[4*i+2] = 0.0
            -- radius follows mass
            att[4*i+3] = 0.2 * att[4*i+0]
            -- rad squared is self-explanatory
            att[4*i+1] = att[4*i+3] * att[4*i+3]
        else
            -- bright star
            -- brite from 0.1 to 1.0
            --att[4*i+2] = 0.05 + 0.9 * math.random()
            att[4*i+2] = math.pow(0.1 + 0.9 * math.random(), 2)
            -- mass from 0.01 to 1.0
            --att[4*i+0] = att[4*i+2] * att[4*i+2]
            att[4*i+0] = att[4*i+2]
            -- radius follows brite, but smaller
            --att[4*i+3] = 0.2 * att[4*i+2]
            att[4*i+3] = 0.06 + 0.3 * math.random()
            --att[4*i+3] = 0.1 + 0.25 * math.random()
            -- rad squared is self-explanatory
            att[4*i+1] = 10. * att[4*i+3] * att[4*i+3]
            -- nah, totally redo brite
            --att[4*i+2] = 1.0 - att[4*i+2]
        end
    end

    -- then positions
    for i=nold,nnew-1 do
        local x = 2*math.random()-1
        local y = 2*math.random()-1
        local z = 2*math.random()-1
        local dist = x*x + y*y + z*z
        while dist > 1.0 do
            x = 2*math.random()-1
            y = 2*math.random()-1
            z = 2*math.random()-1
            dist = x*x + y*y + z*z
        end
        pos[4*i+0] = flocksize * x
        pos[4*i+1] = flocksize * y
        pos[4*i+2] = flocksize * z
        pos[4*i+3] = 1
    end

    -- then velocities
    for i=nold,nnew-1 do
        vel[4*i+0] = 2*math.random()-1
        vel[4*i+1] = 2*math.random()-1
        vel[4*i+2] = 2*math.random()-1
        local velmag = math.sqrt(vel[4*i+0]*vel[4*i+0] + vel[4*i+1]*vel[4*i+1] + vel[4*i+2]*vel[4*i+2])
        vel[4*i+0] = vel[4*i+0] / velmag
        vel[4*i+1] = vel[4*i+1] / velmag
        vel[4*i+2] = vel[4*i+2] / velmag
        vel[4*i+3] = 0
    end

    -- zero out new vels
    for i=4*nold,4*nnew-1 do
        newvel[i] = 0
    end
end

function flock01:grow_point_attributes(oldn, n)
    -- allocate space for the new (larger) arrays

    -- position array: x, y, z, 1
    pos_cpu = ffi.new("float[?]", n*4)
    -- attribute array: mass, radsq, brite, rad
    --   this is to allow creation of dark matter (dark, heavy, large radius)
    att_cpu = ffi.new("float[?]", n*4)
    -- velocity array: dx/dt, dy/dt, dz/dt, 1
    vel_cpu = ffi.new("float[?]", n*4)
    -- acceleration array
    new_cpu = ffi.new("float[?]", n*4)

    -- bind and copy data from GPU to CPU
    local vboP = self.vbos[1]
    local vboM = self.vbos[2]
    local vboV = self.vbos[3]
    local vboVnew = self.vbos[4]

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboP)
    pos_gpu = ffi.cast("float*", gl.glMapBuffer(GL.GL_ARRAY_BUFFER, GL.GL_READ_ONLY))
    ffi.copy(pos_cpu, pos_gpu, oldn*ffi.sizeof("float[4]"))
    gl.glUnmapBuffer(GL.GL_ARRAY_BUFFER)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboM)
    att_gpu = ffi.cast("float*", gl.glMapBuffer(GL.GL_ARRAY_BUFFER, GL.GL_READ_ONLY))
    ffi.copy(att_cpu, att_gpu, oldn*ffi.sizeof("float[4]"))
    gl.glUnmapBuffer(GL.GL_ARRAY_BUFFER)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboV)
    vel_gpu = ffi.cast("float*", gl.glMapBuffer(GL.GL_ARRAY_BUFFER, GL.GL_READ_ONLY))
    ffi.copy(vel_cpu, vel_gpu, oldn*ffi.sizeof("float[4]"))
    gl.glUnmapBuffer(GL.GL_ARRAY_BUFFER)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboVnew)
    new_gpu = ffi.cast("float*", gl.glMapBuffer(GL.GL_ARRAY_BUFFER, GL.GL_READ_ONLY))
    ffi.copy(new_cpu, new_gpu, oldn*ffi.sizeof("float[4]"))
    gl.glUnmapBuffer(GL.GL_ARRAY_BUFFER)

    -- fill out the remainder of the array
    self:set_point_attributes(pos_cpu, att_cpu, vel_cpu, new_cpu, oldn, n)

    -- put the new arrays back on the GPU
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboP)
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(pos_cpu), pos_cpu, GL.GL_DYNAMIC_COPY)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboM)
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(att_cpu), att_cpu, GL.GL_DYNAMIC_COPY)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboV)
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(vel_cpu), vel_cpu, GL.GL_DYNAMIC_COPY)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboVnew)
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(new_cpu), new_cpu, GL.GL_DYNAMIC_COPY)
end

function flock01:init_point_attributes(n)
    -- attribute array: mass, radsq, brite, rad
    -- position array: x, y, z, 1
    pos_array = ffi.new("float[?]", n*4)
    --   this is to allow creation of dark matter (dark, heavy, large radius)
    att_array = ffi.new("float[?]", n*4)
    -- velocity array: dx/dt, dy/dt, dz/dt, 1
    vel_array = ffi.new("float[?]", n*4)
    -- new velocities array
    new_array = ffi.new("float[?]", n*4)

    -- initialize particles
    self:set_point_attributes(pos_array, att_array, vel_array, new_array, 0, n)

    -- set up buffers to contain this data
    local vboIds = ffi.new("int[4]")
    gl.glGenBuffers(4, vboIds)

    local vboP = vboIds[0]
    local vboM = vboIds[1]
    local vboV = vboIds[2]
    local vboVnew = vboIds[3]

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboP)
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(pos_array), pos_array, GL.GL_DYNAMIC_COPY)

    gl.glEnableVertexAttribArray(0)
    gl.glVertexAttribPointer(0, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glVertexAttribDivisor(0, 1)

    -- what do I map this to to get the data passed into the draw pipeline?
    --   the particle radius and brightness need to be used
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboM)
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(att_array), att_array, GL.GL_DYNAMIC_COPY)

    gl.glEnableVertexAttribArray(1)
    gl.glVertexAttribPointer(1, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glVertexAttribDivisor(1, 1)


    -- now one or more velocity arrays
    gl.glBindBuffer(GL.GL_SHADER_STORAGE_BUFFER, vboV)
    gl.glBufferData(GL.GL_SHADER_STORAGE_BUFFER, ffi.sizeof(vel_array), vel_array, GL.GL_DYNAMIC_COPY)

    gl.glBindBuffer(GL.GL_SHADER_STORAGE_BUFFER, vboVnew)
    gl.glBufferData(GL.GL_SHADER_STORAGE_BUFFER, ffi.sizeof(new_array), new_array, GL.GL_DYNAMIC_COPY)

    -- and get these ready for the compute shaders
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vboP)
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 1, vboM)
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 2, vboV)
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 3, vboVnew)

    table.insert(self.vbos, vboP)
    table.insert(self.vbos, vboM)
    table.insert(self.vbos, vboV)
    table.insert(self.vbos, vboVnew)

    gl.glUseProgram(self.prog_findvels)
    gl.glUniform1f(0, self.dt)
    gl.glUniform1i(1, n)

    gl.glUseProgram(self.prog_integrate)
    gl.glUniform1f(0, self.dt)
    gl.glUniform1i(1, n)
end

function flock01:init_quad_attributes()
    local verts = glFloatv(4*3, {
        -1,-1,0,
        1,-1,0,
        1,1,0,
        -1,1,0,
        })

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(2, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo[0])

    gl.glEnableVertexAttribArray(2)
end

function flock01:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.prog_display = sf.make_shader_from_source({
        vsrc = pt_vert,
        fsrc = rad_frag,
        })

    self.prog_findvels = sf.make_shader_from_source({
        compsrc = findvels_comp,
        })

    self.prog_integrate = sf.make_shader_from_source({
        compsrc = integ_comp,
        })

    self:init_point_attributes(self.npts)
    self:init_quad_attributes()

    gl.glBindVertexArray(0)
end

function flock01:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        local vboId = ffi.new("GLuint[1]", v)
        gl.glDeleteBuffers(1,vboId)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog_display)
    gl.glDeleteProgram(self.prog_findvels)
    gl.glDeleteProgram(self.prog_integrate)

    local vaoId = ffi.new("GLuint[2]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

function flock01:renderEye(model, view, proj)
    gl.glUseProgram(self.prog_display)
    gl.glClearColor(0,0,0,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)
    
    gl.glUniformMatrix4fv(0, 1, GL.GL_FALSE, glFloatv(16, model))
    gl.glUniformMatrix4fv(1, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(2, 1, GL.GL_FALSE, glFloatv(16, proj))

    gl.glDisable(GL.GL_DEPTH_TEST)
    gl.glEnable(GL.GL_BLEND)
    --gl.glBlendFunc(GL.GL_ONE, GL.GL_ONE)
    --gl.glBlendFunc(GL.GL_ONE, GL.GL_ONE_MINUS_SRC_COLOR)
    gl.glBlendFunc(GL.GL_SRC_COLOR, GL.GL_ONE_MINUS_SRC_COLOR)
    gl.glBindVertexArray(self.vao)
    --gl.glDrawArrays(GL.GL_POINTS, 0, self.npts)
    gl.glDrawArraysInstanced(GL.GL_TRIANGLE_FAN, 0, 4, self.npts)
    gl.glBindVertexArray(0)
    gl.glEnable(GL.GL_DEPTH_TEST)
    gl.glDisable(GL.GL_BLEND)

    gl.glUseProgram(0)
end

function flock01:timestep(absTime, dt)
    gl.glUseProgram(self.prog_findvels)
    gl.glDispatchCompute(math.ceil(self.npts/128), 1, 1)

    gl.glUseProgram(self.prog_integrate)
    gl.glDispatchCompute(math.ceil(self.npts/128), 1, 1)
    gl.glUseProgram(0)

    self.time = self.time + self.dt
    --print("value is",self.minuteramp:getvalue(self.time))
end

function flock01:scaleBufferSize(k)

    local newn = math.floor(self.npts * k)

    if newn > self.npts then
      -- do something fancy with buffers
      self:grow_point_attributes(self.npts, newn)
    end

    self.npts = newn

    gl.glUseProgram(self.prog_findvels)
    --gl.glUniform1f(0, self.dt)
    gl.glUniform1i(1, self.npts)

    gl.glUseProgram(self.prog_integrate)
    --gl.glUniform1f(0, self.dt)
    gl.glUniform1i(1, self.npts)
end

function flock01:charkeypressed(key)
    local twelthpow = math.exp(math.log(2) / 12.0)
    local fourthpow = math.exp(math.log(2) / 4.0)

    if key == '-' then
        self:scaleBufferSize(1.0/twelthpow)
        print ("Reduced npts to",self.npts)
    elseif key == '=' then
        self:scaleBufferSize(twelthpow)
        print ("Increased npts to",self.npts)
    elseif key == '1' then
        self.dt = self.dt / fourthpow
        print ("Reduced dt to",self.dt)
        gl.glUseProgram(self.prog_findvels)
        gl.glUniform1f(0, self.dt)
        gl.glUseProgram(self.prog_integrate)
        gl.glUniform1f(0, self.dt)
    elseif key == '2' then
        self.dt = self.dt * fourthpow
        print ("Increased dt to",self.dt)
        gl.glUseProgram(self.prog_findvels)
        gl.glUniform1f(0, self.dt)
        gl.glUseProgram(self.prog_integrate)
        gl.glUniform1f(0, self.dt)
--    elseif key == '0' then
        --self:clearInstanceBuffers()
    end
end

function flock01:onmouse(xf, yf)
    --self.joff[1], self.joff[2] = xf, yf
    --print(xf, yf)
end

return flock01
