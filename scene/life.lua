-- life.lua
-- A 2D cellular automata, such as Conway's Game of Life.
-- Rules are expressed as frag shader and are dynamically editable.

local ffi = require("ffi")
local fbf = require("util.fbofunctions")
local sf = require("util.shaderfunctions2")

life = {}
life.__index = life

function life.new(...)
    local self = setmetatable({}, life)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local tex_vert = [[
#version 300 es

in vec2 vPosition;
out vec2 uv;

void main()
{
    uv = .5*vPosition + vec2(.5);
    gl_Position = vec4(vPosition, 0., 1.);
}
]]

local tex_frag = [[
#version 300 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec2 uv;
out vec4 fragColor;

uniform sampler2D sTex;

void main()
{
    vec4 tc = texture(sTex, uv);
    // We could use a 1-bit image here...
    vec3 colOn = vec3(73./255., 117./255., 220./255.);
    vec3 colOff = vec3(0., 120./255., 73./255.);
    fragColor = vec4(mix(colOff, colOn, tc.x), 1.);
}
]]

local step_header = [[
#version 300 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec2 uv;
out vec4 fragColor;
#line 1
]]

-- CA step algo
local step_frag = [[
uniform sampler2D sTex;
uniform ivec2 uResolution;

void main()
{
    vec2 step = vec2(1./float(uResolution.x), 1./float(uResolution.y));

    bool cellLiving = false;
    int liveNeighbors = 0;
    for (int i=-1; i<=1; ++i) {
        for (int j=-1; j<=1; ++j) {
            vec2 dxy = vec2(float(i)*step.x, float(j)*step.y);
            vec4 tc = texture(sTex, uv+dxy);
            if (length(tc) > .5) {
                if (i==0 && j==0)
                    cellLiving = true;
                else
                    liveNeighbors++;
            }
        }
    }

    bool liveToNext = false;
    if (!cellLiving) {
        if (liveNeighbors == 3)
            liveToNext = true;
    }
    else {
        if ((liveNeighbors == 2) || (liveNeighbors == 3))
            liveToNext = true;
    }

    fragColor = liveToNext ? vec4(1.) : vec4(0.);
}
]]

function life:init()
    self.vao = 0
    self.vbos = {}
    self.prog = 0
    self.prog_step = 0
    self.time = 0
    self.fbos = {}
    self.w = 64/4
    self.h = 64/4
    self.ringSz = 2
    self.ringPtr = 1
    self.animate = false
    self.src = step_frag
    self.descr = "2D Cellular Automata"
    self.shadertype = "fragment"
end

function life:stateString()
    return self.w..'x'..self.h..' cells'
end

function life:buildShader(src)
    gl.glBindVertexArray(self.vao)
    gl.glDeleteProgram(self.prog_step)
    local err = nil
    self.prog_step, err = sf.make_shader_from_source({
        vsrc = tex_vert,
        fsrc = step_header..src,
        })
    --gl.glBindVertexArray(0)
    return err
end

function life:initTriAttributes()
    local glIntv   = ffi.typeof('GLint[?]')
    local glFloatv = ffi.typeof('GLfloat[?]')

    -- One big tri to avoid some overdraw on the seam
    local verts = glFloatv(3*2, {
        -1,-1,
        3,-1,
        -1,3,
        })

    local vpos_loc = gl.glGetAttribLocation(self.prog, "vPosition")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
end

function life:resizeFbos(w,h)
    print(w.."x"..h)
    for _,v in pairs(self.fbos) do
        if v then fbf.deallocate_fbo(v) end
    end
    for i=1,self.ringSz do
        table.insert(self.fbos, fbf.allocate_fbo(w, h, true))
    end

    for _,v in pairs(self.fbos) do
        gl.glBindTexture(GL.GL_TEXTURE_2D, v.tex)
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_REPEAT)
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_REPEAT)
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    end
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end

function life:initFbos()
    self:resizeFbos(self.w, self.h)
end

-- Populate front buffer with random
function life:resetState()
    self.ringPtr = 1
    local pxlData = ffi.new("unsigned char[?]", self.w*self.h*4)
    for i=0,self.w*self.h-1 do
        local lum = 255
        local r = math.random()
        if r > .4 then lum = 0 end
        for j=0,3 do
            pxlData[4*i+j] = lum
        end
    end

    gl.glBindTexture(GL.GL_TEXTURE_2D, self.fbos[1].tex)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA8,
                  self.w, self.h, 0,
                  GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, pxlData)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end

function life:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.prog, err = sf.make_shader_from_source({
        vsrc = tex_vert,
        fsrc = tex_frag,
        })
    if err then print(err) end
    self:buildShader(step_frag)

    self:initTriAttributes()
    self:initFbos()

    self:resetState()

    gl.glBindVertexArray(0)
end

function life:exitGL()
    gl.glBindVertexArray(self.vao)

    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    vbos = {}
    gl.glDeleteProgram(self.prog)
    for _,v in pairs(self.fbos) do
        if v then fbf.deallocate_fbo(v) end
    end

    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

function life:evolveStep()
    -- Bind current fbo as tex, render to next
    local nextFbo = 1 + self.ringPtr
    if nextFbo > self.ringSz then nextFbo = 1 end

    -- Save viewport dimensions
    -- TODO: this is very inefficient; store them manually, restore at caller
    local vp = ffi.new("GLuint[4]", 0,0,0,0)
    gl.glGetIntegerv(GL.GL_VIEWPORT, vp)
    -- Save bound FBO
    local boundfbo = ffi.new("int[1]")
    gl.glGetIntegerv(GL.GL_FRAMEBUFFER_BINDING, boundfbo)

        -- Draw quad scene to this scene's first internal fbo
        fbf.bind_fbo(self.fbos[nextFbo])
        gl.glViewport(0,0, self.w, self.h)
        gl.glDisable(GL.GL_DEPTH_TEST)

            gl.glUseProgram(self.prog_step)
                gl.glActiveTexture(GL.GL_TEXTURE0)
                gl.glBindTexture(GL.GL_TEXTURE_2D, self.fbos[self.ringPtr].tex)
                local stex_loc = gl.glGetUniformLocation(self.prog_step, "sTex")
                gl.glUniform1i(stex_loc, 0)

                local ures_loc = gl.glGetUniformLocation(self.prog_step, "uResolution")
                gl.glUniform2i(ures_loc, self.w, self.h)

                gl.glBindVertexArray(self.vao)
                gl.glDrawArrays(GL.GL_TRIANGLES, 0, 3)
                gl.glBindVertexArray(0)

            gl.glUseProgram(0)

        fbf.unbind_fbo()
        gl.glEnable(GL.GL_DEPTH_TEST)

    -- Restore viewport
    gl.glViewport(vp[0],vp[1],vp[2],vp[3])
    -- Restore FBO binding
    if boundfbo[0] ~= 0 then
        gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, boundfbo[0])
    end

    -- Advance ring buf ptr
    self.ringPtr = nextFbo
end

function life:render_for_one_eye(view, proj)
    gl.glUseProgram(self.prog)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.fbos[self.ringPtr].tex)
    local stex_loc = gl.glGetUniformLocation(self.prog, "sTex")
    gl.glUniform1i(stex_loc, 0)

    gl.glBindVertexArray(self.vao)
    gl.glDrawArrays(GL.GL_TRIANGLES, 0, 3)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end

-- Assume GL context is current here
function life:timestep(absTime, dt)
    if self.animate then self:evolveStep() end
end

function life:scaleBufferSize(k)
    self.w = self.w * k
    self.h = self.h * k
    self:resizeFbos(self.w, self.h)
    self:resetState()
end

function life:charkeypressed(key)
    local func_table = {
        [' '] = function (x) self.animate = not self.animate end,
        ['\\'] = function (x) self:evolveStep() end,
        ['z'] = function (x) self:resetState() end,
        ['-'] = function (x) self:scaleBufferSize(.5) end,
        ['='] = function (x) self:scaleBufferSize(2) end,
    }
    local f = func_table[key]
    if f then f() handled = true end
end

function life:keypressed(key, scancode, action, mods)
    local func_table = {
        [298] = function (x) self:scaleBufferSize(.5*.5) end, -- F9
        [299] = function (x) self:scaleBufferSize(.5) end, -- F10
        [300] = function (x) self:scaleBufferSize(2) end, -- F11
        [301] = function (x) self:scaleBufferSize(2*2) end, -- F12
    }
    local f = func_table[key]
    if f then f() handled = true end
end

-- Assume GL context is current here
function life:onmouse(xf, yf)
    local xi,yi = math.floor(xf*self.w), math.floor((1-yf)*self.h)

--[[
    -- TODO: read current pixel val to toggle.
    local px = ffi.new("char[?]",3)
    gl.glReadPixels(xi,yi,1,1,GL.GL_RGB,GL.GL_UNSIGNED_BYTE,px)
    print(px[0], px[1], px[2])
]]
    local charv = ffi.typeof('char[?]')
    local white = charv(4, {255, 255, 255, 255})
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.fbos[self.ringPtr].tex)
    gl.glTexSubImage2D(GL.GL_TEXTURE_2D, 0,
        xi, yi,
        1, 1,
        GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, white)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end

return life
