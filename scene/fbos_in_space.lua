--[[ fbos_in_space.lua

    A set of scenes each drawn to their own in-world quads.
]]
fbos_in_space = {}

fbos_in_space.__index = fbos_in_space

function fbos_in_space.new(...)
    local self = setmetatable({}, fbos_in_space)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

require("util.glfont")
local mm = require("util.matrixmath")

function fbos_in_space:init()
    self.dataDir = nil
    self.glfont = nil
    self.win_w = 400
    self.win_h = 300

    self.Scenes = {}
    local scenenames = {
        "clockface",
        "colorcube",
        "moon",
        "hybrid_scene",
        --"multipass_example",
    }

    for _,v in pairs(scenenames) do
        local InnerScene = require("scene."..v).new()
        table.insert(self.Scenes, require("scene.textured_fbo").new(InnerScene))
        self.Scenes[#self.Scenes].label = v
    end
end

function fbos_in_space:setDataDirectory(dir)
    for _,Scene in pairs(self.Scenes) do
        Scene:setDataDirectory(dir)
    end
    self.dataDir = dir
end

function fbos_in_space:initGL()
    for _,Scene in pairs(self.Scenes) do
        Scene:initGL()
    end

    dir = "fonts"
    if self.dataDir then dir = self.dataDir .. "/" .. dir end
    self.glfont = GLFont.new('segoe_ui128.fnt', 'segoe_ui128_0.raw')
    self.glfont:setDataDirectory(dir)
    self.glfont:initGL()
end

function fbos_in_space:exitGL()
    for _,Scene in pairs(self.Scenes) do
        Scene:exitGL()
    end
    self.glfont:exitGL()
end

function fbos_in_space:renderEye(model, view, proj)
    local m = {}
    for i=1,16 do m[i] = model[i] end
    mm.glh_translate(m, -1.2,0,0)
    local i=1
    for _,Scene in pairs(self.Scenes) do
        Scene:renderEye(m, view, proj)

        if false then
            -- Scene labels are drawn onto their FBO
            -- Why does this text transform so oddly?
            local col = {1, 1, 1}
            local mt = {}
            for i=1,16 do mt[i] = m[i] end
            --mm.glh_translate(mt, 0,1,0)
            local s = .003
            mm.glh_scale(mt, s, -s, s)
            self.glfont:render_string2(mt, view, proj, col, "scene "..i)
        end

        mm.glh_translate(m, 1.2,0,0)
        i = i+1
    end
end

function fbos_in_space:timestep(absTime, dt)
    for _,Scene in pairs(self.Scenes) do
        if Scene.timestep then
            Scene:timestep(absTime, dt)
        end
    end
end

function fbos_in_space:keypressed(ch)
    for _,Scene in pairs(self.Scenes) do
        if Scene.Scene.keypressed then
            return Scene:keypressed(ch)
        end
    end
end

function fbos_in_space:onray(ro, rd)
    for _,Scene in pairs(self.Scenes) do
        if Scene.onray then
            local ret = Scene:onray(ro,rd)
            --print("test: ", Scene.label)
            if ret then
                print("hit: ", Scene.label)
                return true
            end
        end
    end
    return false
end


return fbos_in_space
