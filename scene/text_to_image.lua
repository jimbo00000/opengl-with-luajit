--[[ text_to_image.lua

    Renders text to an image using an fbo.
]]
text_to_image = {}

text_to_image.__index = text_to_image

function text_to_image.new(...)
    local self = setmetatable({}, text_to_image)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end
    return self
end

function text_to_image:init()
    self.vao = 0
    self.vbos = {}
    self.prog = 0
    self.texID = 0
    self.dataDir = nil
    self.clearColor = {.4,.4,.4,.0}
    self.glfont = nil

    self.label = "Label here..."

    --TODO Model matrix
end

function text_to_image:setDataDirectory(dir)
    self.dataDir = dir
end

local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")
local fbf = require("util.fbofunctions")
require("util.glfont")

local glIntv   = ffi.typeof('GLint[?]')
local glUintv  = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local basic_vert = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec4 vPosition;
in vec4 vColor;

out vec3 vfColor;

uniform mat4 mmtx;
uniform mat4 vmtx;
uniform mat4 prmtx;

void main()
{
    vfColor = vColor.xyz;
    gl_Position = prmtx * vmtx * mmtx * vPosition;
}
]]

local basic_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

uniform sampler2D sTex;

void main()
{
    vec4 tc = texture(sTex, vfColor.xy);
    fragColor = vec4(tc.xyz, 1.);
}
]]

function text_to_image:setDataDirectory(dir)
    self.dataDir = dir
end

function text_to_image:init_cube_attributes()
    local v = {
        0,0,0,
        1,0,0,
        1,1,0,
        0,1,0,
    }
    local verts = glFloatv(#v,v)

    local c = {
        0,0, 1,0, 1,1, 0,1,
    }
    local cols = glFloatv(#c,c)

    local vpos_loc = gl.glGetAttribLocation(self.prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(self.prog, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(cols), cols, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)

    local q = { -- Vertex indices for drawing triangles of faces
        0,3,2, 1,0,2,
    }
    local quads = glUintv(#q,q)
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(quads), quads, GL.GL_STATIC_DRAW)
    table.insert(self.vbos, qvbo)
end

function text_to_image:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.prog = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    self:init_cube_attributes()

    -- FBO init
    local sz = 2048
    w,h = sz,sz
    --self.fbo:resize_fbo(w,h)
    self.fbo = fbf.allocate_fbo(w,h,true)
    gl.glBindVertexArray(0)

    dir = "fonts"
    if self.dataDir then dir = self.dataDir .. "/" .. dir end
    self.glfont = GLFont.new(
        --'vcr_512.fnt', 'vcr_512_0.raw'
        'orbitron_512.fnt', 'orbitron_512_0.raw'
        )
    self.glfont:setDataDirectory(dir)
    self.glfont:initGL()
end

function text_to_image:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    gl.glDeleteProgram(self.prog)
    local texdel = ffi.new("GLuint[1]", self.texID)
    gl.glDeleteTextures(1,texdel)

    fbf.deallocate_fbo(self.fbo)

    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)
    self.glfont:exitGL()
end

function text_to_image:renderText(text, xy, sz, col)
    sz = sz or 2
    col = col or {1, 1, 1}

    -- TODO getStringWidth and center text
    local p = {}
    mm.glh_ortho(p, 0, self.fbo.w, self.fbo.h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    do
        local m = {}
        mm.make_identity_matrix(m)

        --xy[2] = 1-xy[2] -- bottom-left origin
        mm.glh_translate(m, xy[1]*self.fbo.w, xy[2]*self.fbo.h, 0)
        mm.glh_scale(m, sz, sz, sz)

        self.glfont:render_string(m, p, col, text)
    end
    gl.glEnable(GL.GL_DEPTH_TEST)
end

function text_to_image:renderPrePass(textItems)
    -- Save viewport dimensions
    -- TODO: this is very inefficient; store them manually, restore at caller
    local vp = ffi.new("GLuint[4]", 0,0,0,0)
    gl.glGetIntegerv(GL.GL_VIEWPORT, vp)
    -- Save bound FBO
    local boundfbo = ffi.new("int[1]")
    gl.glGetIntegerv(GL.GL_FRAMEBUFFER_BINDING, boundfbo)

    -- Render pre-pass to fbo
    fbf.bind_fbo(self.fbo)
    do
        gl.glViewport(0,0, self.fbo.w, self.fbo.h)
        gl.glClearColor(self.clearColor[1], self.clearColor[2], self.clearColor[3], self.clearColor[4])
        gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

        for i=1,#textItems do
            local ti = textItems[i]
            self:renderText(ti[1], ti[2], ti[3], ti[4])
        end
    end
    fbf.unbind_fbo(self.fbo)
    -- TODO: restore VAO?

    -- Restore viewport
    gl.glViewport(vp[0],vp[1],vp[2],vp[3])
    -- Restore FBO binding
    if boundfbo[0] ~= 0 then
        gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, boundfbo[0])
    end
end

function text_to_image:renderEye(model, view, proj)

    -- See also: 
    --   self.clearColor
    --   col in renderText
    --   origin flip line in renderText
    --   fbo size init in initGL
    local textItems = {
        -- String, xy coords, size(optional)
        {"First Line...", {0,0}, 4, {1,0,0,}},
        {"Next Line.", {.5,.5}},
    }
    self:renderPrePass(textItems)
    -- Output now resides in image data in self.fbo.tex


    local um_loc = gl.glGetUniformLocation(self.prog, "mmtx")
    local uv_loc = gl.glGetUniformLocation(self.prog, "vmtx")
    local upr_loc = gl.glGetUniformLocation(self.prog, "prmtx")
    gl.glUseProgram(self.prog)
    gl.glUniformMatrix4fv(uv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.fbo.tex)
    local stex_loc = gl.glGetUniformLocation(self.prog, "sTex")
    gl.glUniform1i(stex_loc, 0)

    gl.glUniformMatrix4fv(um_loc, 1, GL.GL_FALSE, glFloatv(16, model))
    gl.glBindVertexArray(self.vao)
    gl.glDrawElements(GL.GL_TRIANGLES, 6*3*2, GL.GL_UNSIGNED_INT, nil)
    gl.glBindVertexArray(0)

    gl.glUseProgram(0)
end

return text_to_image
