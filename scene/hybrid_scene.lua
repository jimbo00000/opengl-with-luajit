--[[ hybrid_scene.lua

    A combination of two scenes with two different rendering
    techniques and a font renderer to label each type of object.

    All rendering code is owned by the respective scene types and
    is simply invoked from this module.
]]

local hybrid_scene = {}
hybrid_scene.__index = hybrid_scene

function hybrid_scene.new(...)
    local self = setmetatable({}, hybrid_scene)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local RasterLib = require("scene.particles2") --textured_cubes")
local RaymarchLib = require("scene.raymarch_csg2")
local GLFontLibrary = require("util.glfont")
local mm = require("util.matrixmath")

function hybrid_scene:init()
    self.vbos = {}
    self.vao = 0
    self.prog = 0
    self.dataDir = nil
    self.glfont = nil

    -- TODO: can we look into included scenes instead of assigning manually here?
    self.scale = 1
    self.zoff = 0
    self.lightspeed = 1
    self.repeatSpacing = 10/16

    self.Raster = RasterLib.new()
    self.Raymarch = RaymarchLib.new()
end
function hybrid_scene:setDataDirectory(dir)
    self.Raster:setDataDirectory(dir)
    self.dataDir = dir
end

function hybrid_scene:initGL()
    self.Raster:initGL()
    self.Raymarch:initGL()

    dir = "fonts"
    if self.dataDir then dir = self.dataDir .. "/" .. dir end
    self.glfont = GLFontLibrary.new('papyrus_512.fnt', 'papyrus_512_0.raw')
    self.glfont:setDataDirectory(dir)
    self.glfont:initGL()
end

function hybrid_scene:exitGL()
    self.Raster:exitGL()
    self.Raymarch:exitGL()
    self.glfont:exitGL()
end

function hybrid_scene:renderEye(model, view, proj)
    self.Raster:renderEye(model, view, proj)
    self.Raymarch:renderEye(model, view, proj)

    local col = {1, 1, 1}
    local m = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1}
    local s = .002
    mm.glh_translate(m, -1, .8, .5)
    mm.glh_scale(m, s, -s, s)
    mm.pre_multiply(m, view)
    self.glfont:render_string(m, proj, col, "Raymarched CSG Shape")

    local m = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1}
    local s = .002
    mm.glh_translate(m, 1, 0.2, -.5)
    mm.glh_scale(m, s, -s, s)
    mm.pre_multiply(m, view)
    self.glfont:render_string(m, proj, col, "Rasterized cubes")
end

function hybrid_scene:timestep(absTime, dt)
    if self.Raster.timestep then self.Raster:timestep(absTime, dt) end
    self.Raymarch:timestep(absTime, dt)

    --TODO: Can we program this away?
    self.Raster.scale = self.scale
    self.Raster.zoff = self.zoff
    self.Raster.lightspeed = self.lightspeed
    self.Raymarch.lightspeed = self.lightspeed
    self.Raymarch.repeatSpacing = self.repeatSpacing
end

function hybrid_scene:keypressed(key, scancode, action, mods)
    if self.Raster.keypressed then self.Raster:keypressed(key, scancode, action, mods) end
    --self.Raymarch:keypressed(key, scancode, action, mods)
end

return hybrid_scene
