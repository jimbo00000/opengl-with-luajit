--[[ comet.lua

    
]]
comet = {}

comet.__index = comet

function comet.new(...)
    local self = setmetatable({}, comet)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local CubeLib = require("scene.word")
local ComaLib = require("scene.coma")
local mm = require("util.matrixmath")

function comet:init(subdivs)
    self.core = CubeLib.new()
    self.halo = ComaLib.new(subdivs)
    self.billboard = true
    self.size = 1
end
function comet:setDataDirectory(dir)
    self.core:setDataDirectory(dir)
end

function comet:initGL()
    self.core:initGL()
    self.halo:initGL()
end

function comet:exitGL()
    self.core:exitGL()
    self.halo:exitGL()
end

function comet:renderEye(model, view, proj)
    local m = {}
    for i=1,16 do m[i] = model[i] end
    mm.glh_rotate(m, 90, 0,1,0)
    self.halo:renderEye(m, view, proj)

    gl.glDisable(GL.GL_CULL_FACE)
    for i=1,16 do m[i] = model[i] end

    if self.billboard then
        local zvec = {0,0,1,0}
        local ztx = mm.transform(zvec, model)
        local dp = mm.dot(zvec, ztx)
        local ytx = mm.transform({0,1,0,0}, model)
        if ztx[2] < 0 then
            mm.glh_rotate(m, -(180/math.pi)*math.acos(dp), 1,0,0)
        else
            mm.glh_rotate(m, (180/math.pi)*math.acos(dp), 1,0,0)
        end
    end

    --mm.glh_translate(m, -1,.25,0)
    mm.glh_translate(m, -self.size, -.2+.5*self.size,0)

    self.core:renderEye(m, view, proj)
end

function comet:timestep(absTime, dt)
    self.halo:timestep(absTime, dt)
end

function comet:keypressed(key, scancode, action, mods)
    print(key)

    local factor = 1.2
    if key == string.byte('-') then
        self.size = self.size / factor
        self.core.size = self.size
        self.halo.size = self.size
    elseif key == string.byte('=') then
        self.size = self.size * factor
        self.core.size = self.size
        self.halo.size = self.size
    end
    if action == 1 then
        self.billboard = not self.billboard
    end
end

return comet
