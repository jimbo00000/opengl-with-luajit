--
-- poisson_jacobi.lua
--
-- Run a 2D semi-Lagrangian vortex method in vorticity-streamfunction variables
-- Solve the Poisson equation with an iterative Jacobi method
-- Advect using a 2nd order semi-Lagrangian method with linear interpolation
-- Advect a high-resolution color scalar field
--
-- (c)2018 Mark J. Stock markjstock@gmail.com
-- portions from compute_image.lua (c)2018 James Sussino
--

poisson_jacobi = {}
poisson_jacobi.__index = poisson_jacobi

function poisson_jacobi.new(...)
    local self = setmetatable({}, poisson_jacobi)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function poisson_jacobi:init()
    self.vao = 0
    self.vbos = {}
    self.prog_draw = 0

    -- time step and diffusion coefficient (not used)
    self.dt = 0.5
    --self.nu = 0.0001

    -- size of work group in compute shaders
    self.gridx = 16

    -- base resolution (set the power to 1..6)
    local res = self.gridx * math.pow(2,3)

    -- at lowest resolution are the streamfunction and associated velocity
    self.field = {}
    local aspect = 1
    -- sim 0 is dead, 1 is random, 2 is asymmetric vortex patch, 3 is counterrotating vortices
    self.sim = 1

    self.field["psi"] = {x=res, y=res/aspect, efmt=GL.GL_RED, ifmt=GL.GL_R32F}
    self.field["vel"] = {x=res, y=res/aspect, efmt=GL.GL_RG, ifmt=GL.GL_RG32F}
    -- need a temporary to allow ping-ponging buffers
    self.field["tpsi"] = {x=res, y=res/aspect, efmt=GL.GL_RED, ifmt=GL.GL_R32F}

    -- potentially at higher resolution are the conserved scalar variables
    local scalar_mult = 1
    self.field["vort"]   = {x=res*scalar_mult, y=res*scalar_mult/aspect, efmt=GL.GL_RED, ifmt=GL.GL_R32F}
    --self.field["scalar"] = {x=res*scalar_mult, y=res*scalar_mult/aspect, ifmt=GL.GL_R32F}
    -- need a temporary to allow ping-ponging buffers
    self.field["tscal"]   = {x=res*scalar_mult, y=res*scalar_mult/aspect, efmt=GL.GL_RED, ifmt=GL.GL_R32F}

    -- and at highest is the conserved, drawn color field
    local color_mult = 4
    --self.field["color"] = {x=res*color_mult, y=res*color_mult/aspect, efmt=GL.GL_RGBA, ifmt=GL.GL_RGBA32F}
    self.field["color"] = {x=res*color_mult, y=res*color_mult/aspect, efmt=GL.GL_RGBA, ifmt=GL.GL_RGBA8}
    -- need a temporary to allow ping-ponging buffers
    self.field["tcol"] = {x=res*color_mult, y=res*color_mult/aspect, efmt=GL.GL_RGBA, ifmt=GL.GL_RGBA8}
end

function poisson_jacobi:setDataDirectory(dir)
    self.data_dir = dir
end

local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")
local glFloatv = ffi.typeof('GLfloat[?]')

local basic_vert = [[
#version 310 es

in vec4 vPosition;
in vec4 vColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

out vec3 vfColor;

void main()
{
    vfColor = vColor.xyz;
    gl_Position = prmtx * mvmtx * vPosition;
}
]]

local basic_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

uniform sampler2D sTex;
uniform float texMag;

void main()
{
    vec2 tc = vfColor.xy;
    tc.y = 1.-tc.y;
    vec4 col = texture(sTex, tc);
    fragColor = vec4(texMag*col.xyz, 1.);
}
]]

local red_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

uniform sampler2D sTex;
uniform float texMag;

void main()
{
    vec2 tc = vfColor.xy;
    tc.y = 1.-tc.y;
    vec4 col = texture(sTex, tc);
    float px = 0.5 + texMag * col.x;
    fragColor = vec4(px, px, px, 1.);
}
]]

local img_init_rgba8 = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(rgba8, binding = 0) uniform image2D img_output;

uniform int uFillType;

float hash( float n ) { return fract(sin(n)*43758.5453); }

float hash(vec2 p) {
  return fract(sin(dot(p, vec2(43.232, 75.876)))*4526.3257);
}

void main() {
    vec4 pixel = vec4(0.0);
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    if (uFillType == 0)
    {
        // Init image to checkerboard
        if (((pixel_coords.x & 64) == 0)
         != ((pixel_coords.y & 64) == 0))
        {
            pixel = vec4(1.);
        }
    }
    else if (uFillType == 1)
    {
        // Random init
        vec2 coord = vec2(
            float(pixel_coords.x)/float(imageSize(img_output).x),
            float(pixel_coords.y)/float(imageSize(img_output).y));
        float hval = hash(coord);
        pixel = hval > .5 ? vec4(1.) : vec4(0.);
    }
    else if (uFillType == 2)
    {
        // Single black pixel in center init
        pixel = vec4(1.);
        if (pixel_coords.x == imageSize(img_output).x/2)
            pixel = vec4(0.);
    }

    imageStore(img_output, pixel_coords, pixel);
}
]]

local img_init_r32f = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(r32f, binding = 0) uniform image2D img_output;

uniform int uFillType;

float hash( float n ) { return fract(sin(n)*43758.5453); }

float hash(vec2 p) {
  return fract(sin(dot(p, vec2(43.232, 75.876)))*4526.3257);
}

void main() {
    float pixel = 0.0;
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    if (uFillType == 0)
    {
        // Init image to zero
    }
    else if (uFillType == 1)
    {
        // Random init
        vec2 coord = vec2(
            float(pixel_coords.x)/float(imageSize(img_output).x),
            float(pixel_coords.y)/float(imageSize(img_output).y));
        pixel = 2.0*hash(coord) - 1.0;
    }
    else if (uFillType == 2)
    {
        // Asymmetric vortex
        vec2 coord = vec2(
            4.0*(float(pixel_coords.x)/float(imageSize(img_output).x)-0.3),
                (float(pixel_coords.y)/float(imageSize(img_output).y)-0.7) );
        if (length(coord) < 0.15)
            pixel = 1.0;
    }
    else if (uFillType == 3)
    {
        // counter-rotating vortexes
        vec2 coord = vec2(
            (float(pixel_coords.x)/float(imageSize(img_output).x)-0.3),
            (float(pixel_coords.y)/float(imageSize(img_output).y)-0.7) );
        if (length(coord) < 0.1)
            pixel += 1.0;
        coord = vec2(
            (float(pixel_coords.x)/float(imageSize(img_output).x)-0.4),
            (float(pixel_coords.y)/float(imageSize(img_output).y)-0.5) );
        if (length(coord) < 0.13)
            pixel -= 1.0;
    }

    imageStore(img_output, pixel_coords, vec4(pixel,0,0,1));
}
]]

-- perform one step of a relaxation scheme for the Poisson equation
local img_relax_once = [[
#version 430
layout(local_size_x = 1, local_size_y = 1) in;
layout(r32f, binding = 0) uniform readonly restrict image2D img_vort_in;
layout(r32f, binding = 1) uniform readonly restrict image2D img_psi_in;
layout(r32f, binding = 2) uniform writeonly restrict image2D img_psi_out;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // right-hand side
    vec4 temp = imageLoad(img_vort_in, pixel_coords);
    //float ootdx = imageSize(img_psi_in).x / 2.0;
    float psi_new = temp.x * pow(1.0/imageSize(img_psi_in).x, 2);

    // psi <- stuff
    int ishift = pixel_coords.y - 1;
    if (ishift < 0) ishift += imageSize(img_psi_in).y;
    temp = imageLoad(img_psi_in, ivec2(pixel_coords.x, ishift));
    psi_new += temp.x;

    ishift = pixel_coords.y + 1;
    ishift %= imageSize(img_psi_in).y;
    temp = imageLoad(img_psi_in, ivec2(pixel_coords.x, ishift));
    psi_new += temp.x;

    ishift = pixel_coords.x - 1;
    if (ishift < 0) ishift += imageSize(img_psi_in).x;
    temp = imageLoad(img_psi_in, ivec2(ishift, pixel_coords.y));
    psi_new += temp.x;

    ishift = pixel_coords.x + 1;
    ishift %= imageSize(img_psi_in).x;
    temp = imageLoad(img_psi_in, ivec2(ishift, pixel_coords.y));
    psi_new += temp.x;

    imageStore(img_psi_out, pixel_coords, vec4(0.25*psi_new, 0, 0, 1));
}
]]

-- perform one step of a relaxation scheme for the Poisson equation
local img_relax_once_blocked = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(r32f, binding = 0) uniform readonly restrict image2D img_vort_in;
layout(r32f, binding = 1) uniform readonly restrict image2D img_psi_in;
layout(r32f, binding = 2) uniform writeonly restrict image2D img_psi_out;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // right-hand side
    vec4 temp = imageLoad(img_vort_in, pixel_coords);
    //float ootdx = imageSize(img_psi_in).x / 2.0;
    float psi_new = temp.x * pow(1.0/imageSize(img_psi_in).x, 2);

    // psi <- stuff
    int ishift = pixel_coords.y - 1;
    if (ishift < 0) ishift += imageSize(img_psi_in).y;
    temp = imageLoad(img_psi_in, ivec2(pixel_coords.x, ishift));
    psi_new += temp.x;

    ishift = pixel_coords.y + 1;
    ishift %= imageSize(img_psi_in).y;
    temp = imageLoad(img_psi_in, ivec2(pixel_coords.x, ishift));
    psi_new += temp.x;

    ishift = pixel_coords.x - 1;
    if (ishift < 0) ishift += imageSize(img_psi_in).x;
    temp = imageLoad(img_psi_in, ivec2(ishift, pixel_coords.y));
    psi_new += temp.x;

    ishift = pixel_coords.x + 1;
    ishift %= imageSize(img_psi_in).x;
    temp = imageLoad(img_psi_in, ivec2(ishift, pixel_coords.y));
    psi_new += temp.x;

    imageStore(img_psi_out, pixel_coords, vec4(0.25*psi_new, 0, 0, 1));
}
]]

-- perform one step of a relaxation scheme for the Poisson equation
local img_relax_once_blocked_shared = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(r32f, binding = 0) uniform readonly restrict image2D img_vort_in;
layout(r32f, binding = 1) uniform readonly restrict image2D img_psi_in;
layout(r32f, binding = 2) uniform writeonly restrict image2D img_psi_out;

shared float tmp[gl_WorkGroupSize.x+2][gl_WorkGroupSize.y+2];

void main() {
    ivec2 global_index = ivec2(gl_GlobalInvocationID.xy);
    ivec2 local_index = ivec2( int(gl_WorkGroupID.x)*int(gl_WorkGroupSize.x) + int(gl_LocalInvocationID.x),
                               int(gl_WorkGroupID.y)*int(gl_WorkGroupSize.y) + int(gl_LocalInvocationID.y) );

    // load psi_in into shared memory

    // first, the middle (most of the loads)
    tmp[1+int(gl_LocalInvocationID.x)][1+int(gl_LocalInvocationID.y)] = imageLoad(img_psi_in, global_index).x;

    // then the left and right sides (this assumes work groups fit perfectly into overall dimensions)

    if (int(gl_LocalInvocationID.x) == 0) {
        // only some threads do this

        int ileft = int(gl_WorkGroupID.x)*int(gl_WorkGroupSize.x) - 1;
        if (ileft < 0) ileft = imageSize(img_psi_in).x - 1;
        tmp[0][1+int(gl_LocalInvocationID.y)]                         = imageLoad(img_psi_in, ivec2(ileft, global_index.y)).x;

        int iright = int(1+gl_WorkGroupID.x)*int(gl_WorkGroupSize.x);
        if (iright > imageSize(img_psi_in).x-1) iright = 0;
        tmp[1+int(gl_WorkGroupSize.x)][1+int(gl_LocalInvocationID.y)] = imageLoad(img_psi_in, ivec2(iright, global_index.y)).x;
    }

    // and the top and bottom
    if (int(gl_LocalInvocationID.y) == 0) {
        // only some threads do this

        int ileft = int(gl_WorkGroupID.y)*int(gl_WorkGroupSize.y) - 1;
        if (ileft < 0) ileft = imageSize(img_psi_in).y - 1;
        tmp[1+int(gl_LocalInvocationID.x)][0]                         = imageLoad(img_psi_in, ivec2(global_index.x, ileft)).x;

        int iright = int(1+gl_WorkGroupID.y)*int(gl_WorkGroupSize.y);
        if (iright > imageSize(img_psi_in).y-1) iright = 0;
        tmp[1+int(gl_LocalInvocationID.x)][1+int(gl_WorkGroupSize.y)] = imageLoad(img_psi_in, ivec2(global_index.x, iright)).x;
    }

    // don't load the corners because we don't need them

    groupMemoryBarrier();
    barrier();

    // perform some number of iterations
    //for (int i = 0; i < 10; ++i) {

        // right-hand side
        vec4 temp = imageLoad(img_vort_in, global_index);
        float psi_new = temp.x * pow(1.0/imageSize(img_psi_in).x, 2);

        // psi <- stuff
        psi_new += tmp[1+int(gl_LocalInvocationID.x)][0+int(gl_LocalInvocationID.y)];
        psi_new += tmp[1+int(gl_LocalInvocationID.x)][2+int(gl_LocalInvocationID.y)];
        psi_new += tmp[0+int(gl_LocalInvocationID.x)][1+int(gl_LocalInvocationID.y)];
        psi_new += tmp[2+int(gl_LocalInvocationID.x)][1+int(gl_LocalInvocationID.y)];

    //}

    imageStore(img_psi_out, global_index, vec4(0.25*psi_new, 0, 0, 1));
}
]]

-- perform one step of a relaxation scheme for the Poisson equation
local img_relax_many_blocked_shared = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(r32f, binding = 0) uniform readonly restrict image2D img_vort_in;
layout(r32f, binding = 1) uniform readonly restrict image2D img_psi_in;
layout(r32f, binding = 2) uniform writeonly restrict image2D img_psi_out;

shared float tmp[gl_WorkGroupSize.x+2][gl_WorkGroupSize.y+2];

void main() {
    ivec2 global_index = ivec2(gl_GlobalInvocationID.xy);
    ivec2 local_index = ivec2( int(gl_WorkGroupID.x)*int(gl_WorkGroupSize.x) + int(gl_LocalInvocationID.x),
                               int(gl_WorkGroupID.y)*int(gl_WorkGroupSize.y) + int(gl_LocalInvocationID.y) );

    // load psi_in into shared memory

    // first, the middle (most of the loads)
    tmp[1+int(gl_LocalInvocationID.x)][1+int(gl_LocalInvocationID.y)] = imageLoad(img_psi_in, global_index).x;

    // then the left and right sides (this assumes work groups fit perfectly into overall dimensions)

    if (int(gl_LocalInvocationID.x) == 0) {
        // only some threads do this

        int ileft = int(gl_WorkGroupID.x)*int(gl_WorkGroupSize.x) - 1;
        if (ileft < 0) ileft = imageSize(img_psi_in).x - 1;
        tmp[0][1+int(gl_LocalInvocationID.y)]                         = imageLoad(img_psi_in, ivec2(ileft, global_index.y)).x;

        int iright = int(1+gl_WorkGroupID.x)*int(gl_WorkGroupSize.x);
        if (iright > imageSize(img_psi_in).x-1) iright = 0;
        tmp[1+int(gl_WorkGroupSize.x)][1+int(gl_LocalInvocationID.y)] = imageLoad(img_psi_in, ivec2(iright, global_index.y)).x;
    }

    // and the top and bottom
    if (int(gl_LocalInvocationID.y) == 0) {
        // only some threads do this

        int ileft = int(gl_WorkGroupID.y)*int(gl_WorkGroupSize.y) - 1;
        if (ileft < 0) ileft = imageSize(img_psi_in).y - 1;
        tmp[1+int(gl_LocalInvocationID.x)][0]                         = imageLoad(img_psi_in, ivec2(global_index.x, ileft)).x;

        int iright = int(1+gl_WorkGroupID.y)*int(gl_WorkGroupSize.y);
        if (iright > imageSize(img_psi_in).y-1) iright = 0;
        tmp[1+int(gl_LocalInvocationID.x)][1+int(gl_WorkGroupSize.y)] = imageLoad(img_psi_in, ivec2(global_index.x, iright)).x;
    }

    // don't load the corners because we don't need them

    //groupMemoryBarrier();
    //memoryBarrierShared();
    barrier();

    float thisvort = imageLoad(img_vort_in, global_index).x * pow(1.0/imageSize(img_psi_in).x, 2);
    float psi_new = 0.0;

    // perform some number of iterations
    for (int i = 0; i < int(gl_WorkGroupSize.x); ++i) {
    //for (int i = 0; i < 2; ++i) {

        // right-hand side
        psi_new = thisvort;

        // add all neibs
        psi_new += tmp[1+int(gl_LocalInvocationID.x)][0+int(gl_LocalInvocationID.y)];
        psi_new += tmp[1+int(gl_LocalInvocationID.x)][2+int(gl_LocalInvocationID.y)];
        psi_new += tmp[0+int(gl_LocalInvocationID.x)][1+int(gl_LocalInvocationID.y)];
        psi_new += tmp[2+int(gl_LocalInvocationID.x)][1+int(gl_LocalInvocationID.y)];

        // wait for all procs to complete
        //groupMemoryBarrier();
        //memoryBarrierShared();
        barrier();

        // save back in shared array
        tmp[1+int(gl_LocalInvocationID.x)][1+int(gl_LocalInvocationID.y)] = 0.25*psi_new;
        //groupMemoryBarrier();
        barrier();
    }

    imageStore(img_psi_out, global_index, vec4(0.25*psi_new, 0, 0, 1));
}
]]

-- perform second-order, two-point gradient calculation to find velocity
local img_psi_to_vel = [[
#version 430
layout(local_size_x = 1, local_size_y = 1) in;
layout(r32f, binding = 0) uniform readonly restrict image2D img_psi_in;
layout(rg32f, binding = 1) uniform writeonly restrict image2D img_vel_out;

const float TWOPI = 2.0*3.1415926535897932384626433832795;

void main() {
    float ootdx = imageSize(img_psi_in).x / 2.0;
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // u = d psi / dy
    int ileft = pixel_coords.y - 1;
    if (ileft < 0) ileft += imageSize(img_psi_in).y;
    int iright = pixel_coords.y + 1;
    iright %= imageSize(img_psi_in).y;
    vec4 lval = imageLoad(img_psi_in, ivec2(pixel_coords.x, ileft));
    vec4 rval = imageLoad(img_psi_in, ivec2(pixel_coords.x, iright));
    float u = ootdx * (lval.x - rval.x);

    // v = - d psi / dx
    ileft = pixel_coords.x - 1;
    if (ileft < 0) ileft += imageSize(img_psi_in).x;
    iright = pixel_coords.x + 1;
    iright %= imageSize(img_psi_in).x;
    lval = imageLoad(img_psi_in, ivec2(ileft, pixel_coords.y));
    rval = imageLoad(img_psi_in, ivec2(iright, pixel_coords.y));
    float v = -ootdx * (lval.x - rval.x);

    // for testing, just prescribe the velocities
    //u = cos(TWOPI*float(pixel_coords.y)/float(imageSize(img_psi_in).y));
    //v = -sin(TWOPI*float(pixel_coords.x)/float(imageSize(img_psi_in).x));

    imageStore(img_vel_out, pixel_coords, vec4(u, v, 0, 1));
}
]]

-- perform second-order, two-point gradient calculation to find velocity
local img_psi_to_vel_blocked = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(r32f, binding = 0) uniform readonly restrict image2D img_psi_in;
layout(rg32f, binding = 1) uniform writeonly restrict image2D img_vel_out;

const float TWOPI = 2.0*3.1415926535897932384626433832795;

void main() {
    float ootdx = imageSize(img_psi_in).x / 2.0;
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // u = d psi / dy
    int ileft = pixel_coords.y - 1;
    if (ileft < 0) ileft += imageSize(img_psi_in).y;
    int iright = pixel_coords.y + 1;
    iright %= imageSize(img_psi_in).y;
    vec4 lval = imageLoad(img_psi_in, ivec2(pixel_coords.x, ileft));
    vec4 rval = imageLoad(img_psi_in, ivec2(pixel_coords.x, iright));
    float u = ootdx * (lval.x - rval.x);

    // v = - d psi / dx
    ileft = pixel_coords.x - 1;
    if (ileft < 0) ileft += imageSize(img_psi_in).x;
    iright = pixel_coords.x + 1;
    iright %= imageSize(img_psi_in).x;
    lval = imageLoad(img_psi_in, ivec2(ileft, pixel_coords.y));
    rval = imageLoad(img_psi_in, ivec2(iright, pixel_coords.y));
    float v = -ootdx * (lval.x - rval.x);

    // for testing, just prescribe the velocities
    //u = cos(TWOPI*float(pixel_coords.y)/float(imageSize(img_psi_in).y));
    //v = -sin(TWOPI*float(pixel_coords.x)/float(imageSize(img_psi_in).x));

    imageStore(img_vel_out, pixel_coords, vec4(u, v, 0, 1));
}
]]

-- semi-lagrangian advection, 1st order in time, 1st order in space
local comp_sla_euler_lin = [[
#version 430
layout(local_size_x = 1, local_size_y = 1) in;
layout(binding = 0) uniform sampler2D tex_vel_in;
layout(binding = 1) uniform sampler2D tex_scalar_in;
layout(r32f, binding = 2) uniform image2D img_scalar_out;

uniform float dt;

void main() {
    float pixel = 0.0;
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // sample velocity at the current location
    vec2 tc = vec2( float(0.5+pixel_coords.x) / float(imageSize(img_scalar_out).x),
                    float(0.5+pixel_coords.y) / float(imageSize(img_scalar_out).y) );
    vec4 col = texture(tex_vel_in, tc);

    // march backwards in time and sample the scalar there
    tc -= dt*col.xy;
    col = texture(tex_scalar_in, tc);
    pixel = col.x;

    // store the new scalar value in the output buffer
    imageStore(img_scalar_out, pixel_coords, vec4(pixel, 0, 0, 1));
}
]]

-- semi-lagrangian advection, 2nd order in time, 1st order in space
local comp_sla_rk2_lin = [[
#version 430
layout(local_size_x = 1, local_size_y = 1) in;
layout(binding = 0) uniform sampler2D tex_vel_in;
layout(binding = 1) uniform sampler2D tex_scalar_in;
layout(r32f, binding = 2) uniform image2D img_scalar_out;

uniform float dt;

void main() {
    float pixel = 0.0;
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // sample velocity at the current location
    vec2 tc = vec2( float(0.5+pixel_coords.x) / float(imageSize(img_scalar_out).x),
                    float(0.5+pixel_coords.y) / float(imageSize(img_scalar_out).y) );
    vec4 vel = texture(tex_vel_in, tc);

    // march backwards in time and sample the scalar there
    vec2 tc1 = tc - dt*vel.xy;
    vec4 vel1 = texture(tex_vel_in, tc1);

    // combine the two to find the true velocity
    vec2 tc2 = tc - 0.5*dt*(vel.xy+vel1.xy);
    vec4 col = texture(tex_scalar_in, tc2);
    pixel = col.x;

    // store the new scalar value in the output buffer
    imageStore(img_scalar_out, pixel_coords, vec4(pixel, 0, 0, 1));
}
]]

-- semi-lagrangian advection, 2nd order in time, 1st order in space
local comp_sla_rk2_lin_blocked = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(binding = 0) uniform sampler2D tex_vel_in;
layout(binding = 1) uniform sampler2D tex_scalar_in;
layout(r32f, binding = 2) uniform image2D img_scalar_out;

uniform float dt;

void main() {
    float pixel = 0.0;
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // sample velocity at the current location
    vec2 tc = vec2( float(0.5+pixel_coords.x) / float(imageSize(img_scalar_out).x),
                    float(0.5+pixel_coords.y) / float(imageSize(img_scalar_out).y) );
    vec4 vel = texture(tex_vel_in, tc);

    // march backwards in time and sample the scalar there
    vec2 tc1 = tc - dt*vel.xy;
    vec4 vel1 = texture(tex_vel_in, tc1);

    // combine the two to find the true velocity
    vec2 tc2 = tc - 0.5*dt*(vel.xy+vel1.xy);
    vec4 col = texture(tex_scalar_in, tc2);
    pixel = col.x;

    // store the new scalar value in the output buffer
    imageStore(img_scalar_out, pixel_coords, vec4(pixel, 0, 0, 1));
}
]]

-- semi-lagrangian advection, 2nd order in time, 1st order in space
local comp_sla_rk2_lin_blocked_rgba8 = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(binding = 0) uniform sampler2D tex_vel_in;
layout(binding = 1) uniform sampler2D tex_scalar_in;
layout(rgba8, binding = 2) uniform image2D img_scalar_out;

uniform float dt;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // sample velocity at the current location
    vec2 tc = vec2( float(0.5+pixel_coords.x) / float(imageSize(img_scalar_out).x),
                    float(0.5+pixel_coords.y) / float(imageSize(img_scalar_out).y) );
    vec4 vel = texture(tex_vel_in, tc);

    // march backwards in time and sample the scalar there
    vec2 tc1 = tc - dt*vel.xy;
    vec4 vel1 = texture(tex_vel_in, tc1);

    // combine the two to find the true velocity
    vec2 tc2 = tc - 0.5*dt*(vel.xy+vel1.xy);
    vec4 pixel = texture(tex_scalar_in, tc2);

    // store the new scalar value in the output buffer
    imageStore(img_scalar_out, pixel_coords, pixel);
}
]]


function poisson_jacobi:initTriAttributes()
    local glIntv = ffi.typeof('GLint[?]')
    local glUintv = ffi.typeof('GLuint[?]')

    local verts = glFloatv(4*3, {
        0,0,0,
        1,0,0,
        1,1,0,
        0,1,0,
    })

    local vpos_loc = gl.glGetAttribLocation(self.prog_draw, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(self.prog_draw, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)
end

function poisson_jacobi:initTextureImage(w, h, ifmt, dtyp, efmt)
    gl.glActiveTexture(GL.GL_TEXTURE0)
    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    local texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_REPEAT)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_REPEAT)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, ifmt,
                    w, h, 0,
                    efmt, dtyp, nil)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
    gl.glBindImageTexture(0, texID, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, ifmt)

    return texID
end

function poisson_jacobi:initializeTextures()
    for k,v in pairs(self.field) do
        print("poisson_jacobi.initializeTextures initializing (",k,")")

        -- generate and set the tid
        local texID = self:initTextureImage(v.x, v.y, v.ifmt, GL.GL_FLOAT, v.efmt)
        v.tid = texID

        -- set the initialization program
        --if v.ifmt == GL.GL_R32F then v.init_prog = self.prog_init_r32f end
        if k == "vort" then v.init_prog = self.prog_init_r32f end
        if k == "psi" then v.init_prog = self.prog_init_r32f end
        if k == "color" then v.init_prog = self.prog_init_rgba8 end
    end
end

function poisson_jacobi:clearTextures(condition)
    for k,v in pairs(self.field) do
        if v.init_prog ~= nil then
            print("poisson_jacobi.clearTextures clearing (",k,") tid is",v.tid)

            gl.glBindImageTexture(0, v.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, v.ifmt)
            gl.glUseProgram(v.init_prog)

            local sfill_loc = gl.glGetUniformLocation(v.init_prog, "uFillType")
            local fill_type = 0
            if k == "vort" then fill_type = self.sim end
            gl.glUniform1i(sfill_loc, fill_type)

            gl.glDispatchCompute(v.x/self.gridx, v.y/self.gridx, 1)
            gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)
        end
    end
end

function poisson_jacobi:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    -- two draw programs, one for rg and rgb buffers, another for just r
    self.prog_draw = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    self.prog_scalar = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = red_frag,
        })

    -- two init programs for setting and resetting the fields
    self.prog_init_rgba8 = sf.make_shader_from_source({
        compsrc = string.gsub(img_init_rgba8, "GSX", self.gridx, 2),
        })

    self.prog_init_r32f = sf.make_shader_from_source({
        compsrc = string.gsub(img_init_r32f, "GSX", self.gridx, 2),
        })

    -- and our solver programs
    self.prog_relax_once = sf.make_shader_from_source({
        compsrc = img_relax_once,
        })

    self.prog_relax_once_blocked = sf.make_shader_from_source({
        compsrc = string.gsub(img_relax_once_blocked, "GSX", self.gridx, 2),
        })

    self.prog_relax_once_blocked_shared = sf.make_shader_from_source({
        compsrc = string.gsub(img_relax_once_blocked_shared, "GSX", self.gridx, 2),
        })

    self.prog_relax_many_blocked_shared = sf.make_shader_from_source({
        compsrc = string.gsub(img_relax_many_blocked_shared, "GSX", self.gridx, 2),
        })

    self.prog_psi_to_vel = sf.make_shader_from_source({
        compsrc = img_psi_to_vel,
        })

    self.prog_psi_to_vel_blocked = sf.make_shader_from_source({
        compsrc = string.gsub(img_psi_to_vel_blocked, "GSX", self.gridx, 2),
        })

    self.prog_sladvect_1 = sf.make_shader_from_source({
        compsrc = comp_sla_euler_lin,
        })

    self.prog_sladvect_2 = sf.make_shader_from_source({
        compsrc = comp_sla_rk2_lin,
        })

    self.prog_sladvect_2_blocked = sf.make_shader_from_source({
        compsrc = string.gsub(comp_sla_rk2_lin_blocked, "GSX", self.gridx, 2),
        })

    self.prog_sladvect_2_blocked_rgba8 = sf.make_shader_from_source({
        compsrc = string.gsub(comp_sla_rk2_lin_blocked_rgba8, "GSX", self.gridx, 2),
        })

    -- init the meshes and textures
    self:initTriAttributes()
    self:initializeTextures(0)
    self:clearTextures(0)

    gl.glBindVertexArray(0)
end

function poisson_jacobi:advectScalars(dt)
    -- first-order semi-Lagrangian advection
    --local thisprog = self.prog_sladvect_1
    --local thisprog = self.prog_sladvect_2
    local thisprog = self.prog_sladvect_2_blocked
    gl.glUseProgram(thisprog)

    -- first, advect vorticity
    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.field.vel.tid)
    local tex_loc0 = gl.glGetUniformLocation(thisprog, "tex_vel_in")
    gl.glUniform1i(tex_loc0, 0)

    gl.glActiveTexture(GL.GL_TEXTURE1)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.field.vort.tid)
    local tex_loc1 = gl.glGetUniformLocation(thisprog, "tex_scalar_in")
    gl.glUniform1i(tex_loc1, 1)

    gl.glBindImageTexture(2, self.field.tscal.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, self.field.tscal.ifmt)

    local dt_loc = gl.glGetUniformLocation(thisprog, "dt")
    gl.glUniform1f(dt_loc, dt)

    gl.glDispatchCompute(self.field.vort.x/self.gridx, self.field.vort.y/self.gridx, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

    -- now swap the buffers
    local tempid = self.field.vort.tid
    self.field.vort.tid = self.field.tscal.tid
    self.field.tscal.tid = tempid

    -- then, advect color
    gl.glUseProgram(self.prog_sladvect_2_blocked_rgba8)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.field.vel.tid)
    local tex_loc0 = gl.glGetUniformLocation(thisprog, "tex_vel_in")
    gl.glUniform1i(tex_loc0, 0)

    gl.glActiveTexture(GL.GL_TEXTURE1)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.field.color.tid)
    local tex_loc1 = gl.glGetUniformLocation(thisprog, "tex_scalar_in")
    gl.glUniform1i(tex_loc1, 1)

    gl.glBindImageTexture(2, self.field.tcol.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, self.field.tcol.ifmt)

    local dt_loc = gl.glGetUniformLocation(thisprog, "dt")
    gl.glUniform1f(dt_loc, dt)

    gl.glDispatchCompute(self.field.color.x/self.gridx, self.field.color.y/self.gridx, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

    -- now swap the buffers
    local tempid = self.field.color.tid
    self.field.color.tid = self.field.tcol.tid
    self.field.tcol.tid = tempid
end

function poisson_jacobi:jacobiSolver()
    --gl.glUseProgram(self.prog_relax_once)
    --gl.glUseProgram(self.prog_relax_once_blocked)
    --gl.glUseProgram(self.prog_relax_once_blocked_shared)
    gl.glUseProgram(self.prog_relax_many_blocked_shared)

    -- keep reading from the (constant) rhs vector
    gl.glBindImageTexture(0, self.field.vort.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, self.field.vort.ifmt)

    -- loop, relax, and swap buffers
    for i=1,20*self.field.psi.x do
    --for i=1,10 do
        -- bind the last two images
        gl.glBindImageTexture(1, self.field.psi.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, self.field.psi.ifmt)
        gl.glBindImageTexture(2, self.field.tpsi.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, self.field.tpsi.ifmt)

        -- run the computation
        gl.glDispatchCompute(self.field.psi.x/self.gridx, self.field.psi.y/self.gridx, 1)
        gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

        -- swap the buffers
        local tempid = self.field.psi.tid
        self.field.psi.tid = self.field.tpsi.tid
        self.field.tpsi.tid = tempid
    end
end

function poisson_jacobi:solveForVelocity()

    -- first, perform Jacobi relaxation to find psi (streamfunction) from vorticity

    -- clear out psi
    --gl.glClearTexImage(self.field.psi.tid, 0, v.efmt, GL.GL_FLOAT, nil)
    gl.glBindImageTexture(0, self.field.psi.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, self.field.psi.ifmt)
    gl.glUseProgram(self.field.psi.init_prog)
    local sfill_loc = gl.glGetUniformLocation(self.field.psi.init_prog, "uFillType")
    gl.glUniform1i(sfill_loc, 0)
    gl.glDispatchCompute(self.field.psi.x, self.field.psi.y, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

    -- run the iterative solver
    self:jacobiSolver()

    -- then, perform 2nd order derivatives to find velocity from that
    gl.glBindImageTexture(0, self.field.psi.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, self.field.psi.ifmt)
    gl.glBindImageTexture(1, self.field.vel.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, self.field.vel.ifmt)

    --gl.glUseProgram(self.prog_psi_to_vel)
    --gl.glDispatchCompute(self.field.vel.x, self.field.vel.y, 1)
    gl.glUseProgram(self.prog_psi_to_vel_blocked)
    gl.glDispatchCompute(self.field.vel.x/self.gridx, self.field.vel.y/self.gridx, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

    --gl.glUseProgram(0)
end

function poisson_jacobi:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog_draw)
    gl.glDeleteProgram(self.prog_scalar)
    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)
end

function poisson_jacobi:render_for_one_eye(view, proj)
    local initialview = {}
    for i=1,16 do initialview[i] = view[i] end

    mm.glh_translate(view, -2.15,0,1.5)

    gl.glBindVertexArray(self.vao)

    for k,v in pairs(self.field) do
        local thisprog = self.prog_draw
        if v.efmt == GL.GL_RED then thisprog = self.prog_scalar end

        if k == "vel" or k == "vort" or k == "psi" or k == "color" then

        gl.glUseProgram(thisprog)
        local umv_loc = gl.glGetUniformLocation(thisprog, "mvmtx")
        local upr_loc = gl.glGetUniformLocation(thisprog, "prmtx")
        gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

        gl.glActiveTexture(GL.GL_TEXTURE0)
        gl.glBindTexture(GL.GL_TEXTURE_2D, v.tid)
        local stex_loc = gl.glGetUniformLocation(thisprog, "sTex")
        gl.glUniform1i(stex_loc, 0)

        local to_loc = gl.glGetUniformLocation(thisprog, "texMag")
        local val_mag = 1.0
        if k == "vort" then val_mag = 2 end
        if k == "vel" then val_mag = 500 end
        if k == "psi" then val_mag = 2500 end
        gl.glUniform1f(to_loc, val_mag)

        gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
        gl.glDrawArrays(GL.GL_TRIANGLE_FAN, 0, 4)

        mm.glh_translate(view, 1.1,0,0)
        end
    end

    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end

function poisson_jacobi:timestep(absTime, dt)
    -- advect vorticity, scalar, and color
    self:advectScalars(self.dt)

    -- calculate new velocities
    self:solveForVelocity()
end

function poisson_jacobi:keypressed(ch)
    --if ch == string.byte('-') then
    --    self.rule = self.rule - 1
    --    return
    --elseif ch == string.byte('=') then
    --    self.rule = self.rule + 1
    --    return
    --end

    local initials = {
        [string.byte('Z')] = 0,
        [string.byte('X')] = 1,
        [string.byte('C')] = 2,
    }
    local initc = initials[ch]
    if initc then self:clearTextures(initc) return end
end

function poisson_jacobi:onSingleTouch(pointerid, action, x, y)
    --print("poisson_jacobi.onSingleTouch",pointerid, action, x, y)
end

return poisson_jacobi
