-- diaspora04.lua
--
-- recreating the Processing sketch diaspora
--
-- essentially a 2D vortex method with restarts, algorithmic color, streaks
--
-- diaspora04: finally got colors ok, and made it work with newer luajit
--
-- began as powerflock02.lua
-- by Mark Stock (markjstock@gmail.com)

--local openGL = require("opengl")
local ffi = require("ffi")
local sf = require("util.shaderfunctions")
local mm = require("util.matrixmath")

--require("util.timeseries")

diaspora04 = {}

diaspora04.__index = diaspora04

function diaspora04.new(...)
    local self = setmetatable({}, diaspora04)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end
    return self
end

function diaspora04:init()
    self.vbos = {}
    self.vao = 0
    self.prog_display = 0
    self.prog_findvels = 0
    self.prog_integrate = 0
    self.npts = 32*1024
    self.dt = 1/100000
    self.time = 0
    self.dframect = 0

    -- uncommenting this crashes this scene
    --self.minuteramp = timeseries.new("uniform ramp", 60)

    self.descr = "Modified vortex particle system"
end

-- Types from:
-- https://github.com/nanoant/glua/blob/master/init.lua
local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glCharv    = ffi.typeof('GLchar[?]')
local glSizeiv   = ffi.typeof('GLsizei[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

local pt_vert = [[
#version 430
layout(location = 0) in vec4 vposition;
layout(location = 1) in vec4 vattribute;
out vec4 colorrad;
void main() {
   gl_Position = vec4(vposition.x, vposition.y, vposition.z, 1.f);
   colorrad = vattribute;
}
]]

local bb_geom = [[
#version 430
layout(location = 0) uniform mat4 Model;
layout(location = 1) uniform mat4 View;
layout(location = 2) uniform mat4 Projection;
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;
in vec4 colorrad[];
out vec2 txcoord;
out vec4 pcolor;
void main() {
   float rad = colorrad[0].w;
   vec4 pos = (View*Model)*gl_in[0].gl_Position;

   vec4 ppos = Projection*pos;
   // minimum radius
   float fudge = 0.005 * ppos.z;
   float newrad = max(rad, fudge);
   //pcolor = vec4(colorrad[0].x, colorrad[0].y, colorrad[0].z, sqrt(rad / newrad));
   pcolor = vec4(colorrad[0].x, colorrad[0].y, colorrad[0].z, 0.01);

   txcoord = vec2(-1,-1);
   gl_Position = Projection*(pos+newrad*vec4(txcoord,0,0));
   EmitVertex();
   txcoord = vec2( 1,-1);
   gl_Position = Projection*(pos+newrad*vec4(txcoord,0,0));
   EmitVertex();
   txcoord = vec2(-1, 1);
   gl_Position = Projection*(pos+newrad*vec4(txcoord,0,0));
   EmitVertex();
   txcoord = vec2( 1, 1);
   gl_Position = Projection*(pos+newrad*vec4(txcoord,0,0));
   EmitVertex();

   EndPrimitive();
}
]]

local rad_frag = [[
#version 330
in vec2 txcoord;
in vec4 pcolor;
layout(location = 0) out vec4 FragColor;
void main() {
   float rs = dot(txcoord, txcoord);
   float s = 1./(1.+16.*rs*rs*rs) - 0.06;
   //FragColor = pcolor;
   FragColor = vec4(s*pcolor.x, s*pcolor.y, s*pcolor.z, 0.02*s);
}
]]

local vel_tiled_comp = [[
#version 430
layout(local_size_x=256) in;

layout(location = 0) uniform float dt;
layout(std430, binding=0) restrict readonly buffer pblock { vec4 positions[]; };
layout(std430, binding=1) restrict readonly buffer mblock { vec4 attributes[]; };
layout(std430, binding=2) restrict buffer vblock { vec4 velocities[]; };

shared vec4 tmppos[gl_WorkGroupSize.x];
shared vec4 tmpvel[gl_WorkGroupSize.x];
void main() {
   int N = int(gl_NumWorkGroups.x*gl_WorkGroupSize.x);
   int index = int(gl_GlobalInvocationID);

   vec2 oldpos = positions[index].xy;
   vec2 oldvel = velocities[index].xy;

   int neibs = 0;
   vec2 newvel = vec2(0.f,0.f);

   for (int tile=0; tile<N; tile+=int(gl_WorkGroupSize.x)) {
       tmppos[gl_LocalInvocationIndex] = positions[tile + int(gl_LocalInvocationIndex)];
       tmpvel[gl_LocalInvocationIndex] = velocities[tile + int(gl_LocalInvocationIndex)];
       groupMemoryBarrier();
       barrier();

       for (int i=0; i<int(gl_WorkGroupSize.x) ;++i) {
           vec2 otherpos = tmppos[i].xy;
           vec2 othervel = tmpvel[i].xy;
           vec2 diff = otherpos - oldpos;
           float distsq = diff.x*diff.x + diff.y*diff.y + 0.01f;
           float dist = sqrt(distsq);
           // vortex velocity
           float force = tmppos[i].w / (dist*distsq);
           // move toward or away from neighbors
           newvel.x += diff.y * force;
           newvel.y -= diff.x * force;
       }
       groupMemoryBarrier();
       barrier();
   }

   velocities[index] = vec4(oldvel, newvel);
}
]]

local integ_comp = [[
#version 430
layout(local_size_x=256) in;

layout(location = 0) uniform float dt;
layout(std430, binding=0) restrict buffer pblock { vec4 positions[]; };
layout(std430, binding=1) restrict readonly buffer mblock { vec4 attributes[]; };
layout(std430, binding=2) restrict buffer vblock { vec4 velocities[]; };

void main() {
   int index = int(gl_GlobalInvocationID);
   vec4 position = positions[index];
   vec4 velocity = velocities[index];

   vec2 meanvel = 0.5 * (velocity.xy + velocity.zw);
   position.xy += dt*meanvel;
   position.xy *= 0.999999;

   positions[index] = position;
   velocities[index] = vec4(velocity.zw, 0.f, 0.f);
}
]]


function diaspora04:init_point_attributes()

    -- position array: x, y, strength, 1
    pos_array = ffi.new("float[?]", self.npts*4)
    -- attribute array: red, green, blue, rad
    att_array = ffi.new("float[?]", self.npts*4)
    -- velocity array: old dx/dt, old dy/dt, new dx/dt, new dy/dy
    vel_array = ffi.new("float[?]", self.npts*4)

    -- initialize particles

    -- first attributes, really just color and radius

    -- compute some random walks
    walkarry = ffi.new("float[?]", self.npts*3)
    walkarry[0] = 6*math.random();		-- starting hue
    walkarry[1] = 0.1+0.8*math.random();	-- starting saturation
    walkarry[2] = 0.2+0.6*math.random();	-- starting value
    for i=1,self.npts-1 do
        walkarry[3*i+0] = walkarry[3*i-3] + 0.02*(math.random()-0.5)
        walkarry[3*i+1] = walkarry[3*i-2] + 0.05*(math.random()-0.5)
        walkarry[3*i+2] = walkarry[3*i-1] + 0.10*(math.random()-0.5)
    end
    local hmult = walkarry[3*self.npts-3] - walkarry[0]
    local smult = walkarry[3*self.npts-2] - walkarry[1]
    local vmult = walkarry[3*self.npts-1] - walkarry[2]
    for i=1,self.npts-1 do
        walkarry[3*i+0] = walkarry[3*i+0] - hmult*i/self.npts
        walkarry[3*i+1] = walkarry[3*i+1] - smult*i/self.npts
        walkarry[3*i+2] = walkarry[3*i+2] - vmult*i/self.npts
    end

    for i=0,self.npts-1 do
        -- color as hsv (clamp s,v to 0..1, h can be anything)
        local h = walkarry[3*i+0] + 0.100*(math.random()-0.5)
        local s = walkarry[3*i+1] + 0.300*(math.random()-0.5)
        local v = walkarry[3*i+2] + 0.600*(math.random()-0.5)
        if s < 0 then s = 0 elseif s > 1 then s = 1 end
        if v < 0 then v = 0 elseif v > 1 then v = 1 end
        -- color as rgb
        local idx = math.floor(h) % 6
        if idx == 0 then
            att_array[4*i+0] = v
            att_array[4*i+1] = v * (1 - s*(1-h+idx))
            att_array[4*i+2] = v * (1 - s)
        elseif idx == 1 then
            att_array[4*i+0] = v * (1 - s*(h-idx))
            att_array[4*i+1] = v
            att_array[4*i+2] = v * (1 - s)
        elseif idx == 2 then
            att_array[4*i+0] = v * (1 - s)
            att_array[4*i+1] = v
            att_array[4*i+2] = v * (1 - s*(1-h+idx))
        elseif idx == 3 then
            att_array[4*i+0] = v * (1 - s)
            att_array[4*i+1] = v * (1 - s*(h-idx))
            att_array[4*i+2] = v
        elseif idx == 4 then
            att_array[4*i+0] = v * (1 - s*(1-h+idx))
            att_array[4*i+1] = v * (1 - s)
            att_array[4*i+2] = v
        else
            att_array[4*i+0] = v
            att_array[4*i+1] = v * (1 - s)
            att_array[4*i+2] = v * (1 - s*(h-idx))
        end
        -- radius can vary a little
        att_array[4*i+3] = 0.002 * math.pow(0.5 + 1.0 * math.random(),2)
    end

    -- then positions, strength
    local init_radius = 1.0
    for i=0,self.npts-1 do
        local x = 2*math.random()-1
        local y = 2*math.random()-1
        local dist = x*x + y*y
        while dist > 1.0 do
            x = 2*math.random()-1
            y = 2*math.random()-1
            dist = x*x + y*y
        end
        pos_array[4*i+0] = init_radius * math.sin(6.2831854*i/self.npts)
        pos_array[4*i+1] = init_radius * math.cos(6.2831854*i/self.npts)
        pos_array[4*i+2] = 0
        pos_array[4*i+3] = 2*math.random()-1
    end

    -- then velocities
    for i=0,self.npts-1 do
        vel_array[4*i+0] = 0
        vel_array[4*i+1] = 0
        vel_array[4*i+2] = 0
        vel_array[4*i+3] = 0
    end

    -- set up buffers to contain this data
    local vboIds = ffi.new("int[3]")
    gl.glGenBuffers(3, vboIds)

    local vboP = vboIds[0]
    local vboM = vboIds[1]
    local vboV = vboIds[2]

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboP)
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(pos_array), pos_array, GL.GL_DYNAMIC_COPY)

    gl.glEnableVertexAttribArray(0)
    gl.glVertexAttribPointer(0, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    --gl.glVertexAttribDivisor(0, 1) -- Drawing GL_POINTS, not instanced

    -- what do I map this to to get the data passed into the draw pipeline?
    --   the particle radius and brightness need to be used
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboM)
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(att_array), att_array, GL.GL_DYNAMIC_COPY)

    gl.glEnableVertexAttribArray(1)
    gl.glVertexAttribPointer(1, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glVertexAttribDivisor(1, 1)


    -- now one or more velocity arrays
    gl.glBindBuffer(GL.GL_SHADER_STORAGE_BUFFER, vboV)
    gl.glBufferData(GL.GL_SHADER_STORAGE_BUFFER, ffi.sizeof(vel_array), vel_array, GL.GL_DYNAMIC_COPY)

    -- and get these ready for the compute shaders
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vboP)
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 1, vboM)
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 2, vboV)

    table.insert(self.vbos, vboP)
    table.insert(self.vbos, vboM)
    table.insert(self.vbos, vboV)

    gl.glUseProgram(self.prog_findvels)
    --gl.glUniform1f(0, self.dt) -- AMD does not like this
    gl.glUseProgram(self.prog_integrate)
    gl.glUniform1f(0, self.dt)
end

function diaspora04:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.prog_display = sf.make_shader_from_source({
        vsrc = pt_vert,
        gsrc = bb_geom,
        fsrc = rad_frag,
        })

    self.prog_findvels = sf.make_shader_from_source({
        compsrc = vel_tiled_comp,
        })

    self.prog_integrate = sf.make_shader_from_source({
        compsrc = integ_comp,
        })

    self:init_point_attributes()
    gl.glBindVertexArray(0)
end

function diaspora04:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        local vboId = ffi.new("GLuint[1]", v)
        gl.glDeleteBuffers(1,vboId)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog_display)
    gl.glDeleteProgram(self.prog_findvels)
    gl.glDeleteProgram(self.prog_integrate)

    local vaoId = ffi.new("GLuint[2]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

function diaspora04:renderEye(model, view, proj)
    gl.glUseProgram(self.prog_display)
    if self.dframect == 0 then
        gl.glClearColor(0,0,0,0)
        gl.glClear(GL.GL_COLOR_BUFFER_BIT)
    end

    gl.glUniformMatrix4fv(0, 1, GL.GL_FALSE, glFloatv(16, model))
    gl.glUniformMatrix4fv(1, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(2, 1, GL.GL_FALSE, glFloatv(16, proj))

    gl.glDisable(GL.GL_DEPTH_TEST)
    gl.glEnable(GL.GL_BLEND)
    --gl.glBlendFunc(GL.GL_ONE, GL.GL_ONE)
    --gl.glBlendFunc(GL.GL_ONE, GL.GL_ONE_MINUS_SRC_COLOR)
    --gl.glBlendFunc(GL.GL_SRC_COLOR, GL.GL_ONE_MINUS_SRC_COLOR)
    --use this one
    gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)
    gl.glBindVertexArray(self.vao)
    gl.glDrawArrays(GL.GL_POINTS, 0, self.npts)
    gl.glBindVertexArray(0)
    gl.glEnable(GL.GL_DEPTH_TEST)
    gl.glDisable(GL.GL_BLEND)

    gl.glUseProgram(0)

    self.dframect = self.dframect+1
end

function diaspora04:timestep(absTime, dt)
    gl.glUseProgram(self.prog_findvels)
    gl.glDispatchCompute(self.npts/256, 1, 1)

    gl.glUseProgram(self.prog_integrate)
    gl.glDispatchCompute(self.npts/256, 1, 1)
    gl.glUseProgram(0)

    self.time = self.time + self.dt
    --print("value is",self.minuteramp:getvalue(self.time))
end

return diaspora04
