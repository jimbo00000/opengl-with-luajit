--[[ quad_stack.lua

    Draws a stack of alpha-blended quads with parallax control.
]]

local DO_FONTS = true
local TEXTURE_FILES_AND_SIZES = {
    { 'parallax-forest-back-trees.rgba'  , 272, 160, },
    { 'parallax-forest-middle-trees.rgba', 272, 160, },
    { 'parallax-forest-front-trees.rgba' , 272, 160, },
    { 'parallax-forest-lights.rgba'      , 272, 160, },
}

simple_quads = {}
simple_quads.__index = simple_quads

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local mm = require 'util.matrixmath'
local lri = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'
local filesystem = require 'util.filesystem'

function simple_quads.new(...)
    local self = setmetatable({}, simple_quads)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_quads:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_quads:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}

    self.n_quads = 4
    self.draw_font = true
    self.do_alpha = true
    self.xoffset = 0
end

------------------------------------------
-- OpenGL/GLES concerns:
-- Texture, VBO, shader, initGL/exitGL
------------------------------------------

function simple_quads:loadTextures(texfilename, w, h)
    texfilename = 'images/' .. texfilename

    local data, size = filesystem.read(texfilename)
    print(size, 'bytes read from', texfilename)

    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)

    local tex = dtxId[0]
    table.insert(self.texIDs, tex)
    gl.glBindTexture(GL.GL_TEXTURE_2D, tex)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_REPEAT)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_REPEAT)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    --gl.glTexParameteri( GL.GL_TEXTURE_2D, GL.GL_DEPTH_TEXTURE_MODE, GL.GL_INTENSITY ); --deprecated, out in 3.1
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
                  w, h, 0,
                  GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end

------------------------------------------
-- VBO: vertex input to GPU draw
------------------------------------------
function simple_quads:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_quads:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

-- This function both allocates and fills storage.
function simple_quads:populateVBO()
    local s = 1
    local verts = {
        -s,-s,0,
        s,-s,0,
        s,s,0,
        -s,-s,0,
        s,s,0,
        -s,s,0,
    }

    local all = {}
    for i=1,self.n_quads do
        for j=1,6 do
            verts[3*j] = i -- Assign z value
        end
        for j=1,#verts do
            table.insert(all, verts[j])
        end
    end
    local av = ffi.typeof('GLfloat[?]')(#all,all)

    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(av),av,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,3,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
end

local vs=[[
#version 100

#ifdef GL_ES
precision mediump float;
#endif

uniform float uNumInsts;

attribute vec3 vP;
varying vec2 vfTexCoord;
varying float brightness;

void main()
{
    vfTexCoord = vP.xy;
    brightness = vP.z / uNumInsts;

    float scale = 1.;
    vec2 pt = scale * vP.xy;

    gl_Position = vec4(pt,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec2 vfTexCoord;
varying float brightness;

uniform sampler2D tex;
uniform float xOffset;

void main()
{
    vec2 tc = .5*vfTexCoord + vec2(.5);
    tc.y = 1. - tc.y;
    tc.x += .5*brightness*xOffset;
    vec4 col = texture2D(tex, tc);

    gl_FragColor = vec4(col);
}
]]

function simple_quads:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})
    local function uni(var) return gl.glGetUniformLocation(self.prog, var) end
    local unis = {
        'uNumInsts',
        'tex',
        'xOffset',
    }
    self.uniforms = {}
    for _,v in pairs(unis) do self.uniforms[v] = uni(v) end

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()

    self.texIDs = {}
    for i=1,#TEXTURE_FILES_AND_SIZES do
        local item = TEXTURE_FILES_AND_SIZES[i]
        self:loadTextures(
            item[1], item[2], item[3])
    end

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function simple_quads:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    if self.glfont then
        self.glfont:exitGL()
    end

    for i=1,#self.texIDs do
        local texdel = self.texIDs[i]
        gl.glDeleteTextures(1,texdel)
    end
end

function simple_quads:render()
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    if self.do_alpha then
        gl.glEnable(GL.GL_BLEND)
    else
        gl.glDisable(GL.GL_BLEND)
    end

    gl.glUseProgram(self.prog)
    gl.glUniform1f(self.uniforms.uNumInsts, self.n_quads)
    gl.glUniform1f(self.uniforms.xOffset, self.xoffset)

    self:bindVBO()
    for i=1,#self.texIDs do
        gl.glActiveTexture(GL.GL_TEXTURE0)
        gl.glBindTexture(GL.GL_TEXTURE_2D, self.texIDs[i])
        gl.glUniform1i(self.uniforms.tex, 0)

        local QuadSize = 6
        local start = (i-1) * QuadSize
        gl.glDrawArrays(GL.GL_TRIANGLES, start, 6)
    end
    self:unbindVBO()

    if self.glfont and self.draw_font then
        local m = {}
        local p = {}
        mm.make_identity_matrix(m)
        local s = .5
        mm.glh_scale(m, s, s, s)
        mm.glh_translate(m, 30, 30, 0)
        -- TODO getStringWidth and center text
        if not win_w then
            win_w,win_h = 800,600
        end
        mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
        gl.glDisable(GL.GL_DEPTH_TEST)
        local col = {1, 1, 1}
        self.glfont:render_string(m, p, col, "quad_stack "..self.n_quads)
        mm.glh_translate(m, 0, 120, 0)

        self.glfont:render_string(m, p, col, "xoff= "..self.xoffset)
    end
end

------------------------------------------
-- Timestep
------------------------------------------
function simple_quads:timestep(absTime, dt)
    self.t = absTime

    local dx = 0
    if self.holding_left then
        dx = 1
    elseif self.holding_right then
        dx = -1
    end
    self.xoffset = self.xoffset + dt * dx
end

------------------------------------------
-- Control
------------------------------------------
function simple_quads:update_retropad(joystates)
    self.holding_left = false
    self.holding_right = false
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end

        if curstate[lri.JOYPAD_LEFT] ~= 0 then
            self.holding_left = true
        end
        if curstate[lri.JOYPAD_RIGHT] ~= 0 then
            self.holding_right = true
        end
    end
end

function simple_quads:onButtonPressed(padIndex, button)
    if button == lri.JOYPAD_A then
        snd.playSound("test.wav")
        self.t_last = self.t
    elseif button == lri.JOYPAD_B then
        snd.playSound("pop_drip.wav")
        self.t_last = self.t
    elseif button == lri.JOYPAD_X then
        self.n_quads = self.n_quads * 2
        self.n_quads = math.min(self.n_quads, 4096*2*16)
        self:populateVBO()
    elseif button == lri.JOYPAD_Y then
        self.n_quads = self.n_quads * .5
        self.n_quads = math.max(self.n_quads, 1)
        self:populateVBO()
    elseif button == lri.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    elseif button == lri.JOYPAD_START then
        self.do_alpha = not self.do_alpha
    end
end

return simple_quads
