local level_editor = {
    _DESCRIPTION = [[
        A level editor for a simple platformer.
        Uses SDL for mouse input.
    ]],
    _URL = 'https://bitbucket.org/jimbo00000/opengl-with-luajit/src/master/'
}
level_editor.__index = level_editor


local mm = require 'util.matrixmath'
local platformerLib = require 'mouse_rects'


-- Serialization to lua file
local function saveTargetsToFile(targets, filename)
    print('Saving', #targets, 'targets to', filename)
    local inp = assert(io.open(filename, 'w'))
    inp:write('rects = {\n')

    local function serializeLine(line)
        inp:write('        '..line..'\n')
    end

    for _,t in pairs(targets) do
        inp:write('    {\n')
        serializeLine('pos={'..tostring(t.pos[1])..', '..tostring(t.pos[2])..'},')
        serializeLine('sz={'..tostring(t.sz[1])..', '..tostring(t.sz[2])..'},')
        serializeLine('which='..tostring(t.which)..',')
        inp:write('    },\n')
    end

    inp:write('}\n')
    assert(inp:close())
end


--
-- Module functions
--
function level_editor.new(...)
    local self = setmetatable({}, level_editor)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end
    return self
end

function level_editor:setDataDirectory(dir)
    self.dataDir = dir
    self.platformer:setDataDirectory(dir)
end

function level_editor:resizeViewport(win_w,win_h)
	self.platformer:resizeViewport(win_w,win_h)
end

function level_editor:init()
    -- TODO: wrap in pcall and return error
    self.platformer = platformerLib.new()

    self.pan_offset = {}
    self.pan_offset.x = 0
    self.pan_offset.y = 0
end

function level_editor:initGL()
    self.platformer:initGL()
end

function level_editor:exitGL()
    self.platformer:exitGL()
end

function level_editor:render()
    local viewMatrix = {}
    mm.make_identity_matrix(viewMatrix)

    if self.pan_offset.x and self.pan_offset.y then
        mm.glh_translate(viewMatrix, self.pan_offset.x, self.pan_offset.y, 0)
    end

    self.platformer:render(viewMatrix)
end

function level_editor:timestep(absTime, dt)
    self.platformer:timestep(absTime, dt)

    if self.platformer.background then
        local backgroundSpeed = .2
        self.platformer.background.xoffset = -self.pan_offset.x * backgroundSpeed
    end
end

function level_editor:update_retropad(joystates)
	self.platformer:update_retropad(joystates)
end



--
-- Editor Input
--
function level_editor:windowToNDC(x,y)
    local w,h = self.platformer.win_w, self.platformer.win_h
    local x01,y01 = x/w, y/h
    y01 = 1-y01
    local x11,y11 = 2*x01-1, 2*y01-1
    return x11, y11
end

local bit = require("bit")

local function isPointWithinRect(r, x, y)
    if x < r.pos[1] then return false end
    if x > r.pos[1]+r.sz[1] then return false end
    if y < r.pos[2] then return false end
    if y > r.pos[2]+r.sz[2] then return false end
    return true
end

function level_editor:finalizeLastBox(new_box)
	local targets = self.platformer.targets
    local targ = self.grabbing_target
    if not targ then return end
    local box = targ.box

    local x = targ.pos[1]
    local y = targ.pos[2]
    local w = targ.sz[1]
    local h = targ.sz[2]

    --Flip if upside down(negative w|h)
    if w < 0 then
        x,w = x+w,-w
    end
    if h < 0 then
        y,h = y+h,-h
    end
    -- Re-assign flipped coords
    targ.pos={x,y}
    targ.sz={w,h}

    if new_box then
    	self.platformer.world:add(box,  x,y,  w,h)
	else
		self.platformer.world:update(box,  x,y,  w,h)
	end
end

-- x,y in NDC coords
function level_editor:updateLastBox(x,y)
    local t = self.hover_target
    if t then
        local a = x - t.pos[1]
        local b = y - t.pos[2]
        if self.which_button==1 then
            t.sz = {a,b}
            -- NOTE: coords may have negative size
            self.platformer:populateVBO()
        elseif self.which_button==2 then
            t.pos = {
                x - self.grabbing_x,
                y - self.grabbing_y,
            }
            -- NOTE: coords may have negative size
            self.platformer:populateVBO()
        elseif self.which_button==3 then
            local dx = x - self.grabbing_x
            local dy = y - self.grabbing_y
            t.sz = {
                dx + self.grabbing_w,
                dy + self.grabbing_h,
            }
            -- NOTE: coords may have negative size
            self.platformer:populateVBO()
        end
    end
end

-- Find any quads below mouse pointer, increment texture offset for sprite sheets.
function level_editor:incrementTextureSpriteIdx(x, y, delta)
	local targets = self.platformer.targets
    for k,t in pairs(targets) do
        if isPointWithinRect(t, x, y) then
            t.which = t.which or 1
            t.which = t.which + delta
            if t.which < 1 then t.which = 16 end
            if t.which > 16 then t.which = 1 end

            self.platformer:populateVBO()
            self.platformer:repopulateBumpWorld()
        end
    end
end

local function makeRandomTableOfNTargets(n)
    local targets = {}
    for i=1,n do
        local x = -1+2*math.random()
        local y = -1+2*math.random()
        local w = .05 + .1*math.random()
        local h = .05 + .1*math.random()

        local t = {
            pos={x,y},
            sz={w,h},
        }
        table.insert(targets, t)
    end
    return targets
end

-- Clears the existing set of targets and populates the world with a random set of new quads.
function level_editor:randomizeTargets()
    if self.platformer.targets then
        for _,v in pairs(self.platformer.targets) do
            if v then self.platformer.world:remove(v.box) end
        end
    end

    self.platformer.targets = makeRandomTableOfNTargets(self.platformer.n_quads)
end


function level_editor:randomizeAndPopulate(num)
    print(num, self.platformer.n_quads)
    self.platformer.n_quads = math.max(1,num)

    self:randomizeTargets()
    self.platformer:populateBumpWorld()
    self.platformer:populateVBO()
end


--
-- Input callbacks: mouse, keyboard
--
function level_editor:keypressed(key, scancode, action, mods)
    --print('ky', key, scancode, action, mods)
	local targets = self.platformer.targets
    if action == 1 then
        if key == 115 then --'s'
            saveTargetsToFile(targets, 'rect_data.lua')
        elseif key == 114 then --'r'
            local num = self.platformer.n_quads / 2
            self:randomizeAndPopulate(num)
        elseif key == 116 then --'t'
            local num = self.platformer.n_quads * 2
            self:randomizeAndPopulate(num)
        end
    end
end

function level_editor:mousemove(x,y)

    self.lastmouse = {x,y}
    -- Read SDL state values
    local b = self.mouse_state
    if b ~= 0 then
        x,y = self:windowToNDC(x,y)
        local xworld = x - self.pan_offset.x
        local yworld = y - self.pan_offset.y
        self:updateLastBox(xworld,yworld)
        
        if self.hover_target == nil and self.pan_click_x then
            self.pan_offset.x = x - self.pan_click_x
            self.pan_offset.y = y - self.pan_click_y
        end
    end
end

function level_editor:mousebutton(type, which, button, state, x, y)
    self.mouse_state = state
    x,y = self:windowToNDC(x,y)

    local xworld = x - self.pan_offset.x
    local yworld = y - self.pan_offset.y

    -- Check for mouse cursor over rects
    self.hover_target = nil
    local targets = self.platformer.targets
    for k,t in pairs(targets) do
        if isPointWithinRect(t, xworld, yworld) then
            self.hover_target = targets[k]
        end
    end

    local b = self.mouse_state
    if b ~= 0 then
        --
        -- Mouse down
        --
        self.which_button = button
        if button == 1 then -- left click

            local i = #targets + 1
            local b = {
                name='obj'..i,
                idx=i,
                type=1,
            }

            local s = 0
            local t = {
                pos={xworld,yworld},
                sz={s,s},
                box=b,
            }
            table.insert(targets, t)
            self.platformer:populateVBO()
            self.hover_target = targets[#targets]
            self.grabbing_target = targets[#targets]
            --TODO: buffersubdata
        elseif button == 2 then
            if self.hover_target then
                self.grabbing_target = self.hover_target
                self.grabbing_x = xworld - self.hover_target.pos[1]
                self.grabbing_y = yworld - self.hover_target.pos[2]
            else
                self.pan_click_x = xworld
                self.pan_click_y = yworld
            end
        elseif button == 3 then
            if self.hover_target then
                self.grabbing_target = self.hover_target
                self.grabbing_x = xworld
                self.grabbing_y = yworld
                self.grabbing_w = self.hover_target.sz[1]
                self.grabbing_h = self.hover_target.sz[2]
            end
        end
    else
        --
        -- Mouse up
        --
        if self.which_button == 1 then
            self:finalizeLastBox(true)
        elseif self.which_button == 2 then
            self:finalizeLastBox(false)
        elseif self.which_button == 3 then
            self:finalizeLastBox(false)
        end
      
        self.which_button = nil
        self.grabbing_target = nil
    end
end

function level_editor:mousewheel(type, which, dx, dy, direction)
    local lm = self.lastmouse
    local x,y = self:windowToNDC(lm[1], lm[2])
    local xworld = x - self.pan_offset.x
    local yworld = y - self.pan_offset.y
    self:incrementTextureSpriteIdx(xworld,yworld,dx)
end


return level_editor
