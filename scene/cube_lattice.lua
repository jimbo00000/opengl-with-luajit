--[[ cube_lattice.lua

]]
cube_lattice = {}

cube_lattice.__index = cube_lattice

function cube_lattice.new(...)
    local self = setmetatable({}, cube_lattice)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end
    return self
end

local CCTurtleLibrary = require("scene.ccturtle")

-- CC script compatibility
local myself = nil
turtleFuncs = {
    forward = function(x)
        myself:moveCursor('I')
        coroutine.yield()
        return true
    end,
    up = function(x)
        myself:moveCursor('U')
        coroutine.yield()
        return true
    end,
    turnRight = function(x)
        myself:moveCursor('L')
        coroutine.yield()
        return true
    end,
    turnLeft = function(x)
        myself:moveCursor('J')
        coroutine.yield()
        return true
    end,
    getItemCount = function(x)
        coroutine.yield()
        return 1
    end,
    place = function(x)
        myself:moveCursor('H')
        coroutine.yield()
        return true
    end,
    placeUp = function(x)
        myself:moveCursor('Y')
        coroutine.yield()
        return true
    end,
    placeDown = function(x)
        myself:moveCursor('N')
        coroutine.yield()
        return true
    end,
    getFuelLevel = function(x)
        coroutine.yield()
        return 0
    end,
    select = function(x)
        coroutine.yield()
        return true
    end,
    refuel = function(x)
        coroutine.yield()
        return true
    end,
    -- TODO: other API entry points
}
local turtle_mt = {
  __index = function (t,k)
    --print("__index",t,k)
    if turtleFuncs[k] then return turtleFuncs[k] end
    return function(x)
        print('No impl for turtle.',k)
        coroutine.yield()
        return 0
    end
  end,
}
turtle = setmetatable({}, turtle_mt)

osFuncs = {
    pullEvent = function(x)
        print('pullEvent not yet implemented')
        coroutine.yield()
    end,
}
local os_mt = {
  __index = function (t,k)
    --print("__index",t,k)
    if osFuncs[k] then return osFuncs[k] end
    return function(x)
        print('No impl for os.',k)
        coroutine.yield()
        return 0
    end
  end,
}
os = setmetatable({}, os_mt)

-- Block type utils
function getInfoForBlockType(type)
    local names = {
        [0] = "rough_stone",
        [1] = "smooth_stone",
        [2] = "dirt",
        [3] = "grass",
        [4] = "oak_planks",
        [5] = "stone_slabs",
        [6] = "_",
        [7] = "bricks",
        [8] = "tnt",
        [9] = "tnt_top",
        [10] = "tnt_bottom",
        [16] = "cobblestone",
        [17] = "bedrock",
        [18] = "sand",
        [19] = "gravel",
        [20] = "wood",
        [32] = "gold_ore",
        [33] = "iron_ore",
        [34] = "coal_ore",
        [35] = "bookshelf",
        [36] = "mossy_cobblestone",
        [37] = "obsidian",
    }
    local metadatas = {
        [0] = "metadata",
    }
    local name = names[type]
    if not name then return false,nil end
    return true, {name=name, metadata=metadatas[type]}
end

function printBlockInfo(b)
    local success, data = getInfoForBlockType(b)
    if success then
        print("    name=",data.name)
        print("    metadata=",data.metadata)
    end
end

-- Cursor utils
cube_cursor = {}
cube_cursor.__index = cube_cursor
function cube_cursor.new(...)
    local self = setmetatable({}, cube_cursor)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end
    return self
end

function cube_cursor:init(pos, dir)
    self.loc = pos
    self.dir = dir
end

local fwdOffsets = {
    [0] = {0,-1},
    [1] = {1,0},
    [2] = {0,1},
    [3] = {-1,0},
}
function cube_cursor:getFrontXYZ()
    local cdir = self.dir
    local cloc = self.loc
    local off = fwdOffsets[cdir]
    if not off then return nil end
    local x,y,z = cloc[1], cloc[2], cloc[3]
    return x+off[1], y, z+off[2]
end

function cube_cursor:getBackXYZ()
    local cdir = self.dir
    local cloc = self.loc
    local off = fwdOffsets[cdir]
    if not off then return nil end
    local cloc = cloc
    local x,y,z = cloc[1], cloc[2], cloc[3]
    return x-off[1], y, z-off[2]
end

function cube_cursor:getUpXYZ()
    local cloc = self.loc
    local x,y,z = cloc[1], cloc[2], cloc[3]
    return x, y+1, z
end

function cube_cursor:getDownXYZ()
    local cloc = self.loc
    local x,y,z = cloc[1], cloc[2], cloc[3]
    return x, y-1, z
end


-- XYZ -> index utils
function cube_lattice:calcOccupancyIdx(x,y,z)
    local d = self.dim
    return x + y*d + z*d*d
end

function cube_lattice:setOccupancy(x,y,z,type)
    if x < 1 or x > self.dim then return end
    if y < 1 or y > self.dim then return end
    if z < 1 or z > self.dim then return end

    local idx = self:calcOccupancyIdx(x,y,z)
    if not idx then return end
    self.occupancy[idx] = type
end

function cube_lattice:getOccupancy(x,y,z)
    local idx = self:calcOccupancyIdx(x,y,z)
    return self.occupancy[idx]
end




function cube_lattice:drawFront(cursor, type)
    local x,y,z = cursor:getFrontXYZ()
    self:setOccupancy(x,y,z, type)
    self:updateInstanceData()
end

function cube_lattice:drawDown(cursor, type)
    local x,y,z = cursor:getDownXYZ()
    self:setOccupancy(x,y,z, type)
    self:updateInstanceData()
end

function cube_lattice:drawUp(cursor, type)
    local x,y,z = cursor:getUpXYZ()
    self:setOccupancy(x,y,z, type)
    self:updateInstanceData()
end

function cube_lattice:moveCursorTo(cursor, x,y,z)
    local pos = {x,y,z}
    for i=1,3 do
        pos[i] = math.max(pos[i], 1)
        pos[i] = math.min(pos[i], self.dim)
    end
    x,y,z = pos[1],pos[2],pos[3]

    local b = self:getOccupancy(x,y,z)
    if b then
        print("Collision: ",x,y,z,b)
    else
        cursor.loc[1] = x
        cursor.loc[2] = y
        cursor.loc[3] = z

        -- bounds check
        for i=1,3 do
            cursor.loc[i] = math.max(cursor.loc[i], 1)
            cursor.loc[i] = math.min(cursor.loc[i], self.dim)
        end
    end
end

function cube_lattice:moveCursor(cursor, dir)
    local function left(cursor)
        cursor.dir = cursor.dir - 1
        if cursor.dir < 0 then
            cursor.dir = 3
        end
    end

    local function right(cursor)
        cursor.dir = cursor.dir + 1
        if cursor.dir > 3 then
            cursor.dir = 0
        end
    end

    local function fwd(cursor)
        self:moveCursorTo(cursor, cursor:getFrontXYZ())
    end

    local function back(cursor)
        self:moveCursorTo(cursor, cursor:getBackXYZ())
    end

    local function up(cursor)
        self:moveCursorTo(cursor, cursor:getUpXYZ())
    end

    local function down(cursor)
        self:moveCursorTo(cursor, cursor:getDownXYZ())
    end

    local func_table = {
        ['I'] = function(x) fwd(cursor) end,
        ['J'] = function(x) left(cursor) end,
        ['K'] = function(x) back(cursor) end,
        ['L'] = function(x) right(cursor) end,
        ['U'] = function(x) up(cursor) end,
        ['O'] = function(x) down(cursor) end,
        ['H'] = function(x) self:drawFront(cursor, self.curBlockType) end,
        ['Y'] = function(x) self:drawUp(cursor, self.curBlockType) end,
        ['N'] = function(x) self:drawDown(cursor, self.curBlockType) end,
        ['G'] = function(x) self:drawFront(cursor, nil) end,
        ['T'] = function(x) self:drawUp(cursor, nil) end,
        ['B'] = function(x) self:drawDown(cursor, nil) end,
    }

    local f = func_table[dir]
    if f then f() end

    self:updateInstanceData()
end

function cube_lattice:printBlockInfoFwd(cursor)
    local x,y,z = cursor:getFrontXYZ()
    local b = self:getOccupancy(x,y,z)
    print("Fwd Block info:")
    printBlockInfo(b)
end

function cube_lattice:printBlockInfoUp(cursor)
    local x,y,z = cursor:getUpXYZ()
    local b = self:getOccupancy(x,y,z)
    print("Up Block info:")
    printBlockInfo(b)
end

function cube_lattice:printBlockInfoDown(cursor)
    local x,y,z = cursor:getDownXYZ()
    local b = self:getOccupancy(x,y,z)
    print("Down Block info:")
    printBlockInfo(b)
end

-----------------------------------------------------------
-- Scene
-----------------------------------------------------------

function cube_lattice:init()
    -- Object-internal state: hold a list of VBOs for deletion on exitGL
    self.vbos = {}
    self.vao = 0
    self.progLattice = 0
    self.texID = 0
    self.dataDir = nil

    self.dim = 16
    self.occupancy = {}
    self.cubeInsts = 0

    local c = self.dim / 2
    self.cursors = {
        cube_cursor.new({c,c,c}, 0),
        --cube_cursor.new({c+2,c,c}, 0),
        --cube_cursor.new({c+4,c,c}, 0),
    }
    self.curBlockType = 0

    myself = self
    self.running = false
    self.lastRunTime = 0
    self.runInterval = 1

    self.turtleblock = CCTurtleLibrary.new()
end

local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv   = ffi.typeof('GLint[?]')
local glUintv  = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local lattice_vert = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec4 vPosition;
in vec4 vColor;
in vec4 instancePosition;

out vec3 vfColor;
out vec2 vfTexOffset;

uniform mat4 mvmtx;
uniform mat4 prmtx;
uniform float uCubeScale;

void main()
{
    vfColor = vColor.xyz;
    int blockType = int(instancePosition.w);
    float texSc = (1./16.);
    vfTexOffset.x = texSc * float((blockType & 0x0f)     ); // low 8 bits
    vfTexOffset.y = texSc * float((blockType & 0xf0) >> 4); // hi 8 bits

    float offset = .5 * (1.- uCubeScale);
    vec3 pos = uCubeScale * vPosition.xyz + vec3(offset);
    pos += instancePosition.xyz;
    gl_Position = prmtx * mvmtx * vec4(pos, vPosition.w);
}
]]

local lattice_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
in vec2 vfTexOffset;
out vec4 fragColor;

uniform sampler2D sTex;
uniform vec3 uColorScale;

void main()
{
    float texSc = (1./16.);
    vec4 tc = texture(sTex, vfColor.xy*texSc + vfTexOffset);
    fragColor = vec4(uColorScale * tc.xyz, 1.);
}
]]

function cube_lattice:setDataDirectory(dir)
    self.dataDir = dir
    self.turtleblock:setDataDirectory(dir)
end

function cube_lattice:loadtextures()
    local texfilename = "terrainBase256.data"
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end
    local w,h = 256,256
    local inp = io.open(texfilename, "rb")
    local data = nil
    if inp then
        data = inp:read("*all")
        assert(inp:close())
    end

    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    print(dtxId[0])
    self.texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
                  w, h, 0,
                  GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)

end

function cube_lattice:init_cube_attributes()
    local v = { -- Vertex positions, 6 faces of a cube
        0,0,0,
        1,0,0,
        1,1,0,
        0,1,0,

        0,0,1,
        1,0,1,
        1,1,1,
        0,1,1,

        0,0,0,
        1,0,0,
        1,0,1,
        0,0,1,

        0,1,0,
        1,1,0,
        1,1,1,
        0,1,1,

        0,0,0,
        0,1,0,
        0,1,1,
        0,0,1,

        1,0,0,
        1,1,0,
        1,1,1,
        1,0,1,
    }
    local verts = glFloatv(#v,v)

    local c = { -- Texture coordinates, all 6 faces are identical
        0,0, 1,0, 1,1, 0,1,
        0,0, 1,0, 1,1, 0,1,
        0,0, 1,0, 1,1, 0,1,
        0,0, 1,0, 1,1, 0,1,
        0,0, 1,0, 1,1, 0,1,
        0,0, 1,0, 1,1, 0,1,
    }
    local cols = glFloatv(#c,c)

    local vpos_loc = gl.glGetAttribLocation(self.progLattice, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(self.progLattice, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(cols), cols, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)

    local q = { -- Vertex indices for drawing triangles of faces
        0,3,2, 1,0,2,
        4,5,6, 7,4,6,
        8,11,10, 9,8,10,
        12,15,14, 13,12,14,
        16,19,18, 17,16,18,
        20,23,22, 21,20,22
    }
    local quads = glUintv(#q,q)
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(quads), quads, GL.GL_STATIC_DRAW)
    table.insert(self.vbos, qvbo)
end

function cube_lattice:init_instance_attribs()
    local d = self.dim
    local sz = d*d*d

    local insp_loc = gl.glGetAttribLocation(self.progLattice, "instancePosition")
    local ipvbo = glIntv(0)
    gl.glGenBuffers(1, ipvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, ipvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(insp_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    self.vbos.inst_positions = ipvbo
    table.insert(self.vbos, ipvbo)
    gl.glVertexAttribDivisor(insp_loc, 1)
    gl.glEnableVertexAttribArray(insp_loc)
end

function cube_lattice:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.progLattice = sf.make_shader_from_source({
        vsrc = lattice_vert,
        fsrc = lattice_frag,
        })

    self:init_cube_attributes()
    self:init_instance_attribs()
    self:loadtextures()
    gl.glBindVertexArray(0)

    self.turtleblock:initGL()
end

function cube_lattice:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    gl.glDeleteProgram(self.progLattice)
    local texdel = ffi.new("GLuint[1]", self.texID)
    gl.glDeleteTextures(1,texdel)

    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)

    self.turtleblock:exitGL()
end

function cube_lattice:updateInstanceData()
    local insts = 0
    local s = self.dim
    local v = {}
    for k=1,s do
        for j=1,s do
            for i=1,s do
                local idx = self:calcOccupancyIdx(i,j,k)
                local t = self.occupancy[idx]
                if t ~= nil then
                    table.insert(v, i-1)
                    table.insert(v, j-1)
                    table.insert(v, k-1)
                    table.insert(v, t)
                    insts = insts + 1
                end
            end
        end
    end
    local verts = glFloatv(#v,v)
    -- upload instances
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos.inst_positions[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_COPY)

    self.cubeInsts = insts
end

function cube_lattice:renderEye(model, view, proj)
    local umv_loc = gl.glGetUniformLocation(self.progLattice, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(self.progLattice, "prmtx")
    local insp_loc = gl.glGetAttribLocation(self.progLattice, "instancePosition")
    local ucol_loc = gl.glGetUniformLocation(self.progLattice, "uColorScale")
    local usc_loc = gl.glGetUniformLocation(self.progLattice, "uCubeScale")

    gl.glUseProgram(self.progLattice)
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    local stex_loc = gl.glGetUniformLocation(self.progLattice, "sTex")
    gl.glUniform1i(stex_loc, 0)

    local m = {}
    mm.make_identity_matrix(m)
    mm.pre_multiply(m, model)
    local c = -self.dim / 2
    mm.glh_translate(m, c,c,c)
    mm.pre_multiply(m, view)
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, m))


    gl.glBindVertexArray(self.vao)
    
    -- draw lattice
    gl.glUniform3f(ucol_loc, 1,1,1)
    gl.glUniform1f(usc_loc, .9)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos.inst_positions[0])
    gl.glVertexAttribPointer(insp_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glDrawElementsInstanced(GL.GL_TRIANGLES, 6*3*2, GL.GL_UNSIGNED_INT, nil, self.cubeInsts)

    gl.glBindVertexArray(0)

    gl.glUseProgram(0)

    local function drawCursor(cursor)
        local mx = {}
        for i=1,16 do mx[i] = model[i] end
        local sz = 1
        local c = -self.dim / 2
        mm.glh_translate(mx, c,c,c)
        mm.glh_translate(mx,
            cursor.loc[1],
            cursor.loc[2],
            cursor.loc[3])
        mm.glh_translate(mx, -.5,-.5,-.5)
        mm.glh_rotate(mx, -90*cursor.dir, 0,1,0)
        mm.glh_translate(mx, -.5,-.5,-.5)
        mm.glh_scale(mx, sz,sz,sz)

        self.turtleblock:renderEye(mx, view, proj)
    end

    for k,v in pairs(self.cursors) do
        drawCursor(v)
    end
end

function cube_lattice:timestep(absTime, dt)
    if self.running and (absTime-self.lastRunTime > self.runInterval) then

        local scriptName = 'scene/bridge.lua'
        if not self.co then
            local script = assert(loadfile(scriptName))
            self.co = coroutine.create(script)
        end

        local ret = coroutine.resume(self.co)
        self.lastRunTime = absTime

        if coroutine.status(self.co) == 'dead' then
            self.co = nil
            self.running = false
            print("Script "..scriptName.." complete.")
        end
    end
end

function cube_lattice:keypressed(key)
    local function setCurBlockType(c)
        self.curBlockType = c
    end
    
    local func_table = {
        ['1'] = function(x) setCurBlockType(0) end,
        ['2'] = function(x) setCurBlockType(1) end,
        ['3'] = function(x) setCurBlockType(2) end,
        ['4'] = function(x) setCurBlockType(16) end,
        ['5'] = function(x) setCurBlockType(17) end,
        ['6'] = function(x) setCurBlockType(32) end,
        ['7'] = function(x) setCurBlockType(33) end,
        ['8'] = function(x) setCurBlockType(34) end,
        ['9'] = function(x) setCurBlockType(35) end,
        ['0'] = function(x) setCurBlockType(36) end,
        ['\\'] = function(x) self.running = not self.running end,
        ['-'] = function(x) self.runInterval = self.runInterval * 2 end,
        ['='] = function(x) self.runInterval = self.runInterval * .5 end,
        ['P'] = function(x) self:printBlockInfoUp(self.cursors[1]) end,
        [';'] = function(x) self:printBlockInfoFwd(self.cursors[1]) end,
        ['/'] = function(x) self:printBlockInfoDown(self.cursors[1]) end,
        ['.'] = function(x) self:dumpFillCommands() end,
    }

    if key >= 32 and key < 123 then
        local f = func_table[string.char(key)]
        if f then f()
        else
            for k,v in pairs(self.cursors) do
                self:moveCursor(v, string.char(key))
            end
        end
    end
end

function cube_lattice:dumpFillCommands()
    local fname = 'fillcommands.txt'
    print('dumping to file '..fname)
    local file = io.open(fname, 'w')

    local insts = 0
    local s = self.dim
    local v = {}
    for k=1,s do
        for j=1,s do
            for i=1,s do
                local idx = self:calcOccupancyIdx(i,j,k)
                local t = self.occupancy[idx]
                if t ~= nil then
                    -- https://minecraft.gamepedia.com/Commands/fill
                    --[[
fill <from: x y z> <to: x y z> <tileName: Block> [tileData: int] [outline|hollow|destroy|keep]
                    ]]
                    local yoff = 65
                    local x = i
                    local y = j + yoff
                    local z = k
                    local from = ''..x..' '..y..' '..z
                    local to = ''..x..' '..y..' '..z
                    local tname = 'quartz_block'
                    local tdata = 0
                    local cmds = ''--replace'
                    local str = 'fill '..from..' '..to..' '..tname..' '..tdata..' '..cmds
                    file:write(str..'\n')
                end
            end
        end
    end
    file:close()
end

return cube_lattice
