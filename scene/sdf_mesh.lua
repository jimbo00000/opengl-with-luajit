--[[ sdf_mesh.lua

    Spherical sdf_mesh with randomly generated surface craters.

    Uses gridcube_scene.lua to create a fine subdivided mesh topologically
    homeomorphic to a sphere(starts life as a cube), then applies a compute
    shader on vertex positions to cumulatively deform the mesh.
]]
sdf_mesh = {}
sdf_mesh.__index = sdf_mesh

function sdf_mesh.new(...)
    local self = setmetatable({}, sdf_mesh)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local GridLib = require("scene.gridcube")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")
local fbf = require("util.fbofunctions")
local ffi = require("ffi")

function sdf_mesh:init(sdfuncidx, subdivs, numlights)
    sdfuncidx = sdfuncidx or 1
    self.sdfunc = sdfuncidx
    numlights = numlights or 1
    self.progs = {}
    self.grid = GridLib.new(subdivs or 64/4, numlights)
    self.texID = 0
    self.animate_tex = false
end

function sdf_mesh:setDataDirectory(dir)
    self.dataDir = dir
end

function sdf_mesh:loadtextures()
    local texfilename = "stone_128x128.raw"
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end
    local w,h = 128,128
    local inp = io.open(texfilename, "rb")
    local data = nil
    if inp then
        data = inp:read("*all")
        assert(inp:close())
    end

    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    self.texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB,
                  w, h, 0,
                  GL.GL_RGB, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)

    self.grid.texID = self.texID
end

function sdf_mesh:init_rtt()
    local v = "fs"
    local InnerScene = require("scene."..v).new()
    local texFBO = require("scene.textured_fbo").new(InnerScene)
    self.InnerScene = InnerScene
    self.texFBO = texFBO
    self.texFBO:initGL()

    self.grid.texID = fbo.tex

    function readAll(file)
        local f = io.open(file, "rb")
        local content = f:read("*all")
        f:close()
        return content
    end
    --self.Frag:buildShader(hsv)

    local fn = self.dataDir..'/shaders/fs/@party'
    InnerScene:buildShader(readAll(fn))

    local id = {}
    mm.make_identity_matrix(id)
    self:rtt_pass(id, id, id)
end

function sdf_mesh:rtt_pass(model, view, proj)
    -- Save viewport dimensions
    -- TODO: this is very inefficient; store them manually, restore at caller
    local vp = ffi.new("GLuint[4]", 0,0,0,0)
    gl.glGetIntegerv(GL.GL_VIEWPORT, vp)
    -- Save bound FBO
    local boundfbo = ffi.new("int[1]")
    gl.glGetIntegerv(GL.GL_FRAMEBUFFER_BINDING, boundfbo)

    local fbo = self.texFBO.fbo
    local p = {}
    local aspect = fbo.w / fbo.h
    mm.glh_perspective_rh(p, 60 * math.pi/180, aspect, .004, 500)

    -- Render pre-pass to fbo
    fbf.bind_fbo(fbo)
    gl.glViewport(0,0, fbo.w, fbo.h)

    local g = .5
    local c = {g,g,g,0}--self.clearColor
    gl.glClearColor(c[1],c[2],c[3],c[4])
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    local model2 = {}
    mm.make_identity_matrix(model2)
    local view2 = {}
    for i=1,16 do view2[i] = view[i] end
    mm.glh_translate(view2, 0,0,-15)

    if self.InnerScene then
        if self.InnerScene.renderEye then
            self.InnerScene:renderEye(model2, view2, p)
        else
            self.InnerScene:render_for_one_eye(view2, p)
        end
    end

    fbf.unbind_fbo(fbo)
    -- TODO: restore VAO?


    -- Restore viewport
    gl.glViewport(vp[0],vp[1],vp[2],vp[3])
    -- Restore FBO binding
    if boundfbo[0] ~= 0 then
        gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, boundfbo[0])
    end
end

--[[
    Perturb all vertices with a function.
]]
local perturbverts_comp_src = [[
#version 310 es
#line 22
layout(local_size_x=128) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };


vec3 vectorField(in vec3 pos);

float smin( float a, float b, float k )
{
    float h = clamp( 0.5+0.5*(b-a)/k, 0.0, 1.0 );
    return mix( b, a, h ) - k*h*(1.0-h);
}

float sdSphere( vec3 p, float s )
{
    return length(p)-s;
}

float udRoundBox( vec3 p, vec3 b, float r )
{
  return length(max(abs(p)-b,0.0))-r;
}

float sdTorus( vec3 p, vec2 t )
{
  vec2 q = vec2(length(p.xz)-t.x,p.y);
  return length(q)-t.y;
}


float sdShip( vec3 p )
{
    vec3 center = vec3(.5);
    vec3 pc = p - center;

    float d = 
    min(
        udRoundBox(pc-vec3(0.,-.1,0.), vec3(.15,.1,.5), .1),
        sdTorus(pc.yzx, 2.*vec2(.15,.05))
        );

    d = min(d,sdTorus(pc.yzx-vec3(0.,.2,0.), 2.*vec2(.15,.05)));
    d = min(d,sdTorus(pc.yzx-vec3(0.,.4,0.), 2.*vec2(.15,.05)));

    d = max(
        -udRoundBox(pc-vec3(0.,-.5,0.), vec3(.35,.2,.5), .1),
        d);

    return d;
}

float sdShip2( vec3 p )
{
    vec3 center = vec3(.5);
    vec3 pc = p - center;

    float d = sdSphere(pc-vec3(.0,0,-.5), .25);
    d = min(d, 
        udRoundBox(pc-vec3(0.,0.,.0), .5*vec3(.1,.05,.95), .1));
    d = min(d, 
        udRoundBox(pc-vec3(0.,0.,.5), .5*vec3(.3,.13,.3), .1));
    return d;
}

float sdShip3( vec3 p )
{
    vec3 center = vec3(.5);
    vec3 pc = p - center;

    float d = udRoundBox(pc-vec3(0.,0.,.0), .5*vec3(.1,.05,.95), .1);

    d = min(d, sdTorus(pc-vec3(0.25,0.,.0), vec2(.1)));
    d = min(d, sdTorus(pc-vec3(-0.25,0.,.0), vec2(.1)));
    return d;
}

// Try raymarching towards origin
vec3 raymarchPoint(vec3 p)
{
    float strength = .25;
    vec3 center = vec3(.5);

    vec3 ro = p;
    vec3 rd = (center - p);
    vec3 step = strength * rd;
    for (int i=0; i<100; ++i)
    {
        float d = DISTANCE_FUNC(ro.zyx);
        ro += d * step;
    }
    return ro;
}

vec3 displacePoint(vec3 p)
{
    vec3 center = vec3(.5);
    float radius = 1.;
    return center + radius*normalize(p-center);
}

void main()
{
    uint index = gl_GlobalInvocationID.x;
    vec4 p = positions[index];

    //p.xyz = displacePoint(p.xyz);
    p.xyz = raymarchPoint(p.xyz);

    positions[index] = p;
}
]]

function sdf_mesh:perturbVertexPositions()
    local vvbo = self.grid:get_vertices_vbo()
    local num_verts = self.grid:get_num_verts()
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vvbo[0])
    local prog = self.progs.perturbverts
    gl.glUseProgram(prog)

    gl.glDispatchCompute(num_verts/128+1, 1, 1)
    gl.glUseProgram(0)
end

function sdf_mesh:initGL()
    local sdfuncs = {
        "sdShip",
        "sdShip2",
        "sdShip3",
    }
    self.progs.perturbverts= sf.make_shader_from_source({
        compsrc = string.gsub(perturbverts_comp_src, "DISTANCE_FUNC", sdfuncs[self.sdfunc])
        })

    self.grid:initGL()
    self:perturbVertexPositions()
    self.grid:recalc_normals()

    self:loadtextures()
    self:init_rtt()
end

function sdf_mesh:exitGL()
    self.grid:exitGL()
    for _,v in pairs(self.progs) do
        gl.glDeleteProgram(v)
    end
    if self.texFBO then
        self.texFBO:exitGL()
    end
end

function sdf_mesh:renderEye(model, view, proj)
    if self.animate_tex and self.texFBO then
        self:rtt_pass(model, view, proj)
    end
    self.grid:renderEye(model, view, proj)
    self.model_matrix = {}
    for i=1,16 do self.model_matrix[i] = model[i] end
end

function sdf_mesh:timestep(absTime, dt)
    self.grid:timestep(absTime, dt)
    if self.animate_tex and
        self.InnerScene and
        self.InnerScene.timestep then
        self.InnerScene:timestep(absTime, dt)
    end


    -- Animate lighting data
    local r = 12.5
    local speed = 2
    local time = absTime
    local x,y,z = 0,20,20 --r*math.sin(speed*time),20,r*math.cos(speed*time)
    local dir = {-x,-y,-z}
    mm.normalize(dir)
    --print(dir[1], dir[2], dir[3])
    self.lightData = {
        -- Light 1
        {
            x,y,z,0,
            dir[1], dir[2], dir[3],0,
            1,1,1,0,
            0,.9,0,0, -- NOTE: std140 layout adds padding to 16-byte boundaries
        },
    }

    self.grid.lightData = self.lightData
end

function sdf_mesh:keypressed(key, scancode, action, mods)
    if key == string.byte('1') then
        self:perturbVertexPositions()
        self.grid:recalc_normals()
    end
end

return sdf_mesh
