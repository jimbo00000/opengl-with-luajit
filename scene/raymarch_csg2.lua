--[[ raymarch_csg2.lua

]]
local raymarch_csg2 = {}
raymarch_csg2.__index = raymarch_csg2

local RaymarchLib = require("scene.raymarch_csg")
local ffi = require("ffi")

function raymarch_csg2.new(...)
    local self = setmetatable({}, raymarch_csg2)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end


local rm_frag_body = [[

uniform float repeatSpacing;// = 0.63;

// distance function
float dist_sphere( vec3 pos, float r ) {
    return length( pos ) - r;
}

float dist_box( vec3 pos, vec3 size ) {
    return length( max( abs( pos ) - size, 0.0 ) );
}

float dist_box( vec3 v, vec3 size, float r ) {
    return length( max( abs( v ) - size, 0.0 ) ) - r;
}

// https://www.iquilezles.org/www/articles/distfunctions/distfunctions.htm
// get distance in the world
float dist_field( vec3 posxy ) {
    float repLimit = 7.;

    // Multiplying this in reverse order(vec*mtx) is equivalent to transposing the matrix.
    vec3 posx = (vec4(posxy, 1.)* mmtx).xyz;

    // repeat
    vec3 c = vec3(repeatSpacing);
    vec3 p = posx;
    float l = repLimit;
    //vec3 q = mod(p+0.5*c,c)-0.5*c; // infinite repeat
    vec3 q = p-c*clamp(round(p/c),-l-1.,l); // finite, low limit adjusted
    vec3 pos = q;

    float sphRadius = .125;

    float d0 = dist_sphere( pos, sphRadius );

    float d1 = dist_box( pos, vec3( .175*sphRadius ) );
    // union     : min( d0,  d1 )
    // intersect : max( d0,  d1 )
    // subtract  : max( d1, -d0 )
    return min( d1, d0 );
}
]]


function raymarch_csg2:init()
    self.rm = RaymarchLib.new(rm_frag_body)
    self.lightspeed = 1
    self.repeatSpacing = 10/16
end

function raymarch_csg2:initGL()
    self.rm:initGL()
end

function raymarch_csg2:exitGL()
    self.rm:exitGL()
end

--TODO: Somehow set this one additional uniform without copying the child
-- module's display function.
function raymarch_csg2:renderEye1(model, view, proj)
    local function uni(name) return gl.glGetUniformLocation(self.rm.shader.prog, name) end
    local x = uni("repeatSpacing")
    gl.glUniform1f(uni("repeatSpacing"), self.repeatSpacing)

    self.rm:renderEye(model, view, proj)
end

function raymarch_csg2:renderEye(model, view, proj)
    local function set_variables(prog)
        local glFloatv = ffi.typeof('GLfloat[?]')
        local function uni(name) return gl.glGetUniformLocation(prog, name) end
        gl.glUniformMatrix4fv(uni("mmtx"), 1, GL.GL_FALSE, glFloatv(16, model))
        gl.glUniformMatrix4fv(uni("vmtx"), 1, GL.GL_FALSE, glFloatv(16, view))
        gl.glUniformMatrix4fv(uni("prmtx"), 1, GL.GL_FALSE, glFloatv(16, proj))

        gl.glUniform1f(uni("repeatSpacing"), self.repeatSpacing)

        -- Flatten array of light values
        do
            local ldata = {}
            
            for _,light in pairs(self.rm.lightData) do
                for i=1,#light do
                    table.insert(ldata, light[i])
                end
            end
            local lightArr = glFloatv(#ldata, ldata)

            gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, self.rm.uniformbuf[0])
            gl.glBufferSubData(GL.GL_UNIFORM_BUFFER, 0, ffi.sizeof(lightArr), lightArr)
            gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, 0)

            local lightBlock = gl.glGetUniformBlockIndex(prog, "Light")
            gl.glUniformBlockBinding(prog, lightBlock, 0)
            gl.glBindBufferBase(GL.GL_UNIFORM_BUFFER, 0, self.rm.uniformbuf[0])
        end
    end

    self.rm.shader:render(view, proj, set_variables)
end

function raymarch_csg2:timestep(absTime, dt)
    self.rm:timestep(absTime, dt)
    self.rm.lightspeed = self.lightspeed
end

return raymarch_csg2
