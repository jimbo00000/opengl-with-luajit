--[[ moon.lua

    Spherical moon with randomly generated surface craters.

    Uses gridcube_scene.lua to create a fine subdivided mesh topologically
    homeomorphic to a sphere(starts life as a cube), then applies a compute
    shader on vertex positions to cumulatively deform the mesh.
]]
moon = {}
moon.__index = moon

function moon.new(...)
    local self = setmetatable({}, moon)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local GridLib = require("scene.gridcube")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")
local fbf = require("util.fbofunctions")
local ffi = require("ffi")

function moon:init(subdivs, numlights)
    self.progs = {}
    self.grid = GridLib.new(subdivs or 256, numlights)
    self.texID = 0
    self.animate_tex = false
end

function moon:setDataDirectory(dir)
    self.dataDir = dir
end

function moon:loadtextures()
    local texfilename = "stone_128x128.raw"
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end
    local w,h = 128,128
    local inp = io.open(texfilename, "rb")
    local data = nil
    if inp then
        data = inp:read("*all")
        assert(inp:close())
    end

    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    self.texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB,
                  w, h, 0,
                  GL.GL_RGB, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)

    self.grid.texID = self.texID
end

function moon:init_rtt()
    local v = "fs"
    local InnerScene = require("scene."..v).new()
    local texFBO = require("scene.textured_fbo").new(InnerScene)
    self.InnerScene = InnerScene
    self.texFBO = texFBO
    self.texFBO:initGL()

    self.grid.texID = fbo.tex

    function readAll(file)
        local f = io.open(file, "rb")
        local content = f:read("*all")
        f:close()
        return content
    end
    --self.Frag:buildShader(hsv)

    local fn = self.dataDir..'/shaders/fs/@party'
    InnerScene:buildShader(readAll(fn))

    local id = {}
    mm.make_identity_matrix(id)
    self:rtt_pass(id, id, id)
end

function moon:rtt_pass(model, view, proj)
    -- Save viewport dimensions
    -- TODO: this is very inefficient; store them manually, restore at caller
    local vp = ffi.new("GLuint[4]", 0,0,0,0)
    gl.glGetIntegerv(GL.GL_VIEWPORT, vp)
    -- Save bound FBO
    local boundfbo = ffi.new("int[1]")
    gl.glGetIntegerv(GL.GL_FRAMEBUFFER_BINDING, boundfbo)


    local fbo = self.texFBO.fbo
    local p = {}
    local aspect = fbo.w / fbo.h
    mm.glh_perspective_rh(p, 60 * math.pi/180, aspect, .004, 500)

    -- Render pre-pass to fbo
    fbf.bind_fbo(fbo)
    gl.glViewport(0,0, fbo.w, fbo.h)

    local g = .5
    local c = {g,g,g,0}--self.clearColor
    gl.glClearColor(c[1],c[2],c[3],c[4])
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    local model2 = {}
    mm.make_identity_matrix(model2)
    local view2 = {}
    for i=1,16 do view2[i] = view[i] end
    mm.glh_translate(view2, 0,0,-15)

    if self.InnerScene.renderEye then
        self.InnerScene:renderEye(model2, view2, p)
    else
        self.InnerScene:render_for_one_eye(view2, p)
    end


    fbf.unbind_fbo(fbo)
    -- TODO: restore VAO?


    -- Restore viewport
    gl.glViewport(vp[0],vp[1],vp[2],vp[3])
    -- Restore FBO binding
    if boundfbo[0] ~= 0 then
        gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, boundfbo[0])
    end
end

--[[
    Perturb all vertices with a function.
]]
local perturbverts_comp_src = [[
#version 310 es
#line 22
layout(local_size_x=128) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };

vec3 displacePoint(vec3 p)
{
    vec3 center = vec3(.5);
    float radius = 1.;
    return center + radius*normalize(p-center);
}

void main()
{
    uint index = gl_GlobalInvocationID.x;
    vec4 p = positions[index];

    p.xyz = displacePoint(p.xyz);

    positions[index] = p;
}
]]

function moon:perturbVertexPositions()
    local vvbo = self.grid:get_vertices_vbo()
    local num_verts = self.grid:get_num_verts()
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vvbo[0])
    local prog = self.progs.perturbverts
    gl.glUseProgram(prog)

    gl.glDispatchCompute(num_verts/128+1, 1, 1)
    gl.glUseProgram(0)
end


local pushout_sphere_src = [[
#version 310 es
#line 58
layout(local_size_x=128) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };

uniform vec3 crater_center;
uniform float radius;
uniform float strength;

vec3 sphere_center = vec3(.5);

float profile_function(float r)
{
    float x = pow(r,2.);
    return mix(.75*smoothstep(0.,1.,x), -smoothstep(0.,1.,x), smoothstep(0.3,.6,x));
    return .1* -sin(-2.*3.14159*3./4.*x);
}

vec3 displacePoint(vec3 p)
{
    float ltc = length(p - crater_center);
    if (ltc < radius)
    {
        float r_1 = 1. - (ltc/radius);
        vec3 disp = strength * profile_function(r_1) * normalize(p-sphere_center);
        return p + disp;
    }
    return p;
}

void main()
{
    uint index = gl_GlobalInvocationID.x;
    vec4 p = positions[index];

    p.xyz = displacePoint(p.xyz);

    positions[index] = p;
}
]]

function moon:initGL()
    self.progs.perturbverts= sf.make_shader_from_source({
        compsrc = perturbverts_comp_src,
        })

    self.progs.make_crater = sf.make_shader_from_source({
        compsrc = pushout_sphere_src,
        })

    self.grid:initGL()
    self:perturbVertexPositions()
    self.grid:recalc_normals()

    --self:loadtextures()
    self:init_rtt()
end

function moon:exitGL()
    self.grid:exitGL()
    for _,v in pairs(self.progs) do
        gl.glDeleteProgram(v)
    end
    if self.texFBO then
        self.texFBO:exitGL()
    end
end

function moon:renderEye(model, view, proj)
    if self.animate_tex and self.texFBO then
        self:rtt_pass(model, view, proj)
    end
    self.grid:renderEye(model, view, proj)
    self.model_matrix = {}
    for i=1,16 do self.model_matrix[i] = model[i] end
end

function moon:timestep(absTime, dt)
    self.grid:timestep(absTime, dt)
    if self.animate_tex and
        self.InnerScene and
        self.InnerScene.timestep then
        self.InnerScene:timestep(absTime, dt)
    end
end

function random_variance(center, variance)
    -- Return a random value centered on center, with += variance
    return center - variance + variance*2*math.random()
end

function moon:create_random_crater()
    function random_spherical()
        -- Monte Carlo random on sphere
        local cc = {}
        repeat
            local c,v = 0,1
            cc = {random_variance(c,v), random_variance(c,v), random_variance(c,v), }
        until mm.length(cc) < 1
        --mm.normalize(cc)
        for i=1,3 do cc[i] = 2.*cc[i] + 1. end -- Map [-.5,.5] to [0,2]? or [0,1]?
        return cc
    end

    local cc = random_spherical()
    local r = random_variance(.2, .1)
    local strength = .1
    self:create_crater(cc, r, strength)
end

function moon:create_crater(cc, r, strength)
    -- Set uniforms for sphere locations - program must be active.
    local prog = self.progs.make_crater
    gl.glUseProgram(prog)
    local ucc_loc = gl.glGetUniformLocation(prog, "crater_center")
    gl.glUniform3f(ucc_loc, cc[1], cc[2], cc[3])
    local ucr_loc = gl.glGetUniformLocation(prog, "radius")
    gl.glUniform1f(ucr_loc, r)

    local vvbo = self.grid:get_vertices_vbo()
    local num_verts = self.grid:get_num_verts()

    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vvbo[0])
    local ust_loc = gl.glGetUniformLocation(prog, "strength")
    gl.glUniform1f(ust_loc, strength)

    gl.glDispatchCompute(num_verts/128, 1, 1)
    gl.glUseProgram(0)
end

function moon:add_craters(n)
    for i=0,n do self:create_random_crater() end
    gl.glMemoryBarrier(GL.GL_SHADER_STORAGE_BARRIER_BIT)
    self.grid:recalc_normals()
    gl.glMemoryBarrier(GL.GL_SHADER_STORAGE_BARRIER_BIT)
end

function moon:keypressed(ch)
    self:add_craters(30)
end

function moon:onSingleTouch(pointerid, action, x, y)
    self:add_craters(30)
end

function moon:calculateRayHitPoint(ro, rd)
    function raySphereIntersect(ro, rd, s0, sr)
        -- https://gist.github.com/wwwtyro/beecc31d65d1004f5a9d
        -- Thanks tyro!
        local a = mm.dot(rd,rd)
        local s0r0 = {}
        for i=1,3 do s0r0[i] = ro[i] - s0[i] end
        local b = 2 * mm.dot(rd, s0r0)
        local c = mm.dot(s0r0, s0r0) - (sr*sr)
        if b*b - 4*a*c < 0 then return -1 end
        return (-b - math.sqrt(b*b - 4*a*c)) / (2*a)
    end

    -- NOTE: sphere is at center at 1/2, radius=1
    local center = {0,0,0,1}
    if self.model_matrix then
        local m = {}
        for i=1,16 do m[i] = self.model_matrix[i] end
        center = mm.transform(center, m)
    end
    local radius = 1
    local t = raySphereIntersect(ro, rd, center, radius)
    if t == -1 then return nil end
    local hitpt = {}
    for i=1,3 do hitpt[i] = ro[i] + t*rd[i] end

    if self.model_matrix then
        local m = {}
        for i=1,16 do m[i] = self.model_matrix[i] end
        mm.affine_inverse(m)
        hitpt[4] = 1
        hitpt = mm.transform(hitpt, m)
    end

    return hitpt
end

function moon:createCraterAtHitPoint(hitpt, strength)
    strength = strength or 1
    -- Shader expects a center of 1/2
    local hitpt5 = {}
    for i=1,3 do hitpt5[i] = hitpt[i] + .5 end

    self:create_crater(hitpt5, .25*strength, .05*strength)
    self.grid:recalc_normals()
end

function moon:onray(ro, rd)
    self.hitpt = nil
    local hitpt = self:calculateRayHitPoint(ro, rd)
    if not hitpt then return false end

    self.hitpt = hitpt
    --self:createCraterAtHitPoint(hitpt)
    return true
end

return moon
