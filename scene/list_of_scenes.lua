--[[ list_of_scenes.lua

	Holds a list of scenes, displaying them all at once.
]]

list_of_scenes = {}

list_of_scenes.__index = list_of_scenes

function list_of_scenes.new(...)
    local self = setmetatable({}, list_of_scenes)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local scene_names = {
    --"colorcube",
    "ThiSpawns_sunsets",
    "particles", -- breaks nbody
    "meteorite",
    "objscene",
    "eyetest",
    "clockface",
    "cone",
    "fbos_in_space",
    "hybrid_scene",
    "simple_game",
    "volume_render",
    "instanced_tris",
    --"stringedit_scene", -- stringedit_scene.lua:39: attempt to index local 'source' (a nil value
    "boids",
    "diaspora04",
    "exploding_scene",
    "colorcube",
    "flock01",
    "linegraph",
    "multipass_example",
    "poisson_jacobi",
    "poisson_multigrid",
    "wavy_sheet", -- breaks nbody
    "droid",
    "cubemap",
    "font_test",
    "molecule",
    "nbody07", -- particle layer
}

function list_of_scenes:init()
	self.scenes = {}
end

function list_of_scenes:setDataDirectory(dir)
    self.dataDir = dir
end

function list_of_scenes:initGL()
end

function list_of_scenes:exitGL()
    for _,v in pairs(self.scenes) do
        local sc = v[2]
        sc:exitGL()
    end
end

function list_of_scenes:addScene(name)
    if not name then return end

    local scenedir = "scene"
    local fullname = scenedir.."."..name
    local SceneLibrary = require(fullname)
    Scene = SceneLibrary.new()
    if Scene then
        if Scene.setDataDirectory then Scene:setDataDirectory(self.dataDir) end
        Scene:initGL()
    end

    table.insert(self.scenes, {name, Scene})
end

function list_of_scenes:findSceneIdxByName(name)
    -- Check through the list
    for k,v in pairs(self.scenes) do
        if v[1] == name then
            return k
        end
    end
    return nil
end

function list_of_scenes:removeScene(name)
    local idx = self:findSceneIdxByName(name)
    if idx then
        local scene = table.remove(self.scenes, idx)
        scene[2]:exitGL()
        return idx
    end
    return nil
end

function list_of_scenes:toggleScene(name)
    if not name then return false end

    if not self:removeScene(name) then
        self:addScene(name)
    end

    return true
end

function list_of_scenes:renderEye(model, view, proj)
    -- Scenes are drawn in the order they were added.
    for _,v in pairs(self.scenes) do
        local s = v[2]
        if s.renderEye then
            s:renderEye(model, view, proj)
        else
            s:render_for_one_eye(view, proj)
        end
    end
end

function list_of_scenes:timestep(absTime, dt)
    for _,v in pairs(self.scenes) do
        local sc = v[2]
        if sc.timestep then
            sc:timestep(absTime, dt)
        end
    end
end

function list_of_scenes:keypressed(key, scancode, action, mods)
    -- Ctrl-shift-key to switch to scene at index
    if mods == 3 and
        key >= string.byte(',') and
        key <= string.byte('Z') then
        local keys =
            '1234567890'..
            'QWERTYUIOP'..
            'ASDFGHJKL;'..
            'ZXCVBNM,./'
        for i=1,#keys do
            if string.char(key) == keys:sub(i,i) then
                local result = self:toggleScene(scene_names[i])
                if result then return end
            end
        end
    end

    if not result then
        for _,v in pairs(self.scenes) do
            local sc = v[2]
            if sc.keypressed then
                sc:keypressed(key, scancode, action, mods)
            end
        end
    end
end

function list_of_scenes:onray(ro, rd)
    for _,v in pairs(self.scenes) do
        local sc = v[2]
        if sc.onray then
            sc:onray(ro, rd)
        end
    end
end

return list_of_scenes
