--[[ instanced_tris.lua

    A simple example of instancing:
    Draw n copies of a triangle at positions stored in a VBO.
    Initialize instance positions in a compute shader.
]]
instanced_tris = {}

instanced_tris.__index = instanced_tris

function instanced_tris.new(...)
    local self = setmetatable({}, instanced_tris)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function instanced_tris:init()
    self.vao = 0
    self.vbos = {}
    self.progs = {}
    self.numInstances = 16
    self.time = 0
end

--local openGL = require("opengl")
local ffi = require("ffi")
local sf = require("util.shaderfunctions")

local glIntv = ffi.typeof('GLint[?]')
local glUintv = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local basic_vert = [[
#version 310 es

in vec4 vPosition;
in vec4 vColor;
in vec4 vInstPosition;

uniform mat4 mvmtx;
uniform mat4 prmtx;

out vec3 vfColor;

void main()
{
    vfColor = vColor.xyz;
    vec3 pos = vPosition.xyz;
    pos += vInstPosition.xyz;
    gl_Position = prmtx * mvmtx * vec4(pos, 1.);
}
]]

local basic_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfColor, 1.0);
}
]]

function instanced_tris:init_tri_attributes()
    local verts = glFloatv(3*3, {
        0,0,0,
        1,0,0,
        0,1,0,
        })

    local prog = self.progs.basic
    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(prog, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)
end

function instanced_tris:init_instance_attributes()
    local sz = 4 * self.numInstances * ffi.sizeof('GLfloat') -- xyzw
    local insp_loc = gl.glGetAttribLocation(self.progs.basic, "vInstPosition")
    local ipvbo = glIntv(0)
    gl.glGenBuffers(1, ipvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, ipvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(insp_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    self.vbos.inst_positions = ipvbo
    gl.glVertexAttribDivisor(insp_loc, 1)
    gl.glEnableVertexAttribArray(insp_loc)
end

--[[
    Scatter instance positions randomly in instance array.
]]
local randomize_instance_positions_comp_src = [[
#version 430

layout(local_size_x=128) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
uniform int numInstances;
uniform float time;

float hash( float n ) { return fract(sin(n)*43758.5453); }

void main()
{
    uint index = gl_GlobalInvocationID.x;
    if (index >= numInstances)
        return;
    float fi = float(index+1) / float(numInstances);
    fi += time;

    positions[index] = vec4(hash(fi), hash(fi*3.), hash(fi*17.), 1.);
}
]]
function instanced_tris:assign_instance_positions()
    if self.progs.scatter_instances == nil then
        self.progs.scatter_instances = sf.make_shader_from_source({
            compsrc = randomize_instance_positions_comp_src,
            })
    end

    local pvbo = self.vbos.inst_positions
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, pvbo[0])
    local prog = self.progs.scatter_instances
    local function uni(name) return gl.glGetUniformLocation(prog, name) end
    gl.glUseProgram(prog)
    gl.glUniform1i(uni("numInstances"), self.numInstances)
    gl.glUniform1f(uni("time"), self.time)
    gl.glDispatchCompute(self.numInstances/128+1, 1, 1)
    gl.glUseProgram(0)
end

function instanced_tris:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.progs.basic = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    self:init_tri_attributes()
    self:init_instance_attributes()
    self:assign_instance_positions()
    gl.glBindVertexArray(0)
end

function instanced_tris:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    for _,v in pairs(self.progs) do
        gl.glDeleteProgram(v)
    end
    self.progs = {}

    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)
end

function instanced_tris:render_for_one_eye(view, proj)
    local prog = self.progs.basic
    gl.glUseProgram(prog)
    local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(prog, "prmtx")
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))
    gl.glBindVertexArray(self.vao)
    gl.glDrawArraysInstanced(GL.GL_TRIANGLES, 0, 3, self.numInstances)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end

function instanced_tris:timestep(absTime, dt)
    self.time = absTime
end

function instanced_tris:keypressed(key)
    self:assign_instance_positions()
end

function instanced_tris:onSingleTouch(pointerid, action, x, y)
    --print("instanced_tris.onSingleTouch",pointerid, action, x, y)
    self:assign_instance_positions()
end

return instanced_tris
