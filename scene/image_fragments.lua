--[[ image_fragments.lua

    A simple example of instancing:
    Draw n copies of a triangle at positions stored in a VBO.
    Initialize instance positions in a compute shader.
]]
image_fragments = {}

image_fragments.__index = image_fragments

function image_fragments.new(...)
    local self = setmetatable({}, image_fragments)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function image_fragments:init()
    self.vao = 0
    self.vbos = {}
    self.progs = {}
    self.grid_x = 16*4
    self.grid_y = 8*4
    self.numInstances = self.grid_x * self.grid_y
    self.time = 0
    self.startTime = nil
end

--local openGL = require("opengl")
local ffi = require("ffi")
local sf = require("util.shaderfunctions")

local glIntv = ffi.typeof('GLint[?]')
local glUintv = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local basic_vert = [[
#version 310 es

in vec4 vPosition;
in vec4 vColor;
in vec4 vInstPosition;

uniform mat4 mvmtx;
uniform mat4 prmtx;

out vec3 vfColor;

void main()
{
    vfColor = vColor.xyz;
    vec3 pos = vPosition.xyz;
    pos += vInstPosition.xyz;
    gl_Position = prmtx * mvmtx * vec4(pos, 1.);
}
]]

local basic_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfColor, 1.0);
}
]]

function image_fragments:init_tri_attributes()
    local verts = glFloatv(3*4, {
        0,0,0,
        1,0,0,
        1,1,0,
        0,1,0,
        })

    local prog = self.progs.basic
    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(prog, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)
end

function image_fragments:init_instance_attributes()
    local sz = 4 * self.numInstances * ffi.sizeof('GLfloat') -- xyzw
    local insp_loc = gl.glGetAttribLocation(self.progs.basic, "vInstPosition")
    local ipvbo = glIntv(0)
    gl.glGenBuffers(1, ipvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, ipvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(insp_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    self.vbos.inst_positions = ipvbo
    gl.glVertexAttribDivisor(insp_loc, 1)
    gl.glEnableVertexAttribArray(insp_loc)
end

--[[
    Scatter instance positions randomly in instance array.
]]
local randomize_instance_positions_comp_src = [[
#version 430

layout(local_size_x=128) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
uniform int numInstances;
uniform int gridX;
uniform float aspect;
uniform float time;

void main()
{
    uint index = gl_GlobalInvocationID.x;
    if (index >= numInstances)
        return;
    float fi = float(index+1) / float(numInstances);

    uint px = index % gridX;
    uint py = index / gridX;
    vec2 gp = vec2(px/float(gridX), py/float(gridX));
    gp -= vec2(.5, .5/aspect); // center
    vec3 gridPos = vec3(float(gridX)* gp, 0.);

    vec3 linePos = vec3(0., 0., 2. * (fi + time));

    float duration = 4.;
    vec3 pos = mix(gridPos, linePos,
        //clamp(time/duration,0.,1.)
        1.-smoothstep(0.,.2,(1.5+-fi-time/duration))
        );

    positions[index] = vec4(pos, 1.);
}
]]
function image_fragments:assign_instance_positions()
    if not self.startTime then self.startTime = self.time end

    if self.progs.scatter_instances == nil then
        self.progs.scatter_instances = sf.make_shader_from_source({
            compsrc = randomize_instance_positions_comp_src,
            })
    end

    local pvbo = self.vbos.inst_positions
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, pvbo[0])
    local prog = self.progs.scatter_instances
    local function uni(name) return gl.glGetUniformLocation(prog, name) end
    gl.glUseProgram(prog)
    gl.glUniform1i(uni("numInstances"), self.numInstances)
    gl.glUniform1i(uni("gridX"), self.grid_x)
    gl.glUniform1f(uni("aspect"), self.grid_x / self.grid_y)
    gl.glUniform1f(uni("time"), self.time - self.startTime)
    gl.glDispatchCompute(self.numInstances/128+1, 1, 1)
    gl.glUseProgram(0)
end

function image_fragments:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.progs.basic = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    self:init_tri_attributes()
    self:init_instance_attributes()
    self:assign_instance_positions()
    gl.glBindVertexArray(0)

    self.startTime = nil
end

function image_fragments:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    for _,v in pairs(self.progs) do
        gl.glDeleteProgram(v)
    end
    self.progs = {}

    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)
end

function image_fragments:render_for_one_eye(view, proj)
    local prog = self.progs.basic
    gl.glUseProgram(prog)
    local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(prog, "prmtx")
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))
    gl.glBindVertexArray(self.vao)
    gl.glDrawArraysInstanced(GL.GL_TRIANGLE_FAN, 0, 4, self.numInstances)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end

function image_fragments:timestep(absTime, dt)
    self.time = absTime
    if not self.startTime then self.startTime = absTime end
    self:assign_instance_positions()
end

function image_fragments:keypressed(key)
    self.startTime = self.time
end

function image_fragments:onSingleTouch(pointerid, action, x, y)
    --print("image_fragments.onSingleTouch",pointerid, action, x, y)
end

return image_fragments
