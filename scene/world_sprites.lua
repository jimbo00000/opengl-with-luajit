local world_sprites = {
    _DESCRIPTION = [[
        Part of the mouse_rects scene - draws a batch of textured quads
        to display the matching contents of the physics world.
    ]],
    _URL = 'https://bitbucket.org/jimbo00000/opengl-with-luajit/src/master/'
}
world_sprites.__index = world_sprites

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'

function world_sprites.new(...)
    local self = setmetatable({}, world_sprites)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function world_sprites:setDataDirectory(dir)
    self.dataDir = dir
end

function world_sprites:init()
    self.vbos = {}
    self.prog = 0
    self.n_quads = 0
end


local vs = [[
#version 100

uniform vec4 uOffsetSz;
uniform mat4 uViewMatrix;

attribute vec2 aPosition;
attribute vec2 aUV;
varying vec3 vfC;

void main()
{
    vfC = vec3(aUV,0.);
    
    vec2 pt = aPosition;
    vec2 worldPt = uOffsetSz.zw*pt + uOffsetSz.xy;

    gl_Position = uViewMatrix * vec4(worldPt,0.,1.);
}
]]

local fs = [[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
uniform sampler2D uSampler;

void main()
{
    vec3 col = vfC;

    vec2 tc = vfC.xy;
    gl_FragColor = texture2D(uSampler, tc);
}
]]

function world_sprites:allocateVBO()
    local v = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,v)
    table.insert(self.vbos, v)

    local u = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,u)
    table.insert(self.vbos, u)
end

function world_sprites:bindVBO()
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(self.attributes.aPosition, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(self.attributes.aPosition)

    local uvbo = self.vbos[2]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, uvbo[0])
    gl.glVertexAttribPointer(self.attributes.aUV, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(self.attributes.aUV)
end

function world_sprites:unbindVBO()
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)

    gl.glDisableVertexAttribArray(self.attributes.aPosition)
    gl.glDisableVertexAttribArray(self.attributes.aUV)
end



-- Result will be made into a typed array with ffi and passed to GL.
local function makeTableOfTargetPositions(targets)
    local all = {}
    for _,t in pairs(targets) do
        local x = t.pos[1]
        local y = t.pos[2]
        local a = t.sz[1]
        local b = t.sz[2]

        local verts = {
            0+x, 0+y,
            a+x, 0+y,
            a+x, b+y,
            0+x, 0+y,
            a+x, b+y,
            0+x, b+y,
        }
        for j=1,#verts do
            table.insert(all, verts[j])
        end
    end
    return all
end

--- TODO: take count param
local function makeTableOfTargetUVs(targets)
    local all = {}
    for _,t in pairs(targets) do
        local x = 0
        local y = 0
        local a = 1/4
        local b = 1/4

        -- Sprite sheet logic
        if t.which then
            local which0 = t.which - 1 -- [0-15]
            require 'bit'
            local xi = bit.band(which0, 3)
            local yi = bit.rshift(bit.band(which0, 12), 2)
            x = x + xi/4
            y = y + yi/4
        end

        local verts = {
            0+x, 0+y,
            a+x, 0+y,
            a+x, b+y,
            0+x, 0+y,
            a+x, b+y,
            0+x, b+y,
        }
        for j=1,#verts do
            table.insert(all, verts[j])
        end
    end
    return all
end

-- Bake instanced data into an array of triangle coords.
function world_sprites:populateVBOFromTargetData(targets)
    self.n_quads = #targets
    
    local vboPos = self.vbos[1]
    local all = makeTableOfTargetPositions(targets)
    local av = ffi.typeof('GLfloat[?]')(#all, all)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboPos[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(av), av, GL.GL_STATIC_DRAW)

    local vboUV = self.vbos[2]
    local alluv = makeTableOfTargetUVs(targets) --TODO: #targets
    local arruv = ffi.typeof('GLfloat[?]')(#alluv, alluv)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboUV[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(arruv), arruv, GL.GL_STATIC_DRAW)
end


function world_sprites:loadTextures(texfilename, w, h)
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end
    local inp = assert(io.open(texfilename, "rb"))
    local data = inp:read("*all")
    assert(inp:close())
    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    self.texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
                  w, h, 0,
                  GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end


function world_sprites:initGL()
    self.prog, err = sf.make_shader_from_source({vsrc=vs,fsrc=fs})
    if self.prog == 0 then
        local dbg = debug.getinfo(1)
        local tag = dbg.short_src..':'..dbg.currentline
        err = err:sub(1,#err-1) -- strip newline
        err = tag..'\n    '..err
        print(err)
        return err
    end

    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})
    local unis = {
        'uOffsetSz',
        'uViewMatrix',
        'uSampler',
    }
    self.uniforms = {}
    local function uni(var) return gl.glGetUniformLocation(self.prog, var) end
    for k,v in pairs(unis) do
        self.uniforms[v] = uni(v)
    end

    local attrs = {
        'aPosition',
        'aUV',
    }
    self.attributes = {}
    local function attr(var) return gl.glGetAttribLocation(self.prog, var) end
    for k,v in pairs(attrs) do
        self.attributes[v] = attr(v)
    end

    self:allocateVBO()

    self.texID = 0
    --self:loadTextures('frog1.data', 32, 32)
    self:loadTextures('master-tileset-4x4-256x256.rgba', 256, 256)
    --self:loadTextures('floortiles-4x4-192x192.rgba', 192, 192)
end

function world_sprites:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
end

function world_sprites:render(viewMatrix)
    gl.glUseProgram(self.prog)
    gl.glUniform4f(self.uniforms.uOffsetSz, 0,0,1,1)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glUniform1i(self.uniforms.uSampler, 0)
    local glFloatv = ffi.typeof('GLfloat[?]')
    gl.glUniformMatrix4fv(self.uniforms.uViewMatrix, 1, GL.GL_FALSE, glFloatv(16,viewMatrix))

    self:bindVBO()
    gl.glDrawArrays(GL.GL_TRIANGLES, 0, 6*self.n_quads)
    self:unbindVBO()
end

return world_sprites
