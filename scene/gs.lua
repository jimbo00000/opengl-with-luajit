-- geomshader.lua
-- Uses a geometry shader to amplify geometry, taking points in
-- and writing out triangles.
-- NOTE This will not work on OpenGL ES.

local ffi = require("ffi")
local sf = require("util.shaderfunctions2")
local glFloatv = ffi.typeof('GLfloat[?]')

geomshader = {}
geomshader.__index = geomshader

function geomshader.new(...)
    local self = setmetatable({}, geomshader)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local basic_vert = [[
#version 310 es

in vec4 vPosition; ///@todo Can we remove this
void main()
{
    gl_Position = vec4(0.,0.,0.,1.);
}
]]

local geom_header = [[
#version 330

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

#line 1
]]

local oddity = [[
layout (points) in;
layout (triangle_strip, max_vertices = 3) out;

uniform mat4 modelMtx;
uniform mat4 viewMtx;
uniform mat4 projMtx;

uniform int numParticles;
uniform float time;

out vec3 gfNormal;

// rotation matrix
mat3 rotmat(vec3 v, float angle)
{
    float c = cos(angle);
    float s = sin(angle);
    
    return mat3(c + (1.0 - c) * v.x * v.x, (1.0 - c) * v.x * v.y - s * v.z, (1.0 - c) * v.x * v.z + s * v.y,
        (1.0 - c) * v.x * v.y + s * v.z, c + (1.0 - c) * v.y * v.y, (1.0 - c) * v.y * v.z - s * v.x,
        (1.0 - c) * v.x * v.z - s * v.y, (1.0 - c) * v.y * v.z + s * v.x, c + (1.0 - c) * v.z * v.z
        );
}

#define PI 3.14159265359

void main()
{
    float t = float(gl_PrimitiveIDIn) / float(numParticles);
    vec4 pos = vec4(t*sin(time), 0., 0., 1.);
    pos = projMtx * viewMtx * modelMtx * mat4(rotmat(vec3(0.,0.,1.), PI * t)) * pos;

    vec3 r = rotmat(vec3(0.,0.,1.), PI * t) * vec3(1.,0.,0.);
    vec3 u = rotmat(vec3(0.,0.,1.), PI * t) * vec3(0.,1.,0.);
    vec4 rt = vec4(r,1.);
    vec4 up = vec4(u,1.);

    float sz = .03;
    gl_Position = pos + sz * -up;
    gfNormal = vec3(1.,0.,0.);
    EmitVertex();

    gl_Position = pos + sz * up;
    gfNormal = vec3(0.,1.,0.);
    EmitVertex();
    
    gl_Position = pos + sz * rt;
    gfNormal = vec3(0.,0.,1.);
    EmitVertex();

    EndPrimitive();
}
]]

local geom_body = [[
layout (points) in;
layout (triangle_strip, max_vertices = 16) out;

uniform mat4 modelMtx;
uniform mat4 viewMtx;
uniform mat4 projMtx;

uniform int numParticles;
uniform float time;

out vec3 gfPos;
out vec3 gfNormal;

void main()
{
    vec3 pts[9] = vec3[](
        vec3(0.),
        vec3(1.,0.,0.),
        vec3(0.,2.,0.),
        vec3(0.),
        vec3(1.,0.,0.),
        vec3(0.,0.,1.),
        vec3(0.),
        vec3(0.,2.,0.),
        vec3(0.,0.,1.)
        );

    vec3 cols[3] = vec3[](
        vec3(0.,0.,1.),
        vec3(0.,1.,0.),
        vec3(1.,0.,0.)
        );

    mat4 m = projMtx * viewMtx * modelMtx;
    mat3 normMtx = mat3(modelMtx);

    for (int h=0; h<3; ++h)
    {
        for (int i=0; i<3; ++i)
        {
            gfPos = pts[i+h*3];
            gl_Position = m * vec4(gfPos, 1.);
            gfNormal = abs(normMtx * cols[h]);
            EmitVertex();
        }
        EndPrimitive();
    }
}
]]

local basic_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 gfNormal;
out vec4 fragColor;

void main()
{
    fragColor = vec4(gfNormal, 1.0); // Normals as color
}
]]

local lighting_frag = [[
#version 310 es

#ifdef GL_ES
precision highp float;
precision mediump int;
#endif

in vec3 gfPos;
in vec3 gfNormal;
out vec4 fragColor;

uniform float time;

vec3 lightPos = vec3(0., 20., 10.);
float shininess = 125.;

vec3 directionalLight(vec3 normal)
{
    vec3 lightDir = normalize(vec3(cos(time), 1., sin(time)));
    float intensity = max(dot(normal,lightDir), 0.);

#if 1
    vec4 spec = vec4(0.);
    if (intensity > 0.)
    {
        vec3 h = normalize(lightDir);
        float intSpec = max(dot(h,normal), 0.);
        spec = vec4(pow(intSpec, shininess));
    }
    return vec3(intensity + spec);
#endif    
}
#line 307

vec3 pointLight(vec3 pos, vec3 normal)
{
    float r = 12.5;
    float speed = 2.;
    vec3 lightPos = vec3(r*sin(speed*time), 5.*abs(sin(7.*time)), r*cos(speed*time));
    //vec3 lightPos = vec3(0., 4.*abs(sin(time)), 0.);

    vec3 lightDir = normalize(vec3(lightPos - pos));
    vec3 eye = vec3(0.);

    float spec = 0.;

    float intensity = max(dot(normal,lightDir), 0.);
    if (intensity > 0.)
    {
        vec3 h = normalize(lightDir + eye);
        float intSpec = max(dot(h,normal), 0.);
        spec = pow(intSpec, shininess);
    }
 
    return vec3(intensity + spec);
}

vec3 spotLight(vec3 pos, vec3 normal)
{
    float r = 12.5;
    float speed = 2.;
    vec3 lightPos = vec3(r*sin(speed*time), 15., r*cos(speed*time));
    //vec3 lightPos = vec3(0., 4.*abs(sin(time)), 0.);

    vec3 spotDir = normalize(vec3(0.,-1.,0.));
    vec3 lightDir = normalize(vec3(lightPos - pos));
    vec3 eye = vec3(0.);

    float spec = 0.;

    float intensity = 0.;
    float spotCutoff = .75;
    if (dot(-spotDir,lightDir) > spotCutoff)
    {
        intensity = max(dot(normal,lightDir), 0.);
        if (intensity > 0.)
        {
            vec3 h = normalize(lightDir + eye);
            float intSpec = max(dot(h,normal), 0.);
            spec = pow(intSpec, shininess);
        }
    } 
    return vec3(intensity + spec);
}

void main()
{
    vec3 normal = normalize(gfNormal);
    //fragColor = vec4(directionalLight(normal), 1.);
    //fragColor = vec4(pointLight(gfPos, normal), 1.);
    vec3 L = spotLight(gfPos, normal);
    fragColor = vec4(L, length(L));
    fragColor = vec4(normal, 1.);
}
]]

function geomshader:init()
    self.vao = 0
    self.vbos = {}
    self.prog = 0
    self.npts = 128
    self.time = 0

    self.src = geom_body
    self.descr = "Geometry amplifier"
    self.shadertype = "geometry"
    self.dynamic_scale = false
end

function geomshader:stateString()
    return self.npts..' points'
end

function geomshader:buildShader(src)
    -- Do we need to bind vao to compile a program?
    --gl.glBindVertexArray(self.vao)
    gl.glDeleteProgram(self.prog)

    self.prog,err = sf.make_shader_from_source({
        vsrc = basic_vert,
        gsrc = geom_header..src,
        fsrc = lighting_frag,
        })
    --gl.glBindVertexArray(0)
    return err
end

function geomshader:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self:buildShader(geom_body)
    gl.glBindVertexArray(0)
end

function geomshader:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)
end

function geomshader:renderEye(model, view, proj)
    if self.prog == 0 then return end
    local um_loc = gl.glGetUniformLocation(self.prog, "modelMtx")
    local uv_loc = gl.glGetUniformLocation(self.prog, "viewMtx")
    local up_loc = gl.glGetUniformLocation(self.prog, "projMtx")
    local un_loc = gl.glGetUniformLocation(self.prog, "numParticles")
    local ut_loc = gl.glGetUniformLocation(self.prog, "time")
    gl.glUseProgram(self.prog)

    gl.glUniformMatrix4fv(um_loc, 1, GL.GL_FALSE, glFloatv(16, model))
    gl.glUniformMatrix4fv(uv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(up_loc, 1, GL.GL_FALSE, glFloatv(16, proj))
    gl.glUniform1i(un_loc, self.npts)
    gl.glUniform1f(ut_loc, self.time)

    gl.glEnable(GL.GL_BLEND)
    gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)
    gl.glBindVertexArray(self.vao)
    gl.glDrawArrays(GL.GL_POINTS, 0, self.npts)
    gl.glBindVertexArray(0)
    gl.glDisable(GL.GL_BLEND)

    gl.glUseProgram(0)
end

function geomshader:render_for_one_eye(view, proj)
    if self.prog == 0 then return end
    gl.glUseProgram(self.prog)

    local umv_loc = gl.glGetUniformLocation(self.prog, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(self.prog, "prmtx")
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))
    local un_loc = gl.glGetUniformLocation(self.prog, "numParticles")
    gl.glUniform1i(un_loc, self.npts)
    local ut_loc = gl.glGetUniformLocation(self.prog, "time")
    gl.glUniform1f(ut_loc, self.time)

    gl.glBindVertexArray(self.vao)
    gl.glDrawArrays(GL.GL_POINTS, 0, self.npts)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end

function geomshader:timestep(absTime, dt)
    self.time = absTime
    -- Active scaling
    if self.dynamic_scale then
        local a = 40000
        self.npts = math.floor(100 + a*(1+math.sin(absTime))) --math.pow(10,exp)
    end
end

function geomshader:scaleBufferSize(k)
    self.npts = self.npts * k
end

function geomshader:charkeypressed(key)
    if key == '-' then
        self:scaleBufferSize(.5)
    elseif key == '=' then
        self:scaleBufferSize(2)
    end
end

function geomshader:keypressed(key, scancode, action, mods)
    local func_table = {
        [298] = function (x) self:scaleBufferSize(.5*.5) end, -- F9
        [299] = function (x) self:scaleBufferSize(.5) end, -- F10
        [300] = function (x) self:scaleBufferSize(2) end, -- F11
        [301] = function (x) self:scaleBufferSize(2*2) end, -- F12
    }
    local f = func_table[key]
    if f then f() handled = true end
end

function geomshader:onSingleTouch(pointerid, action, x, y)
    --print("geomshader.onSingleTouch",pointerid, action, x, y)
end

return geomshader
