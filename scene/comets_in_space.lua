--[[ comets_in_space.lua

    
]]
comets_in_space = {}

comets_in_space.__index = comets_in_space

function comets_in_space.new(...)
    local self = setmetatable({}, comets_in_space)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local CometLib = require("scene.comet")
local StarfieldLib = require("scene.starfield")
local mm = require("util.matrixmath")

function comets_in_space:init(subdivs)
    self.wordlist = {
        {"@#%$^!",{0,0,0}},
        {"7647axx",{1,2,0}},
        {"abd6472",{2,4,0}},
    }
    self.comets = {}
    for k,v in pairs(self.wordlist) do
        local com = CometLib.new(subdivs)
        com.core.word = v[1]
        com.startpos = {} for i=1,3 do com.startpos[i] = v[2][i] end
        com.pos = v[2]
        table.insert(self.comets, com)
    end
    self.stars = StarfieldLib.new()
    self.stars.xvel = .5
    self.reset_flag = 0
    self.comet_vel = -3
end
function comets_in_space:setDataDirectory(dir)
    for k,v in pairs(self.comets) do
        v:setDataDirectory(dir)
    end
end

function comets_in_space:initGL()
    for k,v in pairs(self.comets) do
        v:initGL()
    end
    self.stars:initGL()
end

function comets_in_space:exitGL()
    for k,v in pairs(self.comets) do
        v:exitGL()
    end
    self.stars:exitGL()
end

function comets_in_space:renderEye(model, view, proj)
    self:handleResetStateFlag()

    for k,v in pairs(self.comets) do
        local m = {}
        for i=1,16 do m[i] = model[i] end
        mm.glh_translate(m, 10,-5,-10)
        mm.glh_translate(m, v.pos[1], v.pos[2], v.pos[3])
        local sz = .3
        --mm.glh_scale(m, sz,sz,sz)
        v:renderEye(m, view, proj)
    end
    self.stars:renderEye(model, view, proj)
end

function comets_in_space:timestep(absTime, dt)
    for k,v in pairs(self.comets) do
        local vel = {self.comet_vel,0,0}
        for i=1,3 do
            v.pos[i] = v.pos[i] + dt*vel[i]
        end
        v:timestep(absTime, dt)
    end
    self.stars:timestep(absTime, dt)
end

function comets_in_space:keypressed(key, scancode, action, mods)
    local factor = 1.2
    if key == string.byte(';') then
        self.comet_vel = self.comet_vel / factor
        print('comet_vel',self.comet_vel)
    elseif key == string.byte("'") then
        self.comet_vel = self.comet_vel * factor
        print('comet_vel',self.comet_vel)
    elseif key == 259 then -- bksp
        self.reset_flag = self.reset_flag + 1
    end
    self.stars:keypressed(key, scancode, action, mods)
end

function comets_in_space:handleResetStateFlag()
    -- Check against last value
    -- For resetting state from sync tracker
    if self.last_reset_flag ~= self.reset_flag then
        for k,v in pairs(self.comets) do
            for i=1,3 do
                v.pos[i] = v.startpos[i]
            end
        end
    end
    self.last_reset_flag = self.reset_flag
end

return comets_in_space
