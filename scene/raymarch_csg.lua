--[[ raymarch_csg.lua

    Takes some basic raymarching code from shadertoy and applies the incoming
    view and projection matrices so raymarched objects can coexist with
    rasterized ones by sharing a color and depth buffer.
]]
local NUMBER_OF_LIGHTS = 6

local raymarch_csg = {}
raymarch_csg.__index = raymarch_csg

function raymarch_csg.new(...)
    local self = setmetatable({}, raymarch_csg)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local ffi = require("ffi")
local FullScreenShaderLib = require("util.fullscreen_shader")

local rm_frag_header = [[
uniform mat4 mmtx;
uniform mat4 vmtx;
uniform mat4 prmtx;

struct PointLight {
    vec4 pos;
    vec4 diffCol;

    float intensity;
    float constant;
    float linear;
    float exponent;
};

const int numberOfLights = NUMLIGHTS;
layout (std140) uniform Light
{
    PointLight lights[numberOfLights];
} Lgt;

const int max_iterations = 255;
const float stop_threshold = 0.001;
const float grad_step = 0.1;
const float clip_far = 40.0;

const float PI = 3.14159265359;
const float DEG_TO_RAD = PI / 180.0;
]]

local rm_frag_body = [[
// distance function
float dist_sphere( vec3 pos, float r ) {
    return length( pos ) - r;
}

float dist_box( vec3 pos, vec3 size ) {
    return length( max( abs( pos ) - size, 0.0 ) );
}

float dist_box( vec3 v, vec3 size, float r ) {
    return length( max( abs( v ) - size, 0.0 ) ) - r;
}

// get distance in the world
float dist_field( vec3 posx ) {

    // Repeat
    float modDist = 2.0;
    //posx = vec3(mod(posx-vec3(3.), modDist) - vec3(1.));

    // Multiplying this in reverse order(vec*mtx) is equivalent to transposing the matrix.
    vec3 pos = (vec4(posx, 1.)* mmtx).xyz;


    // object 0 : sphere
    float d0 = dist_sphere( pos, 0.5*1.35 );

    // object 1 : cube
    float d1 = dist_box( pos, vec3( 0.5*1.0 ) );

    // union     : min( d0,  d1 )
    // intersect : max( d0,  d1 )
    // subtract  : max( d1, -d0 )
    return max( d1, -d0 );
    //return min( d2, max( d1, -d0 ) );
}
]]

local rm_frag_footer = [[
// phong shading
vec3 shading1( vec3 v, vec3 n, vec3 eye ) {
    // ...add lights here...
    float shininess = 16.0;
    vec3 final = vec3( 0.0 );

    vec3 ev = normalize( v - eye );
    vec3 ref_ev = reflect( ev, n );

    // light 0
    {
        vec3 light_pos   = vec3( 20.0, 20.0, 20.0 );
        vec3 light_color = vec3( 1.0, 0.7, 0.7 );

        vec3 vl = normalize( light_pos - v );

        float diffuse  = max( 0.0, dot( vl, n ) );
        float specular = max( 0.0, dot( vl, ref_ev ) );
        specular = pow( specular, shininess );

        final += light_color * ( diffuse + specular );
    }

    // light 1
    {
        vec3 light_pos   = vec3( -20.0, -20.0, -20.0 );
        vec3 light_color = vec3( 0.3, 0.7, 1.0 );

        vec3 vl = normalize( light_pos - v );

        float diffuse  = max( 0.0, dot( vl, n ) );
        float specular = max( 0.0, dot( vl, ref_ev ) );
        specular = pow( specular, shininess );

        final += light_color * ( diffuse + specular );
    }

    return final;
}

vec3 shading(vec3 pos, vec3 normal, vec3 eye)
{
    vec3 sumColor = vec3(0.);

    for (int i=0; i<numberOfLights; ++i)
    {
        PointLight plight = Lgt.lights[i];
        vec3 lightVec = vec3(plight.pos.xyz - pos);
        vec3 lightDir = normalize(lightVec);
        float distance = length(lightVec); 

        float dp = max(dot(normal,lightDir), 0.);

        float a = plight.constant +
            (plight.linear * distance) +
            (plight.exponent * (distance * distance)) + 0.0001;


        sumColor += dp * vec3(
            plight.intensity * 
            plight.diffCol) / a;
        // + spec * plight.diffCol)/ a;
    }

    return sumColor;
}

// get gradient in the world
vec3 gradient( vec3 pos ) {
    const vec3 dx = vec3( grad_step, 0.0, 0.0 );
    const vec3 dy = vec3( 0.0, grad_step, 0.0 );
    const vec3 dz = vec3( 0.0, 0.0, grad_step );
    return normalize (
        vec3(
            dist_field( pos + dx ) - dist_field( pos - dx ),
            dist_field( pos + dy ) - dist_field( pos - dy ),
            dist_field( pos + dz ) - dist_field( pos - dz )
        )
    );
}

// ray marching
float ray_marching( vec3 origin, vec3 dir, float start, float end ) {
    float depth = start;
    for ( int i = 0; i < max_iterations; i++ ) {
        float dist = dist_field( origin + dir * depth );
        if ( dist < stop_threshold ) {
            return depth;
        }
        depth += dist;
        if ( depth >= end) {
            return end;
        }
    }
    return end;
}

vec3 getSceneColor( in vec3 ro, in vec3 rd, inout float depth )
{
    // ray marching
    depth = ray_marching( ro, rd, 0.0, clip_far );
    if ( depth >= clip_far ) {
        discard;
    }

    // shading
    vec3 pos = ro + rd * depth;
    vec3 n = gradient( pos );
    vec3 col = shading( pos, n, ro );

    return col;
}

///////////////////////////////////////////////////////////////////////////////
// Patch in the Rift's heading to raymarch shader writing out color and depth.
// http://blog.hvidtfeldts.net/

// Translate the origin to the camera's location in world space.
vec3 getEyePoint(mat4 mvmtx)
{
    vec3 ro = -mvmtx[3].xyz;
    return ro;
}

// Construct the usual eye ray frustum oriented down the negative z axis.
// http://antongerdelan.net/opengl/raycasting.html
vec3 getRayDirection(vec2 uv)
{
    vec4 ray_clip = vec4(uv.x, uv.y, -1., 1.);
    vec4 ray_eye = inverse(prmtx) * ray_clip;
    return normalize(vec3(ray_eye.x, ray_eye.y, -1.));
}

void main()
{
    mat4 mvmtx = vmtx;
    vec2 uv11 = uv * 2.0 - vec2(1.0);
    vec3 ro = getEyePoint(mvmtx);
    vec3 rd = getRayDirection(uv11);

    ro *= mat3(mvmtx);
    rd *= mat3(mvmtx);

    float depth = 9999.0;
    vec3 col = getSceneColor(ro, rd, depth);

    // Write to depth buffer
    vec3 eyeFwd = vec3(0.,0.,-1.) * mat3(mvmtx);
    float eyeHitZ = -depth * dot(rd, eyeFwd);
    float p10 = prmtx[2].z;
    float p11 = prmtx[3].z;
    // A little bit of algebra...
    float ndcDepth = -p10 + -p11 / eyeHitZ;
    float dep = ((gl_DepthRange.diff * ndcDepth) + gl_DepthRange.near + gl_DepthRange.far) / 2.0;

    gl_FragDepth = dep;
    fragColor = vec4(col, 1.0);
}
]]

function raymarch_csg:init(frag_body)
    self.frag_body = frag_body or rm_frag_body
    self.lightData = {}
    self.lightspeed = 1
end

function raymarch_csg:initGL()
    self.shader = FullScreenShaderLib.new(
        string.gsub(rm_frag_header, "NUMLIGHTS", NUMBER_OF_LIGHTS)..
        self.frag_body..
        rm_frag_footer)
    self.shader:initGL()

    -- Init lighting data block
    do
        local lv = {}
        local numLights = NUMBER_OF_LIGHTS
        local numflts = numLights*(3*4) -- sizeof light struct with padding
        local glIntv = ffi.typeof('GLint[?]')
        local ubo = glIntv(0)
        gl.glGenBuffers(1, ubo)
        gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, ubo[0])
        gl.glBufferData(GL.GL_UNIFORM_BUFFER, 4*numflts, nil, GL.GL_DYNAMIC_DRAW)
        self.uniformbuf = ubo

        local prog = self.shader.prog --Grab it from FullscreenShader
        local lightBlock = gl.glGetUniformBlockIndex(prog, "Light")
        gl.glUniformBlockBinding(prog, lightBlock, 0)
        gl.glBindBufferBase(GL.GL_UNIFORM_BUFFER, 0, self.uniformbuf[0])
        gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, 0)
    end
end

function raymarch_csg:exitGL()
    self.shader:exitGL()
end

function raymarch_csg:renderEye(model, view, proj)
    local function set_variables(prog)
        local glFloatv = ffi.typeof('GLfloat[?]')
        local function uni(name) return gl.glGetUniformLocation(prog, name) end
        gl.glUniformMatrix4fv(uni("mmtx"), 1, GL.GL_FALSE, glFloatv(16, model))
        gl.glUniformMatrix4fv(uni("vmtx"), 1, GL.GL_FALSE, glFloatv(16, view))
        gl.glUniformMatrix4fv(uni("prmtx"), 1, GL.GL_FALSE, glFloatv(16, proj))

        -- Flatten array of light values
        do
            local ldata = {}
            
            for _,light in pairs(self.lightData) do
                for i=1,#light do
                    table.insert(ldata, light[i])
                end
            end
            local lightArr = glFloatv(#ldata, ldata)

            gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, self.uniformbuf[0])
            gl.glBufferSubData(GL.GL_UNIFORM_BUFFER, 0, ffi.sizeof(lightArr), lightArr)
            gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, 0)

            local lightBlock = gl.glGetUniformBlockIndex(prog, "Light")
            gl.glUniformBlockBinding(prog, lightBlock, 0)
            gl.glBindBufferBase(GL.GL_UNIFORM_BUFFER, 0, self.uniformbuf[0])
        end

    end

    self.shader:render(view, proj, set_variables)
end

function raymarch_csg:timestep(absTime, dt)
    do
        -- Animate lighting data
        local time = self.lightspeed * absTime

        local function animLightPos(t)
            local r = 3
            local speed = 2
            return r*math.sin(speed*t), .5*r*math.sin(speed*1.3*t), r*math.cos(speed*t)
        end

        local function colorFromIdx(idx)
            local ci = math.fmod(idx-1, 7) + 1
            local r = bit.band(ci, 1)
            local g = bit.band(ci, 2) / 2
            local b = bit.band(ci, 4) / 4
            return r,g,b
        end

        self.lightData = {}
        for i=1,NUMBER_OF_LIGHTS do
            local x,y,z = animLightPos(time - (.7*i-1))
            local r,g,b = colorFromIdx(i)
            light = {
                x,y,z, 0,
                r,g,b,0,
                1, .1, 1, .1,
                -- NOTE: std140 layout adds padding to 16-byte boundaries
            }
            table.insert(self.lightData, light)
        end
    end
end

return raymarch_csg
