--[[ ships_in_space.lua

    
]]
ships_in_space = {}

ships_in_space.__index = ships_in_space

function ships_in_space.new(...)
    local self = setmetatable({}, ships_in_space)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local ShipLib = require("scene.sdf_mesh")
local StarfieldLib = require("scene.starfield")
local mm = require("util.matrixmath")

function ships_in_space:init(subdivs)
    self.shiptypes = {
        {1,{0,0,0}},
        {2,{1,2,0}},
        {3,{2,4,0}},
    }
    self.ships = {}
    for k,v in pairs(self.shiptypes) do
        local com = ShipLib.new(v[1], subdivs)
        com.startpos = {} for i=1,3 do com.startpos[i] = v[2][i] end
        com.pos = v[2]
        table.insert(self.ships, com)
    end
    self.stars = StarfieldLib.new()
    self.stars.xvel = .5
    self.reset_flag = 0
    self.comet_vel = -3
end
function ships_in_space:setDataDirectory(dir)
    for k,v in pairs(self.ships) do
        v:setDataDirectory(dir)
    end
end

function ships_in_space:initGL()
    for k,v in pairs(self.ships) do
        v:initGL()
    end
    self.stars:initGL()
end

function ships_in_space:exitGL()
    for k,v in pairs(self.ships) do
        v:exitGL()
    end
    self.stars:exitGL()
end

function ships_in_space:renderEye(model, view, proj)
    self:handleResetStateFlag()

    for k,v in pairs(self.ships) do
        local m = {}
        for i=1,16 do m[i] = model[i] end
        mm.glh_translate(m, 10,-5,-10)
        mm.glh_translate(m, v.pos[1], v.pos[2], v.pos[3])
        local sz = .3
        --mm.glh_scale(m, sz,sz,sz)
        v:renderEye(m, view, proj)
    end
    self.stars:renderEye(model, view, proj)
end

function ships_in_space:timestep(absTime, dt)
    for k,v in pairs(self.ships) do
        local vel = {self.comet_vel,0,0}
        for i=1,3 do
            v.pos[i] = v.pos[i] + dt*vel[i]
        end
        v:timestep(absTime, dt)
    end
    self.stars:timestep(absTime, dt)
end

function ships_in_space:keypressed(key, scancode, action, mods)
    local factor = 1.2
    if key == string.byte(';') then
        self.comet_vel = self.comet_vel / factor
        print('comet_vel',self.comet_vel)
    elseif key == string.byte("'") then
        self.comet_vel = self.comet_vel * factor
        print('comet_vel',self.comet_vel)
    elseif key == 259 then -- bksp
        self.reset_flag = self.reset_flag + 1
    end
    self.stars:keypressed(key, scancode, action, mods)
end

function ships_in_space:handleResetStateFlag()
    -- Check against last value
    -- For resetting state from sync tracker
    if self.last_reset_flag ~= self.reset_flag then
        for k,v in pairs(self.ships) do
            for i=1,3 do
                v.pos[i] = v.startpos[i]
            end
        end
    end
    self.last_reset_flag = self.reset_flag
end

return ships_in_space
