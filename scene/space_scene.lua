--[[ space_scene.lua

]]
space_scene = {}

space_scene.__index = space_scene

function space_scene.new(...)
    local self = setmetatable({}, space_scene)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local MeteoriteLib = require("scene.meteorite")
local PhotosphereLib = require("scene.photosphere")
local StarfieldLib = require("scene.starfield")
local mm = require("util.matrixmath")

function space_scene:init()
    self.sunpos = {5,5,5}
    self.Meteorite = MeteoriteLib.new()
    self.Sun = PhotosphereLib.new()
    self.Meteorite.sunpos = self.sunpos

    self.Stars = StarfieldLib.new()
    self.Stars.xvel = 0

    self.launch_flag = 0
    self.last_launch_flag = self.launch_flag
    self.comet_strength = 1
    self.cur_comet = 1
    self.cometItems = {
        -- String, hitpt(to be normalized), traveltime, strength
        {"First", {1,0,1}, .8, .2},
        {"Second", {1,.2,1}, .8, .3},
        {"Third", {1,.4,1}, .8, .4},
        {"Fourth", {1,.6,1}, .8, .5},
        {"Fifth", {1,.8,1}, .8, .6},
    }
end

function space_scene:setDataDirectory(dir)
    self.Meteorite:setDataDirectory(dir)
    self.dataDir = dir

    -- Load comet data from config file
    local strfilename = 'comet_data.lua'
    local fullname = self.dataDir..'/'..strfilename
    local f = loadfile(fullname)
    f()
    if comets then
        self.cometItems = comets.cometItems
    end
end

function space_scene:initGL()
    self.Meteorite:initGL()
    self.Sun:initGL()
    self.Stars:initGL()
end

function space_scene:exitGL()
    self.Meteorite:exitGL()
    self.Sun:exitGL()
    self.Stars:exitGL()
end

function space_scene:renderEye(model, view, proj)
    local ms = {}
    mm.make_identity_matrix(ms)
    mm.glh_translate(ms, 0,0,0)

    -- TODO: hacky depth..
    gl.glDepthMask(GL.GL_FALSE)
    self.Stars:renderEye(ms, view, proj)
    gl.glDepthMask(GL.GL_TRUE)

    local mmoon = {}
    for i=1,16 do mmoon[i] = model[i] end
    do
        mm.glh_rotate(mmoon, 18, 0,1,0)
        mm.glh_rotate(mmoon, -4, 1,0,0)
    end
    self.Meteorite:renderEye(mmoon, view, proj)

    local m = {}
    mm.make_identity_matrix(m)
    local p = self.sunpos
    mm.glh_translate(m, p[1], p[2], p[3])
    local sz = .9
    mm.glh_scale(m,sz,sz,sz)
    self.Sun:renderEye(m, view, proj)
end

function space_scene:timestep(absTime, dt)
    self.Stars:timestep(absTime, dt)
    self.Meteorite:timestep(absTime, dt)
    self.Sun:timestep(absTime, dt)

    self:handleResetStateFlag()
end

function space_scene:keypressed(key, scancode, action, mods)
    if key == 259 then -- bksp
        self.launch_flag = self.launch_flag + 1
        return true
    end

    local factor = 1.2
    if key == string.byte(';') then
        self.comet_strength = self.comet_strength / factor
        print(self.comet_strength)
        return true
    elseif key == string.byte("'") then
        self.comet_strength = self.comet_strength * factor
        print(self.comet_strength)
        return true
    end

    local ret = self.Stars:keypressed(key, scancode, action, mods)
    if ret then return true end
    local ret = self.Meteorite:keypressed(key, scancode, action, mods)
end

function space_scene:onray(ro, rd)
    local ret = self.Meteorite:onray(ro, rd)
end

function space_scene:handleResetStateFlag()
    -- Check against last value
    -- For resetting state from sync tracker
    if self.last_launch_flag ~= self.launch_flag then

        -- bububu.mp3 - 16 beats in 7.114 seconds
        local traveltime = 7.114/16
        local hitpt = {
            -1+2*math.random(),
            -1+2*math.random(),
            math.random(),
        }
        for i=1,3 do hitpt[i] = hitpt[i] * .7 end
        local strength = .2 + self.comet_strength * math.random()
        local word = "meteor"

        local item = self.cometItems[self.cur_comet]
        if item then
            word = item[1]
            hitpt = item[2]
            traveltime = item[3]
            strength = item[4]
        end
        mm.normalize(hitpt)
        self.Meteorite:launchMeteor(hitpt, traveltime, strength, word)
        self.cur_comet = self.cur_comet + 1
    end
    self.last_launch_flag = self.launch_flag
end

return space_scene
