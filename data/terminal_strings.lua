-- terminal_strings.lua

terminal = {}
terminal.textItems = {
    {
        -- String, xy coords, size(optional)
        {"Terminal line 1", {0,0}, 8, {0,1,0,}},
        {"Line 2.", {.0,.14}, 8},
        {"Approaching.", {.0,.14*2}, 8},
        {"", {.0,.14*3}, 8},
        {"QWERTYUIOP", {.0,.14*4}, 8},
    },
    {
        -- String, xy coords, size(optional)
        {"Status:", {0,0}, 8, {1,1,0,}},
        {"IN PROGRESS", {.0,.14}, 8, {1,0,0}},
    },
}
