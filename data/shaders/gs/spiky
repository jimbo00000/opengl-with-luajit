layout (points) in;
layout (triangle_strip, max_vertices = 6) out;

uniform mat4 modelMtx;
uniform mat4 viewMtx;
uniform mat4 projMtx;

uniform int numParticles;
uniform float time;

out vec3 gfPos;
out vec3 gfNormal;

// rotation matrix
mat3 rotmat(vec3 v, float angle)
{
    float c = cos(angle);
    float s = sin(angle);
    
    return mat3(c + (1.0 - c) * v.x * v.x, (1.0 - c) * v.x * v.y - s * v.z, (1.0 - c) * v.x * v.z + s * v.y,
        (1.0 - c) * v.x * v.y + s * v.z, c + (1.0 - c) * v.y * v.y, (1.0 - c) * v.y * v.z - s * v.x,
        (1.0 - c) * v.x * v.z - s * v.y, (1.0 - c) * v.y * v.z + s * v.x, c + (1.0 - c) * v.z * v.z
        );
}

float parabola( float x, float k )
{
    return pow( 4.0f*x*(1.0f-x), k );
}

#define PI 3.14159265359

void main()
{
    float t = float(gl_PrimitiveIDIn) / float(numParticles);
    //vec4 pos = vec4(t, 0., 0., 1.);

    int spokes = int(PI*log(float(numParticles)));
    int spoke = gl_PrimitiveIDIn % spokes;
    int ring = gl_PrimitiveIDIn / spokes;
    int numRings = numParticles / spokes;
    //if (ring >= numRings) return;

    float spokePos = float(spoke) / float(spokes);
    float ringPos = float(ring) / float(numRings);
    float theta = 2.*PI * ringPos;
    float rr = 1. - spokePos;

    mat3 rmat = rotmat(vec3(0.,0.,1.), theta);
    vec3 rpt = rmat * vec3(rr,0.,0.);

    vec3 p3 = rpt;

    vec3 ccw = rmat * vec3(0.,1.,0.);
    vec3 outward = rmat * vec3(1.,0.,0.);
    vec3 z = vec3(0.,0.,1.);

    float platerot = .5+.5*sin(time);
    outward *= rotmat(ccw, platerot);
    z *= rotmat(ccw, platerot);

    float ph = 10.*max(0.,.1-abs(fract(.185*time)-t));
    ph = pow(ph,14.);
    float twistrot = ph;//1.*max(0., pow(sin(5.3*time-spokePos-3.*ringPos),5.));
    outward *= rotmat(z, twistrot);
    ccw *= rotmat(z, twistrot);

    float sz = .5/float(numRings);
    sz *= 1.8*pow(15. / float(spoke), .465);
    ccw *= sz;
    outward *= sz;
    z *= sz;

    p3 += 6.*ph * z * .3;//spoke/float(spokes);

    vec3 tripts[5] = vec3[](
        p3 + ccw,
        p3 + outward + z,
        p3 - outward,
        p3 + -ccw,// + -1.3*outward,
        p3 + 1.*ccw + -.9*outward
        );
    vec3 tricols[5] = vec3[](
        vec3(1.,0.,0.),
        vec3(0.,1.,0.),
        vec3(0.,0.,1.),
        vec3(1.,1.,1.),
        vec3(1.,0.,1.)
        );

    mat4 mvmtx = viewMtx * modelMtx;
    mat4 m = projMtx * mvmtx;

#if 0
    for (int i=0; i<4; ++i) {
        gl_Position = m * vec4(tripts[i], 1.);
        gfNormal = tricols[i];
        EmitVertex();
    }
    EndPrimitive();
#else
    // First flat tri
    vec3 norm = normalize(-cross(tripts[1]-tripts[0], tripts[2]-tripts[0]));
    gfNormal = normalize(mat3(mvmtx) * norm);
    for (int i=0; i<3; ++i) {
        gfPos = tripts[i];
        gl_Position = m * vec4(gfPos, 1.);
        EmitVertex();
    }
    EndPrimitive();

    // Second flat tri
    norm = normalize(cross(tripts[2]-tripts[1], tripts[3]-tripts[1]));
    gfNormal = normalize(mat3(mvmtx) * norm);
    for (int i=1; i<4; ++i) {
        gfPos = tripts[i];
        gl_Position = m * vec4(gfPos, 1.);
        EmitVertex();
    }
    EndPrimitive();
#endif
}




