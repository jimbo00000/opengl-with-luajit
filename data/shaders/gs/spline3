layout (points) in;
layout (triangle_strip, max_vertices = 64) out;

uniform mat4 modelMtx;
uniform mat4 viewMtx;
uniform mat4 projMtx;

uniform int numParticles;
uniform float time;

out vec3 gfNormal;

vec3 GetCatmullRomPoint(vec3 p0, vec3 p1, vec3 p2, vec3 p3, float t)
{
    float t2 = t*t;
    float t3 = t*t*t;
    return 0.5*(
            (2.*p1) +
            (-p0 + p2)*t +
            (2.*p0 - 5.*p1 + 4.*p2 - p3)*t2 +
            (-p0 + 3.*p1 - 3.*p2 + p3)*t3
            );
}

float radiusScale = 1.;

/// Emit a set of triangles in a cylindrical shape around the history path.
void AppendTube(vec3 start, vec3 end, vec3 nextEnd, float radius, int slices, mat4 mvpMtx, mat3 normMtx)
{
    const float thresh = 0.01;

    vec3 axis = normalize(end - start);
    vec3 up   = normalize(vec3(0,1,0));
    if (dot(axis, up) > (1.0-thresh))
    {
        up = vec3(1,0,0);
    }
    vec3 left = cross(up, axis);
    up = cross(axis, left);

    float radscale = radiusScale * radius;
    up   *= radscale;
    left *= radscale;
    
    // Determine axis for the next segment to seal cylinder ends together
    vec3 axis2 = normalize(nextEnd - end);
    vec3 up2   = normalize(vec3(0,1,0));
    if (dot(axis2, up2) > (1.0-thresh))
    {
        up2 = vec3(1,0,0);
    }
    vec3 left2 = cross(up2, axis2);
    up2 = cross(axis2, left2);
    
    // Smaller radius here
    radscale = radiusScale * (radius);// - recencyStep);
    up2   *= radscale;
    left2 *= radscale;


    for (int i=0; i<slices; ++i)
    {
        const float PI = 3.1415927;
        float angle = 2 * PI * (float(i)/float(slices));
        float angle2 = 2 * PI * (float(i+1)/float(slices));
        vec3 pt0 = start + sin(angle )*up + cos(angle )*left;
        vec3 pt1 = end   + sin(angle )*up2 + cos(angle )*left2;
        vec3 pt2 = start + sin(angle2)*up + cos(angle2)*left;
        vec3 pt3 = end   + sin(angle2)*up2 + cos(angle2)*left2;
        vec3 norm0 = normalize(cross(pt2-pt0,pt1-pt0));
        //vec3 norm1 = normalize(cross(pt3-pt2,pt1-pt2));
        ///@todo lighting in frag shader
        norm0 = normMtx * norm0;

        /// First tri
        gl_Position = mvpMtx * vec4(pt0,1);
        gfNormal = norm0;
        EmitVertex();
        gl_Position = mvpMtx * vec4(pt1,1);
        gfNormal = norm0;
        EmitVertex();
        gl_Position = mvpMtx * vec4(pt2,1);
        gfNormal = norm0;
        EmitVertex();

        // Second tri
        gl_Position = mvpMtx * vec4(pt2,1);
        gfNormal = norm0;
        EmitVertex();
        gl_Position = mvpMtx * vec4(pt1,1);
        gfNormal = norm0;
        EmitVertex();
        gl_Position = mvpMtx * vec4(pt3,1);
        gfNormal = norm0;
        EmitVertex();
    }

    EndPrimitive();
}

void appendTris(vec3 splpt, vec3 splpt1, mat3 normMtx, mat4 mvpMtx)
{
    // Assemble basis
    vec3 bx = normalize(splpt1-splpt);
    vec3 by =
        //vec3(1.,0.,0.);
        mat3(viewMtx) * normalize(vec3(1.,1.,0.));
    if (dot(bx,by) > .9)
        by = by.yxz;
    vec3 bz = normalize(cross(bx,by));

    gfNormal = abs(
               normMtx * 
              bz);

    float sl = 10.;
    float sz = sl * length(splpt1-splpt);
    //sz = 20./ float(numParticles);

    // Assemble geom
    vec3 or = sl*splpt;
    vec3 pos;

    pos = or;
    gl_Position = mvpMtx * vec4(pos, 1.);
    EmitVertex();

    pos = or + sz*by;
    gl_Position = mvpMtx * vec4(pos, 1.);
    EmitVertex();

    pos = or + sz*bx;
    gl_Position = mvpMtx * vec4(pos, 1.);
    EmitVertex();

    EndPrimitive();

    gfNormal = abs(
               normMtx * 
              bx);

    pos = or;
    gl_Position = mvpMtx * vec4(pos, 1.);
    EmitVertex();

    pos = or + sz*bx;
    gl_Position = mvpMtx * vec4(pos, 1.);
    EmitVertex();

    pos = or + sz*bz;
    gl_Position = mvpMtx * vec4(pos, 1.);
    EmitVertex();


    EndPrimitive();
}

void main()
{
    mat4 mvpMtx = projMtx * viewMtx * modelMtx;
    mat3 normMtx = mat3(modelMtx);

    float t0 = float(gl_PrimitiveIDIn) / float(numParticles);
    float t1 = float(gl_PrimitiveIDIn + 1) / float(numParticles);
    float t2 = float(gl_PrimitiveIDIn + 2) / float(numParticles);

    float mr = 2.631;
    vec3 p0 = vec3(mr*cos(1.3*time),-2.+mr*cos(2.1*time),mr*sin(time));
    vec3 p1 = vec3(0.);
    vec3 p2 = vec3(1.);
    vec3 p3 = vec3(-4.,3.+mr*cos(1.7*time),mr*sin(.9*time));

    vec3 splpt = GetCatmullRomPoint(p0,p1,p2,p3,t0);
    vec3 splpt1 = GetCatmullRomPoint(p0,p1,p2,p3,t1);
    vec3 splpt2 = GetCatmullRomPoint(p0,p1,p2,p3,t2);

    //appendTris(splpt, splpt1, normMtx, mvpMtx);
    AppendTube(splpt, splpt1, splpt2, .1, 6, mvpMtx, normMtx);
}

