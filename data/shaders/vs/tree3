uniform mat4 modelMtx;
uniform mat4 viewMtx;
uniform mat4 projMtx;

uniform mediump float time;
uniform int numParticles;

float precPhase = 0.;// .2*time;
float spinPhase = 0.;//time;
float colorPhase = 0.;//.3*time;

vec3 pal( in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d )
{
    return a + b*cos( 6.28318*(c*t+d) );
}

vec3 getColor(float i)
{
    float colorPhase = -.4;
    return pal( i+colorPhase, vec3(0.5,0.5,0.5),vec3(0.5,0.5,0.5),vec3(1.0,1.0,0.5),vec3(0.8,0.90,0.30) );
}

// Quaternion rotation of a vector
vec3 qtransform(vec4 q, vec3 v)
{
    return v + 2.0*cross(cross(v, q.xyz) + q.w*v, q.xyz);
}

// http://www.geeks3d.com/20141201/how-to-rotate-a-vertex-by-a-quaternion-in-glsl/
vec4 quatFromAxisAngle(vec3 axis, float angle)
{
    float half_angle = angle / 2.;
    vec4 q;
    q.x = axis.x * sin(half_angle);
    q.y = axis.y * sin(half_angle);
    q.z = axis.z * sin(half_angle);
    q.w = cos(half_angle);
    return q;
}

void main()
{
    int index = gl_InstanceID;
    float ix = float(index) / float(numParticles);
    float level = floor(log(float(index)) / log(2.));
    float toplevel = 8.;//floor(log(float(numParticles)) / log(2.));
    float branch = float(index - int(pow(2.,level)));

    float horiz = (branch) / pow(2.,level);
    const float PI = acos(-1.);
    float theta = 2.*PI * horiz;
    float r = -.5+2.*sqrt(level);

    vec3 position = vec3(
        r*sin(theta),
        r*cos(theta),
        0.);
    float sz = .4 / level;

    mat4 mvpMatrix = projMtx * viewMtx * modelMtx;
    mat4 vpMatrix = projMtx * viewMtx;
    mat3 normMtx = mat3(modelMtx);

    vec4 mPos = vPosition;
    vfTexCoord = vec3(1.);
    
    float twist = 2.3*(1.-(level/toplevel))*spinPhase;
    vec3 twaxis = vec3(sin(precPhase), cos(precPhase),0.);

    vec4 rq = quatFromAxisAngle(twaxis, twist);
    position = qtransform(rq, position);
    vec3 lpos = mPos.xyz-vec3(.5);
    lpos = qtransform(rq, lpos);

    vfNormal = abs(normMtx * qtransform(rq, vNormal.rgb));

    vec3 wpos = sz*lpos + .3*position.xyz;
    wpos = (modelMtx * vec4(wpos,1.)).xyz;
    vfTexCoord = getColor(fract(ix+colorPhase));
    vfWorldPos = (vec4(wpos,1.)).xyz;
    gl_Position = vpMatrix * vec4(wpos,1.);
}

