-- comet_data.lua
-- comet_data.lua

comets = {}

comets.cometItems = {
    -- String, hitpt(to be normalized), traveltime, strength
    {"Small1", {1,0,1}, .8, .2},
    {"Small2", {1,.2,1}, .8, .2},
    {"Small3", {1,.4,1}, .8, .3},
    {"Medium1", {1,.6,1}, .8, .5},
    {"Medium2", {1,.8,1}, .8, .5},
    {"Medium3", {1,1,1}, .8, .6},
    {"Large1", {1,1.3,1}, .8, .8},
    {"Large2", {1,1.6,1}, .8, .8},
    {"Large3", {1,1.8,1}, .8, .9},
}
