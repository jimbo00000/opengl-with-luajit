
#include <stdio.h>
#include "funclib.h"

#define NUM_VALUES 4

int main()
{
	// Allocate some float arrays,
	// do some operations on them,
	// and print the results.

	value_t vals[NUM_VALUES];
	value_t params[NUM_VALUES];

	for (int i=0; i<NUM_VALUES; ++i)
	{
		vals[i] = (value_t)i;
		params[i] = 1.f / (vals[i]+1.f);
	}
	printFloats(vals, NUM_VALUES);
	//printFloats(params, NUM_VALUES);


	for (int i=0; i<15; ++i)
	{
		incrementFloats(vals, params, NUM_VALUES);
	}


	printFloats(vals, NUM_VALUES);
	//printFloats(params, NUM_VALUES);

	return 0;
}
