// funclib.h

#ifdef _WIN32
#  ifdef MODULE_API_EXPORTS
#    define MODULE_API __declspec(dllexport)
#  else
#    define MODULE_API __declspec(dllimport)
#  endif
#else
#  define MODULE_API
#endif

typedef float value_t;
//typedef __fp16 value_t; // ARM only

MODULE_API void gridPositions(value_t* positions, int num);
MODULE_API void incrementFloats(value_t* positions, value_t* velocities, int num);
MODULE_API void assembleQuads(value_t* offsets, value_t* attributes, int num);
MODULE_API void printFloats(value_t* f, int num);
