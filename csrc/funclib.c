// funclib.c
// Do some things directly to a float array.

#include "funclib.h"
#include <math.h>
#include <stdio.h>

void gridPositions(value_t* positions, int num)
{
    const value_t dim = sqrt(num);
    for (int i=0; i<num; ++i)
    {
        const value_t xdim = fmod(i, dim);
        const value_t ydim = floor(i/dim);
        positions[2*i] = -1.f + 2.f*(xdim/dim);
        positions[2*i+1] = -1.f + 2.f*(ydim/dim);
    }
}

// Does not do any bounds checks!
// Iterates over 2 arrays of vec2s for speed.
///@todo Why is the LuaJIT version so slow?
void incrementFloats(value_t* positions, value_t* velocities, int num)
{
	const value_t gravity = -0.0098f * .5f;
    const value_t minX = -1.f;
    const value_t maxX = 1.f;
    const value_t minY = -1.f;
    const value_t maxY = 1.f;

	for (int i=0; i<num; ++i)
	{
		int xidx = 2*i;
		int yidx = xidx + 1;

		value_t posX = positions[xidx];
		value_t posY = positions[yidx];
		value_t velX = velocities[xidx];
		value_t velY = velocities[yidx];

		posX += velX;
		posY += velY;

		velY += gravity;

		if (posX > maxX)
		{
			velX *= -.9f;
			posX = maxX;
		}
		else if (posX < minX)
		{
			velX *= -.9f;
			posX = minX;
		}

		if (posY > maxY)
		{
			velY *= -.9f;
			posY = maxY;
		}
		else if (posY < minY)
		{
			velY *= -.9f;
			posY = minY;
		}

		positions[xidx] = posX;
		positions[yidx] = posY;
		velocities[xidx] = velX;
		velocities[yidx] = velY;
	}
}

void assembleQuads(value_t* offsets, value_t* attributes, int num)
{
	int o = 0;
	for (int i=0; i<num; ++i)
	{
		value_t x = offsets[2*i  ];
		value_t y = offsets[2*i+1];
		for (int j=0; j<6; ++j)
		{
			attributes[o  ] = x;
			attributes[o+1] = y;
			o += 2;
		}
	}
}

void printFloats(value_t* f, int num)
{	
	for (int i=0; i<num; ++i)
	{
		printf(" [%d]=%f", i, f[i]);
	}
	printf("\n");
}
