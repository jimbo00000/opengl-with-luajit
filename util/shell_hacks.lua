--[[ shell_hacks.lua

    One way out of this ugliness is to build LuaFilesystem.
]]

local ffi = require("ffi")

function scandir_shellcmd(cmd)
    local i, t, popen = 0, {}, io.popen
    local pfile = popen(cmd)
    for filename in pfile:lines() do
        if filename ~= "." and filename ~= ".." then
            i = i + 1
            t[i] = filename
         end
    end
    pfile:close()
    return t
end

function scandir_portable_sorta(directory)
    local wincmd = 'dir /b '..'"'..directory..'"'
    local poscmd = 'ls -a '..'"'..directory..'"'
    local f = scandir_shellcmd(poscmd)
    if #f == 0 then
        f = scandir_shellcmd(wincmd)
    end
    return f
end

function listdirs_portable_sorta(directory)
    local wincmd = 'dir /b /ad '..'"'..directory..'"'
    local poscmd = 'find '..directory..'/ -maxdepth 1 -type d'

    local f = scandir_shellcmd(poscmd)
    if #f > 0 then
        -- Pull off the directory prefix
        for k,v in pairs(f) do
            f[k] = string.sub(v, #directory+2, #v)
        end
    else
        f = scandir_shellcmd(wincmd)
    end
    return f
end

--[[ Usage:
local files = scandir_portable_sorta("scene")
print(ffi.os, #files)
for k,v in pairs(files) do
    print(k,v)
end
]]
