-- camera2.lua
-- Simple animated motion

local mm = require 'util.matrixmath'

camera2 = {}
camera2.__index = camera2

function camera2.new(...)
    local self = setmetatable({}, camera2)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function camera2:init()
    self.chassis = {0,0,1}
end

function camera2:reset()
    self.chassis = {0,0,1}
end

function camera2:timestep(absTime, dt)
    self.chassis[1] = math.sin(absTime)
    self.chassis[2] = math.cos(absTime)
end

function camera2:getViewMatrix()
    local v = {}
    mm.make_identity_matrix(v)
    mm.glh_translate(v, self.chassis[1], self.chassis[2], self.chassis[3])
    return v
end

return camera2
