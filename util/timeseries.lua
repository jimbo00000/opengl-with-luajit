--
-- timeseries.lua
--
-- methods to create and query a variety of time series
-- originally from Untitled (Computational Composition)
-- (c)2018 Mark J. Stock

-- guided/narrative vs. stochastic/statistical
-- continuous vs. discrete vs. impulsive
-- how to apply functions upon these? to allow recursion/octaves/bitcrunch?
-- accept a table of functions or objects, which could be other timeseries?

local ffi = require("ffi")

timeseries = {}

timeseries.__index = timeseries

function timeseries.new(...)
    local self = setmetatable({}, timeseries)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

--
-- seriestype is a unique string descriptor of the function
-- time constant is in seconds (not frames)
--
function timeseries:init(seriestype, timeconstant)
    self.stype = seriestype
    self.timeconstant = timeconstant
    --self.time = 0
    self.data = {}

    -- initialize data, if necessary
    if self.stype == "uniform ramp" then
        -- also known as triangle wave
        -- nothing to initialize

    elseif self.stype == "fade in and out" then
        -- sinusoidal ramp from 0 to 1 at beginning and 1 to 0 at end
        -- useful for overall brightness scaling, but also activity scaling
        -- timeconstant is overall program length, fade is always 3 seconds in and 5 out
        -- nothing to initialize

    elseif self.stype == "constrained random walk" then
        -- returns the mean of a fixed number of random numbers, 
        --    one of which changes every frame
        -- data position 1 is the number of values in the summation
        local n = self.timeconstant * 10
        table.insert(self.data, n)
        -- data position 2 is the array itself
        arry = ffi.new("float[?]", n)
        for i=0,n-1 do
            arry[i] = math.random()
        end
        table.insert(self.data, arry)
        -- and data position 3 is the index of the entry to change next
        local idx = 0
        table.insert(self.data, idx)

    elseif self.stype == "geiger counter" then
        -- returns 0 most of the time, but 1 when triggered
        -- uses Poisson process algorithm from Knuth

    elseif self.stype == "shakespeare play" then
        -- returns a scalar intensity from 0..1 given the normalized
        --    position in the play (5 acts): 0 1/2 0 1/2 1 0
        -- nothing to initialize
    end
end

--
-- return a scalar or vector set of values for the given seriestype
--
function timeseries:getval(t)
    local retval = 0

    if self.stype == "uniform ramp" then
        retval = (t % self.timeconstant) / self.timeconstant

    elseif self.stype == "fade in and out" then
        local trel = t % self.timeconstant
        if trel < 3 then
            retval = 0.5-0.5*math.cos(trel*math.pi/3)
        elseif trel > self.timeconstant-5 then
            retval = 0.5-0.5*math.cos((self.timeconstant-trel)*math.pi/5)
        end

    elseif self.stype == "constrained random walk" then
        local n = self.data[1]
        local arry = self.data[2]
        local idx = self.data[3]
        -- change the value
        arry[idx] = math.random()
        -- perform the summation
        for i=0,n-1 do
            retval = retval + arry[i]
        end
        -- set the single return value
        retval = retval / n
        -- and increment the next index
        idx = (idx+1) % n

    elseif self.stype == "geiger counter" then
        -- do we trigger?

    elseif self.stype == "shakespeare play" then
        local trel = (t % self.timeconstant) / self.timeconstant
        if (trel < 0.2) then
            retval = 2.5*trel
        elseif (trel < 0.4) then
            retval = 1 - 2.5*trel
        elseif (trel < 0.8) then
            retval = 2.5*(trel-0.4)
        else
            retval = 5 - 5*trel
        end

    elseif self.stype == "sin wave" then
        retval = math.sin(2*math.pi * t * self.timeconstant)
    else
        -- default is to return a constant 0
        return 0
    end

    return retval
end

return timeseries
