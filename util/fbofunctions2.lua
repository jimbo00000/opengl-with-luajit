--[[ fbofunctions2.lua

    Modifications for OpenGLES 2 on th pi
]]

local ffi = require 'ffi'

local fbofunctions = {}

function fbofunctions.allocate_fbo(w, h)
    fbo = {}
    fbo.w = w
    fbo.h = h

    local texId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, texId)
    fbo.tex = texId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, fbo.tex)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
                  w, h, 0,
                  GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, nil)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)

    local fboId = ffi.new("GLuint[1]")
    gl.glGenFramebuffers(1, fboId)
    fbo.id = fboId[0]
    gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, fbo.id)
    gl.glFramebufferTexture2D(GL.GL_FRAMEBUFFER, GL.GL_COLOR_ATTACHMENT0, GL.GL_TEXTURE_2D, fbo.tex, 0)
    gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0)

    local status = gl.glCheckFramebufferStatus(GL.GL_FRAMEBUFFER)
    if status ~= GL.GL_FRAMEBUFFER_COMPLETE then
        print("ERROR: Framebuffer status: "..status)
    end

    gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0)
    return fbo
end

function fbofunctions.deallocate_fbo(fbo)
    if fbo == nil then return end
    local fboId = ffi.new("int[1]")
    fboId[0] = fbo.id
    gl.glDeleteFramebuffers(1, fboId)

    local texId = ffi.new("int[1]")
    texId[0] = fbo.tex
    gl.glDeleteTextures(1, texId)

    if fbo.depth then
        local depthId = ffi.new("int[1]")
        depthId[0] = fbo.depth
        gl.glDeleteTextures(1, depthId)
    end
end

function fbofunctions.bind_fbo(fbo)
    gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, fbo.id)
    -- Note: viewport is not set here
end

function fbofunctions.unbind_fbo()
    gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0)
    -- Note: viewport is not set here
end

return fbofunctions
