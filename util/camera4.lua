--[[ camera4.lua

    camera controls and state.
    Plugs into glfw for input.
]]

local ffi = require("ffi")
local glfw = require("glfw")
local SDL = require "lib/sdl"
local mm = require("util.matrixmath")

camera4 = {}
camera4.__index = camera4

function camera4.new(...)
    local self = setmetatable({}, camera4)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function camera4:init()
    self.clickpos = {0,0}

    self.obj = {}
    self.obj.rot = {0,0}
    self.obj.trans = {0,0,0}

    self.cam = {}
    self.cam.chassis = {0,0,1}
    self.cam.pan = {0,0,0}
    self.cam.rot = {0,0} -- x,y TODO: quat  

    self.lastClick = {obj={}, cam={}}

    self.keymove = {0,0,0}
    self.altdown = false
    self.ctrldown = false
    self.shiftdown = false
    self.use_arcball = true
    self.keystates = {}
    for i=0, glfw.GLFW.KEY_LAST do
        self.keystates[i] = glfw.GLFW.RELEASE
    end
    self:reset()
end

function camera4:reset_model()
    self.obj.rot = {0,0}
    self.obj.trans = {0,0,0}
    self.obj.arcball = {}
    mm.make_identity_matrix(self.obj.arcball)
end

function camera4:reset_view()
    self.cam.chassis = {0,0,1}
    self.cam.pan = {0,1,3}
    self.cam.rot = {0,15}
end

function camera4:reset()
    self:reset_model()
    self:reset_view()
end

function camera4:onkey(key, scancode, action, mods)
    self.keystates[key] = action
    self.altdown = 0 ~= bit.band(mods, glfw.GLFW.MOD_ALT)
    self.ctrldown = 0 ~= bit.band(mods, glfw.GLFW.MOD_CONTROL)
    self.shiftdown = 0 ~= bit.band(mods, glfw.GLFW.MOD_SHIFT)

    if ffi.os == "OSX" then
        -- Glfw on OSX does not seem to capture Ctrl modifier state correctly.
        self.ctrldown = 0 ~= bit.band(mods, glfw.GLFW.MOD_ALT)
    end

    if self.ctrldown then
        -- ctrl-r Reset
        if self.keystates[glfw.GLFW.KEY_R] ~= glfw.GLFW.RELEASE then
            self:reset()
        elseif self.keystates[glfw.GLFW.KEY_M] ~= glfw.GLFW.RELEASE then
            self:reset_model()
        elseif self.keystates[glfw.GLFW.KEY_V] ~= glfw.GLFW.RELEASE then
            self:reset_view()
        end
    end

    local mag = 1
    local spd = 10
    local km = {0,0,0}
    if self.keystates[glfw.GLFW.KEY_W] ~= glfw.GLFW.RELEASE then km[3] = km[3] + -spd end -- -z forward
    if self.keystates[glfw.GLFW.KEY_S] ~= glfw.GLFW.RELEASE then km[3] = km[3] - -spd end
    if self.keystates[glfw.GLFW.KEY_A] ~= glfw.GLFW.RELEASE then km[1] = km[1] - spd end
    if self.keystates[glfw.GLFW.KEY_D] ~= glfw.GLFW.RELEASE then km[1] = km[1] + spd end
    if self.keystates[glfw.GLFW.KEY_Q] ~= glfw.GLFW.RELEASE then km[2] = km[2] - spd end
    if self.keystates[glfw.GLFW.KEY_E] ~= glfw.GLFW.RELEASE then km[2] = km[2] + spd end
    if self.keystates[glfw.GLFW.KEY_Z] ~= glfw.GLFW.RELEASE then km[2] = km[2] - spd end
    if self.keystates[glfw.GLFW.KEY_X] ~= glfw.GLFW.RELEASE then km[2] = km[2] + spd end
    if self.keystates[glfw.GLFW.KEY_UP] ~= glfw.GLFW.RELEASE then km[3] = km[3] + -spd end
    if self.keystates[glfw.GLFW.KEY_DOWN] ~= glfw.GLFW.RELEASE then km[3] = km[3] - -spd end
    if self.keystates[glfw.GLFW.KEY_LEFT] ~= glfw.GLFW.RELEASE then km[1] = km[1] - spd end
    if self.keystates[glfw.GLFW.KEY_RIGHT] ~= glfw.GLFW.RELEASE then km[1] = km[1] + spd end

    if self.keystates[SDL.SDLK_w] ~= 0 then km[3] = km[3] + -spd end -- -z forward
    if self.keystates[SDL.SDLK_s] ~= 0 then km[3] = km[3] - -spd end
    if self.keystates[SDL.SDLK_a] ~= glfw.GLFW.RELEASE then km[1] = km[1] - spd end
    if self.keystates[SDL.SDLK_d] ~= glfw.GLFW.RELEASE then km[1] = km[1] + spd end
    if self.keystates[SDL.SDLK_q] ~= glfw.GLFW.RELEASE then km[2] = km[2] - spd end
    if self.keystates[SDL.SDLK_e] ~= glfw.GLFW.RELEASE then km[2] = km[2] + spd end
    if self.keystates[SDL.SDLK_z] ~= glfw.GLFW.RELEASE then km[2] = km[2] - spd end
    if self.keystates[SDL.SDLK_x] ~= glfw.GLFW.RELEASE then km[2] = km[2] + spd end
    if self.keystates[SDL.SDLK_UP] ~= glfw.GLFW.RELEASE then km[3] = km[3] + -spd end
    if self.keystates[SDL.SDLK_DOWN] ~= glfw.GLFW.RELEASE then km[3] = km[3] - -spd end
    if self.keystates[SDL.SDLK_LEFT] ~= glfw.GLFW.RELEASE then km[1] = km[1] - spd end
    if self.keystates[SDL.SDLK_RIGHT] ~= glfw.GLFW.RELEASE then km[1] = km[1] + spd end

    if self.keystates[glfw.GLFW.KEY_LEFT_CONTROL] ~= glfw.GLFW.RELEASE then mag = 10 * mag end
    if self.keystates[glfw.GLFW.KEY_LEFT_SHIFT] ~= glfw.GLFW.RELEASE then mag = .1 * mag end
    for i=1,3 do
        self.keymove[i] = km[i] * mag
    end

    -- Dump state to stdout
    if key == string.byte('/') and action == glfw.GLFW.PRESS then
        for k,v in pairs(self:stateStrings()) do
            print(v)
        end
    end
end

function table.shallow_copy(t)
  local t2 = {}
  for k,v in pairs(t) do
    t2[k] = v
  end
  return t2
end

function table.recursive_copy(t)
  local t2 = {};
  for k,v in pairs(t) do
    if type(v) == "table" then
      t2[k] = table.recursive_copy(v)
    else
      t2[k] = v
    end
  end
  return t2
end

function camera4:onclick(button, action, mods, x, y)
    if action == 1 then
        self.holding = button
        self.click_mods = mods
        self.clickpos = {x,y}

        self.lastClick.obj = table.recursive_copy(self.obj)
        self.lastClick.cam = table.recursive_copy(self.cam)
    elseif action == 0 then
        self.holding = nil
        self.clickpos = nil
    end
end

function get_arcball_vector(x, y)
    local c = {
        2*x-1,
        2*y-1,
        0,
    }
    c[2] = -c[2]
    local op2 = c[1]*c[1] + c[2]*c[2]
    if op2 < 1 then
        c[3] = math.sqrt(1-op2)
    else
        mm.normalize(c)
    end
    return c
end

function camera4:onmousemove(x, y)
    local s = 1000
    if self.ctrldown then s = s * 10 end
    if self.shiftdown then s = s / 10 end

    local function translateModel(x,y,s)
        s = s * .01
        self.obj.trans[1] = self.lastClick.obj.trans[1] + s * (x-self.clickpos[1])
        self.obj.trans[2] = self.lastClick.obj.trans[2] + s * (y-self.clickpos[2])
    end

    local function rotateModel(x,y,s)
        self.obj.rot = {
            self.lastClick.obj.rot[1] + s * (x-self.clickpos[1]),
            self.lastClick.obj.rot[2] + s * (y-self.clickpos[2]),
        }
    end

    local function rotateModelArcball(x,y,s)
        local mys = 100
        if self.ctrldown then mys = mys * 5 end
        if self.shiftdown then mys = mys / 5 end
        s = mys

        local va = get_arcball_vector(self.clickpos[1], self.clickpos[2])
        local vb = get_arcball_vector(x, y)
        local angle = s * math.acos(math.min(1, mm.dot(va,vb)))
        local axis_in_camera_coord = mm.cross(va,vb)
        axis_in_camera_coord[4] = 1

        local camera_to_object = {}
        mm.make_identity_matrix(camera_to_object)
        for i=1,16 do camera_to_object[i] = self.obj.arcball[i] end
        mm.affine_inverse(camera_to_object)

        mm.transform(axis_in_camera_coord, camera_to_object)

        mm.make_identity_matrix(self.obj.arcball)
        mm.glh_rotate(self.obj.arcball, angle,
            axis_in_camera_coord[1],
            axis_in_camera_coord[2],
            axis_in_camera_coord[3])
        mm.post_multiply(self.obj.arcball, self.lastClick.obj.arcball)
    end
    local function viewLook(x,y,s)
        s = s * .2
        self.cam.rot = {
            self.lastClick.cam.rot[1] + s * (x-self.clickpos[1]),
            self.lastClick.cam.rot[2] + s * (y-self.clickpos[2]),
        }
    end

    local function viewStrafeXY(x,y,s)
        s = s * .03
        local xb = mm.transform({1,0,0,0}, self:getViewMatrix())
        local yb = mm.transform({0,1,0,0}, self:getViewMatrix())
        for i=1,3 do
            self.cam.pan[i] = self.lastClick.cam.pan[i]
                + s *  (x-self.clickpos[1])*xb[i]
                + s * -(y-self.clickpos[2])*yb[i]
        end
    end

    local function viewStrafeXZ(x,y,s)
        s = s * .01
        self.cam.pan[1] = self.lastClick.cam.pan[1] + s * (x-self.clickpos[1])
        self.cam.pan[3] = self.lastClick.cam.pan[3] + s * (y-self.clickpos[2])
    end

    if self.holding == 0 then
        if bit.band(self.click_mods, glfw.GLFW.MOD_ALT) ~= 0 then
            translateModel(x,y,s)
        else
            if self.use_arcball then
                rotateModelArcball(x,y,s)
            else
                rotateModel(x,y,s)
            end
        end
    elseif self.holding == 1 then -- Right click
        if bit.band(self.click_mods, glfw.GLFW.MOD_ALT) ~= 0 then
            viewStrafeXZ(x,y,s)
        else
            viewLook(x,y,s)
        end
    elseif self.holding == 2 then -- Middle click
        if bit.band(self.click_mods, glfw.GLFW.MOD_ALT) ~= 0 then
            viewLook(x,y,s)
        else
            viewStrafeXY(x,y,s)
        end
    end
end

function camera4:onwheel(x,y)
    local s = 1
    if self.ctrldown then s = s * 10 end
    if self.shiftdown then s = s / 10 end

    local fwd = {0,0,1,0}
    fwd = mm.transform(fwd, self:getViewMatrix())
    --self.cam.pan[3] = self.cam.pan[3] - s * y
    for i=1,3 do
        self.cam.pan[i] = self.cam.pan[i] - s*y*fwd[i]
    end
end

function camera4:timestep(absTime, dt)
    local txmv = self.keymove

    if true then
        local mv = {}
        for i=1,3 do
            mv[i] = self.keymove[i]
        end
        mv[4] = 0
        txmv = mm.transform(mv, self:getViewMatrix())
    end

    for i=1,3 do
        self.cam.chassis[i] = self.cam.chassis[i] + dt * txmv[i]
    end
end

function camera4:getModelMatrix()
    local m = {}
    mm.make_identity_matrix(m)
    mm.glh_translate(m,
        self.obj.trans[1], 
        self.obj.trans[2], 
        self.obj.trans[3])

    if self.use_arcball then
        -- Arcball rotation
        mm.post_multiply(m, self.obj.arcball)
    else
        mm.glh_rotate(m, self.obj.rot[1], 0,1,0)
        mm.glh_rotate(m, self.obj.rot[2], 1,0,0)
    end

    return m
end

function camera4:getViewMatrix()
    local v = {}
    mm.make_identity_matrix(v)

    -- Lookaround camera
    mm.glh_translate(v, self.cam.chassis[1], self.cam.chassis[2], self.cam.chassis[3])
    mm.glh_translate(v, self.cam.pan[1], self.cam.pan[2], self.cam.pan[3])

    mm.glh_rotate(v, -self.cam.rot[1], 0,1,0)
    mm.glh_rotate(v, -self.cam.rot[2], 1,0,0)

    return v
end

-- Return a list of strings(lines) describing commands and state
function camera4:stateStrings()
    local panstr = 'pan: '
        ..tostring(self.cam.pan[1])..','
        ..tostring(self.cam.pan[2])..','
        ..tostring(self.cam.pan[3])

    local chasstr = 'chassis: '
        ..tostring(self.cam.chassis[1])..','
        ..tostring(self.cam.chassis[2])..','
        ..tostring(self.cam.chassis[3])

    local posstr = 'pos: '
        ..tostring(self.cam.pan[1] + self.cam.chassis[1])..','
        ..tostring(self.cam.pan[2] + self.cam.chassis[2])..','
        ..tostring(self.cam.pan[3] + self.cam.chassis[3])

    local rotstr = 'Rot: '
        ..tostring(self.cam.rot[1])..','
        ..tostring(self.cam.rot[2])

    local objtstr = 'Obj Pos: '
        ..tostring(self.obj.trans[1])..','
        ..tostring(self.obj.trans[2])..','
        ..tostring(self.obj.trans[3])
    
    local objrstr = 'Obj Rot: '
        ..tostring(self.obj.rot[1])..','
        ..tostring(self.obj.rot[2])

    return {
       '',
        --panstr,
        --chasstr,
        posstr,
        rotstr,
        objtstr,
        objrstr,
    }
end

function camera4:commandHelp()
    local list = self:stateStrings()
    local cmds = {
        'camera4 help',
        'Commands: reset arcball thetaphi',
    }
    for k,v in pairs(cmds) do
        table.insert(list,v)
    end
    return list
end

function camera4:handleCommand(args)
    if #args < 1 then return end

    if args[1] == 'reset' then
        self:reset()
    elseif args[1] == 'arcball' then
        self.use_arcball = true
    elseif args[1] == 'thetaphi' then
        self.use_arcball = false
    end
end

return camera4
