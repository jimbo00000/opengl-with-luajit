-- filesystem.lua

local filesystem = {}

filesystem.dataDirectory = ''

function filesystem.exists(filename)
    local file = io.open(filename, "r")
    if file ~= nil then
        assert(file:close())
        return true
    else
        return false
    end
end

function filesystem.read(filename) -- TODO:, size)

    --TODO: check multiple paths
    filename = filesystem.dataDirectory..'/'..filename

    local file = assert(io.open(filename, "rb"))
    local size = nil

    if size == nil then
        local current = file:seek()      -- get current position
        local sizeBytes = file:seek("end")    -- get file size
        file:seek("set", current)        -- restore position
        size = sizeBytes

        --print('Read entire file. Size=',size)
    end

    local data = file:read("*all")
    assert(file:close())
    return data, size
end

-- Does not return an iterator, but pulls all lines of the file into a table.
function filesystem.lines(filename)
  
    --TODO: check multiple paths
    filename = filesystem.dataDirectory..'/'..filename
    
    local file = assert(io.open(filename, "rt"))
    local linetable = {}
    for line in file:lines() do
        table.insert(linetable, line)
    end
    io.close(file)
    return linetable
end

return filesystem
