--[[ packagereloader.lua
Keeps a list of packages that were loaded at app startup, and on command
deletes everything else in package.loaded so they may be reloaded.
]]
local lfs = require("lfs_ffi")

PackageReloader = {}

PackageReloader.__index = PackageReloader

function PackageReloader.new(...)
    local self = setmetatable({}, PackageReloader)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function PackageReloader:init()
    self.packageKeepList = {}
    self.filesToMonitor = {}
    self.lastMonitorList = {}
    self.filesWithErrors = {}
end

function PackageReloader:clearWatchList()
    self.lastMonitorList = self.filesToMonitor
    self.filesToMonitor = {}
    self.filesWithErrors = {}
end

function PackageReloader:restoreLastWatchList()
    self.filesToMonitor = self.lastMonitorList
end

function PackageReloader:addErrorFileToList(errmodulename)
    table.insert(self.filesWithErrors, errmodulename)
    self.filesToMonitor[errmodulename] = nil
end

function PackageReloader:initKeepList()
    for k,v in pairs(package.loaded) do
        table.insert(self.packageKeepList, k)
        self.packageKeepList[k] = true
        print(k)
    end
    table.sort(self.packageKeepList, function(a, b) return a:upper() < b:upper() end)
end

function PackageReloader:reloadAllScenePackages()
    for name,_ in pairs(package.loaded) do
        if not self.packageKeepList[name] then
            package.loaded[name] = nil
            print('    Reloading ',name)
        end
    end

    -- First time can put =bjects into a "finalized" state, 2nd time deletes them.
    collectgarbage()
    collectgarbage()
end

function PackageReloader:packageNameToPathName(pack)
    local fullname = lfs.currentdir()
    for a,b in string.gmatch(pack, "([^.]*).(.*)") do
        fullname = fullname.. '/'..a..'/'..b..'.lua'
    end
    return fullname
end


function PackageReloader:pollForFileChanges()
    if not lfs then
        print("Error: lfs is required - not found.")
        return
    end

    -- First populate list with loaded modules
    local watchList = {}
    for name,_ in pairs(package.loaded) do
        watchList[name] = 1
    end
    -- Then add files that recently showed errors.
    for _,name in pairs(self.filesWithErrors) do
        watchList[name] = 1
    end

    for name,_ in pairs(watchList) do
        if not self.packageKeepList[name] then
            if self.filesToMonitor[name] then
                local f = self.filesToMonitor[name]
                
                local t = lfs.attributes(f.pathname)
                if f.lasttime ~= t.modification then
                    f.lasttime = t.modification

                    print("Change detected: ", f.pathname, f.lasttime)
                    self:reloadAllScenePackages()
                    return true
                end
            else
                local newfile = {}
                newfile.packname = name
                newfile.pathname = self:packageNameToPathName(name)

                local t = lfs.attributes(newfile.pathname)
                if t then
                    newfile.lasttime = t.modification
                    self.filesToMonitor[name] = newfile
                    --print("monitoring new file", name, newfile.pathname)
                end
            end
        end
    end

    return false
end

return PackageReloader
