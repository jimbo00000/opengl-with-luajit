-- util_bunnymark.lua

local ffi = require 'ffi'
local mm = require 'util.matrixmath'

util_bunnymark = {}

function util_bunnymark.load_texture_from_raw(texfilename, w, h)
    local inp = assert(io.open(texfilename, "rb"))
    local data = inp:read("*all")
    assert(inp:close())
    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    local texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
                  w, h, 0,
                  GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
    return texID
end


function util_bunnymark.render_text_string(glfont, str)
    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    glfont:render_string(m, p, col, str)
    mm.glh_translate(m, 30, 30, 0)
end

--
-- Instance position functions
--
function util_bunnymark.randomize_positions(offset_array, n_quads)
    local function randPos() return -1+2*math.random() end
    for i=0,n_quads-1 do
        offset_array[2*i] = randPos()
        offset_array[2*i+1] = randPos()
    end
end

function util_bunnymark.lua_gridPositions(offset_array, n_quads)
    local dim = math.sqrt(n_quads)
    for i=0,n_quads-1 do
        local xdim = math.fmod(i, dim)
        local ydim = math.floor(i/dim)
        offset_array[2*i] = -1+2*(xdim/dim)
        offset_array[2*i+1] = -1+2*(ydim/dim)
    end
end

function util_bunnymark.burst_position_velocity(offset_array, velocity_array, n_quads)
    math.randomseed(1)
    local x,y = 0,.5
    local function randV() return -.05+.1*math.random() end
    for i=0,n_quads-1 do
        offset_array[2*i] = x
        offset_array[2*i+1] = y
        local vx, vy = randV(), randV()
        velocity_array[2*i] = vx
        velocity_array[2*i+1] = vy
    end
end

-- Transcribed from https://github.com/rishavs/love2d-bunnymark
function util_bunnymark.lua_incrementFloats(offset_array, velocity_array, n_quads)
    local gravity = -0.0098 * .5
    local minX, maxX = -1,1
    local minY, maxY = -1,1

    for i=0,n_quads-1 do
        local xidx = 2*i
        local yidx = xidx+1

        local tempPosX = offset_array[xidx]
        local tempPosY = offset_array[yidx]
        local tempVelX = velocity_array[xidx]
        local tempVelY = velocity_array[yidx]

        tempPosX = tempPosX + tempVelX
        tempPosY = tempPosY + tempVelY

        tempVelY = tempVelY + gravity

        if tempPosX > maxX then
            tempVelX = tempVelX * -0.9
            tempPosX = maxX
        elseif tempPosX < minX then
            tempVelX = tempVelX * -0.9
            tempPosX = minX
        end

        if tempPosY > maxY then
            tempVelY = tempVelY * -0.9
            tempPosY = maxY
        elseif tempPosY < minY then
            tempVelY = tempVelY * -0.9
            tempPosY = minY
        end

        -- Re-assign values into array
        offset_array[xidx] = tempPosX;
        offset_array[yidx] = tempPosY;
        velocity_array[xidx] = tempVelX;
        velocity_array[yidx] = tempVelY;
    end
end

-- Modifies the VBO memory in-place, adding offsets to quad verts on CPU.
function util_bunnymark.lua_assembleQuadArray(offsets, attributes, num)
    local o = 0
    for i=0,num-1 do
        local x = offsets[2*i ]
        local y = offsets[2*i+1]

        for j=1,6 do
            attributes[o  ] = x
            attributes[o+1] = y
            o = o + 2
        end
    end
end

function util_bunnymark.loadLibrary()
    ffi.cdef[[
        typedef float value_t;
        //typedef short value_t; // ARM only

        void gridPositions(value_t* positions, int num);
        void incrementFloats(value_t* positions, value_t* velocities, int num);
        void assembleQuads(value_t* offsets, value_t* attributes, int num);
        void printFloats(value_t* f, int num);
    ]]

    -- Remember to run on desktop with LD_LIBRARY_PATH set
    local libpaths = {
        '/home/pi/RetroPie/roms/jouleplating/benchmarks/', --TODO: query this from Retropie
        './piscene/benchmarks/',
        './csrc/',
    }
    --pi@retropie:~/code/csrc $ make && cp libfunclib.so ~/RetroPie/roms/jouleplating/benchmarks/

    -- Hold a reference to the lib object so it doesn't fall out of scope
    -- Segfaults will result if it does.
    util_bunnymark.funcLib = nil
    for i=1,#libpaths do
        local libpath = libpaths[i]
        local status,err = pcall(function ()
            util_bunnymark.funcLib = ffi.load(libpath..'libfunclib.so')
            end)
        if status then
            print('Found libfunclib.so in '..libpath)
            break
        end
    end

    if util_bunnymark.funcLib then
        print('Using native update func library', util_bunnymark.funcLib)
    else
        print('Using lua update functions.')
    end
end

function util_bunnymark.getGridInitFunction()
    if util_bunnymark.funcLib then
        return util_bunnymark.funcLib.gridPositions
    end
    return util_bunnymark.lua_gridPositions
end

function util_bunnymark.getUpdateFunction()
    if util_bunnymark.funcLib then
        return util_bunnymark.funcLib.incrementFloats
    end
    return util_bunnymark.lua_incrementFloats
end

function util_bunnymark.getAssembleQuadFunction()
    if util_bunnymark.funcLib then
        return util_bunnymark.funcLib.assembleQuads
    end
    return util_bunnymark.lua_assembleQuadArray
end

return util_bunnymark
