--[[ simple_shader.lua

    A shader that passes dot locations in through a uniform.
    Does string substitution to pass the number of dots to
    shader for static array allocation.
]]

local DO_FONTS = true

simple_shader = {}
simple_shader.__index = simple_shader

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local glf = require 'util.glfont'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'

function simple_shader.new(...)
    local self = setmetatable({}, simple_shader)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_shader:setDataDirectory(dir)
    self.dataDir = dir
    if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_shader:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}
    self.n_dots = 32
    self.draw_font = true
end

local vs=[[
#version 100
attribute vec2 vP;
varying vec3 vfC;
void main()
{
    vfC=vec3(vP,0.);
    gl_Position=vec4(vP,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
uniform float uBlue;

#define NUM_DOTS $NUM_DOTS
uniform vec4 uOffsets[NUM_DOTS];

vec3 getDotCol(vec2 pos, vec4 dotPosColRad, float radius, vec3 bgCol)
{
    vec2 dotPos = dotPosColRad.xy;
    float dist = length(pos - dotPos);

    if (dist < (.05 + .025*dotPosColRad.w))
    {
        vec3 col = vec3(1.);
        col.g = dotPosColRad.z;
        return col;
    }
    return bgCol;
}

void main()
{
    vec3 col = .5*vfC+vec3(.5);

    // dot loop
    {
        float radius = .03;        
        vec2 pos = .5*vfC.xy+vec2(.5);
        vec3 bgCol = vec3(.1);
        col = bgCol;

        for (int i=0; i<NUM_DOTS; ++i)
        {
            col = getDotCol(pos, uOffsets[i], radius, col);
        }
    }
    col.b = uBlue;
    gl_FragColor = vec4(col,1.);
}
]]

function simple_shader:populateVBO()
    local v = ffi.typeof('GLfloat[?]')(12,{-1,-1,1,-1,1,1,-1,-1,1,1,-1,1,})
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(v), v, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_shader:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_shader:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function simple_shader:initShader()

    gl.glDeleteProgram(self.prog)
    self.prog = 0

    math.randomseed(1)
    self.offsets = {}
    for i=1,self.n_dots do
        --table.insert(self.offsets,math.random())

        local dim = math.ceil(math.sqrt(self.n_dots))
        local a,b = math.modf((i-1)/dim)
        local off = {
            a/dim + .5/dim,
            b + .5/dim
        }
        table.insert(self.offsets,off[1])
        table.insert(self.offsets,off[2])
        table.insert(self.offsets,math.random())
        table.insert(self.offsets,math.random())
    end

    self.velocities = {}
    for i=1,4*self.n_dots do
        table.insert(self.velocities,2*math.random()-1)
    end

    local fragSrc = string.format("%s", fs)
    fragSrc = string.gsub(fragSrc, "$NUM_DOTS", tostring(self.n_dots))
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fragSrc})
end

function simple_shader:initGL()
    local unisz = ffi.typeof('GLint[?]')(0)
    gl.glGetIntegerv(GL.GL_MAX_VERTEX_UNIFORM_VECTORS, unisz)
    print('GL_MAX_VERTEX_UNIFORM_VECTORS',unisz[0])

    self:initShader()

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function simple_shader:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    if self.glfont then
        self.glfont:exitGL()
    end
end

function simple_shader:render()
    gl.glClearColor(0,1,0,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")

    -- NOTE: rpi requires re-setting VBO data each frame. Why?
    --gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos[1][0])
    self:bindVBO()

    gl.glUniform1f(u_loc, b)


    -- instance positions
    local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffsets")
    local o = ffi.typeof('GLfloat[?]')(self.n_dots*4, self.offsets)
    gl.glUniform4fv(uoff_loc, self.n_dots, o)


    gl.glDrawArrays(GL.GL_TRIANGLES,0,6)

    if self.draw_font then
        self:drawtext()
    end

    self:unbindVBO()
end

function simple_shader:drawtext()
    if not self.glfont then return end
    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    self.glfont:render_string(m, p, col, "simple_shader_dots "..self.n_dots)
end

function simple_shader:timestep(absTime, dt)
    self.t = absTime

    movedots = true
    if movedots then
        for i=1,4*self.n_dots do
            self.offsets[i] = self.offsets[i] + self.velocities[i] * .001*math.sin(absTime)
        end
    end
end

function simple_shader:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function simple_shader:onButtonPressed(padIndex, button)
    self.t_last = self.t
    
    local soundmap = {
        [lr_input.JOYPAD_A] = "pop_drip.wav",
        [lr_input.JOYPAD_B] = "Jump5.wav",
        [lr_input.JOYPAD_X] = "Pickup_Coin7.wav",
        [lr_input.JOYPAD_Y] = "test.wav",
    }
    local s = soundmap[button]
    if s then snd.playSound(s) end


    if button == lr_input.JOYPAD_X then
        self.n_dots = self.n_dots * 2
        self.n_dots = math.min(self.n_dots, 256)
        self:initShader()
    elseif button == lr_input.JOYPAD_Y then
        self.n_dots = math.floor(self.n_dots * .5)
        self.n_dots = math.max(self.n_dots, 1)
        self:initShader()

    elseif button == lr_input.JOYPAD_A then
        self.n_dots = self.n_dots + 1
        self.n_dots = math.min(self.n_dots, 256)
        self:initShader()
    elseif button == lr_input.JOYPAD_B then
        self.n_dots = self.n_dots -1
        self.n_dots = math.max(self.n_dots, 1)
        self:initShader()

    elseif button == lr_input.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    end
end

return simple_shader
