--[[ bunnymark1_naive.lua

    A naive CPU loop over quad positions in an ffi-allocated array.
]]

local DO_FONTS = true

simple_quads = {}
simple_quads.__index = simple_quads

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'
local util_bunny = require 'util_bunnymark'

function simple_quads.new(...)
    local self = setmetatable({}, simple_quads)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_quads:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_quads:init()
    self.vbos = {}
    self.prog = 0
    self.buttons = {}

    self.n_quads = 32
    self:initInstances()

    self.draw_font = true
    self.do_alpha = true
    self.do_motion = true

    util_bunny.loadLibrary()
    self.updateFunc = util_bunny.getUpdateFunction()
    self.gridInitFunc = util_bunny.getGridInitFunction()
end

function simple_quads:initInstances()
    self.offset_array = ffi.typeof('GLfloat[?]')(self.n_quads*2)
    self.velocity_array = ffi.typeof('GLfloat[?]')(self.n_quads*2)
    util_bunny.burst_position_velocity(self.offset_array, self.velocity_array, self.n_quads)
end

------------------------------------------
-- OpenGL/GLES concerns:
-- Texture, VBO, shader, initGL/exitGL
------------------------------------------
function simple_quads:loadTexture(texfilename, w, h)
    if self.dataDir then texfilename = self.dataDir .. '/images/' .. texfilename end
    return util_bunny.load_texture_from_raw(texfilename, w, h)
end

------------------------------------------
-- VBO: vertex input to GPU draw
------------------------------------------
function simple_quads:bindVBO()
    local vlocation = gl.glGetAttribLocation(self.shader.prog, 'vPos')
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vlocation, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vlocation)
end

function simple_quads:unbindVBO()
    local vlocation = gl.glGetAttribLocation(self.shader.prog, 'vPos')
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vlocation)
end

function simple_quads:populateVBO()
    local s = 1
    local v = ffi.typeof('GLfloat[?]')(12,{0,0,s,0,s,s,0,0,s,s,0,s,})
    local vlocation = gl.glGetAttribLocation(self.shader.prog, 'vPos')
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(v), v, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vlocation, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vlocation)
end

local vs=[[
#version 100

#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 uOffset;
uniform float uScale;

attribute vec2 vPos;
varying vec3 vfUV;

void main()
{
    vfUV = vec3(vPos, 0.);
    vec2 pt = uScale * vPos.xy;
    gl_Position = vec4(pt + uOffset, 0., 1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfUV;

uniform sampler2D uTex;

void main()
{
    vec2 tc = vfUV.xy;
    gl_FragColor = texture2D(uTex, tc);
}
]]

function simple_quads:initGL()
    local shader = {
        prog = sf.make_shader_from_source({vsrc=vs, fsrc=fs}),
        uniforms = {},
        attribs = {}
    }
    local unis = {
        'uScale',
        'uOffset',
        'uTex',
    }
    local function uni(var) return gl.glGetUniformLocation(shader.prog, var) end
    for k,v in pairs(unis) do
        shader.uniforms[v] = uni(v)
    end

    local attribs = {
        'vPos',
    }
    local function attr(var) return gl.glGetAttribLocation(shader.prog, var) end
    for k,v in pairs(attribs) do
        shader.attribs[v] = attr(v)
    end

    self.shader = shader

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()

    self.texID = self:loadTexture('frog1.data', 32, 32)

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir..'/fonts')
        self.glfont:initGL()
    end
end

function simple_quads:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    gl.glDeleteProgram(self.shader.prog)
    self.shader = nil

    local texId = ffi.new("int[1]")
    texId[0] = self.texID
    gl.glDeleteTextures(1, texId)
    self.texID = nil

    if self.glfont then
        self.glfont:exitGL()
    end
end

function simple_quads:render()
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    if self.do_alpha then
        gl.glEnable(GL.GL_BLEND)
    else
        gl.glDisable(GL.GL_BLEND)
    end

    gl.glUseProgram(self.shader.prog)

    gl.glUniform1f(self.shader.uniforms.uScale, .4/math.pow(self.n_quads,.25))

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glUniform1i(self.shader.uniforms.uTex, 0)

    self:bindVBO()

    -- Here's the slow part
    for i=0,self.n_quads-1 do
        local off = {self.offset_array[2*i],self.offset_array[2*i+1]}
        gl.glUniform2f(self.shader.uniforms.uOffset, off[1], off[2])

        gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    end

    self:unbindVBO()

    if self.glfont and self.draw_font then
        local str = "bunnymark1_naive n="..self.n_quads
        util_bunny.render_text_string(self.glfont, str)
    end
end

function simple_quads:timestep(absTime, dt)
    if self.do_motion and self.updateFunc then
        self.updateFunc(self.offset_array, self.velocity_array, self.n_quads)
    end
end

function simple_quads:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function simple_quads:onButtonPressed(padIndex, button)
    if button == lr_input.JOYPAD_A then
        print('hit A!')
    elseif button == lr_input.JOYPAD_B then
        self.do_alpha = not self.do_alpha

    elseif button == lr_input.JOYPAD_UP then
        self.n_quads = self.n_quads * 2
        self:initInstances()
    elseif button == lr_input.JOYPAD_DOWN then
        self.n_quads = self.n_quads * .5
        self.n_quads = math.max(self.n_quads, 1)
        self:initInstances()

    elseif button == lr_input.JOYPAD_LEFT then
        util_bunny.randomize_positions(self.offset_array, self.n_quads)
    elseif button == lr_input.JOYPAD_RIGHT then
        self.gridInitFunc(self.offset_array, self.n_quads)

    elseif button == lr_input.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    elseif button == lr_input.JOYPAD_START then
        self.do_motion = not self.do_motion
    end
end

return simple_quads
