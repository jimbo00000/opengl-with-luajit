--[[ fontlorem.lua

]]

fontlorem = {}
fontlorem.__index = fontlorem

local ffi = require 'ffi'
local mm = require 'util.matrixmath'
local glf = require 'util.glfont'
local lr_input = require 'util.libretro_input'

function fontlorem.new(...)
    local self = setmetatable({}, fontlorem)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function fontlorem:setDataDirectory(dir)
    self.dataDir = dir
end

function fontlorem:init()
    self.max_lines = 5
    self.max_line_len = 32
    self.scale_fac = 4
end


function fontlorem:initGL()
    -- Load text file
    self.lines = {}
    local filename = "loremipsumbreaks.txt"
    if self.dataDir then filename = self.dataDir .. "/" .. filename end
    local file = io.open(filename)
    if file then
        for line in file:lines() do
            table.insert(self.lines, line)
        end
    end

    if true then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function fontlorem:exitGL()
end

function fontlorem:render()

    gl.glDisable(GL.GL_DEPTH_TEST)


    local p = {}
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)


    local lineh = self.glfont.font.common.lineHeight

    local m = {}
    local s = self.scale_fac / self.max_lines
    mm.make_identity_matrix(m)
    mm.glh_translate(m, 2, 1.2, 0)
    mm.glh_scale(m, s, s, s)
    --mm.pre_multiply(m, view)

    local col = {1, 1, 1}

    local i = 0
    for k,v in pairs(self.lines) do
        local str = string.sub(v,0,self.max_line_len)
        self.glfont:render_string(m, p, col, str)
        mm.glh_translate(m, 0, lineh, 0)
        i = i + 1
        if i >= self.max_lines then return end
    end
end

function fontlorem:timestep(absTime, dt)
end

function fontlorem:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:on_button_pressed(j,b)
                end
            end
        end
    end
end

function fontlorem:on_button_pressed(padIndex, button)
    if button == lr_input.JOYPAD_X then
        self.max_lines = self.max_lines + 1
    elseif button == lr_input.JOYPAD_Y then
        self.max_lines = math.max(1, self.max_lines - 1)
    elseif button == lr_input.JOYPAD_A then
        self.max_line_len = self.max_line_len * 2
        self.max_line_len = math.min(self.max_line_len, 128)
    elseif button == lr_input.JOYPAD_B then
        self.max_line_len = self.max_line_len / 2
        self.max_line_len = math.max(self.max_line_len, 1)
    elseif button == lr_input.JOYPAD_L then
        self.scale_fac = self.scale_fac * .85
    elseif button == lr_input.JOYPAD_R then
        self.scale_fac = self.scale_fac / .85
    end
end

return fontlorem
