--[[ bunnymark1_naive.lua

    A naive CPU loop over quad positions in an ffi-allocated array.
]]

local DO_FONTS = true

simple_quads = {}
simple_quads.__index = simple_quads

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'

function simple_quads.new(...)
    local self = setmetatable({}, simple_quads)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_quads:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_quads:init()
    self.vbos = {}
    self.prog = 0
    self.buttons = {}

    self.n_quads = 32
    self:initInstances()

    self.draw_font = true
    self.do_alpha = true
    self.do_motion = true
    self.do_random = true
    self.off = {-.5,-.5}
    self.texdim = 32
end

function simple_quads:initInstances()
end

------------------------------------------
-- OpenGL/GLES concerns:
-- Texture, VBO, shader, initGL/exitGL
------------------------------------------
function simple_quads:createRandomTexture(w, h)
    local szbytes = w * h * 4
    local data = ffi.typeof('GLubyte[?]')(szbytes)
    self.texdata = data
    for i=0,szbytes-1 do
        data[i] = math.random(0,255)
    end

    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    local texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
                  w, h, 0,
                  GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)

    return texID
end

function simple_quads:reinitializeRandomTexture()
    if self.texID and self.texID ~= 0 then
        local texId = ffi.new("int[1]")
        texId[0] = self.texID
        gl.glDeleteTextures(1, texId)
        self.texID = nil
    end

    local maxsz = self.texdim
    self.texID = self:createRandomTexture(maxsz, maxsz)
end

------------------------------------------
-- VBO: vertex input to GPU draw
------------------------------------------
function simple_quads:bindVBO()
    local vlocation = gl.glGetAttribLocation(self.shader.prog, 'vPos')
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vlocation, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vlocation)
end

function simple_quads:unbindVBO()
    local vlocation = gl.glGetAttribLocation(self.shader.prog, 'vPos')
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vlocation)
end

function simple_quads:populateVBO()
    local s = 1
    local v = ffi.typeof('GLfloat[?]')(12,{0,0,s,0,s,s,0,0,s,s,0,s,})
    local vlocation = gl.glGetAttribLocation(self.shader.prog, 'vPos')
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(v), v, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vlocation, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vlocation)
end

local vs=[[
#version 100

#ifdef GL_ES
precision lowp float;
#endif

uniform vec2 uOffset;
uniform float uScale;

attribute vec2 vPos;
varying vec2 vfUV;

void main()
{
    vec2 offsetPt = (uScale * vPos.xy) + uOffset;
    vfUV = vec2(.5) + .5*offsetPt;
    gl_Position = vec4(offsetPt, 0., 1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec2 vfUV;

uniform sampler2D uTex;

void main()
{
    vec2 tc = vfUV;
    gl_FragColor = texture2D(uTex, tc);
}
]]

function simple_quads:initGL()
    local unisz = ffi.typeof('GLint[?]')(0)
    local vals = {
        'GL_MAX_TEXTURE_SIZE',
        'GL_MAX_RENDERBUFFER_SIZE',
        'GL_MAX_VIEWPORT_DIMS',
    }
    for k,v in pairs(vals) do
        gl.glGetIntegerv(GL[v], unisz)
        print(v, unisz[0])
    end

    local shader = {
        prog = sf.make_shader_from_source({vsrc=vs, fsrc=fs}),
        uniforms = {},
        attribs = {}
    }
    local unis = {
        'uScale',
        'uOffset',
        'uTex',
    }
    local function uni(var) return gl.glGetUniformLocation(shader.prog, var) end
    for k,v in pairs(unis) do
        shader.uniforms[v] = uni(v)
    end

    local attribs = {
        'vPos',
    }
    local function attr(var) return gl.glGetAttribLocation(shader.prog, var) end
    for k,v in pairs(attribs) do
        shader.attribs[v] = attr(v)
    end

    self.shader = shader

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()

    self:reinitializeRandomTexture()

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir..'/fonts')
        self.glfont:initGL()
    end
end

function simple_quads:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    gl.glDeleteProgram(self.prog)
    self.prog = 0

    local texId = ffi.new("int[1]")
    texId[0] = self.texID
    gl.glDeleteTextures(1, texId)
    self.texID = nil

    if self.glfont then
        self.glfont:exitGL()
    end
end

function simple_quads:render()
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    if self.do_alpha then
        gl.glEnable(GL.GL_BLEND)
    else
        gl.glDisable(GL.GL_BLEND)
    end

    gl.glUseProgram(self.shader.prog)

    gl.glUniform1f(self.shader.uniforms.uScale, 2./math.pow(self.n_quads,.25))

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glUniform1i(self.shader.uniforms.uTex, 0)

    self:bindVBO()
    do
        local off = self.off
        gl.glUniform2f(self.shader.uniforms.uOffset, off[1], off[2])

        gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    end
    self:unbindVBO()

    if self.draw_font and self.glfont then
        local m = {}
        local p = {}
        mm.make_identity_matrix(m)
        local s = .5
        mm.glh_scale(m, s, s, s)
        mm.glh_translate(m, 30, 30, 0)
        -- TODO getStringWidth and center text
        if not win_w then
            win_w,win_h = 800,600
        end
        mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
        gl.glDisable(GL.GL_DEPTH_TEST)
        local col = {1, 1, 1}
        self.glfont:render_string(m, p, col, "tex_stress n="..self.texdim)
    end
end

function simple_quads:timestep(absTime, dt)
    if self.do_motion then
        if self.do_random then
            self.off = {-1+2*math.random(), -1+2*math.random()}
        else
            self.off = --{-.5,-.5}
                {-.4+.7*math.sin(absTime), -.4+.7*math.cos(absTime)}
        end
    end
end

function simple_quads:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function simple_quads:onButtonPressed(padIndex, button)
    if button == lr_input.JOYPAD_A then
        print('hit A!')
    elseif button == lr_input.JOYPAD_B then
        self.do_alpha = not self.do_alpha

    elseif button == lr_input.JOYPAD_X then
        self.do_random = not self.do_random

    elseif button == lr_input.JOYPAD_UP then
        self.n_quads = self.n_quads * 2
        self:initInstances()
    elseif button == lr_input.JOYPAD_DOWN then
        self.n_quads = self.n_quads * .5
        self.n_quads = math.max(self.n_quads, 1)
        self:initInstances()


    elseif button == lr_input.JOYPAD_RIGHT then
        self.texdim = self.texdim * 2
        self.texdim = math.min(self.texdim, 2048)
        self:reinitializeRandomTexture()
    elseif button == lr_input.JOYPAD_LEFT then
        self.texdim = self.texdim * .5
        self.texdim = math.max(self.texdim, 1)
        self:reinitializeRandomTexture()


    elseif button == lr_input.JOYPAD_LEFT then
    elseif button == lr_input.JOYPAD_RIGHT then

    elseif button == lr_input.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    elseif button == lr_input.JOYPAD_START then
        self.do_motion = not self.do_motion
    end
end

return simple_quads
