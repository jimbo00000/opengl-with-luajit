--[[ bunnymark4_pseudoinst.lua

]]

local DO_FONTS = true

local simple_quads = {}
simple_quads.__index = simple_quads

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions2'
local mm = require 'util.matrixmath'
local lri = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'
local util_bunny = require 'util_bunnymark'

function simple_quads.new(...)
    local self = setmetatable({}, simple_quads)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_quads:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_quads:init()
    self.t = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}

    self.n_quads = 32
    self.chunkSz = 256

    self.draw_font = true
    self.do_alpha = true
    self.do_motion = true

    util_bunny.loadLibrary()
    self.updateFunc = util_bunny.getUpdateFunction()
    self.gridInitFunc = util_bunny.getGridInitFunction()
end

-- An array of {position,velocity} tuples for drawing
function simple_quads:initInstances()
    self.offset_array = ffi.typeof('GLfloat[?]')(self.n_quads*2)
    self.velocity_array = ffi.typeof('GLfloat[?]')(self.n_quads*2)
    util_bunny.burst_position_velocity(self.offset_array, self.velocity_array, self.n_quads)
    self:populateVBO()
end

------------------------------------------
-- OpenGL/GLES concerns:
-- Texture, VBO, shader, initGL/exitGL
------------------------------------------
function simple_quads:loadTextures(texfilename, w, h)
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end
    self.texID = util_bunny.load_texture_from_raw(texfilename, w, h)
end

------------------------------------------
-- VBO: vertex input to GPU draw
------------------------------------------
function simple_quads:bindVBO()
    local vl = self.shader.attribs.vPos
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_quads:unbindVBO()
    local vl = self.shader.attribs.vPos
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

-- This function both allocates and fills storage.
function simple_quads:populateVBO()
    local all = {}
    local z = 0
    for i=1,self.n_quads do
        local s = 1

        local verts = {
            0, 0, z,
            s, 0, z,
            s, s, z,
            0, 0, z,
            s, s, z,
            0, s, z,
        }

        for j=1,#verts do
            table.insert(all, verts[j])
        end
        z = z + 1
    end
    local av = ffi.typeof('GLfloat[?]')(#all,all)

    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(av),av,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,3,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
end

local vs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform float uScale;
uniform vec2 uOffsets[256];
uniform float uNumInsts;

attribute vec3 vPos;
varying vec2 vfC;

void main()
{
    int id = int(vPos.z);
    vec2 Offset = uOffsets[id];
    vfC = vPos.xy;
    vec2 pt = (uScale * vPos.xy);
    gl_Position = vec4(pt+Offset,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec2 vfC;

uniform sampler2D tex;

void main()
{
    vec2 tc = vfC;
    gl_FragColor = texture2D(tex, tc);
}
]]

function simple_quads:initGL()
    local unis = {
        'uScale',
        'uNumInsts',
        'uOffsets',
        'tex',
    }
    local attrs = {
        'vPos',
    }
    local err = nil
    self.shader, err = sf.make_shader_object({vsrc=vs,fsrc=fs}, unis, attrs)


    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:initInstances()

    self.texID = 0
    self:loadTextures('frog1.data', 32, 32)

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function simple_quads:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    if self.shader then
        gl.glDeleteProgram(self.shader.prog)
    end

    if self.texID then
        local texId = ffi.new("int[1]")
        texId[0] = self.texID
        gl.glDeleteTextures(1, texId)
        self.texID = nil
    end

    if self.glfont then
        self.glfont:exitGL()
    end
end

function simple_quads:render()
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    if self.do_alpha then
        gl.glEnable(GL.GL_BLEND)
    else
        gl.glDisable(GL.GL_BLEND)
    end

    local shader = self.shader
    gl.glUseProgram(shader.prog)

    gl.glUniform1f(shader.uniforms.uScale, .4/math.pow(self.n_quads,.25))

    local chunkSz = math.min(self.chunkSz, self.n_quads)
    gl.glUniform1f(shader.uniforms.uNumInsts, chunkSz)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glUniform1i(shader.uniforms.tex, 0)

    -- instance positions
    self:bindVBO()
    local i=0
    --TODO: handle incomplete chunks
    for i=0, self.n_quads-1,chunkSz do
        gl.glUniform2fv(shader.uniforms.uOffsets, chunkSz, self.offset_array+2*i)
        gl.glDrawArrays(GL.GL_TRIANGLES, 0, 6*chunkSz)
    end
    self:unbindVBO()

    if self.glfont and self.draw_font then
        local str = '4_pseudo n='..self.n_quads
        str = str..' c='..self.chunkSz
        local ndraws = math.max(1,math.floor(self.n_quads/self.chunkSz))
        str = str..' draws='..ndraws
        util_bunny.render_text_string(self.glfont, str)
    end
end

------------------------------------------
-- Timestep
------------------------------------------
function simple_quads:timestep(absTime, dt)
    if self.do_motion and self.updateFunc then
        self.updateFunc(self.offset_array, self.velocity_array, self.n_quads)
    end
end

------------------------------------------
-- Control
------------------------------------------
function simple_quads:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function simple_quads:onButtonPressed(padIndex, button)
    if button == lri.JOYPAD_Y then
        snd.playSound("test.wav")
        self.chunkSz = self.chunkSz * 2
        self.chunkSz = math.min(self.chunkSz, 256)
    elseif button == lri.JOYPAD_X then
        snd.playSound("pop_drip.wav")
        self.chunkSz = self.chunkSz * .5
        self.chunkSz = math.max(self.chunkSz, 1)

    elseif button == lri.JOYPAD_B then
        self.do_alpha = not self.do_alpha

    elseif button == lri.JOYPAD_UP then
        self.n_quads = self.n_quads * 2
        self.n_quads = math.min(self.n_quads, 1024*1024)
        self:initInstances()
    elseif button == lri.JOYPAD_DOWN then
        self.n_quads = self.n_quads * .5
        self.n_quads = math.max(self.n_quads, 1)
        self:initInstances()

    elseif button == lri.JOYPAD_LEFT then
        util_bunny.randomize_positions(self.offset_array, self.n_quads)
    elseif button == lri.JOYPAD_RIGHT then
        self.gridInitFunc(self.offset_array, self.n_quads)

    elseif button == lri.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
        
    elseif button == lri.JOYPAD_START then
        self.do_motion = not self.do_motion
    end
end

return simple_quads
