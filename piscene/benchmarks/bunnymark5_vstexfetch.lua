--[[ bunnymark5_vstexfetch.lua

]]

local DO_FONTS = true

local simple_quads = {}
simple_quads.__index = simple_quads

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions2'
local mm = require 'util.matrixmath'
local lri = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'
local fbf = require "util.fbofunctions"
local util_bunny = require 'util_bunnymark'

function simple_quads.new(...)
    local self = setmetatable({}, simple_quads)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_quads:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_quads:init()
    self.vbos = {}
    self.buttons = {}

    self.n_quads = 32

    self.draw_font = true
    self.do_alpha = true
    self.do_motion = true
    self.show_datatex = false
end

-- An array of {position,velocity} tuples for drawing
function simple_quads:init_quads()
    self:populateVBO()
end

------------------------------------------
-- OpenGL/GLES concerns:
-- Texture, VBO, shader, initGL/exitGL
------------------------------------------
function simple_quads:loadTextures(texfilename, w, h)
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end
    self.texID = util_bunny.load_texture_from_raw(texfilename, w, h)
end

function simple_quads:populateTexture(dataTexID, dim)
    local w,h = dim, dim

    -- Make a diagonal line x=y
    local imgdata = {}
    for i=0,dim*dim-1 do
        --table.insert(imgdata, math.min(255, 255*i/self.n_quads))
        --table.insert(imgdata, math.min(255, 255*i/self.n_quads))
        table.insert(imgdata, 128)
        table.insert(imgdata, 192)
        table.insert(imgdata, math.floor(255*math.random()))
        table.insert(imgdata, math.floor(255*math.random()))
    end
    local data = ffi.typeof('GLubyte[?]')(#imgdata,imgdata)

    gl.glBindTexture(GL.GL_TEXTURE_2D, dataTexID)
    do
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
                      w, h, 0,
                      GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, data)
    end
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end

------------------------------------------
-- VBO: vertex input to GPU draw
------------------------------------------
function simple_quads:bindVBO()
    local vl = self.instanceshader.attribs.vP
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_quads:unbindVBO()
    local vl = self.instanceshader.attribs.vP
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

-- This function both allocates and fills storage.
function simple_quads:populateVBO()
    local all = {}
    local z = 0
    for i=1,self.n_quads do
        local s = 1

        local verts = {
            0, 0, z,
            s, 0, z,
            s, s, z,
            0, 0, z,
            s, s, z,
            0, s, z,
        }

        for j=1,#verts do
            table.insert(all, verts[j])
        end
        z = z + 1
    end
    local av = ffi.typeof('GLfloat[?]')(#all,all)

    local vl = self.instanceshader.attribs.vP
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(av),av,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,3,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
end

------------------------------------------
-- Fullscreen quad for bounce pass
------------------------------------------
function simple_quads:initFullscreenQuad()
    local vl = self.datashader.attribs.vPos
    local vvbo = self.vbos[2] ---todo table/named indexing
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    --gl.glEnableVertexAttribArray(vl)

    local s = 1
    local v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
end

function simple_quads:initBounceBuffers()
    if self.fbos then
        for k,v in pairs(self.fbos) do
            if v then
                fbf.deallocate_fbo(v)
            end
        end
    end

    local dim = math.ceil(math.sqrt(self.n_quads))
    local w,h = dim,dim
    self.fbos = {
        fbf.allocate_fbo(w, h, false),
        fbf.allocate_fbo(w, h, false),
    }
    self.pingpong01 = 0

    self:populateTexture(self.fbos[1].tex, dim)
    self:populateTexture(self.fbos[2].tex, dim)
end

local vs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform float uScale;
uniform float uSqrtNumInsts;

uniform sampler2D texData;

attribute vec3 vP;
varying vec3 vfC;

vec2 getTexCoord(float id, float dim)
{
    float x = mod(id, dim);
    float y = floor(id / dim);
    return vec2(x,y) / vec2(dim);
}

void main()
{
    vec2 texCoord = getTexCoord(vP.z+.25, uSqrtNumInsts); // Add .5 for low precision
    //int id = int(vP.z);
    texCoord += .5 / uSqrtNumInsts; // center in pixel

    vec2 Offset =
        //.5*(-1. + 2.*texCoord);
        -1. + 2.*texture2DLod(texData, texCoord, 0.).xy;

    vfC = vec3(vP.xy,0.);
    vec2 pt = (uScale * vP.xy);
    gl_Position = vec4(pt+Offset,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;

uniform sampler2D tex;

void main()
{
    vec2 tc = vfC.xy;
    gl_FragColor = texture2D(tex, tc);
}
]]

local quadvs=[[
#version 100

attribute vec2 vPos;
varying vec2 vfC;

void main()
{
    vfC = vPos;
    gl_Position = vec4(vPos, 0., 1.);
}
]]

local quadfs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec2 vfC;
uniform sampler2D uTex;

void main()
{
    vec2 tc = vec2(.5) + .5*vfC;

    vec2 pos = texture2D(uTex, tc).rg;
    vec2 vel = -1.+2.*texture2D(uTex, tc).ba;

    // Bunnymark update function
    float gravity = -0.098 * .25;
    float minX = 0.;
    float maxX = 1.;
    float minY = 0.;
    float maxY = 1.;

    pos += .05*vel;
    vel.y += gravity;

    if (pos.x > maxX)
    {
        vel.x *= -0.9;
        pos.x = maxX;
    }
    else if (pos.x < minX)
    {
        vel.x *= -0.9;
        pos.x = minX;
    }

    if (pos.y > maxY)
    {
        vel.y *= -0.9;
        pos.y = maxY;
    }
    else if (pos.y < minY)
    {
        vel.y *= -0.9;
        pos.y = minY;
    }

    vel = .5*vel + vec2(.5);
    gl_FragColor = vec4(pos.x, pos.y, vel.x, vel.y);
}
]]

function simple_quads:initGL()
    local unisz = ffi.typeof('GLint[?]')(0)
    gl.glGetIntegerv(GL.GL_MAX_TEXTURE_IMAGE_UNITS, unisz)
    print('GL_MAX_TEXTURE_IMAGE_UNITS',unisz[0])
    gl.glGetIntegerv(GL.GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, unisz)
    print('GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS',unisz[0])
    gl.glGetIntegerv(GL.GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, unisz)
    print('GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS',unisz[0])


    local unis = {
        'uScale',
        'uSqrtNumInsts',
        'tex',
        'texData',
    }
    local attrs = {
        'vP',
    }
    local err = nil
    self.instanceshader, err = sf.make_shader_object({vsrc=vs,fsrc=fs}, unis, attrs)

    local unis = {
        'uTex',
    }
    local attrs = {
        'vPos',
    }
    self.datashader, err = sf.make_shader_object({vsrc=quadvs,fsrc=quadfs}, unis, attrs)

    for i=1,2 do
        local vvbo = ffi.typeof('GLint[?]')(0)
        gl.glGenBuffers(1,vvbo)
        table.insert(self.vbos, vvbo)
    end

    self:init_quads()
    self:initFullscreenQuad()

    self.texID = 0
    self:loadTextures('frog1.data', 32, 32)

    self:initBounceBuffers()

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function simple_quads:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    if self.instanceshader then
        gl.glDeleteProgram(self.instanceshader.prog)
    end
    if self.glfont then
        self.glfont:exitGL()
    end
end

function simple_quads:render()
    if self.show_datatex then
        gl.glDisable(GL.GL_BLEND)
        self:showDataTexture()

        if self.glfont and self.draw_font then
            local str = '5_vstexfetch n='..self.n_quads
            util_bunny.render_text_string(self.glfont, str)
        end
        return
    end

    if self.do_alpha then
        gl.glEnable(GL.GL_BLEND)
    else
        gl.glDisable(GL.GL_BLEND)
    end

    local shader = self.instanceshader
    gl.glUseProgram(shader.prog)

    gl.glUniform1f(shader.uniforms.uScale, .4/math.pow(self.n_quads,.25))
    gl.glUniform1f(shader.uniforms.uSqrtNumInsts, math.ceil(math.sqrt(self.n_quads)))

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glUniform1i(shader.uniforms.tex, 0)

    -- instance positions
    local fboidx = 1 + self.pingpong01
    local dataTexID = self.fbos[fboidx].tex
    gl.glActiveTexture(GL.GL_TEXTURE1)
    gl.glBindTexture(GL.GL_TEXTURE_2D, dataTexID)
    gl.glUniform1i(shader.uniforms.texData, 1)

    self:bindVBO()
    gl.glDrawArrays(GL.GL_TRIANGLES, 0, 6*self.n_quads)
    self:unbindVBO()

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glUseProgram(0)

    if self.glfont and self.draw_font then
        local str = '5_vstexfetch n='..self.n_quads
        util_bunny.render_text_string(self.glfont, str)
    end
end

function simple_quads:renderToTexture()
    local targetidx = (1-self.pingpong01) + 1 -- opposite of draw
    local fbo = self.fbos[targetidx]

    -- Save state
    local fb = ffi.typeof('GLint[?]')(1)
    gl.glGetIntegerv(GL.GL_FRAMEBUFFER_BINDING, fb)
    local vpsz = ffi.typeof('GLint[?]')(4)
    gl.glGetIntegerv(GL.GL_VIEWPORT, vpsz)

    gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, fbo.id)
    gl.glViewport(0,0, fbo.w, fbo.h)
    --print("renderToTexture", fbo.w, fbo.h)

    --gl.glClearColor(0.75,0.25,0.5,0.5)
    --gl.glClear(GL.GL_COLOR_BUFFER_BIT)
    gl.glDisable(GL.GL_DEPTH_TEST)
    gl.glDisable(GL.GL_BLEND)
    self:showDataTexture()

    -- Restore state
    gl.glViewport(vpsz[0], vpsz[1], vpsz[2], vpsz[3])
    gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, fb[0])

    --TODO: will viewport ever change on us? Can we cache it once at init?

    self.pingpong01 = 1 - self.pingpong01
end

function simple_quads:showDataTexture()
    local shader = self.datashader
    gl.glUseProgram(shader.prog)

    local fboidx = 1 + self.pingpong01
    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.fbos[fboidx].tex)
    gl.glUniform1i(shader.uniforms.uTex, 0)

    local vl = shader.attribs.vPos
    local vvbo = self.vbos[2]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
    
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

------------------------------------------
-- Timestep
------------------------------------------
function simple_quads:timestep(absTime, dt)
    if self.do_motion then
        self:renderToTexture()
    end
end

------------------------------------------
-- Control
------------------------------------------
function simple_quads:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function simple_quads:onButtonPressed(padIndex, button)
    if button == lri.JOYPAD_Y then
    elseif button == lri.JOYPAD_X then

    elseif button == lri.JOYPAD_B then
        self.do_alpha = not self.do_alpha

    elseif button == lri.JOYPAD_R then
        self.show_datatex = not self.show_datatex

    elseif button == lri.JOYPAD_L then
        self:renderToTexture()

    elseif button == lri.JOYPAD_UP then
        self.n_quads = self.n_quads * 2
        self.n_quads = math.min(self.n_quads, 1024*1024)
        self:init_quads()
        self:initBounceBuffers()
    elseif button == lri.JOYPAD_DOWN then
        self.n_quads = self.n_quads * .5
        self.n_quads = math.max(self.n_quads, 1)
        self:init_quads()
        self:initBounceBuffers()

    elseif button == lri.JOYPAD_LEFT then
        -- TODO randomize_positions
    elseif button == lri.JOYPAD_RIGHT then
        -- TODO grid_positions

    elseif button == lri.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
        
    elseif button == lri.JOYPAD_START then
        self.do_motion = not self.do_motion
    end
end

return simple_quads
