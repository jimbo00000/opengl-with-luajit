--[[ simple_quads.lua

    Naively draws a bunch of rotating quads. This will
    quickly become draw-call bound, around 128 instances
    will start to slow down on the Pi 3B.
    The Pi Zero can only do 8 before dropping below 60fps!
]]

local DO_FONTS = true

simple_quads = {}
simple_quads.__index = simple_quads

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'

function simple_quads.new(...)
    local self = setmetatable({}, simple_quads)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_quads:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_quads:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}
    self.n_quads = 16*16*4
    self:init_quads()
    self.draw_font = true
end

function simple_quads:init_quads()
    math.randomseed(1)
    self.offsets = {}
    for i=1,self.n_quads do
        local off = {-1+2*math.random(), -1+2*math.random()}
        table.insert(self.offsets,off)
    end
end

local vs=[[
#version 100

#ifdef GL_ES
precision lowp float;
#endif

uniform float uTime;
uniform vec2 uOffset;
uniform float uScale;

attribute vec2 vP;
varying vec3 vfC;

void main()
{
    float rot = uTime;
    mat2 m = mat2(cos(rot), -sin(rot), sin(rot), cos(rot));
    vfC = vec3(vP/.05,0.);
    vec2 pt = m * (uScale * vP);
    gl_Position = vec4(pt+uOffset,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
uniform float uBlue;
void main()
{
    vec3 col = .5*vfC+vec3(.5);
    col.b = uBlue;
    gl_FragColor = vec4(col,1.);
}
]]

function simple_quads:populateVBO()
    local s = .05
    local v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_quads:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_quads:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function simple_quads:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function simple_quads:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    if self.glfont then
        self.glfont:exitGL()
    end
end

function simple_quads:render()
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    gl.glUseProgram(self.prog)
    
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")
    gl.glUniform1f(u_loc, b)

    local ut_loc = gl.glGetUniformLocation(self.prog, "uTime")
    gl.glUniform1f(ut_loc, 2*self.t)

    local us_loc = gl.glGetUniformLocation(self.prog, "uScale")
    gl.glUniform1f(us_loc, 4/math.pow(self.n_quads,.25))

    self:bindVBO()

    -- Here's the slow part
    for i=1,self.n_quads do
        local off = self.offsets[i]
        local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffset")
        gl.glUniform2f(uoff_loc, off[1], off[2])

        gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    end

    self:unbindVBO()

    if self.glfont and self.draw_font then
        local m = {}
        local p = {}
        mm.make_identity_matrix(m)
        local s = .5
        mm.glh_scale(m, s, s, s)
        mm.glh_translate(m, 30, 30, 0)
        -- TODO getStringWidth and center text
        if not win_w then
            win_w,win_h = 800,600
        end
        mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
        gl.glDisable(GL.GL_DEPTH_TEST)
        local col = {1, 1, 1}
        self.glfont:render_string(m, p, col, "simple_quads "..self.n_quads)
        mm.glh_translate(m, 30, 30, 0)
    end
end

function simple_quads:timestep(absTime, dt)
    self.t = absTime
end

function simple_quads:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function simple_quads:onButtonPressed(padIndex, button)
    if button == lr_input.JOYPAD_A then
        snd.playSound("test.wav")
        self.t_last = self.t
    elseif button == lr_input.JOYPAD_B then
        snd.playSound("pop_drip.wav")
        self.t_last = self.t
    elseif button == lr_input.JOYPAD_X then
        self.n_quads = self.n_quads * 2
        self:init_quads()
    elseif button == lr_input.JOYPAD_Y then
        self.n_quads = self.n_quads * .5
        self.n_quads = math.max(self.n_quads, 1)
        self:init_quads()
    elseif button == lr_input.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    end
end

return simple_quads
