--[[ bunnymark6_instanced.lua

    Use instanced rendering with quad positions in an ffi-allocated array.
]]

local DO_FONTS = true

simple_quads = {}
simple_quads.__index = simple_quads

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'
local util_bunny = require 'util_bunnymark'

local glIntv = ffi.typeof('GLint[?]')
local glUintv = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

function simple_quads.new(...)
    local self = setmetatable({}, simple_quads)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_quads:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_quads:init()
    self.vao = 0
    self.vbos = {}
    self.progs = {}
    self.buttons = {}

    self.numInstances = 64
    self:initInstances()

    self.draw_font = true
    self.do_alpha = true
    self.do_motion = true
    self.removeIdx = 0
    self.damping = .5
    self.dt = .03

    util_bunny.loadLibrary()
    self.updateFunc = util_bunny.getUpdateFunction()
    self.gridInitFunc = util_bunny.getGridInitFunction()
end

function simple_quads:initInstances()
    self.offset_array = ffi.typeof('GLfloat[?]')(self.numInstances*2)
    self.velocity_array = ffi.typeof('GLfloat[?]')(self.numInstances*2)
    self.force_array = ffi.typeof('GLfloat[?]')(self.numInstances*2)
    util_bunny.burst_position_velocity(self.offset_array, self.velocity_array, self.numInstances)
    self.connectivity_array = ffi.typeof('GLfloat[?]')(self.numInstances*4) -- 4 neighbors per node
    for i=0, self.numInstances*4-1 do
        self.connectivity_array[i] = -1
    end

    self.removeIdx = 0
end

------------------------------------------
-- OpenGL/GLES concerns:
-- Texture, VBO, shader, initGL/exitGL
------------------------------------------
function simple_quads:loadTexture(texfilename, w, h)
    if self.dataDir then texfilename = self.dataDir .. '/images/' .. texfilename end
    return util_bunny.load_texture_from_raw(texfilename, w, h)
end

------------------------------------------
-- VBO: vertex input to GPU draw
------------------------------------------

function simple_quads:populateVBO()
    local s = 1
    local v = ffi.typeof('GLfloat[?]')(8,{0,0,s,0,s,s,0,s,})
    local vlocation = gl.glGetAttribLocation(self.shader.prog, 'vPos')
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(v), v, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vlocation, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vlocation)
end

function simple_quads:init_instance_attributes()
    local sz = 2 * self.numInstances * ffi.sizeof('GLfloat') -- xy
    local insp_loc = gl.glGetAttribLocation(self.shader.prog, "vInstPosition")
    local ipvbo = glIntv(0)
    gl.glGenBuffers(1, ipvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, ipvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(insp_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    self.vbos.inst_positions = ipvbo
    gl.glVertexAttribDivisor(insp_loc, 1)
    gl.glEnableVertexAttribArray(insp_loc)
end

function simple_quads:assign_instance_positions()
    local dim = math.sqrt(self.numInstances)

    for i=0,self.numInstances-1 do
        local row = math.floor(i/dim)
        local col = math.fmod(i, dim)
        local x01 = row / dim
        local y01 = col / dim
        local sc = 1.4
        self.offset_array[2*i] = sc*(x01 - .5)
        self.offset_array[2*i+1] = sc*(y01 - .5)

        local north = i - dim
        local south = i + dim
        local west = i - 1
        local east = i + 1

        numneighbs = 0
        if row > 0 then
            --print('  N', north)
            self.connectivity_array[4*i + numneighbs] = north
            numneighbs = numneighbs + 1
        end
        if row < dim-1 then
            --print('  S', south)
            self.connectivity_array[4*i + numneighbs] = south
            numneighbs = numneighbs + 1
        end
        if col > 0 then
            --print('  W', west)
            self.connectivity_array[4*i + numneighbs] = west
            numneighbs = numneighbs + 1
        end
        if col < dim-1 then
            --print('  E', east)
            self.connectivity_array[4*i + numneighbs] = east
            numneighbs = numneighbs + 1
        end
    end

    gl.glBindVertexArray(self.vao)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos.inst_positions[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(self.offset_array), self.offset_array, GL.GL_STATIC_DRAW)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glBindVertexArray(0)
end

local vs=[[
#version 310 es

#ifdef GL_ES
precision mediump float;
#endif

in vec2 vPos;
in vec2 vInstPosition;

out vec3 vfUV;

uniform float uScale;

void main()
{
    vfUV = vec3(vPos, 0.);
    vec2 pt = uScale * vPos.xy;
    gl_Position = vec4(pt + vInstPosition, 0., 1.);
}
]]

local fs=[[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfUV;
out vec4 fragColor;

uniform sampler2D uTex;

void main()
{
    vec2 tc = vfUV.xy;
    fragColor = texture(uTex, tc);
}
]]

function simple_quads:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    local shader = {
        prog = sf.make_shader_from_source({vsrc=vs, fsrc=fs}),
        uniforms = {},
        attribs = {}
    }
    local unis = {
        'uScale',
        'uTex',
    }
    local function uni(var) return gl.glGetUniformLocation(shader.prog, var) end
    for k,v in pairs(unis) do
        shader.uniforms[v] = uni(v)
    end

    local attribs = {
        'vPos',
        'vInstPosition',
    }
    local function attr(var) return gl.glGetAttribLocation(shader.prog, var) end
    for k,v in pairs(attribs) do
        shader.attribs[v] = attr(v)
    end

    self.shader = shader

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()
    self:init_instance_attributes()
    self:assign_instance_positions()


    self.texID = self:loadTexture('frog1.data', 32, 32)

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir..'/fonts')
        self.glfont:initGL()
    end
    gl.glBindVertexArray(0)
end

function simple_quads:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    for _,v in pairs(self.progs) do
        gl.glDeleteProgram(v)
    end
    self.shader = nil

    local texId = ffi.new("int[1]")
    texId[0] = self.texID
    gl.glDeleteTextures(1, texId)
    self.texID = nil

    if self.glfont then
        self.glfont:exitGL()
    end

    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)
end

function simple_quads:render()
    if self.do_alpha then
        gl.glEnable(GL.GL_BLEND)
    else
        gl.glDisable(GL.GL_BLEND)
    end

    gl.glUseProgram(self.shader.prog)

    gl.glUniform1f(self.shader.uniforms.uScale, .4/math.pow(self.numInstances,.25))

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glUniform1i(self.shader.uniforms.uTex, 0)

    gl.glBindVertexArray(self.vao)
    gl.glDrawArraysInstanced(GL.GL_TRIANGLE_FAN, 0, 4, self.numInstances)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)

    if self.glfont and self.draw_font then
        local str = "n="..self.numInstances
        str = str..' damp='..string.sub(self.damping,0,5)
        str = str..' dt='..string.sub(self.dt,0,5)
        util_bunny.render_text_string(self.glfont, str)
    end
end

function simple_quads:timestep(absTime, dt)
    if self.do_motion and self.updateFunc then
        ---TODO: update function
        --self.updateFunc(self.offset_array, self.velocity_array, self.numInstances)

        --Calculate forces
        for i=0, self.numInstances-1 do
            local xidx = 2*i
            local yidx = xidx+1

            local tempPosX = self.offset_array[xidx]
            local tempPosY = self.offset_array[yidx]
            local tempVelX = self.velocity_array[xidx]
            local tempVelY = self.velocity_array[yidx]

            tempPosX = tempPosX + tempVelX
            tempPosY = tempPosY + tempVelY

            local posx = self.offset_array[xidx]
            local posy = self.offset_array[yidx]

            self.force_array[xidx] = 0
            self.force_array[yidx] = 0
            local idx = i*4 -- 4 neighbors max per item
            for j=0,3 do
                local neighb = self.connectivity_array[idx+j]
                if neighb > -1 then
                    local nposx = self.offset_array[neighb*2]
                    local nposy = self.offset_array[neighb*2+1]

                    local eq = 0 --.03
                    local dx = nposx - posx - eq
                    local dy = nposy - posy - eq
                    local dist2 = dx*dx + dy*dy
                    local eq = 0 --.28
                    local mag = dist2 - eq * eq
                    local k = mag
                    local fx = k * dx -- * dx
                    local fy = k * dy -- * dy
                    self.force_array[xidx] = self.force_array[xidx] + fx
                    self.force_array[yidx] = self.force_array[yidx] + fy
                end
            end

            -- All nodes repel
            for j=0,self.numInstances-1 do
                if i ~= j then
                    if self.offset_array[2*j] < 900 then
                        local nposx = self.offset_array[j*2]
                        local nposy = self.offset_array[j*2+1]

                        local dx = nposx - posx
                        local dy = nposy - posy
                        local dist2 = dx*dx + dy*dy
                        local mag = -1 / dist2
                        local k = mag * .001
                        local fx = k * dx
                        local fy = k * dy
                        self.force_array[xidx] = self.force_array[xidx] + fx
                        self.force_array[yidx] = self.force_array[yidx] + fy
                    end
                end
            end

            local mass = self.damping
            self.velocity_array[xidx] = self.velocity_array[xidx] + mass * self.force_array[xidx]
            self.velocity_array[yidx] = self.velocity_array[yidx] + mass * self.force_array[yidx]
        end

        -- Advect particles
        for i=0, self.numInstances-1 do
            local xidx = 2*i
            local yidx = xidx+1

            local dt = self.dt
            self.offset_array[xidx] = self.offset_array[xidx] + dt*self.velocity_array[xidx]
            self.offset_array[yidx] = self.offset_array[yidx] + dt*self.velocity_array[yidx]
        end

        gl.glBindVertexArray(self.vao)
        gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos.inst_positions[0])
        gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(self.offset_array), self.offset_array, GL.GL_STATIC_DRAW)
        gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
        gl.glBindVertexArray(0)
    end
end

function simple_quads:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function simple_quads:onButtonPressed(padIndex, button)
    local dampcoeff = .9
    if button == lr_input.JOYPAD_A then
        self.damping = self.damping * dampcoeff
    elseif button == lr_input.JOYPAD_B then
        self.damping = self.damping / dampcoeff

    elseif button == lr_input.JOYPAD_L then
        self.dt = self.dt * dampcoeff
    elseif button == lr_input.JOYPAD_R then
        self.dt = self.dt / dampcoeff

    elseif button == lr_input.JOYPAD_X then
        local i = math.random(0,self.numInstances)
        print('remove', i)
        local xidx = 2*i
        local yidx = xidx+1
        self.offset_array[xidx] = 999
        self.offset_array[yidx] = 999
        self.connectivity_array[4*i  ] = -1
        self.connectivity_array[4*i+1] = -1
        self.connectivity_array[4*i+2] = -1
        self.connectivity_array[4*i+3] = -1
        for j=0,self.numInstances*4-1 do
            if self.connectivity_array[j] == i then
                self.connectivity_array[j] = -1
            end
        end
        self.removeIdx = self.removeIdx + 1

    elseif button == lr_input.JOYPAD_UP then
        local dim = math.sqrt(self.numInstances)
        dim = dim + 1
        self.numInstances = dim*dim
        self:initInstances()
        self:assign_instance_positions()
    elseif button == lr_input.JOYPAD_DOWN then
        local dim = math.sqrt(self.numInstances)
        dim = dim - 1
        self.numInstances = dim*dim
        self.numInstances = math.max(self.numInstances, 1)
        self:initInstances()
        self:assign_instance_positions()

    elseif button == lr_input.JOYPAD_LEFT then
        util_bunny.randomize_positions(self.offset_array, self.numInstances)
    elseif button == lr_input.JOYPAD_RIGHT then
        self.gridInitFunc(self.offset_array, self.numInstances)

    elseif button == lr_input.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    elseif button == lr_input.JOYPAD_START then
        self.do_motion = not self.do_motion
    end
end

return simple_quads
