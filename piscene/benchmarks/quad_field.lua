--[[ quad_field.lua

    A field of stars for use as a background.
]]

local DO_FONTS = true

simple_quads = {}
simple_quads.__index = simple_quads

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local mm = require 'util.matrixmath'
local lri = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'

function simple_quads.new(...)
    local self = setmetatable({}, simple_quads)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_quads:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_quads:init()
    self.vbos = {}
    self.prog = 0
    self.buttons = {}

    self.n_quads = 8
    self.draw_font = true
    self.do_alpha = true
    self.xoffset = 0
end

------------------------------------------
-- OpenGL/GLES concerns:
-- Texture, VBO, shader, initGL/exitGL
------------------------------------------

function simple_quads:loadTextures(texfilename, w, h)
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end
    local inp = assert(io.open(texfilename, "rb"))
    local data = inp:read("*all")
    assert(inp:close())
    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    self.texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_REPEAT)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_REPEAT)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    --gl.glTexParameteri( GL.GL_TEXTURE_2D, GL.GL_DEPTH_TEXTURE_MODE, GL.GL_INTENSITY ); --deprecated, out in 3.1
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
                  w, h, 0,
                  GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end

------------------------------------------
-- VBO: vertex input to GPU draw
------------------------------------------
function simple_quads:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)

    local ul = gl.glGetAttribLocation(self.prog,"vUV")
    local uvbo = self.vbos[2]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,uvbo[0])
    gl.glVertexAttribPointer(ul,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(ul)
end

function simple_quads:unbindVBO()
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)

    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glDisableVertexAttribArray(vl)
    local ul = gl.glGetAttribLocation(self.prog, "vUV")
    gl.glDisableVertexAttribArray(ul)
end

-- This function both allocates and fills storage.
function simple_quads:populateVBO()
    local function randSz() return .005+.02*math.random() end
    local function randOff() return -1 + 2.*math.random() end

    local all = {}
    for i=1,self.n_quads do
        local s = 1
        local verts = {
            -s, 0, 0,0,
             0,-s, 0,0,
             0, s, 0,0,
             0,-s, 0,0,
             s, 0, 0,0,
             0, s, 0,0,
        }

        local sz = 3.*randSz()
        for j=1,6 do
            verts[4*j-3] = verts[4*j-3] * sz
            verts[4*j-2] = verts[4*j-2] * sz
        end

        local offx = randOff()
        local offy = randOff()
        for j=1,6 do
            --verts[4*j-3] = verts[4*j-3] + offx
            verts[4*j-2] = verts[4*j-2] + offy
        end

        for j=1,6 do
            verts[4*j-1] = i
        end

        -- Workaround for position wrapping and split quads:
        -- Store the quad's center x coord redundantly in the w field
        -- of all 6 verts.
        for j=1,6 do
            verts[4*j] = offx
        end

        for j=1,#verts do
            table.insert(all, verts[j])
        end
    end
    local av = ffi.typeof('GLfloat[?]')(#all,all)
    print('sz all', #all)

    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(av),av,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,4,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)

    local alltex = {}
    for i=1,self.n_quads do
        local texs = {
        --[[ 
            0,0, 
            1,0, 
            0,1, 
            1,0, 
            1,1, 
            0,1,]]
            0,.5,
            .5,0,
            .5,1,
            .5,0,
            1,.5,            
            .5,1,
        }
        for j=1,#texs do
            table.insert(alltex, texs[j])
        end
    end
    local at = ffi.typeof('GLfloat[?]')(#alltex, alltex)
    local ul = gl.glGetAttribLocation(self.prog,"vUV")
    print('sz alltex', #alltex, ul)
    local uvbo = self.vbos[2]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,uvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(at),at,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(ul,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(ul)
end

local vs=[[
#version 100

#ifdef GL_ES
precision mediump float;
#endif

uniform float uNumInsts;
uniform float xOffset;

attribute vec4 vP;
attribute vec2 vUV;
varying vec2 vfTexCoord;
varying float brightness;

void main()
{
    vfTexCoord = vUV;//vP.xy;
    brightness = vP.z / uNumInsts;

    vec2 pt = vP.xy;

    // Wrap center positions around [-1,1]
    {
        float xwrap = vP.w + xOffset * brightness;
        xwrap = .5*xwrap + .5;
        xwrap = fract(xwrap);
        xwrap = 2.*xwrap - 1.;
        pt.x += xwrap;
    }

    gl_Position = vec4(pt,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec2 vfTexCoord;
varying float brightness;

uniform sampler2D tex;
uniform float xOffset;

void main()
{
    vec2 tc = vfTexCoord;
    vec2 pt = 2.*tc - vec2(1.);
    pt = 10.*pt;
    float br = abs( (1./pt.x) * (1./pt.y) );
    br = sqrt(br);
    br = clamp(br-.2, 0., 1.);

    gl_FragColor = vec4(br);
}
]]

function simple_quads:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})
    local function uni(var) return gl.glGetUniformLocation(self.prog, var) end
    local unis = {
        'uNumInsts',
        'tex',
        'xOffset',
    }
    self.uniforms = {}
    for _,v in pairs(unis) do self.uniforms[v] = uni(v) end

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)

    local uvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,uvbo)
    table.insert(self.vbos, uvbo)

    self:populateVBO()

    self.texID = 0
    self:loadTextures('frog1.data', 32, 32)

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function simple_quads:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    if self.glfont then
        self.glfont:exitGL()
    end
end

function simple_quads:render()
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    if self.do_alpha then
        gl.glEnable(GL.GL_BLEND)
    else
        gl.glDisable(GL.GL_BLEND)
    end

    gl.glUseProgram(self.prog)
    gl.glUniform1f(self.uniforms.uNumInsts, self.n_quads)
    gl.glUniform1f(self.uniforms.xOffset, self.xoffset)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glUniform1i(self.uniforms.tex, 0)

    self:bindVBO()
    gl.glDrawArrays(GL.GL_TRIANGLES, 0, 6*self.n_quads)
    self:unbindVBO()

    if self.glfont and self.draw_font then
        local m = {}
        local p = {}
        mm.make_identity_matrix(m)
        local s = .5
        mm.glh_scale(m, s, s, s)
        mm.glh_translate(m, 30, 30, 0)
        -- TODO getStringWidth and center text
        if not win_w then
            win_w,win_h = 800,600
        end
        mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
        gl.glDisable(GL.GL_DEPTH_TEST)
        local col = {1, 1, 1}
        self.glfont:render_string(m, p, col, "quad_field "..self.n_quads)
        mm.glh_translate(m, 0, 120, 0)

        self.glfont:render_string(m, p, col, "xoff= "..self.xoffset)
    end
end

------------------------------------------
-- Timestep
------------------------------------------
function simple_quads:timestep(absTime, dt)
    self.t = absTime

    local dx = 0
    if self.holding_left then
        dx = 1
    elseif self.holding_right then
        dx = -1
    end
    self.xoffset = self.xoffset + dt * dx
end

------------------------------------------
-- Control
------------------------------------------
function simple_quads:update_retropad(joystates)
    self.holding_left = false
    self.holding_right = false
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end

        if curstate[lri.JOYPAD_LEFT] ~= 0 then
            self.holding_left = true
        end
        if curstate[lri.JOYPAD_RIGHT] ~= 0 then
            self.holding_right = true
        end
    end
end

function simple_quads:onButtonPressed(padIndex, button)
    if button == lri.JOYPAD_A then
        snd.playSound("test.wav")
    elseif button == lri.JOYPAD_B then
        snd.playSound("pop_drip.wav")
        self.do_alpha = not self.do_alpha

    elseif button == lri.JOYPAD_UP then
        self.n_quads = self.n_quads * 2
        self.n_quads = math.min(self.n_quads, 4096*2*16)
        self:populateVBO()
    elseif button == lri.JOYPAD_DOWN then
        self.n_quads = self.n_quads * .5
        self.n_quads = math.max(self.n_quads, 1)
        self:populateVBO()

    elseif button == lri.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    elseif button == lri.JOYPAD_START then
    end
end

return simple_quads
