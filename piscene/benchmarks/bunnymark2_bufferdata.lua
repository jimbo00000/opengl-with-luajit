--[[ bunnymark2_bufferdata.lua


    Uses vertex attribs to update the 6 xy verts of every quad using glBufferSubData. 
]]

local DO_FONTS = true

local simple_quads = {}
simple_quads.__index = simple_quads

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions2'
local mm = require 'util.matrixmath'
local lri = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'
local util_bunny = require 'util_bunnymark'

function simple_quads.new(...)
    local self = setmetatable({}, simple_quads)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_quads:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_quads:init()
    self.vbos = {}
    self.prog = 0
    self.buttons = {}

    self.n_quads = 32

    self.draw_font = true
    self.do_alpha = true
    self.do_subdata = true
    self.do_motion = true
    self.do_pingpong = true
    self.pingpong = 1
    self.scale_tweak = 1

    util_bunny.loadLibrary()
    self.updateFunc = util_bunny.getUpdateFunction()
    self.gridInitFunc = util_bunny.getGridInitFunction()
    self.assembleFunc = util_bunny.getAssembleQuadFunction()
end

-- An array of {position,velocity} tuples for tracking state.
-- Must be copied to packed structs for drawing.
function simple_quads:initInstances()
    self.offset_array = ffi.typeof('GLfloat[?]')(self.n_quads*2)
    self.velocity_array = ffi.typeof('GLfloat[?]')(self.n_quads*2)
    util_bunny.burst_position_velocity(self.offset_array, self.velocity_array, self.n_quads)
    
    self:allocateVBO(self.vbos[1])
    self:allocateVBO(self.vbos[2])
    self.vposarray = ffi.typeof('GLfloat[?]')(self.n_quads*2*6)
end

------------------------------------------
-- OpenGL/GLES concerns:
-- Texture, VBO, shader, initGL/exitGL
------------------------------------------
function simple_quads:loadTextures(texfilename, w, h)
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end
    self.texID = util_bunny.load_texture_from_raw(texfilename, w, h)
end

------------------------------------------
-- VBO: vertex input to GPU draw
------------------------------------------
function simple_quads:getXYVBO()
    if self.do_pingpong then
        return self.vbos[1 + self.pingpong]
    else
        return self.vbos[1]
    end
end

function simple_quads:bindVBO()
    local vl = self.shader.attribs.vPos
    local vvbo = self:getXYVBO()
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    local format = GL.GL_FLOAT
    --format = 36193 -- GL_HALF_FLOAT_OES
    gl.glVertexAttribPointer(vl, 2, format, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_quads:unbindVBO()
    local vl = self.shader.attribs.vPos
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function simple_quads:allocateVBO(vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof('float')*12*self.n_quads, nil, GL.GL_DYNAMIC_DRAW)
end

function simple_quads:updateVBO()
    self.assembleFunc(self.offset_array, self.vposarray, self.n_quads)

    self:bindVBO()
    if self.do_subdata then
        gl.glBufferSubData(GL.GL_ARRAY_BUFFER,
            0,
            ffi.sizeof(self.vposarray),
            self.vposarray)
    else
        gl.glBufferData(GL.GL_ARRAY_BUFFER,
            ffi.sizeof(self.vposarray),
            self.vposarray,
            GL.GL_STREAM_DRAW)
    end
end

function simple_quads:populateUVs()
    local all = {}
    for i=1,self.n_quads do
        local s = 1
        local verts = {
            0, 0,
            s, 0,
            s, s,
            0, 0,
            s, s,
            0, s,
        }

        for j=1,#verts do
            table.insert(all, verts[j])
        end
    end
    local av = ffi.typeof('GLfloat[?]')(#all,all)

    local uvbo = self.vbos[3]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,uvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(av),av,GL.GL_STATIC_DRAW)

    local vuv = gl.glGetAttribLocation(self.prog,"vUV")
    gl.glVertexAttribPointer(vuv,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vuv)
end

local vs=[[
#version 100

#ifdef GL_ES
precision mediump float;
#endif

uniform float uScale;
attribute vec2 vPos;
attribute vec2 vUV;

varying vec2 vfC;

void main()
{
    vfC = vUV;
    vec2 pt = uScale * vUV.xy + vPos.xy;
    gl_Position = vec4(pt,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec2 vfC;
uniform sampler2D uTex;

void main()
{
    vec4 col = texture2D(uTex, vfC);
    gl_FragColor = vec4(col);
}
]]

function simple_quads:initGL()
    local unis = {
        'uScale',
        'uTex',
    }
    local attrs = {
        'vPos',
        'vUV',
    }
    local err = nil
    self.shader, err = sf.make_shader_object({vsrc=vs,fsrc=fs}, unis, attrs)

    -- 2 VBOs for positions(updated per-frame), one for UVs
    for i=1,3 do
        local vvbo = ffi.typeof('GLint[?]')(0)
        gl.glGenBuffers(1,vvbo)
        table.insert(self.vbos, vvbo)
    end
    self:initInstances()
    self:populateUVs()

    self.texID = 0
    self:loadTextures('frog1.data', 32, 32)

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function simple_quads:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    if self.shader then
        gl.glDeleteProgram(self.shader.prog)
    end

    if self.texID then
        local texId = ffi.new("int[1]")
        texId[0] = self.texID
        gl.glDeleteTextures(1, texId)
        self.texID = nil
    end

    if self.glfont then
        self.glfont:exitGL()
    end
end

function simple_quads:render()
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    if self.do_alpha then
        gl.glEnable(GL.GL_BLEND)
    else
        gl.glDisable(GL.GL_BLEND)
    end

    local shader = self.shader
    gl.glUseProgram(shader.prog)

    gl.glUniform1f(shader.uniforms.uScale, self.scale_tweak * .4/math.pow(self.n_quads,.25))

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glUniform1i(shader.uniforms.uTex, 0)

    self.pingpong = 1 - self.pingpong
    self:bindVBO()

    if self.do_motion then
        -- set instance positions
        self:updateVBO()
    end

    local uvbo = self.vbos[3]
    local vuv = shader.attribs.vUV
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,uvbo[0])
    gl.glVertexAttribPointer(vuv,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vuv)

    gl.glDrawArrays(GL.GL_TRIANGLES, 0, 6*self.n_quads)

    self:unbindVBO()

    if self.glfont and self.draw_font then
        local str = "2_bufferdata n="..self.n_quads
        str = str..' sub='..tostring(self.do_subdata and 1 or 0)
        str = str..' pong='..tostring(self.do_pingpong and 1 or 0)
        util_bunny.render_text_string(self.glfont, str)
    end
end

------------------------------------------
-- Timestep
------------------------------------------
function simple_quads:timestep(absTime, dt)
    if self.do_motion and self.updateFunc then
        self.updateFunc(self.offset_array, self.velocity_array, self.n_quads)
    end
end

------------------------------------------
-- Control
------------------------------------------
function simple_quads:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function simple_quads:onButtonPressed(padIndex, button)

    if button == lri.JOYPAD_A then
    elseif button == lri.JOYPAD_B then
        self.do_alpha = not self.do_alpha

    elseif button == lri.JOYPAD_Y then
        self.do_pingpong = not self.do_pingpong
    elseif button == lri.JOYPAD_X then
        self.do_subdata = not self.do_subdata

    elseif button == lri.JOYPAD_L then
        self.scale_tweak = self.scale_tweak / 2
        self.scale_tweak = math.max(self.scale_tweak, 1/8)
    elseif button == lri.JOYPAD_R then
        self.scale_tweak = self.scale_tweak * 2
        self.scale_tweak = math.min(self.scale_tweak, 8)

    elseif button == lri.JOYPAD_UP then
        self.n_quads = self.n_quads * 2
        self.n_quads = math.min(self.n_quads, 1024*1024)
        self:initInstances()
        self:populateUVs()
    elseif button == lri.JOYPAD_DOWN then
        self.n_quads = self.n_quads * .5
        self.n_quads = math.max(self.n_quads, 1)
        self:initInstances()
        self:populateUVs()

    elseif button == lri.JOYPAD_LEFT then
        util_bunny.randomize_positions(self.offset_array, self.n_quads)
        self:updateVBO()
    elseif button == lri.JOYPAD_RIGHT then
        self.gridInitFunc(self.offset_array, self.n_quads)
        self:updateVBO()

    elseif button == lri.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    elseif button == lri.JOYPAD_START then
        self.do_motion = not self.do_motion
    end
end

return simple_quads
