--[[ simple_quads3.lua

    Draws a bunch of rotating quads.
    Uses a pseudo-instancing technique, baking the instance ID
    into each vert's z coord. Draws in batches of up to 256.
]]

local DO_FONTS = true

simple_quads = {}
simple_quads.__index = simple_quads

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local mm = require 'util.matrixmath'
local lri = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'

function simple_quads.new(...)
    local self = setmetatable({}, simple_quads)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_quads:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_quads:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}

    self.n_quads = 32
    self.chunkSz = 256
    self.draw_font = true
    self.offset_array = ffi.typeof('GLfloat[?]')(132*4) --Pi's max chunk size/uniform size
end

function simple_quads:init_quads()
    math.randomseed(1)
    self.instances = {}
    for i=1,self.n_quads do
        local x = -1+2*math.random()
        local y = -1+2*math.random()
        table.insert(self.instances, {x,y})
    end
    self:populateVBO()
end

local vs=[[
#version 100

#ifdef GL_ES
precision lowp float;
#endif

uniform mat2 uRotMtx;
uniform float uScale;
uniform vec2 uOffsets[256];
uniform float uNumInsts;

attribute vec3 vP;
varying vec3 vfC;
varying float brightness;

void main()
{
    int id = int(vP.z) - 1;
    vec2 Offset = uOffsets[id];
    brightness = vP.z / uNumInsts;
    vfC = vec3(vP.xy/.05,0.);
    vec2 pt = uRotMtx * (uScale * vP.xy);
    gl_Position = vec4(pt+Offset,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
varying float brightness;

uniform float uBlue;

void main()
{
    vec3 col = .5*vfC+vec3(.5);
    col.b = uBlue;
    gl_FragColor = vec4(brightness * col,1.);
}
]]

function simple_quads:populateVBO()
    local s = .05
    local verts = {
        -s,-s,0,
        s,-s,0,
        s,s,0,
        -s,-s,0,
        s,s,0,
        -s,s,0,
    }

    local all = {}
    for i=1,self.n_quads do
        for j=1,6 do
            verts[3*j] = i
        end
        for j=1,#verts do
            table.insert(all, verts[j])
        end
    end
    local av = ffi.typeof('GLfloat[?]')(#all,all)

    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(av),av,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,3,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_quads:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_quads:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function simple_quads:initGL()
    local unisz = ffi.typeof('GLint[?]')(0)
    gl.glGetIntegerv(GL.GL_MAX_VERTEX_UNIFORM_VECTORS, unisz)
    print('GL_MAX_VERTEX_UNIFORM_VECTORS',unisz[0])

    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})
    local unis = {
        'uBlue',
        'uRotMtx',
        'uScale',
        'uNumInsts',
        'uOffsets',
    }
    self.uniforms = {}
    local function uni(var) return gl.glGetUniformLocation(self.prog, var) end
    for k,v in pairs(unis) do
        self.uniforms[v] = uni(v)
    end

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:init_quads()

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function simple_quads:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    if self.glfont then
        self.glfont:exitGL()
    end
end

function simple_quads:render()
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    gl.glUniform1f(self.uniforms.uBlue, b)

    local rot = self.t
    local m = {
        math.cos(rot), -math.sin(rot),
        math.sin(rot), math.cos(rot),
    }
    gl.glUniformMatrix2fv(self.uniforms.uRotMtx, 1, GL.GL_FALSE, ffi.typeof('GLfloat[?]')(4, m))
    gl.glUniform1f(self.uniforms.uScale, 4/math.pow(self.n_quads,.25))

    local chunkSz = math.min(self.chunkSz, self.n_quads)
    gl.glUniform1f(self.uniforms.uNumInsts, chunkSz)

    -- instance positions
    self:bindVBO()
    --TODO: handle incomplete chunks
    for i=0, self.n_quads-1, chunkSz do
        -- Populate instance attrib/uniform array
        local x = 0
        for j=1,chunkSz do
            local idx = i + j
            local inst = self.instances[idx]
            for k=1,2 do
                self.offset_array[x] = inst[k]
                x = x + 1
            end
        end
        gl.glUniform2fv(self.uniforms.uOffsets, chunkSz, self.offset_array)

        gl.glDrawArrays(GL.GL_TRIANGLES, 0, 6*chunkSz)
    end
    self:unbindVBO()


    if self.glfont and self.draw_font then
        local m = {}
        local p = {}
        mm.make_identity_matrix(m)
        local s = .35
        mm.glh_scale(m, s, s, s)
        mm.glh_translate(m, 30, 30, 0)
        -- TODO getStringWidth and center text
        if not win_w then
            win_w,win_h = 800,600
        end
        mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
        gl.glDisable(GL.GL_DEPTH_TEST)
        local col = {1, 1, 1}
        local ndraws = math.max(1,math.floor(self.n_quads/self.chunkSz))
        local str = "pseudo-inst n="..self.n_quads
        str = str.. " chunkSz="..self.chunkSz
        str = str.. " ndraws="..ndraws
        self.glfont:render_string(m, p, col, str)
    end
end

function simple_quads:timestep(absTime, dt)
    self.t = absTime
end

function simple_quads:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function simple_quads:onButtonPressed(padIndex, button)
    if button == lri.JOYPAD_A then
        snd.playSound("test.wav")
        self.t_last = self.t
        self.chunkSz = self.chunkSz * 2
        self.chunkSz = math.min(self.chunkSz, 256)
    elseif button == lri.JOYPAD_B then
        snd.playSound("pop_drip.wav")
        self.t_last = self.t
        self.chunkSz = self.chunkSz * .5
        self.chunkSz = math.max(self.chunkSz, 1)
    elseif button == lri.JOYPAD_X then
        self.n_quads = self.n_quads * 2
        self.n_quads = math.min(self.n_quads, 4096*2*16)
        self:init_quads()
    elseif button == lri.JOYPAD_Y then
        self.n_quads = self.n_quads * .5
        self.n_quads = math.max(self.n_quads, 1)
        self:init_quads()
    elseif button == lri.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    end
end

return simple_quads
