--[[ conway.lua

]]

local DO_FONTS = true

local simple_quads = {}
simple_quads.__index = simple_quads

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local mm = require 'util.matrixmath'
local lri = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'
local fbf = require "util.fbofunctions"
local util_bunny = require 'util_bunnymark'

function simple_quads.new(...)
    local self = setmetatable({}, simple_quads)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_quads:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_quads:init()
    self.t = 0
    self.vbos = {}
    self.quadprog = 0
    self.buttons = {}

    self.n_quads = 32

    self.draw_font = true
    self.do_motion = true
end

------------------------------------------
-- OpenGL/GLES concerns:
-- Texture, VBO, shader, initGL/exitGL
------------------------------------------
function simple_quads:populateTexture(dataTexID, dim)
    local w,h = dim, dim

    -- Make a diagonal line x=y
    local imgdata = {}
    for i=0,dim*dim-1 do
        table.insert(imgdata, math.floor(255*math.random()))
        table.insert(imgdata, math.floor(255*math.random()))
        table.insert(imgdata, math.floor(255*math.random()))
        table.insert(imgdata, math.floor(255*math.random()))
    end
    local data = ffi.typeof('GLubyte[?]')(#imgdata,imgdata)

    gl.glBindTexture(GL.GL_TEXTURE_2D, dataTexID)
    do
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
                      w, h, 0,
                      GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, data)
    end
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end

------------------------------------------
-- Fullscreen quad for bounce pass
------------------------------------------
function simple_quads:initFullscreenQuad()
    local vl = gl.glGetAttribLocation(self.quadprog, "vP")
    local vvbo = self.vbos[1] ---todo table/named indexing
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    --gl.glEnableVertexAttribArray(vl)

    local s = 1
    local v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
end

function simple_quads:initBounceBuffers()
    if self.fbos then
        for k,v in pairs(self.fbos) do
            if v then
                fbf.deallocate_fbo(v)
            end
        end
    end

    local dim = math.ceil(math.sqrt(self.n_quads))
    local w,h = dim,dim
    self.fbos = {
        fbf.allocate_fbo(w, h, false),
        fbf.allocate_fbo(w, h, false),
    }
    self.pingpong01 = 0

    self:populateTexture(self.fbos[1].tex, dim)
    self:populateTexture(self.fbos[2].tex, dim)
end

local quadvs=[[
#version 100
attribute vec2 vP;
varying vec2 vfC;
void main()
{
    vfC = vP;
    gl_Position = vec4(vP,0.,1.);
}
]]

local quadfs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec2 vfC;
uniform float uRes;
uniform sampler2D tex;

void main()
{
    vec2 tc = vec2(.5)+.5*vfC;
#line 138
    float d = uRes;
    float r00 = (texture2D(tex, tc+vec2(-d ,-d)).r);
    float r01 = (texture2D(tex, tc+vec2( 0.,-d)).r);
    float r02 = (texture2D(tex, tc+vec2( d ,-d)).r);

    float r10 = (texture2D(tex, tc+vec2(-d ,0.)).r);
    float r11 = (texture2D(tex, tc+vec2( 0.,0.)).r);
    float r12 = (texture2D(tex, tc+vec2( d ,0.)).r);

    float r20 = (texture2D(tex, tc+vec2(-d , d)).r);
    float r21 = (texture2D(tex, tc+vec2( 0., d)).r);
    float r22 = (texture2D(tex, tc+vec2( d , d)).r);

    float finalSumf =
        (r00 + r10 + r20) +
        (r01 + r11 + r21) +
        (r02 + r12 + r22);
    int finalSum = int(finalSumf);

    if (finalSum == 3)
        gl_FragColor = vec4(1.);
    else if(finalSum == 4 && r11 == 1.)
        gl_FragColor = vec4(1.);
    else
        gl_FragColor = vec4(0.);
}
]]
function simple_quads:initGL()
    local unisz = ffi.typeof('GLint[?]')(0)
    gl.glGetIntegerv(GL.GL_MAX_TEXTURE_IMAGE_UNITS, unisz)
    print('GL_MAX_TEXTURE_IMAGE_UNITS',unisz[0])
    gl.glGetIntegerv(GL.GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, unisz)
    print('GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS',unisz[0])
    gl.glGetIntegerv(GL.GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, unisz)
    print('GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS',unisz[0])

    self.quadprog = sf.make_shader_from_source({vsrc=quadvs,fsrc=quadfs})
    --todo: cache uniforms, attribs

    local fsqvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,fsqvbo)
    table.insert(self.vbos, fsqvbo)
    self:initFullscreenQuad()

    self.texID = 0

    self:initBounceBuffers()

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function simple_quads:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.quadprog)

    --fbf.deallocate_fbo(self.fbos[1])
    --fbf.deallocate_fbo(self.fbos[2])

    if self.glfont then
        self.glfont:exitGL()
    end
end

function simple_quads:render()
    gl.glDisable(GL.GL_BLEND)
    self:showDataTexture()

    if self.glfont and self.draw_font then
        local dim = math.ceil(math.sqrt(self.n_quads))
        local str = 'conway n='..self.n_quads..'('..dim..'^2)'
        util_bunny.render_text_string(self.glfont, str)
    end
end

function simple_quads:renderToTexture()
    local targetidx = (1-self.pingpong01) + 1 -- opposite of draw
    local fbo = self.fbos[targetidx]

    -- Save state
    local fb = ffi.typeof('GLint[?]')(1)
    gl.glGetIntegerv(GL.GL_FRAMEBUFFER_BINDING, fb)
    local vpsz = ffi.typeof('GLint[?]')(4)
    gl.glGetIntegerv(GL.GL_VIEWPORT, vpsz)

    gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, fbo.id)
    gl.glViewport(0,0, fbo.w, fbo.h)
    --print("renderToTexture", fbo.w, fbo.h)

    --gl.glClearColor(0.75,0.25,0.5,0.5)
    --gl.glClear(GL.GL_COLOR_BUFFER_BIT)
    gl.glDisable(GL.GL_DEPTH_TEST)
    gl.glDisable(GL.GL_BLEND)
    self:showDataTexture()

    -- Restore state
    gl.glViewport(vpsz[0], vpsz[1], vpsz[2], vpsz[3])
    gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, fb[0])

    --TODO: will viewport ever change on us? Can we cache it once at init?

    self.pingpong01 = 1 - self.pingpong01
end

function simple_quads:showDataTexture()
    gl.glUseProgram(self.quadprog)

    local dim = math.ceil(math.sqrt(self.n_quads))
    local res_loc = gl.glGetUniformLocation(self.quadprog, "uRes") --todo: cache uni
    gl.glUniform1f(res_loc, 1/dim)

    local fboidx = 1 + self.pingpong01
    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.fbos[fboidx].tex)
    local stex_loc = gl.glGetUniformLocation(self.quadprog, "tex") --todo: cache uni
    gl.glUniform1i(stex_loc, 0)

    local vl = gl.glGetAttribLocation(self.quadprog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
    
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

------------------------------------------
-- Timestep
------------------------------------------
function simple_quads:timestep(absTime, dt)
    if self.do_motion then
        self:renderToTexture()
    end
end

------------------------------------------
-- Control
------------------------------------------
function simple_quads:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function simple_quads:onButtonPressed(padIndex, button)
    if button == lri.JOYPAD_Y then
        snd.playSound("test.wav")
    elseif button == lri.JOYPAD_X then
        snd.playSound("pop_drip.wav")

    elseif button == lri.JOYPAD_B then

    elseif button == lri.JOYPAD_R then

    elseif button == lri.JOYPAD_L then
        self:renderToTexture()

    elseif button == lri.JOYPAD_UP then
        self.n_quads = self.n_quads * 2
        self.n_quads = math.min(self.n_quads, 1024*1024)
        self:initBounceBuffers()
    elseif button == lri.JOYPAD_DOWN then
        self.n_quads = self.n_quads * .5
        self.n_quads = math.max(self.n_quads, 1)
        self:initBounceBuffers()

    elseif button == lri.JOYPAD_LEFT then
        -- TODO randomize_positions
    elseif button == lri.JOYPAD_RIGHT then
        -- TODO grid_positions

    elseif button == lri.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
        
    elseif button == lri.JOYPAD_START then
        self.do_motion = not self.do_motion
    end
end

return simple_quads
