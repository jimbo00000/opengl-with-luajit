--[[ simple_raymarch.lua

    A fullscreen quad drawn with shader.
    Same as a new shadertoy, screen space coords mapped to rg.
]]
local DO_FONTS = true

simple_raymarch = {}
simple_raymarch.__index = simple_raymarch

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local glf = require 'util.glfont'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'

function simple_raymarch.new(...)
    local self = setmetatable({}, simple_raymarch)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_raymarch:setDataDirectory(dir)
    self.dataDir = dir
    if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_raymarch:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}
    self.draw_font = true
    self.n_steps = 16
end

local vs=[[
#version 100
attribute vec2 vP;
varying vec3 vfC;
void main()
{
    vfC=vec3(vP,0.);
    gl_Position=vec4(vP,0.,1.);
}
]]

-- https://www.shadertoy.com/view/4t3fzn
local shadertoySource = [[
float sdSphere(vec3 p, float r)
{
    return length(p)-r;
}

float SDF(vec3 pos)
{
    float t = sdSphere(pos-vec3(0,0,10), 6.0);
    
    return t;
}

vec3 calcNormal(vec3 pos)
{
    // Center sample
    float c = SDF(pos);
    // Use offset samples to compute gradient / normal
    vec2 eps_zero = vec2(0.001, 0.0);
    return normalize(vec3(
        SDF(pos + eps_zero.xyy),
        SDF(pos + eps_zero.yxy),
        SDF(pos + eps_zero.yyx)) - c);
}

float castRay(vec3 rayOrigin, vec3 rayDir)
{
    float t = 0.0; // Stores current distance along ray
    
    for (int i = 0; i < RAYMARCH_STEPS; i++)
    {
        float res = SDF(rayOrigin + rayDir * t);
        if (res < (0.0001*t))
        {
            return t;
        }
        t += res;
    }
    
    return -1.0;
}

vec3 render(vec3 rayOrigin, vec3 rayDir)
{
    vec3 col;
    float t = castRay(rayOrigin, rayDir);

    vec3 L = normalize(vec3(sin(iTime)*1.0, cos(iTime*0.5)+0.5, -0.5));

    if (t == -1.0)
    {
        col = vec3(0.30, 0.36, 0.60) - rayDir.y*0.4;
    }
    else
    {   
        vec3 pos = rayOrigin + rayDir * t;
        vec3 N = calcNormal(pos);

        vec3 objectSurfaceColour = vec3(0.4, 0.8, 0.1);
        // L is vector from surface point to light, N is surface normal. N and L must be normalized!
        float NoL = max(dot(N, L), 0.0);
        vec3 LDirectional = vec3(1.80,1.27,0.99) * NoL;
        vec3 LAmbient = vec3(0.03, 0.04, 0.1);
        vec3 diffuse = objectSurfaceColour * (LDirectional + LAmbient);
        
        col = diffuse;
        
        
        float shadow = 0.0;
        vec3 shadowRayOrigin = pos + N * 0.01;
        vec3 shadowRayDir = L;
        float t = castRay(shadowRayOrigin, shadowRayDir);
        if (t >= -1.0)
        {
            shadow = 1.0;
        }
        col = mix(col, col*0.8, shadow);
        
        // Visualize normals:
        //col = N * vec3(0.5) + vec3(0.5);
    }
    
    return col;
}

vec3 getCameraRayDir(vec2 uv, vec3 camPos, vec3 camTarget)
{
    vec3 camForward = normalize(camTarget - camPos);
    vec3 camRight = normalize(cross(vec3(0.0, 1.0, 0.0), camForward));
    vec3 camUp = normalize(cross(camForward, camRight));
                              
    float fPersp = 2.0;
    vec3 vDir = normalize(uv.x * camRight + uv.y * camUp + camForward * fPersp);

    return vDir;
}

vec2 normalizeScreenCoords(vec2 screenCoord)
{
    vec2 result = 2.0 * (screenCoord);///iResolution.xy - 0.5);
    //result.x *= iResolution.x/iResolution.y; // Correct for aspect ratio
    return result;
}

void mainImage(out vec4 fragColor, vec2 fragCoord)
{
    vec3 camPos = vec3(0, 0, -1);
    vec3 at = vec3(0, 0, 0);
    
    vec2 uv = normalizeScreenCoords(fragCoord);
    vec3 rayDir = getCameraRayDir(uv, camPos, at);  
    
    vec3 col = render(camPos, rayDir);
    
    col = pow(col, vec3(0.4545)); // Gamma correction (1.0 / 2.2)
    
    fragColor = vec4(col,1.0); // Output to screen
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
uniform float iTime;
#define RAYMARCH_STEPS $RAYMARCH_STEPS
]]
..shadertoySource..
[[
void main()
{
    vec4 col = vec4(0.);
    mainImage(col, vfC.xy);
    gl_FragColor = col;
}
]]

function simple_raymarch:populateVBO()
    local v = ffi.typeof('GLfloat[?]')(12,{-1,-1,1,-1,1,1,-1,-1,1,1,-1,1,})
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(v), v, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_raymarch:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_raymarch:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function simple_raymarch:init_shader()
    if self.prog > 0 then
        gl.glDeleteProgram(self.prog)
        self.prog = 0
    end
    local fragSrc = string.format("%s", fs)
    fragSrc = string.gsub(fragSrc, "$RAYMARCH_STEPS", tostring(self.n_steps))
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fragSrc})
end

function simple_raymarch:initGL()
    self:init_shader()

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function simple_raymarch:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
    
    if self.glfont then
        self.glfont:exitGL()
    end
end

function simple_raymarch:render()
    gl.glClearColor(0,1,0,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local u_loc = gl.glGetUniformLocation(self.prog, "iTime")

    -- NOTE: rpi requires re-setting VBO data each frame. Why?
    --gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos[1][0])
    self:bindVBO()

    gl.glUniform1f(u_loc, dt)
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6)

    if self.draw_font then
        self:drawtext()
    end

    self:unbindVBO()
end

function simple_raymarch:drawtext()
    if not self.glfont then return end
    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}

    local str = 'simple_raymarch n='..self.n_steps
    self.glfont:render_string(m, p, col, str)
end

function simple_raymarch:timestep(absTime, dt)
    self.t = absTime
end

function simple_raymarch:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function simple_raymarch:onButtonPressed(padIndex, button)
    self.t_last = self.t
    
    local soundmap = {
        [lr_input.JOYPAD_A] = "pop_drip.wav",
        [lr_input.JOYPAD_B] = "Jump5.wav",
        [lr_input.JOYPAD_X] = "Pickup_Coin7.wav",
        [lr_input.JOYPAD_Y] = "test.wav",
    }
    local s = soundmap[button]
    if s then snd.playSound(s) end

    if button == lr_input.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    elseif button == lr_input.JOYPAD_UP then
        self.n_steps = self.n_steps * 2
        self.n_steps = math.min(self.n_steps, 1024)
        self:init_shader()
    elseif button == lr_input.JOYPAD_DOWN then
        self.n_steps = self.n_steps * .5
        self.n_steps = math.max(self.n_steps, 1)
        self:init_shader()
    end
end

return simple_raymarch
