--[[ simple_shader.lua

 The quickest and easiest scene that:
  - draws something 
  - plays sounds
  - takes input
]]
local DO_FONTS = true

simple_shader = {}
simple_shader.__index = simple_shader

local ffi = require "ffi"
local sf = require "util.shaderfunctions"

require("util.glfont")
local mm = require("util.matrixmath")
local glfont = nil
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'

function simple_shader.new(...)
    local self = setmetatable({}, simple_shader)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_shader:setDataDirectory(dir)
    self.dataDir = dir
    if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_shader:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}
    self.draw_font = true
end

vs=[[
#version 100
attribute vec2 vP;
varying vec3 vfC;
void main()
{
    vfC=vec3(vP,0.);
    gl_Position=vec4(vP,0.,1.);
}
]]

fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
uniform float uBlue;

#line 77

/*
https://www.shadertoy.com/view/ltfXD2
16 segment display

Segment bit positions:

  __2__ __1__
 |\    |    /|
 | \   |   / |
 3  11 10 9  0
 |   \ | /   |
 |    \|/    |
  _12__ __8__
 |           |
 |    /|\    |
 4   / | \   7
 | 13 14  15 |
 | /   |   \ |
  __5__|__6__

15                 0
 |                 |
 0000 0000 0000 0000

example: letter A

   12    8 7  4 3210
    |    | |  | ||||
 0001 0001 1001 1111

 binary to hex -> 0x119F
 
 float c_a = float(0x119F)
*/

float c_a = float(0x119F);
float c_b = float(0x927E);
float c_c = float(0x007E);
float c_d = float(0x44E7);
float c_e = float(0x107E);
float c_f = float(0x101E);
float c_g = float(0x807E);
float c_h = float(0x1199);
float c_i = float(0x4466);
float c_j = float(0x4436);
float c_k = float(0x9218);
float c_l = float(0x0078);
float c_m = float(0x0A99);
float c_n = float(0x8899);
float c_o = float(0x00FF);
float c_p = float(0x111F);
float c_q = float(0x80FF);
float c_r = float(0x911F);
float c_s = float(0x8866);
float c_t = float(0x4406);
float c_u = float(0x00F9);
float c_v = float(0x2218);
float c_w = float(0xA099);
float c_x = float(0xAA00);
float c_y = float(0x4A00);
float c_z = float(0x2266);

float c_com = float(0x2000);
float c_spc = 0.0;

float c_nl = -1.0;

//Distance to line p0,p1 at uv
float dseg(vec2 p0,vec2 p1,vec2 uv)
{
    vec2 dir = normalize(p1 - p0);
    uv = (uv - p0) * mat2(dir.x, dir.y,-dir.y, dir.x);
    return distance(uv, clamp(uv, vec2(0), vec2(distance(p0, p1), 0)));   
}

//Checks if bit 'b' is set in number 'n'
bool bit(float n, float b)
{
    return mod(floor(n / exp2(floor(b))), 2.0) != 0.0;
}

//Distance to the character defined in 'bits'
float dchar(float bits, vec2 uv)
{
    float d = 1e6;  

    float n = floor(bits);
    
    if(bits != 0.0)
    {
        //Segment layout and checking.
        d = bit(n,  0.0) ? min(d, dseg(vec2( 0.500,  0.063), vec2( 0.500,  0.937), uv)) : d;
        d = bit(n,  1.0) ? min(d, dseg(vec2( 0.438,  1.000), vec2( 0.063,  1.000), uv)) : d;
        d = bit(n,  2.0) ? min(d, dseg(vec2(-0.063,  1.000), vec2(-0.438,  1.000), uv)) : d;
        d = bit(n,  3.0) ? min(d, dseg(vec2(-0.500,  0.937), vec2(-0.500,  0.062), uv)) : d;
        d = bit(n,  4.0) ? min(d, dseg(vec2(-0.500, -0.063), vec2(-0.500, -0.938), uv)) : d;
        d = bit(n,  5.0) ? min(d, dseg(vec2(-0.438, -1.000), vec2(-0.063, -1.000), uv)) : d;
        d = bit(n,  6.0) ? min(d, dseg(vec2( 0.063, -1.000), vec2( 0.438, -1.000), uv)) : d;
        d = bit(n,  7.0) ? min(d, dseg(vec2( 0.500, -0.938), vec2( 0.500, -0.063), uv)) : d;
        d = bit(n,  8.0) ? min(d, dseg(vec2( 0.063,  0.000), vec2( 0.438, -0.000), uv)) : d;
        d = bit(n,  9.0) ? min(d, dseg(vec2( 0.063,  0.063), vec2( 0.438,  0.938), uv)) : d;
        d = bit(n, 10.0) ? min(d, dseg(vec2( 0.000,  0.063), vec2( 0.000,  0.937), uv)) : d;
        d = bit(n, 11.0) ? min(d, dseg(vec2(-0.063,  0.063), vec2(-0.438,  0.938), uv)) : d;
        d = bit(n, 12.0) ? min(d, dseg(vec2(-0.438,  0.000), vec2(-0.063, -0.000), uv)) : d;
        d = bit(n, 13.0) ? min(d, dseg(vec2(-0.063, -0.063), vec2(-0.438, -0.938), uv)) : d;
        d = bit(n, 14.0) ? min(d, dseg(vec2( 0.000, -0.938), vec2( 0.000, -0.063), uv)) : d;
        d = bit(n, 15.0) ? min(d, dseg(vec2( 0.063, -0.063), vec2( 0.438, -0.938), uv)) : d;
    }
    
    return d;
}

const int NUM_CHARS = 16;

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec2 uv = ( fragCoord+vec2(.5,.5)  ) * 8.0;
    
    float ch[NUM_CHARS];
    
    ch[ 0] = c_h;
    ch[ 1] = c_e;
    ch[ 2] = c_l;
    ch[ 3] = c_l;
    ch[ 4] = c_o;
    ch[ 5] = c_com;
    ch[ 6] = c_nl;
    ch[ 7] = c_s;
    ch[ 8] = c_h;
    ch[ 9] = c_a;
    ch[10] = c_d;
    ch[11] = c_e;
    ch[12] = c_r;
    ch[13] = c_t;
    ch[14] = c_o;
    ch[15] = c_y;
    
    //Printing and spacing
    vec2 ch_size = vec2(1.0, 2.0);
    vec2 ch_space = ch_size + vec2(0.25, 0.25);
    
    vec2 uvc = mod(uv, ch_space) - (ch_size / 2.0) - 0.125; //Per-character repeated uv space
    vec2 uvt = floor(uv / ch_space); //Character screen position
    
    float char2 = 0.0; //Character to print
    
    vec2 cursor = vec2(0.0,2.0); //Print position cursor
    
    float index = 0.0; //Character index (for animation)
    
    for(int i = 0;i < NUM_CHARS;i++)
    {
        if(uvt == cursor)
        {
            if(ch[i] >= 0.0) //Don't print control characters.
            {
                char2 = ch[i];
            }
            break;
        }
        
        if(ch[i] == c_nl) //Insert a new line after c_nl
        {
            cursor.y--;
            cursor.x = 0.0;
        }
        else
        {
            index++; 
            cursor.x++; //Move cursor.
        }
    }
    
    //Glitch fade-in animation
    float anim_time = clamp((1.-uBlue) * 2., 0.0, 1.0) * float(NUM_CHARS);
    
    char2 = mix(0.0, char2, clamp(anim_time - index, 0.0, 1.0));
    
    float dist = dchar(char2, uvc);
    
    //Shading
    vec3 color = vec3(0.0);
    
    color = mix(vec3(2.0, 0.8, 0.1), vec3(0.0, 0.0, 0.0), smoothstep(0.01, 0.04, dist) - (0.0001 / (dist * dist)));
    
    fragColor = vec4(color, 1.0);

}

/*
void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec3 col = .5*vec3(fragCoord,.5)+vec3(.5);
    col.b = uBlue;
    fragColor = vec4(col,1.);
}
*/

void main()
{
    vec4 outcol;
    mainImage(outcol, vfC.xy);
    gl_FragColor = outcol;
}
]]

function simple_shader:populateVBO()
    local v = ffi.typeof('GLfloat[?]')(12,{-1,-1,1,-1,1,1,-1,-1,1,1,-1,1,})
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(v), v, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_shader:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_shader:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function simple_shader:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        glfont = GLFont.new(fontname..'.fnt', fontname..'_0.raw')
        glfont:setDataDirectory(self.dataDir.."/fonts")
        glfont:initGL()
    end
end

function simple_shader:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
    
    if glfont then
        glfont:exitGL()
    end
end

function simple_shader:render()
    gl.glClearColor(0,1,0,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")

    -- NOTE: rpi requires re-setting VBO data each frame. Why?
    --gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos[1][0])
    self:bindVBO()

    gl.glUniform1f(u_loc, b)
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6)

    if self.draw_font then
        self:drawtext()
    end

    self:unbindVBO()
end

function simple_shader:drawtext()
    if not glfont then return end
    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    glfont:render_string(m, p, col, "simple_shader")
end

function simple_shader:timestep(absTime, dt)
    self.t = absTime
end

function simple_shader:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function simple_shader:onButtonPressed(padIndex, button)
    self.t_last = self.t
    
    local soundmap = {
        [lr_input.JOYPAD_A] = "pop_drip.wav",
        [lr_input.JOYPAD_B] = "Jump5.wav",
        [lr_input.JOYPAD_X] = "Pickup_Coin7.wav",
        [lr_input.JOYPAD_Y] = "test.wav",
    }
    local s = soundmap[padIndex]
    if s then snd.playSound(s) end

    if button == lr_input.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    end
end

return simple_shader
