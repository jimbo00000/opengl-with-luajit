--[[ atparty_shader.lua

    A shaderfrom shadertoy drawn to a fullscreen quad.
]]
local DO_FONTS = true

atparty_shader = {}
atparty_shader.__index = atparty_shader

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'
local glf = require 'util.glfont'
local snd = require 'util.audio'

function atparty_shader.new(...)
    local self = setmetatable({}, atparty_shader)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function atparty_shader:setDataDirectory(dir)
    self.dataDir = dir
    if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function atparty_shader:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}
    self.draw_font = true
end

local vs=[[
#version 100
attribute vec2 vP;
varying vec2 vfC;
void main()
{
    vfC = vP;
    gl_Position=vec4(vP,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec2 vfC;
uniform float uBlue;

// raymarched @party logo
//uniform 
mat4 modelMtx = mat4(1.);

uniform float time;

// https://www.shadertoy.com/view/Mll3W2
//#define PARTY_ON

#define t (3.*time)
float party = 0.;

// math
const float PI = 3.14159265359;
const float DEG_TO_RAD = PI / 180.0;
mat3 rotationX(float r) {
    float ct=cos(r), st=sin(r);
    return mat3(1., 0.,  0.,  0., ct, -st,  0., st,  ct);}
mat3 rotationY(float r) {
    float ct=cos(r), st=sin(r);
    return mat3(ct, 0., st,  0., 1., 0.,  -st, 0., ct);}
mat3 rotationZ(float r) {
    float ct=cos(r), st=sin(r);
    return mat3(ct, -st, 0.,  st, ct, 0.,  0., 0., 1.);}
mat3 rotationXY(vec2 angle) {
    vec2 c = cos(angle);
    vec2 s = sin(angle);
    return mat3(
        c.y    ,  0.0, -s.y,
        s.y*s.x,  c.x,  c.y*s.x,
        s.y*c.x, -s.x,  c.y*c.x);
}


// libiq

// exponential smooth min (k = 32);
float smine( float a, float b, float k )
{
    float res = exp( -k*a ) + exp( -k*b );
    return -log( res )/k;
}

// polynomial smooth min (k = 0.1);
float smin( float a, float b, float k )
{
    float h = clamp( 0.5+0.5*(b-a)/k, 0.0, 1.0 );
    return mix( b, a, h ) - k*h*(1.0-h);
}

////////////// DISTANCE FUNCTIONS
//
// Primitives from http://www.iquilezles.org/www/articles/distfunctions/distfunctions.htm
//
float udBox( vec3 p, vec3 b )
{
  return length(max(abs(p)-b,0.0));
}
float sdBox( vec3 p, vec3 b )
{
  vec3 d = abs(p) - b;
  return min(max(d.x,max(d.y,d.z)),0.0) +
         length(max(d,0.0));
}
float udRoundBox( vec3 p, vec3 b, float r )
{
    return length(max(abs(p)-b,0.0))-r;
}
float sdPlane( vec3 p, vec4 n )
{
    // n must be normalized
    return dot(p,n.xyz) + n.w;
}

//
// Composites
//
float rollprism(vec3 pos)
{
    return max(
        udRoundBox(pos, vec3(2.,1.4,5.), 3.5),
        -sdPlane(pos, vec4(normalize(vec3(-1.,0.,0.)),3.))
        );
}

float rollprism2(vec3 pos)
{
    return max(
        udRoundBox(pos, vec3(2.,2.,5.), 7.5),
        -udRoundBox(pos, vec3(2.,2.,15.), 5.5)
        );
}

float atsign(vec3 pos)
{
    float d = min(
        rollprism(pos-vec3(1.,0.,0.)),
        rollprism2(pos)
        );
    d = smin(d,
             sdBox(pos-vec3(4.2,-3.5,0.), vec3(3.5,1.4,5.))
            ,1.);    
    d = max(d, -sdBox(pos-vec3(6.,-8.4,0.), vec3(6.,3.5,12.))); // chop horiz
    
    // chop off front and back
    float w = 0.;//1.+sin(t);
    d = max(d, -sdPlane(pos                 , vec4(vec3(0.,0.,-1.),0.)));
    d = max(d, -sdPlane(pos-vec3(0.,0.,-1.5-w), vec4(vec3(0.,0.,1.),0.)));
    return d;
}

float attail(vec3 pos)
{
    float s = -0.+0.5*pow((1.+0.1*pos.x),1.7);
    return sdBox(pos, vec3(5.,0.8+s,.6+s));
}

float at(vec3 pos)
{
    // dance
    float p = 0.02*party;
    pos = rotationY(p*pos.y*sin(3.*t)) * pos;
    pos = rotationX(p*pos.y*sin(5.*t)) * pos;
    
    return min(
        atsign(pos),
        attail(
            rotationY(-.03*(pos.x+7.)) *
            (pos-vec3(3.,-8.5,.0)))
        );
}

float DE_atlogo( vec3 pos )
{
    mat3 rotmtx = mat3(modelMtx);//1.);//getMouseRotMtx();
    pos = rotmtx * pos;
    float d2 = 9999.;
    return min(d2, at(pos));
}


//
// lighting and shading
//
vec3 shading( vec3 v, vec3 n, vec3 eye ) {
    float shininess = 16.0;
    vec3 ev = normalize( v - eye );
    vec3 ref_ev = reflect( ev, n ); 
    vec3 final = vec3( 0.0 );
    // disco light
    {
        vec3 light_pos   = vec3( 1.0, 10.0, 30.0 );
        float p = party;
        vec3 light_color = vec3(p*sin(3.*t), p*sin(3.3*t), p*sin(4.*t));
        vec3 vl = normalize( light_pos - v );
    
        float diffuse  = 0.;//max( 0.0, dot( vl, n ) );
        float specular = max( 0.0, dot( vl, ref_ev ) );
        specular = pow( specular, shininess );
        
        final += light_color * ( diffuse + specular ); 
    }
    
    // white light
    {
        vec3 light_pos   = vec3( 10.0, 20.0, -10.0 );
        //vec3 light_pos   = vec3( -20.0, -20.0, -20.0 );
        vec3 light_color = vec3( 1.);//0.3, 0.7, 1.0 );
        vec3 vl = normalize( light_pos - v );
    
        shininess = 8.;
        float diffuse  = 0.;//max( 0.0, dot( vl, n ) );
        float specular = max( 0.0, dot( vl, ref_ev ) );
        specular = pow( specular, shininess );
        
        final += light_color * ( diffuse + specular ); 
    }

    return final;
}

// get gradient in the world
vec3 gradient( vec3 pos ) {
    const float grad_step = 0.31;
    const vec3 dx = vec3( grad_step, 0.0, 0.0 );
    const vec3 dy = vec3( 0.0, grad_step, 0.0 );
    const vec3 dz = vec3( 0.0, 0.0, grad_step );
    return normalize (
        vec3(
            DE_atlogo( pos + dx ) - DE_atlogo( pos - dx ),
            DE_atlogo( pos + dy ) - DE_atlogo( pos - dy ),
            DE_atlogo( pos + dz ) - DE_atlogo( pos - dz )   
        )
    );
}

// ray marching
float ray_marching( vec3 origin, vec3 dir, float start, float end ) {
    const int max_iterations = 255;
    const float stop_threshold = 0.001;
    float depth = start;
    for ( int i = 0; i < max_iterations; i++ ) {
        float dist = DE_atlogo( origin + dir * depth );
        if ( dist < stop_threshold ) {
            return depth;
        }
        depth += dist;
        if ( depth >= end) {
            return end;
        }
    }
    return end;
}

// get ray direction from pixel position
vec3 ray_dir( float fov, vec2 xy ) {
    float cot_half_fov = tan( ( 90.0 - fov * 0.5 ) * DEG_TO_RAD );  
    float z = 1. * cot_half_fov;
    
    return normalize( vec3( xy, -z ) );
}


vec4 getSceneColor_atlogo( in vec3 ro, in vec3 rd )
{
    const float clip_far = 100.0;
    float depth = ray_marching( ro, rd, -10.0, clip_far );
    if ( depth >= clip_far ) {
        return vec4(vec3(.5), 0.);
    }
    vec3 pos = ro + rd * depth;
    vec3 n = gradient( pos );
    return vec4(shading( pos, n, ro ), 1.);
}


//
// The cube
//
mat3 getCubeMtx()
{
    return rotationY(.8+party*pow(abs(sin(PI*2.*time)),5.)) * rotationX(.3);
}

float DE_cube( vec3 pos )
{
    pos = getCubeMtx() * mat3(modelMtx) * pos;
    return udRoundBox(pos, vec3(1.5), .15);
}

vec3 shade_cube(vec3 v, vec3 n, vec3 ntx, vec3 eye) {
    vec3 ev = normalize(v - eye);
    vec3 final = vec3(0.);
    vec3 light_pos = vec3(-10.,20.,40.);
    vec3 vl = normalize(light_pos - v);
    float diffuse = max(0.0, dot( vl, n ));
    final += 1.3 * diffuse; 
    // transform normals with the cube to find flat faces/edges
    float px = abs(dot(ntx,vec3(1.,0.,0.)));
    float py = abs(dot(ntx,vec3(0.,1.,0.)));
    float pz = abs(dot(ntx,vec3(0.,0.,1.)));
    float p = max(px,max(py,pz));
    final *= smoothstep(0.9,1.,length(p));
    return final;
}

vec3 grad_cube(vec3 pos) {
    const float gs = 0.02;
    const vec3 dx = vec3(gs, 0., 0.);
    const vec3 dy = vec3(0., gs, 0.);
    const vec3 dz = vec3(0., 0., gs);
    return normalize( vec3(
            DE_cube(pos + dx) - DE_cube(pos - dx),
            DE_cube(pos + dy) - DE_cube(pos - dy),
            DE_cube(pos + dz) - DE_cube(pos - dz)   
        ));
}

float raymarch_cube(vec3 origin, vec3 dir, float start, float end) {
    const int max_iterations = 64;
    const float stop_threshold = 0.01;
    float depth = start;
    for (int i=0; i<max_iterations; i++) {
        float dist = DE_cube(origin + dir*depth);
        if (dist < stop_threshold) return depth;
        depth += dist;
        if (depth >= end) return end;
    }
    return end;
}

vec4 getCubeColor(in vec3 ro, in vec3 rd) {
    const float clip_far = 100.0;
    float depth = raymarch_cube(ro, rd, 0., clip_far);
    if ( depth >= clip_far )
        return getSceneColor_atlogo(ro,rd);
    vec3 pos = ro + rd * depth;
    vec3 ne = grad_cube(pos);
    vec3 ntx = getCubeMtx() * ne;
    return vec4(shade_cube(pos, ne, ntx, ro), 1.);
}

///////////////////////////////////////////////////////////////////////////////
// Patch in the Rift's heading to raymarch shader writing out color and depth.
// http://blog.hvidtfeldts.net/

// Translate the origin to the camera's location in world space.
vec3 getEyePoint(mat4 mvmtx)
{
    vec3 ro = -mvmtx[3].xyz;
    return ro;
}

// Construct the usual eye ray frustum oriented down the negative z axis.
// http://antongerdelan.net/opengl/raycasting.html
vec3 getRayDirection(vec2 uv)
{
    vec4 ray_clip = vec4(uv.x, uv.y, -1., 1.);
    vec4 ray_eye = ray_clip;
    return normalize(vec3(ray_eye.x, ray_eye.y, -1.));
}

void main()
{
#ifdef PARTY_ON
    party = 1.;
#endif

    vec2 uv = vfC;
    uv.x *= 1.3;
    vec2 uv11 = uv;// * 2.0 - vec2(1.0);
    uv11 *= 1.2;
    uv11.y -= .06;
    vec3 ro = vec3(0.,0.,4.);//getEyePoint(viewMtx);
    vec3 rd = getRayDirection(uv11);

    ro.z += 10.;

    gl_FragColor = getCubeColor(ro, rd);
}

]]

function atparty_shader:populateVBO()
    local v = ffi.typeof('GLfloat[?]')(12,{-1,-1,1,-1,1,1,-1,-1,1,1,-1,1,})
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(v), v, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function atparty_shader:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function atparty_shader:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function atparty_shader:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function atparty_shader:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
    
    if self.glfont then
        self.glfont:exitGL()
    end
end

function atparty_shader:render()
    gl.glClearColor(0,1,0,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")

    local tloc = gl.glGetUniformLocation(self.prog, "time")
    gl.glUniform1f(tloc, self.t)

    -- NOTE: rpi requires re-setting VBO data each frame. Why?
    --gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos[1][0])
    self:bindVBO()

    gl.glUniform1f(u_loc, b)
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6)

    if self.draw_font then
        self:drawtext()
    end

    self:unbindVBO()
end

function atparty_shader:drawtext()
    if not self.glfont then return end
    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    self.glfont:render_string(m, p, col, "atparty_shader")
end

function atparty_shader:timestep(absTime, dt)
    self.t = absTime
end

function atparty_shader:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function atparty_shader:onButtonPressed(padIndex, button)
    self.t_last = self.t
    
    local soundmap = {
        [lr_input.JOYPAD_A] = "pop_drip.wav",
        [lr_input.JOYPAD_B] = "Jump5.wav",
        [lr_input.JOYPAD_X] = "Pickup_Coin7.wav",
        [lr_input.JOYPAD_Y] = "test.wav",
    }
    local s = soundmap[padIndex]
    if s then snd.playSound(s) end

    if button == lr_input.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    end
end

return atparty_shader
