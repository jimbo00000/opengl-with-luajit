--[[ simple_container.lua

    Draws a single inner scene with a post processing effect.
]]

local ffi = require 'ffi'

simple_container = {}
simple_container.__index = simple_container

function simple_container.new(...)
    local self = setmetatable({}, simple_container)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_container:setDataDirectory(dir)
    self.datadir = dir -- cache this; self.Scene is not created yet
end

function simple_container:init()
end

function simple_container:initGL()
    local name =
        --'frametrail'
        'benchmarks.simple_quads3'
        --'simple_sprite'

    local status,err = pcall(function ()
        local SceneLibrary = require(name)
        self.Scene = SceneLibrary.new()
    end)
    if not status then
        print(err)
    end
    if self.Scene then
        if self.Scene.setDataDirectory then self.Scene:setDataDirectory(self.datadir) end
        if self.Scene.resizeViewport then self.Scene:resizeViewport(win_w,win_h) end
        self.Scene:initGL()
    end

    local PostFXLibrary = require(
        'effect.effect_chain'
        --'effect.effect_es2'
        )
    local params = {}
    params.filter_names = {
        'invert',
        'wobble',
        --'passthrough',
    }
    local status,err = pcall(function ()
        self.PostFX = PostFXLibrary.new(params)
        self.PostFX:initGL(self.win_w or 320, self.win_h or 240) -- typical libretro resolution
        self.PostFX.uniforms.wobbleSpeed = 5
    end)
    if not status then
        print(err)
    end
end

function simple_container:exitGL()
    if self.Scene and self.Scene.exitGL then self.Scene:exitGL() end
end

function simple_container:resizeViewport(w, h)
    self.win_w = w
    self.win_h = h
    if self.Scene and self.Scene.resize then self.Scene:resize(w,h) end
    if self.PostFX then self.PostFX:resize_fbo(w,h) end
end

function simple_container:render()
    local fb = ffi.typeof('GLint[?]')(1)
    gl.glGetIntegerv(GL.GL_FRAMEBUFFER_BINDING, fb)

    if self.PostFX then self.PostFX:bind_fbo() end

    local b = .6
    --gl.glClearColor(b,b,b,0.8)
    --gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    if self.Scene and self.Scene.render then self.Scene:render() end
    if self.PostFX then self.PostFX:unbind_fbo() end

    gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, fb[0])

    -- Apply post-processing and present
    if self.PostFX then
        gl.glDisable(GL.GL_DEPTH_TEST)
        gl.glViewport(0,0, self.win_w, self.win_h)
        self.PostFX:present(self.win_w, self.win_h)
    end
end

function simple_container:timestep(absTime, dt)
    if self.Scene and self.Scene.timestep then self.Scene:timestep(absTime, dt) end
    if self.PostFX and self.PostFX.timestep then self.PostFX:timestep(absTime, dt) end
end

function simple_container:update_retropad(joystates)
    if self.Scene and self.Scene.update_retropad then self.Scene:update_retropad(joystates) end
end

function simple_container:on_button_pressed(i)
    if self.Scene and self.Scene.on_button_pressed then self.Scene:on_button_pressed(i) end
end

return simple_container
