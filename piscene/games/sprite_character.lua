--[[ sprite_character.lua

    Draws a sprite in the center of the screen.
    Takes controller input and changes the sprite's texture by last direction.
    Also does abs(sin) position animation for added interest.

    Meant to be overlaid on top of a map(which is what really does the scrolling).
]]
local DO_FONTS = false

sprite_character = {}
sprite_character.__index = sprite_character

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'

function sprite_character.new(...)
    local self = setmetatable({}, sprite_character)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function sprite_character:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function sprite_character:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.movers = {}
    self.lastdir = 1
    self.draw_font = false
end

function sprite_character:loadTexture(texfilename, w, h)
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end
    local inp = assert(io.open(texfilename, "rb"))
    local data = inp:read("*all")
    assert(inp:close())
    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    local texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
                  w, h, 0,
                  GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
    return texID
end


local vs=[[
#version 100
uniform vec2 uOffset;
attribute vec2 vP;
varying vec3 vfC;
void main()
{
    vfC=vec3(vP,0.);
    gl_Position=vec4(.2*vP+uOffset,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
uniform float uBlue;
uniform sampler2D tex;

void main()
{
    vec2 tc = vec2(.5)+.5*vfC.xy;
    vec4 col = texture2D(tex, tc);
    col.b = uBlue;
    gl_FragColor = col;
}
]]

function sprite_character:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function sprite_character:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function sprite_character:populateVBO()
    self:bindVBO()
    local s = 1
    local v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
end

function sprite_character:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end

    -- Sprites of the character for each direction
    self.tex1 = self:loadTexture('frog1.data', 32,32)
    self.tex2 = self:loadTexture('tongue1.data', 32,32)
    self.tex3 = self:loadTexture('fly1_spritesheet.data', 32,32)
end

function sprite_character:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    if self.glfont then
        self.glfont:exitGL()
    end

    local texdel = ffi.new("GLuint[1]", self.tex1) gl.glDeleteTextures(1,texdel)
    local texdel = ffi.new("GLuint[1]", self.tex2) gl.glDeleteTextures(1,texdel)
    local texdel = ffi.new("GLuint[1]", self.tex3) gl.glDeleteTextures(1,texdel)
end

function sprite_character:render()
    gl.glUseProgram(self.prog)

    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")
    gl.glUniform1f(u_loc, b)

    -- Select which sprite to use by last direction
    local texs = {
        self.tex1,
        self.tex2,
        self.tex3,
        self.tex1,
    }
    local texID = texs[self.lastdir]
    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, texID)
    local stex_loc = gl.glGetUniformLocation(self.prog, "tex")
    gl.glUniform1i(stex_loc, 0)


    gl.glEnable(GL.GL_BLEND)
    gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)


    self:bindVBO()
    for i=1,#self.movers do
        local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffset")
        local off = self.movers[i][1]
        local vel = self.movers[i][2]
        local y = off[2]
        local spd = math.sqrt(vel[1]*vel[1] + vel[2]*vel[2])
        y = y + .03 * math.abs(math.sin((1.+100*spd) * 4*self.t))
        gl.glUniform2f(uoff_loc, off[1], y)

        gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    end
    self:unbindVBO()

    gl.glDisable(GL.GL_BLEND)


    if self.glfont and self.draw_font then
        local m = {}
        local p = {}
        mm.make_identity_matrix(m)
        local s = .5
        mm.glh_scale(m, s, s, s)
        mm.glh_translate(m, 30, 330, 0)
        -- TODO getStringWidth and center text
        if not win_w then
            win_w,win_h = 800,600
        end
        mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
        gl.glDisable(GL.GL_DEPTH_TEST)
        local col = {1, 1, 1}
        self.glfont:render_string(m, p, col, "sprite_character x"..tostring(#self.movers))
    end
end

function sprite_character:timestep(absTime, dt)
    self.t = absTime
    for j=1,#self.movers do
        local vel = self.movers[j][2]
        for i=1,2 do
            local b = .2
            --self.movers[j][1][i] = math.min(math.max(-1+b,self.movers[j][1][i] + vel[i]),1-b)
        end
    end
end

function sprite_character:update_retropad(joystates)
    for j=1,1 do -- only works for first player
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:on_button_pressed(j,b)
                end
            end
        end

        local js = joystates[j]
        if js then

            -- Create mover if not already there
            if not self.movers[j] then
                local offset = {0,0}
                local vel = {0,0}
                self.movers[j] = {offset,vel}
            end

            local v = .01
            local vel = self.movers[j][2]
            vel[1] = 0
            vel[2] = 0

            if js[lr_input.JOYPAD_LEFT] ~= 0 then
                vel[1] = -v
                self.lastdir = 1
            elseif js[lr_input.JOYPAD_RIGHT] ~= 0 then
                vel[1] = v
                self.lastdir = 2
            elseif js[lr_input.JOYPAD_UP] ~= 0 then
                vel[2] = v
                self.lastdir = 3
            elseif js[lr_input.JOYPAD_DOWN] ~= 0 then
                vel[2] = -v
                self.lastdir = 4
            end
        end
    end
end

function sprite_character:on_button_pressed(padIndex, button)
    if button == lr_input.JOYPAD_A then
        snd.playSound("test.wav")
        self.t_last = self.t
    elseif button == lr_input.JOYPAD_B then
        snd.playSound("pop_drip.wav")
        self.t_last = self.t
    end

    if button == lr_input.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    end
end

return sprite_character
