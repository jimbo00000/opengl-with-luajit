--[[ simple_hud2.lua

]]

local DO_FONTS = true

simple_hud = {}
simple_hud.__index = simple_hud

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local glf = require 'util.glfont'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'

function simple_hud.new(...)
    local self = setmetatable({}, simple_hud)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_hud:setDataDirectory(dir)
    self.dataDir = dir
end

function simple_hud:loadTexture(texfilename, w, h)
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end
    local inp = assert(io.open(texfilename, "rb"))
    local data = inp:read("*all")
    assert(inp:close())
    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    local texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
                  w, h, 0,
                  GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
    return texID
end

function simple_hud:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}
    self.n_quads = 16
    self.max_quads = 16
    self.draw_font = false
end

local vs=[[
#version 100

attribute vec4 vP;
attribute float vInstId;
varying vec2 vfTexCoord;
uniform vec2 uOffsets[16]; // self.max_quads

void main()
{
    vec2 pt = vP.xy;
    //pt.y -= .1*vInstId;

    // Calculate tex offset into 4x4 sprite sheet
    vec2 tc = vP.zw;
    tc.y = 1.-tc.y;
    tc *= .25;
    int instId = int(vInstId);
    vfTexCoord = tc + uOffsets[instId];
    gl_Position = vec4(pt,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec2 vfTexCoord;
uniform float uBlue;
uniform sampler2D tex;

void main()
{
    vec4 col = texture2D(tex, vfTexCoord);

    col.b = uBlue;
    gl_FragColor = col;
}
]]

function simple_hud:populateVBO()
    local s = .04
    local spac = .1
    local aspect = 4/3

    local a = aspect
    local quadverts = {
        -s,-s*a, 0,0,
         s,-s*a, 1,0,
         s, s*a, 1,1,
        -s,-s*a, 0,0,
         s, s*a, 1,1,
        -s, s*a, 0,1,
    }

    local verts = {}
    for q=1,self.max_quads do
        for i=1,6 do
            for j=1,4 do
                local idx = j+(i-1)*4
                --print(idx)
                local val = quadverts[idx]

                if math.fmod(j,4) == 1 then
                    val = val - 1 + spac*q
                elseif math.fmod(j,4) == 2 then
                    val = val + .9
                end

                table.insert(verts, val)
            end
        end
    end

    local varr = ffi.typeof('GLfloat[?]')(#verts,verts)
    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(varr),varr,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)

    local idxs = {}
    for q=1,self.max_quads do
        for i=1,6 do
            table.insert(idxs,q-1)
        end
    end
    
    local iarr = ffi.typeof('GLfloat[?]')(#idxs,idxs)
    local vi = gl.glGetAttribLocation(self.prog,"vInstId")
    local ivbo = self.vbos[2]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,ivbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(iarr),iarr,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vi, 1, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vi)
end

function simple_hud:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)

    local vid = gl.glGetAttribLocation(self.prog, "vInstId")
    local ivbo = self.vbos[2]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, ivbo[0])
    gl.glVertexAttribPointer(vid, 1, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vid)
end

function simple_hud:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glDisableVertexAttribArray(vl)

    local vid = gl.glGetAttribLocation(self.prog, "vInstId")
    gl.glDisableVertexAttribArray(vid)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
end

function simple_hud:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)

    local ivbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,ivbo)
    table.insert(self.vbos, ivbo)

    self:populateVBO()

    self.texID = self:loadTexture('digits4x4.rgba', 32,32)

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function simple_hud:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    if self.glfont then
        self.glfont:exitGL()
    end
end

function simple_hud:getOffsetArrayFromString(digitstring)
    local digitToOffsets = {
        ['1']={  0,0},
        ['2']={.25,0},
        ['3']={.5 ,0},
        ['4']={.75,0},
        ['5']={  0, .25},
        ['6']={.25, .25},
        ['7']={.5 , .25},
        ['8']={.75, .25},
        ['9']={  0, .5},
        ['0']={.25, .5},
        ['.']={.5 , .5},
        [':']={.75, .5},
        ['/']={  0, .75},
        ['x']={.25, .75},
        ['v']={.5 , .75},
        ['-']={.75, .75},
    }
    local offsets = {}
    local num = #digitstring
    num = math.min(num, self.n_quads)
    for i=1,num do
        local ch = digitstring:sub(i,i)
        local off = digitToOffsets[ch]
        offsets[2*i-1] = off[1]
        offsets[2*i  ] = off[2]
    end
    return offsets
end

function simple_hud:render()
    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")
    gl.glUniform1f(u_loc, b)

    local texID = self.texID
    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, texID)
    local stex_loc = gl.glGetUniformLocation(self.prog, "tex")
    gl.glUniform1i(stex_loc, 0)

    -- tex offsets in uniform buffer per-character
    local digitstring = string.format('%d:%f-/xv', 12, dt)
    --local digitstring = '12:34.567-8/9'
    local offsets = self:getOffsetArrayFromString(digitstring)
    local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffsets")
    local o = ffi.typeof('GLfloat[?]')(#offsets, offsets)
    gl.glUniform2fv(uoff_loc, self.n_quads, o)

    self:bindVBO()
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6*self.n_quads)
    self:unbindVBO()

    if self.glfont and self.draw_font then
        local m = {}
        local p = {}
        mm.make_identity_matrix(m)
        local s = .5
        mm.glh_scale(m, s, s, s)
        mm.glh_translate(m, 30, 30, 0)
        -- TODO getStringWidth and center text
        if not win_w then
            win_w,win_h = 800,600
        end
        mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
        gl.glDisable(GL.GL_DEPTH_TEST)
        local col = {1, 1, 1}
        self.glfont:render_string(m, p, col, "simple_hud "..self.n_quads)
        mm.glh_translate(m, 30, 30, 0)
    end
end

function simple_hud:timestep(absTime, dt)
    self.t = absTime
end

function simple_hud:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function simple_hud:onButtonPressed(padIndex, button)
    if button == lr_input.JOYPAD_A then
        self.t_last = self.t
    elseif button == lr_input.JOYPAD_B then
        self.t_last = self.t
    elseif button == lr_input.JOYPAD_X then
        self.n_quads = self.n_quads + 1
        self.n_quads = math.min(self.n_quads, self.max_quads)
    elseif button == lr_input.JOYPAD_Y then
        self.n_quads = self.n_quads - 1
        self.n_quads = math.max(self.n_quads, 0)
    elseif button == lr_input.JOYPAD_SELECT then
        self.draw_font = not self.draw_font
    end
end

return simple_hud
