--[[ tiled_map_image.lua

    Load a map from a PNG file and draw a grid of textured tiles.
    Check the center point for collisions with map image pixels/tiles.
    Move the center point with controller input, check for collisions
    with tile types.

    Designed to work with a sprite drawn at the center point
    (sprite in a separate module).
]]

local DO_FONTS = true

tiled_map_image = {}
tiled_map_image.__index = tiled_map_image

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'

function tiled_map_image.new(...)
    local self = setmetatable({}, tiled_map_image)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function tiled_map_image:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function tiled_map_image:loadTexture(texfilename, w, h)
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end
    local inp = assert(io.open(texfilename, "rb"))
    local data = inp:read("*all")
    assert(inp:close())
    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    local texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
                  w, h, 0,
                  GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
    return texID
end

-- Load image data into tile map.
-- Color channels encode map tile properties.
function tiled_map_image:readPixelDataIntoMap(texfilename, w, h)
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end
    local inp = assert(io.open(texfilename, "rb"))
    local data = inp:read("*all")
    assert(inp:close())

    local size = w*h*4
    local buffer = ffi.new("uint8_t[?]", size)
    ffi.copy(buffer, data, size)
    for j=1,h do
        for i=1,w do
            local idx = (i-1)+(j-1)*w
            local lum = 0
            for k=1,4 do
                local pidx = 4*idx + k - 1
                local val = buffer[pidx]
                -- Store 4-bit channel mask
                if val > 0 then
                    lum = lum + math.pow(2,k)
                end
            end
            self.tiletable[idx] = lum
            --print('..'..w..h..' '..idx..' -> '..lum)
        end
    end
end

-- Returns index of cell at the given position
local function getCellXY(pos, xdim)
    local xt = -math.floor(pos[1])-1
    local yt = -math.floor(pos[2])-1
    if xt < 0 or xt >= xdim then return nil end
    if yt < 0 then return nil end
    if yt >= xdim then return nil end
    return xt + xdim*yt
end

function tiled_map_image:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}
    self.n_quads = 12
    self.gridscale = 1/8
    self.draw_font = false
    self.pos = {-0.1,-0.1}
    self.vel = {0,0}
    self.velscale = 1
    self.tiletable = {}
end

local vs=[[
#version 100

attribute vec4 vP;
varying vec4 vfTileMapUV;
uniform vec2 uOffset;
uniform vec2 uScale;

void main()
{
    vfTileMapUV.xy = vP.xy/32.;
    vfTileMapUV.zw = vP.zw;

    vec2 pt = (vP.xy + uOffset);
    gl_Position = vec4(uScale * pt,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec4 vfTileMapUV;
uniform float uBlue;

uniform sampler2D tex;
uniform sampler2D tileMap;

void main()
{
    vec4 tileType = texture2D(tex, vfTileMapUV.xy);
    vec2 tc = vfTileMapUV.zw;
    tc *= 1./9.;
    if (tileType.g > .5)
    {
        tc += 1./9.;
    }
    if (tileType.a > .5)
    {
        tc += 1./9.;
    }
    vec4 col = texture2D(tileMap, tc);
    if (col.a < .5) col.a = .5;

    col.b = uBlue;
    gl_FragColor = col;
}
]]

function tiled_map_image:populateVBO()
    local s = 1

    -- 2 triangles arranged in a quad
    local quadverts = {0,0,0,0, s,0,1,0, s,s,1,1, 0,0,0,0, s,s,1,1, 0,s,0,1,}
    local verts = {}
    local xdim = 32
    local ydim = 32
    self.n_quads = xdim * ydim
    for i=1,xdim do
        for j=1,ydim do
            for k=1,#quadverts do
                -- Offset quads into a grid
                local xyval = quadverts[k]
                local spacing = 1
                if math.fmod(k,4) == 1 then
                    xyval = xyval + (i-1)*spacing
                elseif math.fmod(k,4) == 2 then
                    xyval = xyval + (j-1)*spacing
                end
                table.insert(verts, xyval)
            end
        end
    end

    local varr = ffi.typeof('GLfloat[?]')(#verts,verts)
    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(varr),varr,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,4,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
end

function tiled_map_image:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function tiled_map_image:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function tiled_map_image:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()

    self.tex1 = self:loadTexture('frog1.data', 32,32)
    self:readPixelDataIntoMap('frog1.data', 32,32)
    self.tex2 = self:loadTexture('expl_spritesheet1.data', 900,900)

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function tiled_map_image:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    local texdel = ffi.new("GLuint[1]", self.tex1) gl.glDeleteTextures(1,texdel)

    if self.glfont then
        self.glfont:exitGL()
    end
end

function tiled_map_image:render()
    self:bindVBO()

    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")
    gl.glUniform1f(u_loc, b)

    local aspect = 3/2
    local us_loc = gl.glGetUniformLocation(self.prog, "uScale")
    gl.glUniform2f(us_loc, self.gridscale, self.gridscale * aspect)

    local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffset")
    gl.glUniform2f(uoff_loc, self.pos[1], self.pos[2])

    local texID = self.tex1
    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, texID)
    local stex_loc = gl.glGetUniformLocation(self.prog, "tex")
    gl.glUniform1i(stex_loc, 0)

    local texID = self.tex2
    gl.glActiveTexture(GL.GL_TEXTURE1)
    gl.glBindTexture(GL.GL_TEXTURE_2D, texID)
    local stex2_loc = gl.glGetUniformLocation(self.prog, "tileMap")
    gl.glUniform1i(stex2_loc, 1)

    gl.glEnable(GL.GL_BLEND)
    gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)

    gl.glDrawArrays(GL.GL_TRIANGLES,0,6*self.n_quads)

    self:unbindVBO()

    if self.glfont and self.draw_font then
        local m = {}
        local p = {}
        mm.make_identity_matrix(m)
        local s = .25
        mm.glh_scale(m, s, s, s)
        mm.glh_translate(m, 30, 30, 0)
        -- TODO getStringWidth and center text
        if not win_w then
            win_w,win_h = 800,600
        end
        mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
        gl.glDisable(GL.GL_DEPTH_TEST)
        local col = {1, 1, 1}
        self.glfont:render_string(m, p, col, "tiled_map_image "..self.n_quads.." x"..self.gridscale)
        mm.glh_translate(m, 0, 100, 0)
        self.glfont:render_string(m, p, col, self.pos[1]..", "..self.pos[2])
        mm.glh_translate(m, 0, 100, 0)

        ---@todo save this
        local xdim = math.sqrt(self.n_quads)
        self.glfont:render_string(m, p, col, "dim:"..xdim.."  speed:"..self.velscale)
        mm.glh_translate(m, 0, 100, 0)

        local tidx = getCellXY(self.pos, 32) --todo:save value
        local label = ""
        if tidx then
            label = label..'['.. tidx..']'
            if self.tiletable[tidx] then
                label = label..' -> '.. self.tiletable[tidx]
            end
        end
        self.glfont:render_string(m, p, col, label)
    end
end

function tiled_map_image:timestep(absTime, dt)
    self.t = absTime

    -- check collisions with tile types
    local targetpos = {}
    for i=1,2 do
        targetpos[i] = self.pos[i] + self.vel[i] * self.velscale * dt
    end

    -- Only orthogonal motion is allowed
    local domove = true
    local tidx = getCellXY(targetpos, 32) --todo:save value

    if tidx then
        local tileval = self.tiletable[tidx]
        if tileval and tileval > 0 then
            domove = false
        end
    else
        domove = false
    end

    if domove then
        for i=1,2 do
            self.pos[i] = targetpos[i]
        end
    end
end

function tiled_map_image:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end

    --TODO: pass in last frame's state as well?
    self.vel[1] = 0
    self.vel[2] = 0

    for j=1,#joystates do
        local js = joystates[j]
        if js then
            local v = -1

            if js[lr_input.JOYPAD_LEFT] ~= 0 then
                self.vel[1] = -v
            elseif js[lr_input.JOYPAD_RIGHT] ~= 0 then
                self.vel[1] = v
            elseif js[lr_input.JOYPAD_UP] ~= 0 then
                self.vel[2] = v
            elseif js[lr_input.JOYPAD_DOWN] ~= 0 then
                self.vel[2] = -v
            end

        end
    end
end

function tiled_map_image:onButtonPressed(padIndex, button)
    local scalefac = 1.3
    if button == lr_input.JOYPAD_A then
        snd.playSound("test.wav")
        self.t_last = self.t
    elseif button == lr_input.JOYPAD_B then
        snd.playSound("pop_drip.wav")
        self.t_last = self.t
    elseif button == lr_input.JOYPAD_X then
        self.gridscale = self.gridscale * scalefac
    elseif button == lr_input.JOYPAD_Y then
        self.gridscale = self.gridscale / scalefac
        self.gridscale = math.max(self.gridscale, 1/64)
    elseif button == lr_input.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    elseif button == lr_input.JOYPAD_R then
        self.velscale = self.velscale * scalefac
    elseif button == lr_input.JOYPAD_L then
        self.velscale = self.velscale / scalefac
        self.velscale = math.max(self.velscale, 1/64)
    end
end

return tiled_map_image
