--[[ simple_hud.lua

]]

local DO_FONTS = true

simple_hud = {}
simple_hud.__index = simple_hud

local ffi = require "ffi"
local sf = require "util.shaderfunctions"
local glf = require 'util.glfont'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'

function simple_hud.new(...)
    local self = setmetatable({}, simple_hud)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_hud:setDataDirectory(dir)
    self.dataDir = dir
end

function simple_hud:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}
    self.n_quads = 8
    self.max_quads = 16
    self:init_quads()
    self.draw_font = true
end

function simple_hud:init_quads()
    math.randomseed(1)
    self.offsets = {}
    for i=1,self.n_quads do
        local off = {-1+2*math.random(), -1+2*math.random()}
        table.insert(self.offsets,off)
    end
end

local vs=[[
#version 100
uniform float uTime;
uniform float uScale;

attribute vec4 vP;
varying vec3 vfC;

void main()
{
    vfC = vec3(vP.zw,0.);
    vec2 pt = vP.xy;
    gl_Position = vec4(pt,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
uniform float uBlue;
void main()
{
    vec3 col = vfC;
    col.b = uBlue;
    gl_FragColor = vec4(col,1.);
}
]]

function simple_hud:populateVBO()
    local s = .04
    local spac = .1
    local aspect = 4/3

    local a = aspect
    local quadverts = {
        -s,-s*a, 0,0,
         s,-s*a, 1,0,
         s, s*a, 1,1,
        -s,-s*a, 0,0,
         s, s*a, 1,1,
        -s, s*a, 0,1,
    }

    local verts = {}
    for q=1,self.max_quads do
        for i=1,6 do
            for j=1,4 do
                local idx = j+(i-1)*4
                --print(idx)
                local val = quadverts[idx]

                if math.fmod(j,4) == 1 then
                    val = val - 1 + spac*q
                elseif math.fmod(j,4) == 2 then
                    val = val + .9
                end

                table.insert(verts, val)
            end
        end
    end

    local varr = ffi.typeof('GLfloat[?]')(#verts,verts)
    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(varr),varr,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_hud:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_hud:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function simple_hud:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function simple_hud:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    if self.glfont then
        self.glfont:exitGL()
    end
end

function simple_hud:render()

    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")
    gl.glUniform1f(u_loc, b)

    local ut_loc = gl.glGetUniformLocation(self.prog, "uTime")
    gl.glUniform1f(ut_loc, 2*self.t)

    self:bindVBO()
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6*self.n_quads)

    self:unbindVBO()

    if self.glfont and self.draw_font then
        local m = {}
        local p = {}
        mm.make_identity_matrix(m)
        local s = .5
        mm.glh_scale(m, s, s, s)
        mm.glh_translate(m, 30, 30, 0)
        -- TODO getStringWidth and center text
        if not win_w then
            win_w,win_h = 800,600
        end
        mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
        gl.glDisable(GL.GL_DEPTH_TEST)
        local col = {1, 1, 1}
        self.glfont:render_string(m, p, col, "simple_hud "..self.n_quads)
        mm.glh_translate(m, 30, 30, 0)
    end
end

function simple_hud:timestep(absTime, dt)
    self.t = absTime
end

function simple_hud:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function simple_hud:onButtonPressed(padIndex, button)
    if button == lr_input.JOYPAD_A then
        self.t_last = self.t
    elseif button == lr_input.JOYPAD_B then
        self.t_last = self.t
    elseif button == lr_input.JOYPAD_X then
        self.n_quads = self.n_quads + 1
        self.n_quads = math.min(self.n_quads, self.max_quads)
        self:init_quads()
    elseif button == lr_input.JOYPAD_Y then
        self.n_quads = self.n_quads - 1
        self.n_quads = math.max(self.n_quads, 0)
        self:init_quads()
    elseif button == lr_input.JOYPAD_SELECT then
        self.draw_font = not self.draw_font
    end
end

return simple_hud
