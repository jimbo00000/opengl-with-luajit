--[[ construct.lua

    Just a container for a number of other scenes.
    Does nothing by itself. Add scenes to the list in init.
]]

construct = {}
construct.__index = construct

function construct.new(...)
    local self = setmetatable({}, construct)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function construct:init()
    local inner_scenes = {
        "tiled_map_image",
        "sprite_character",
    }
    self.scenes = {}
    for _,v in pairs(inner_scenes) do
        table.insert(self.scenes, require(v).new())
    end
end

function construct:setDataDirectory(dir)
    for _,v in pairs(self.scenes) do
        v:setDataDirectory(dir)
    end
end

function construct:initGL()
    for _,v in pairs(self.scenes) do
        v:initGL()
    end
end

function construct:exitGL()
    for _,v in pairs(self.scenes) do
        v:exitGL()
    end
end

function construct:render()
    for _,v in pairs(self.scenes) do
        v:render()
    end
end

function construct:timestep(absTime, dt)
    for _,v in pairs(self.scenes) do
        v:timestep(absTime, dt)
    end
end

function construct:update_retropad(joystates)
    for _,v in pairs(self.scenes) do
        v:update_retropad(joystates)
    end
end

return construct
