--[[ frametrail.lua

    A demo effect, draws a scene to texture with no clear and
    displaysit onto a sequence of polygons with holes in the
    center arranged like a tunnel.
]]

local DO_FONTS = true

frametrail = {}
frametrail.__index = frametrail

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions2'
local glf = require 'util.glfont'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'

function frametrail.new(...)
    local self = setmetatable({}, frametrail)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function frametrail:init(innerSceneName)
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}

    self.n_quads = 16
    self.chunkSz = 256
    self.npts = 5
    self.draw_font = true
    self.open = 0

    self.innerSceneName = innerSceneName
end

function frametrail:setDataDirectory(dir)
    self.dataDir = dir
end

function frametrail:loadtextures()
    local texfilename = "stone_128x128.raw"
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end
    local w,h = 128,128
    local inp = io.open(texfilename, "rb")
    local data = nil
    if inp then
        data = inp:read("*all")
        assert(inp:close())
    end

    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    self.texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB,
                  w, h, 0,
                  GL.GL_RGB, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end

local vs=[[
#version 100
#line 94

uniform mat2 uRotMtx;
uniform float uScale;
uniform vec2 uOffsets[256];
uniform float uNumInsts;
uniform float uTime;
uniform float uOpen;

attribute vec3 vP;
varying vec3 vfC;
varying float brightness;

void main()
{
    int id = int(vP.z) - 1;
    float id01 = (vP.z-1.) / (uNumInsts);
    vec2 Offset = vec2(0.);
    brightness = 1.;//pow(id01,3.);

    float size = id01;
    float fp = fract(uTime);
    size += (1./(uNumInsts))*fp; // move to next
    size = pow(size,3.);
    size *= 35.;
    size *= 8.*(1.+uOpen);

    vec2 pt = vP.xy;
    pt -= uOpen * normalize(pt);

    vfC = vec3(pt/.05,0.);

    pt = uRotMtx * (size * pt);
    gl_Position = vec4(pt+Offset,0.,1.);
}
]]

local fs=[[
#version 100
#line 133

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
varying float brightness;

uniform sampler2D sTex;
uniform float uBlue;

void main()
{
    vec2 tc = vfC.xy*.35 + vec2(.5);
    vec3 col = texture2D(sTex, tc).xyz;
    col.b += .3*uBlue;

    gl_FragColor = vec4(brightness * col,1.);
}
]]

function frametrail:populateVBO()
    local s = .05
    local verts = {
        -s,-s,0,
        s,-s,0,
        s,s,0,
        -s,-s,0,
        s,s,0,
        -s,s,0,
    }

    local all = {}
    for i=1,self.n_quads do
        for j=1,6 do
            verts[3*j] = i
        end
        for j=1,#verts do
            table.insert(all, verts[j])
        end
    end
    local av = ffi.typeof('GLfloat[?]')(#all,all)

    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(av),av,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,3,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)

    self.nitems = 18
end

function frametrail:populateVBOStar()
    local s = .05
    local pts = {}
    local twopi = 2*math.pi
    local ox,oy = 0,0
    for z=1,self.n_quads do

    local function put(x, y)
        table.insert(pts, x)
        table.insert(pts, y)
        table.insert(pts, z)
    end
    for i=1,self.npts do
        local a,b = (i-1-.5)/self.npts, (i-.5)/self.npts
        local c = (a+b)/2
        local ax,ay = s*math.cos(twopi*a), s*math.sin(twopi*a)
        local bx,by = s*math.cos(twopi*b), s*math.sin(twopi*b)
        local cx,cy = 2*s*math.cos(twopi*c), 2*s*math.sin(twopi*c)

        if false then -- filled polygon
            put(ox,oy)
            put(bx,by)
            put(ax,ay)
            if true then -- points
                put(bx,by)
                put(cx,cy)
                put(ax,ay)
            end
        else -- torus w/hole
            put(bx,by)
            put(2*ax,2*ay)
            put(ax,ay)
            put(2*ax,2*ay)
            put(2*bx,2*by)
            put(bx,by)
        end
    end
    end

    local v = ffi.typeof('GLfloat[?]')(#pts, pts)
    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]

    local fltsPerInst = #pts / self.n_quads
    self.nitems = fltsPerInst / 3
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,3,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
end

function frametrail:init_quads()
    math.randomseed(1)
    self.offsets = {}
    for i=1,self.n_quads do
        table.insert(self.offsets,-1+2*math.random())
        table.insert(self.offsets,-1+2*math.random())
    end
    self:populateVBOStar()
end

function frametrail:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function frametrail:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function frametrail:initGL()
    local unisz = ffi.typeof('GLint[?]')(0)
    gl.glGetIntegerv(GL.GL_MAX_VERTEX_UNIFORM_VECTORS, unisz)
    print('GL_MAX_VERTEX_UNIFORM_VECTORS',unisz[0])

    self.prog, err = sf.make_shader_from_source({vsrc=vs,fsrc=fs})
    if self.prog == 0 then
        err = "initGL - Shader error:\n" ..err
        print(err)
        return err
    end

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:init_quads()

    self:loadtextures()


    local innerName =
        'cpu_quad'
        --'starburst' 
        --'simple_quads3'
        --'simple_sprite'

    local status,err = pcall(function ()
        local SceneLibrary = require('scene_rtt_quad')
        self.Scene = SceneLibrary.new(innerName)

        local win_w,win_h = 512,512 --256,256
        if self.Scene.setDataDirectory then self.Scene:setDataDirectory(self.dataDir) end
        if self.Scene.resize then self.Scene:resize(win_w,win_h) end

        local err = self.Scene:initGL()
        if type(err)=='string' then
            self.Scene:exitGL()
            error(err)
        end
    end)

    if not status then
        local dbg = debug.getinfo(1)
        local tag = dbg.short_src..':'..dbg.currentline
        err =
            --tag..
            '\n    '..err
        print(err)
        package.loaded['scene_rtt_quad'] = nil
        package.loaded[innerName] = nil
        return err
    end

    if self.Scene then
    else
        print('nil inner scene')
        return 'nil scene'
    end

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function frametrail:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    if self.texID then
        local texdel = ffi.new("GLuint[1]", self.texID)
        gl.glDeleteTextures(1,texdel)
    end

    if self.Scene and self.Scene.exitGL then self.Scene:exitGL() end
    if self.glfont then
        self.glfont:exitGL()
    end
end

function frametrail:render()
    -- This function will save and restore vp/fb state
    if self.Scene and self.Scene.render_to_texture then self.Scene:render_to_texture(absTime, dt) end

    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")
    gl.glUniform1f(u_loc, b)

    local rot = .3*self.t
    local m = {
        math.cos(rot), -math.sin(rot),
        math.sin(rot), math.cos(rot),
    }
    local urm_loc = gl.glGetUniformLocation(self.prog, "uRotMtx")
    gl.glUniformMatrix2fv(urm_loc, 1, GL.GL_FALSE, ffi.typeof('GLfloat[?]')(4, m))

    local us_loc = gl.glGetUniformLocation(self.prog, "uScale")
    gl.glUniform1f(us_loc, 4/math.pow(self.n_quads,.25))

    local chunkSz = math.min(self.chunkSz, self.n_quads)

    local us_loc = gl.glGetUniformLocation(self.prog, "uNumInsts")
    gl.glUniform1f(us_loc, self.n_quads)

    local us_loc = gl.glGetUniformLocation(self.prog, "uTime")
    gl.glUniform1f(us_loc, self.t)

    local uo_loc = gl.glGetUniformLocation(self.prog, "uOpen")
    gl.glUniform1f(uo_loc, self.open)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D,
        --self.texID
        self.Scene.fbo.tex)
    local stex_loc = gl.glGetUniformLocation(self.prog, "sTex")
    gl.glUniform1i(stex_loc, 0)

    -- instance positions
    local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffsets")

    self:bindVBO()
    local i=0
    --TODO: handle incomplete chunks
    for i=0, self.n_quads-1,chunkSz do
        local offs = {}
        for j=1,chunkSz do
            local idx = i + j - 1
            table.insert(offs, self.offsets[2*idx+1])
            table.insert(offs, self.offsets[2*idx+2])
        end
        local o = ffi.typeof('GLfloat[?]')(chunkSz*2, offs)
        gl.glUniform2fv(uoff_loc, chunkSz, o)

        gl.glDrawArrays(GL.GL_TRIANGLES, 0, self.nitems*chunkSz)
    end
    self:unbindVBO()


    if self.glfont and self.draw_font then
        local m = {}
        local p = {}
        mm.make_identity_matrix(m)
        local s = .5
        mm.glh_scale(m, s, s, s)
        mm.glh_translate(m, 30, 30, 0)
        -- TODO getStringWidth and center text
        if not win_w then
            win_w,win_h = 800,600
        end
        mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
        gl.glDisable(GL.GL_DEPTH_TEST)
        local col = {1, 1, 1}
        self.glfont:render_string(m, p, col, "frametrail3 "..self.n_quads)
        mm.glh_translate(m, 0, 120, 0)
        self.glfont:render_string(m, p, col, "chunkSz "..self.chunkSz)
        mm.glh_translate(m, 0, 120, 0)
        local ndraws = math.max(1,math.floor(self.n_quads/self.chunkSz))
        self.glfont:render_string(m, p, col, self.t .. ' =t')
        mm.glh_translate(m, 0, 120, 0)
        self.glfont:render_string(m, p, col, "open="..self.open)
    end
end

function frametrail:timestep(absTime, dt)
    self.t = absTime
    if self.Scene and self.Scene.timestep then self.Scene:timestep(absTime, dt) end
end

--
function frametrail:mousemove(x,y)
    --self.t = x/700
    --self.open = .1 * x/700
end

function frametrail:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function frametrail:onButtonPressed(padIndex, button)
    if self.Scene and self.Scene.onButtonPressed then self.Scene:onButtonPressed(padIndex, button) end

    if button == lr_input.JOYPAD_A then
        self.n_quads = self.n_quads * 2
        self.n_quads = math.min(self.n_quads, 4096*2*16)
        self:init_quads()
    elseif button == lr_input.JOYPAD_B then
        self.n_quads = self.n_quads * .5
        self.n_quads = math.max(self.n_quads, 1)
        self:init_quads()
    elseif button == lr_input.JOYPAD_UP then
        self.n_quads = self.n_quads + 1
        self.n_quads = math.min(self.n_quads, 4096*2*16)
        self:init_quads()
    elseif button == lr_input.JOYPAD_DOWN then
        self.n_quads = self.n_quads - 1
        self.n_quads = math.max(self.n_quads, 1)
        self:init_quads()
    elseif button == lr_input.JOYPAD_SELECT then
        self.draw_font = not self.draw_font
    end
end

return frametrail
