--[[ starburst.lua

    A bunch of instanced star shapes exploding out from
    a point. Loops continuously, re-firing.
]]

local DO_FONTS = false

starburst = {}
starburst.__index = starburst

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'

function starburst.new(...)
    local self = setmetatable({}, starburst)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function starburst:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function starburst:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}

    self.n_quads = 32
    self.chunkSz = 128
    self.npts = 7
    self.draw_font = true
end

-- Fast trig thanks to Peter Knight
-- https://github.com/going-digital/tasty-pastry
local vs=[[
#version 100

uniform mat2 uRotMtx;
uniform float uScale;
uniform vec2 uOffsets[256];
uniform float uNumInsts;
uniform float uBurst;
uniform vec2 uMove;

attribute vec3 vP;
varying vec3 vfC;
varying float brightness;

void main()
{
    int id = int(vP.z) - 1;
    vec2 Offset = 
        //vec2(.05 * float(id), 0.);
        uOffsets[id] * (1.-uBurst);
    brightness = vP.z / uNumInsts;
    vfC = vec3(vP.xy/.05,0.);
    vec2 pt = uRotMtx * (uBurst * uScale * vP.xy);
    gl_Position = vec4(pt+Offset+uMove,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
varying float brightness;

uniform float uBlue;

void main()
{
    vec3 col = .5*vfC+vec3(.5);
    col.b = uBlue;
    gl_FragColor = vec4(brightness * col,1.);
}
]]

function starburst:populateVBO()
    local s = .05
    local verts = {
        -s,-s,0,
        s,-s,0,
        s,s,0,
        -s,-s,0,
        s,s,0,
        -s,s,0,
    }

    local all = {}
    for i=1,self.n_quads do
        for j=1,6 do
            verts[3*j] = i
        end
        for j=1,#verts do
            table.insert(all, verts[j])
        end
    end
    local av = ffi.typeof('GLfloat[?]')(#all,all)

    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(av),av,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,3,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)

    self.nitems = 18
end

function starburst:populateVBOStar()
    local s = .05
    local pts = {}
    local twopi = 2*math.pi
    local ox,oy = 0,0
    for z=1,self.n_quads do

    local function put(x, y)
        table.insert(pts, x)
        table.insert(pts, y)
        table.insert(pts, z)
    end
    for i=1,self.npts do
        local a,b = (i-1-.5)/self.npts, (i-.5)/self.npts
        local c = (a+b)/2
        local ax,ay = s*math.cos(twopi*a), s*math.sin(twopi*a)
        local bx,by = s*math.cos(twopi*b), s*math.sin(twopi*b)
        local cx,cy = 2*s*math.cos(twopi*c), 2*s*math.sin(twopi*c)

        if true then -- filled polygon
            put(ox,oy)
            put(bx,by)
            put(ax,ay)
            if true then -- points
                put(bx,by)
                put(cx,cy)
                put(ax,ay)
            end
        else -- torus w/hole
            put(bx,by)
            put(2*ax,2*ay)
            put(ax,ay)
            put(2*ax,2*ay)
            put(2*bx,2*by)
            put(bx,by)
        end
    end
    end

    local v = ffi.typeof('GLfloat[?]')(#pts, pts)
    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]

    local fltsPerInst = #pts / self.n_quads
    self.nitems = fltsPerInst / 3
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,3,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
end

function starburst:init_quads()
    math.randomseed(1)
    self.offsets = {}
    for i=1,self.n_quads do
        local x,y,d
        d=99
        while d > 1 do
            x,y = 
                -1+2*math.random(),
                -1+2*math.random()
            d = x*x + y*y
        end
        table.insert(self.offsets,x)
        table.insert(self.offsets,y)
    end
    self:populateVBOStar()
end

function starburst:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function starburst:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function starburst:initGL()
    local unisz = ffi.typeof('GLint[?]')(0)
    gl.glGetIntegerv(GL.GL_MAX_VERTEX_UNIFORM_VECTORS, unisz)
    print('GL_MAX_VERTEX_UNIFORM_VECTORS',unisz[0])

    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:init_quads()

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function starburst:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    if self.glfont then
        self.glfont:exitGL()
    end
end

function starburst:render()
    --gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")
    gl.glUniform1f(u_loc, b)

    local ub_loc = gl.glGetUniformLocation(self.prog, "uBurst")
    local burst = self.t - math.floor(self.t)
    gl.glUniform1f(ub_loc, 1-burst)

    local rot = self.t
    local m = {
        math.cos(rot), -math.sin(rot),
        math.sin(rot), math.cos(rot),
    }

    function fract(n) return n - math.floor(n) end
    function randx(n) return fract(math.sin(n) * 43758.5453123) end
    local umv_loc = gl.glGetUniformLocation(self.prog, "uMove")
    local x = randx(1+math.floor(self.t))
    local y = randx(2+math.floor(self.t))
    --gl.glUniform2f(umv_loc, x,y)
    gl.glUniform2f(umv_loc, 2*x-1, 2*y-1)

    local urm_loc = gl.glGetUniformLocation(self.prog, "uRotMtx")
    gl.glUniformMatrix2fv(urm_loc, 1, GL.GL_FALSE, ffi.typeof('GLfloat[?]')(4, m))

    local us_loc = gl.glGetUniformLocation(self.prog, "uScale")
    gl.glUniform1f(us_loc, 4/math.pow(self.n_quads,.25))

    local chunkSz = math.min(self.chunkSz, self.n_quads)

    local us_loc = gl.glGetUniformLocation(self.prog, "uNumInsts")
    gl.glUniform1f(us_loc, chunkSz)

    -- instance positions
    local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffsets")

    self:bindVBO()
    local i=0
    --TODO: handle incomplete chunks
    for i=0, self.n_quads-1,chunkSz do
        local offs = {}
        for j=1,chunkSz do
            local idx = i + j - 1
            table.insert(offs, self.offsets[2*idx+1])
            table.insert(offs, self.offsets[2*idx+2])
        end
        local o = ffi.typeof('GLfloat[?]')(chunkSz*2, offs)
        gl.glUniform2fv(uoff_loc, chunkSz, o)

        gl.glDrawArrays(GL.GL_TRIANGLES, 0, self.nitems*chunkSz)
    end
    self:unbindVBO()


    if self.glfont and self.draw_font then
        local m = {}
        local p = {}
        mm.make_identity_matrix(m)
        local s = .5
        mm.glh_scale(m, s, s, s)
        mm.glh_translate(m, 30, 30, 0)
        -- TODO getStringWidth and center text
        if not win_w then
            win_w,win_h = 800,600
        end
        mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
        gl.glDisable(GL.GL_DEPTH_TEST)
        local col = {1, 1, 1}
        self.glfont:render_string(m, p, col, "starburst3 "..self.n_quads)
        mm.glh_translate(m, 0, 120, 0)
        self.glfont:render_string(m, p, col, "chunkSz "..self.chunkSz)
        mm.glh_translate(m, 0, 120, 0)
        local ndraws = math.max(1,math.floor(self.n_quads/self.chunkSz))
        self.glfont:render_string(m, p, col, ndraws .. ' draw calls')
    end
end

function starburst:timestep(absTime, dt)
    self.t = absTime
end

function starburst:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function starburst:onButtonPressed(padIndex, button)
    if button == lr_input.JOYPAD_A then
        snd.playSound("test.wav")
        self.t_last = self.t
        self.chunkSz = self.chunkSz * 2
        self.chunkSz = math.min(self.chunkSz, 256)
    elseif button == lr_input.JOYPAD_B then
        snd.playSound("pop_drip.wav")
        self.t_last = self.t
        self.chunkSz = self.chunkSz * .5
        self.chunkSz = math.max(self.chunkSz, 1)
    elseif button == lr_input.JOYPAD_X then
        self.n_quads = self.n_quads * 2
        self.n_quads = math.min(self.n_quads, 4096*2*16)
        self:init_quads()
    elseif button == lr_input.JOYPAD_Y then
        self.n_quads = self.n_quads * .5
        self.n_quads = math.max(self.n_quads, 1)
        self:init_quads()
    elseif button == lr_input.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    end
end

return starburst
