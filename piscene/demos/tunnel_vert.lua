--[[ tunnel_vert.lua

    Draws a tunnel with real cylinder geometry, and applies some
    animation in vertex shader. Gives it a bit wigglier feel without
    having to expensive tricks in the fragment shader.
]]
local DO_FONTS = false

tunnel_vert = {}
tunnel_vert.__index = tunnel_vert

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local glf = require 'util.glfont'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'
local filesystem = require 'util.filesystem'

function tunnel_vert.new(...)
    local self = setmetatable({}, tunnel_vert)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function tunnel_vert:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.texID = 0
    self.buttons = {}
    self.draw_font = true
end

function tunnel_vert:setDataDirectory(dir)
    self.dataDir = dir
    if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function tunnel_vert:loadtextures()
    local texfilename = "stone_128x128.raw"
    local w,h = 128,128
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end

    local data, size = filesystem.read(texfilename)
    print(size, 'bytes read from texfilename')

    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    self.texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB,
                  w, h, 0,
                  GL.GL_RGB, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end

local function generate_cylinder(slices, stacks)
    local n = slices
    local m = stacks
    local r = .3

    local v = {}
    local t = {}
    for j=0,m do
        for i=0,n do
            local phase = i / n
            local rot = 2 * math.pi * (phase+.5)
            local x,y = math.sin(rot), math.cos(rot)
            table.insert(v, r*x)
            table.insert(v, r*y)
            table.insert(v, j/m)
            table.insert(t, phase)
            table.insert(t, j/m)
        end
    end

    local f = {}
    for i=0,m*n-2 do
        table.insert(f, i+1)
        table.insert(f, i)
        table.insert(f, i+n+1)
        table.insert(f, i+1)
        table.insert(f, i+n+1)
        table.insert(f, i+n+2)
    end

    return v,t,f
end

function tunnel_vert:init_cube_attributes()
    local v,t,f = generate_cylinder(64,64)
    local verts = ffi.typeof('GLfloat[?]')(#v, v)
    local texs = ffi.typeof('GLfloat[?]')(#t, t)
    local vpos_loc = gl.glGetAttribLocation(self.prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(self.prog, "vColor")

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    local cvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(texs), texs, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)

    self.numTris = #f
    local quads = ffi.typeof('GLushort[?]')(#f, f)
    local qvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(quads), quads, GL.GL_STATIC_DRAW)
    table.insert(self.vbos, qvbo)
end

local vs=[[
#version 100
#line 146
#ifdef GL_ES
precision highp float;
#endif

attribute vec4 vPosition;
attribute vec2 vColor;

varying vec3 vfColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

uniform float absTime;

void main()
{
    vfColor = vec3(vColor.xy, 0.);
    vec4 pos = vPosition;
    pos.xy += .08 * vec2(
        sin(15.*pos.z + 6.*absTime),
        sin(15.*pos.z + 5.*absTime)
        );
    vfColor.z = 2.*pos.z - .5;
    pos.z *= 15.;
    pos.z -= 10.;
    gl_Position = prmtx * mvmtx * pos;
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision highp float;
precision mediump int;
#endif

varying vec3 vfColor;

uniform float absTime;
uniform sampler2D sTex;
float speed = 1.;

void main()
{
    //vec3 col = vec3(vfColor.x, vfColor.y, sin(100.*vfColor.z - 10.*absTime));

    // Map texture to the walls
    vec2 tc = fract(vec2(vfColor.x, 10.*vfColor.y - speed*absTime));
    vec3 col = texture2D(sTex, tc).xyz;
    col *= vfColor.z;
    gl_FragColor = vec4(col, 1.0);
}
]]

function tunnel_vert:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vPosition")
    local vc = gl.glGetAttribLocation(self.prog, "vColor")
    local vvbo = self.vbos[1]
    local cvbo = self.vbos[2]
    local qvbo = self.vbos[3]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glVertexAttribPointer(vc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)

    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])

    gl.glEnableVertexAttribArray(vl)
    gl.glEnableVertexAttribArray(vc)
end

function tunnel_vert:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vPosition")
    local vc = gl.glGetAttribLocation(self.prog, "vColor")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
    gl.glDisableVertexAttribArray(vc)
end

function tunnel_vert:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    self:init_cube_attributes()
    self:loadtextures()

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function tunnel_vert:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
    
    local texdel = ffi.new("GLuint[1]", self.texID)
    gl.glDeleteTextures(1,texdel)

    if self.glfont then
        self.glfont:exitGL()
    end
end

function tunnel_vert:render()
    gl.glUseProgram(self.prog)

    local p = {}
    local aspect = 4/3
    mm.glh_perspective_rh(p, 60 * math.pi/180, aspect, .004, 500)
    local proj = {}
    mm.make_identity_matrix(proj)
    local view = {}
    mm.make_identity_matrix(view)
    local umv_loc = gl.glGetUniformLocation(self.prog, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(self.prog, "prmtx")
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, ffi.typeof('GLfloat[?]')(16, p))
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, ffi.typeof('GLfloat[?]')(16, view))

    local tloc = gl.glGetUniformLocation(self.prog, "absTime")
    gl.glUniform1f(tloc, self.t)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    local stex_loc = gl.glGetUniformLocation(self.prog, "sTex")
    gl.glUniform1i(stex_loc, 0)

    -- NOTE: rpi requires re-setting VBO data each frame. Why?
    --gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos[1][0])
    self:bindVBO()

    gl.glDrawElements(GL.GL_TRIANGLES, self.numTris, GL.GL_UNSIGNED_SHORT, nil)

    if self.draw_font then
        self:drawtext()
    end

    self:unbindVBO()

    gl.glUseProgram(0)
end

function tunnel_vert:drawtext()
    if not self.glfont then return end
    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    self.glfont:render_string(m, p, col, "tunnel_vert")
end

function tunnel_vert:timestep(absTime, dt)
    self.t = absTime
end

function tunnel_vert:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function tunnel_vert:onButtonPressed(padIndex, button)
    self.t_last = self.t
    
    local soundmap = {
        [lr_input.JOYPAD_A] = "pop_drip.wav",
        [lr_input.JOYPAD_B] = "Jump5.wav",
        [lr_input.JOYPAD_X] = "Pickup_Coin7.wav",
        [lr_input.JOYPAD_Y] = "test.wav",
    }
    local s = soundmap[padIndex]
    if s then snd.playSound(s) end

    if button == lr_input.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    end
end

return tunnel_vert
