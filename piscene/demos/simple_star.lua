--[[ simple_star.lua

    Do some rudimentary geometry generation on CPU.
]]

simple_star = {}
simple_star.__index = simple_star

local ffi = require "ffi"
local sf = require "util.shaderfunctions"

function simple_star.new(...)
    local self = setmetatable({}, simple_star)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_star:setDataDirectory(dir)
end

function simple_star:init()
    self.vbos = {}
    self.prog = 0
    self.npts = 4
    self.nitems = 0
end

local vs=[[
#version 100
attribute vec2 vP;
varying vec3 vfC;

void main()
{
    vfC = vec3(vP/.5,0.);
    vec2 pt = vP;
    gl_Position = vec4(pt,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
void main()
{
    vec3 col = .5*vfC+vec3(.5);
    gl_FragColor = vec4(col,1.);
}
]]

function simple_star:populateVBO()
    local s = .5

    local pts = {}
    local twopi = 2*math.pi
    local ox,oy = 0,0
    for i=1,self.npts do
        local a,b = (i-1-.5)/self.npts, (i-.5)/self.npts
        local c = (a+b)/2
        local ax,ay = s*math.cos(twopi*a), s*math.sin(twopi*a)
        local bx,by = s*math.cos(twopi*b), s*math.sin(twopi*b)
        local cx,cy = 2*s*math.cos(twopi*c), 2*s*math.sin(twopi*c)

        if false then -- filled polygon
            table.insert(pts, ox)
            table.insert(pts, oy)
            table.insert(pts, bx)
            table.insert(pts, by)
            table.insert(pts, ax)
            table.insert(pts, ay)
            if true then -- points
                table.insert(pts, bx)
                table.insert(pts, by)
                table.insert(pts, cx)
                table.insert(pts, cy)
                table.insert(pts, ax)
                table.insert(pts, ay)
            end
        else -- torus w/hole
            table.insert(pts, bx)
            table.insert(pts, by)
            table.insert(pts, 2*ax)
            table.insert(pts, 2*ay)
            table.insert(pts, ax)
            table.insert(pts, ay)
            table.insert(pts, 2*ax)
            table.insert(pts, 2*ay)
            table.insert(pts, 2*bx)
            table.insert(pts, 2*by)
            table.insert(pts, bx)
            table.insert(pts, by)
        end
    end

    local v = ffi.typeof('GLfloat[?]')(#pts, pts)
    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    self.nitems = #pts/2

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_star:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_star:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function simple_star:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()
end

function simple_star:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
end

function simple_star:render()
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    gl.glUseProgram(self.prog)

    self:bindVBO()
    gl.glDrawArrays(GL.GL_TRIANGLES,0,self.nitems)
    self:unbindVBO()

    gl.glUseProgram(0)
end

function simple_star:timestep(absTime, dt)
end

return simple_star
