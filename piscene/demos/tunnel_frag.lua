--[[ tunnel_frag.lua

    Draws a tunnel using pure fragment shader - polar coordinates about
    screen center, mapping a texture to the walls.

    Presents the pixels to screen using a viewport-filling quad.
]]
local DO_FONTS = false

tunnel_frag = {}
tunnel_frag.__index = tunnel_frag

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local glf = require 'util.glfont'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'
local filesystem = require 'util.filesystem'

function tunnel_frag.new(...)
    local self = setmetatable({}, tunnel_frag)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function tunnel_frag:setDataDirectory(dir)
    self.dataDir = dir
    if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function tunnel_frag:loadtextures()
    local texfilename = "stone_128x128.raw"
    local w,h = 128,128
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end

    local data, size = filesystem.read(texfilename)
    print(size, 'bytes read from texfilename')

    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    self.texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB,
                  w, h, 0,
                  GL.GL_RGB, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end

function tunnel_frag:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.texID = 0
    self.buttons = {}
    self.draw_font = true
end

local vs=[[
#version 100

attribute vec2 vP;
varying vec2 vfUV;

void main()
{
    vfUV = vP;
    gl_Position = vec4(vP,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec2 vfUV;

uniform float absTime;
uniform sampler2D sTex;
float speed = 10.;

#define PI 3.14159265359    

void main()
{
    float angle_11 = atan(vfUV.y, vfUV.x) / PI; // atan range in [-PI,PI]
    float angle01 = .5*(1. + angle_11);
    float radial = 1. / (vfUV.x*vfUV.x + vfUV.y*vfUV.y);

    //vec3 col = vec3(0., sin(2.*radial + speed*absTime), 0.);
    //col *= 2.*pow(length(vfUV), 3.);

    // Map texture to the walls
    float z = radial + speed*absTime;
    vec2 tc = fract(vec2(2.*angle01, .1*z));
    vec3 col = texture2D(sTex, tc).xyz;

    // Put some fog/darkness at the end of the tunnel to
    // obscure a rather unsightly infinity.
    col = mix(col, vec3(0.), 1.-clamp(length(vfUV), 0., 1.));

    gl_FragColor = vec4(col, 1.0);
}
]]

function tunnel_frag:populateVBO()
    local v = ffi.typeof('GLfloat[?]')(12,{-1,-1,1,-1,1,1,-1,-1,1,1,-1,1,})
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(v), v, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function tunnel_frag:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function tunnel_frag:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function tunnel_frag:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()

    self:loadtextures()

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function tunnel_frag:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
    
    local texdel = ffi.new("GLuint[1]", self.texID)
    gl.glDeleteTextures(1,texdel)

    if self.glfont then
        self.glfont:exitGL()
    end
end

function tunnel_frag:render()
    gl.glUseProgram(self.prog)

    local tloc = gl.glGetUniformLocation(self.prog, "absTime")
    gl.glUniform1f(tloc, self.t)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    local stex_loc = gl.glGetUniformLocation(self.prog, "sTex")
    gl.glUniform1i(stex_loc, 0)

    -- NOTE: rpi requires re-setting VBO data each frame. Why?
    --gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos[1][0])
    self:bindVBO()

    gl.glDrawArrays(GL.GL_TRIANGLES,0,6)

    if self.draw_font then
        self:drawtext()
    end

    self:unbindVBO()
end

function tunnel_frag:drawtext()
    if not self.glfont then return end
    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    self.glfont:render_string(m, p, col, "tunnel_frag")
end

function tunnel_frag:timestep(absTime, dt)
    self.t = absTime
end

function tunnel_frag:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end


function tunnel_frag:onButtonPressed(padIndex, button)
    self.t_last = self.t
    
    local soundmap = {
        [lr_input.JOYPAD_A] = "pop_drip.wav",
        [lr_input.JOYPAD_B] = "Jump5.wav",
        [lr_input.JOYPAD_X] = "Pickup_Coin7.wav",
        [lr_input.JOYPAD_Y] = "test.wav",
    }
    local s = soundmap[padIndex]
    if s then snd.playSound(s) end

    if button == lr_input.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    end
end

return tunnel_frag
