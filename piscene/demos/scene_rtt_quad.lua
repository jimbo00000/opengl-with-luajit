--[[ scene_rtt_quad.lua

    Renders an inner scene to an fbo, then presents that fbo to screen.
    Does not clear between frames, so trails are left on the fbo.
    Uses a closure to pass the inner rendering function into an
    fbofunctions utility to save and restore state.
]]

scene_rtt_quad = {}
scene_rtt_quad.__index = scene_rtt_quad

local ffi = require "ffi"
local sf = require "util.shaderfunctions2"
local fbf = require "util.fbofunctions"
local lr_input = require 'util.libretro_input'

function scene_rtt_quad.new(...)
    local self = setmetatable({}, scene_rtt_quad)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function scene_rtt_quad:setDataDirectory(dir)
    self.datadir = dir -- cache this; self.Scene is not created yet
end

function scene_rtt_quad:init(innerSceneName)
    self.vbos = {}
    self.prog = 0
    self.t = 0
    self.innerSceneName = innerSceneName or
        --'demos.cpu_quad'
        --'tests.04sprite'
        'tests.05mover'
end

--[[
    For drawing the results
]]
local vs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

attribute vec2 vP;
varying vec3 vfC;

uniform float absTime;

void main()
{
    vfC = vec3(vP,0.);
    vec2 pt = vP;
    float t = absTime;
    if (false)
    {
        pt *= .95;
        pt += .04*vec2(sin( t*(4.+pt.x) ), cos( t*(3.+pt.y) ));
    }
    gl_Position = vec4(pt,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform float absTime;
uniform sampler2D sTex;

varying vec3 vfC;

void main()
{
    vec2 tc = vfC.xy*.35 + vec2(.5);
    vec3 col = texture2D(sTex, tc).xyz;

    gl_FragColor =
        vec4(col,1.);
        //vec4(.5 + .5*sin(13.*absTime+36.*col),1.);
}
]]

function scene_rtt_quad:populateVBO()
    local s = 1
    local v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
end

function scene_rtt_quad:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function scene_rtt_quad:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function scene_rtt_quad:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()

    local status,err = pcall(function ()
        local SceneLibrary = require(self.innerSceneName)
        self.Scene = SceneLibrary.new()
    end)
    if not status then
        print(err)
        package.loaded[self.innerSceneName] = nil
        return err
    end
    if self.Scene then
        if self.Scene.setDataDirectory then self.Scene:setDataDirectory(self.datadir) end
        if self.Scene.resizeViewport then self.Scene:resizeViewport(win_w,win_h) end
        local err = self.Scene:initGL()
        if type(err)=='string' then
            local dbg = debug.getinfo(1)
            local tag = dbg.short_src..':'..dbg.currentline
            err =
                --tag..
                '\n    '..err
            self.Scene:exitGL()
            package.loaded[self.innerSceneName] = nil
            return err
        end
    end
end

function scene_rtt_quad:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    fbf.deallocate_fbo(self.fbo)
    if self.Scene and self.Scene.exitGL then self.Scene:exitGL() end
end

function scene_rtt_quad:resizeViewport(win_w,win_h)
    local down = 16
    self:resize(win_w/down, win_h/down)
end

function scene_rtt_quad:resize(w,h)
    fbf.deallocate_fbo(self.fbo)
    self.fbo = fbf.allocate_fbo(w, h, false)

    gl.glBindTexture(GL.GL_TEXTURE_2D, self.fbo.tex)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end

function scene_rtt_quad:render()
    self:render_to_texture()

    --gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    -- Present texture
    gl.glUseProgram(self.prog)

    local tloc = gl.glGetUniformLocation(self.prog, "absTime")
    gl.glUniform1f(tloc, self.t)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.fbo.tex)
    local stex_loc = gl.glGetUniformLocation(self.prog, "sTex")
    gl.glUniform1i(stex_loc, 0)

    self:bindVBO()
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    self:unbindVBO()

    gl.glUseProgram(0)
end

function scene_rtt_quad:render_to_texture()
    local renderFunc = function()
        if self.Scene and self.Scene.render then self.Scene:render() end
    end
    fbf.renderToTexture(self.fbo, renderFunc)
end

function scene_rtt_quad:timestep(absTime, dt)
    if self.Scene and self.Scene.timestep then self.Scene:timestep(absTime, dt) end
    self.t = absTime
end

function scene_rtt_quad:update_retropad(joystates)
    if self.Scene and self.Scene.update_retropad then
        self.Scene:update_retropad(joystates)
    end
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:on_button_pressed(j,b)
                end
            end
        end
    end
end

function scene_rtt_quad:onButtonPressed(padIndex, button)
    if self.Scene and self.Scene.onButtonPressed then self.Scene:onButtonPressed(padIndex, button) end
end

function scene_rtt_quad:on_button_pressed(padIndex, button)
    self.t_last = self.t

    if button == lr_input.JOYPAD_B then
        self:readpix()
    end
end


function scene_rtt_quad:readpix()
    -- Save state
    local fb = ffi.typeof('GLint[?]')(1)
    gl.glGetIntegerv(GL.GL_FRAMEBUFFER_BINDING, fb)

    gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, fbo.id)

    print("Reading pixels...")
    print(self.fbo.w, self.fbo.h)
    local data = ffi.new("unsigned char[?]", self.fbo.w*self.fbo.h*4*4)
    gl.glReadPixels(0,0, self.fbo.w, self.fbo.h, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, data)

    self.rs = 0
    self.bs = 0
    for i=0,self.fbo.w*self.fbo.h do
        local idx = 4*i
        local r = data[idx]
        local g = data[idx+1]
        local b = data[idx+2]
        local a = data[idx+3]
        if r == b then
        elseif r > b then
            self.rs = self.rs + 1
        elseif r < b then
            self.bs = self.bs + 1
        end
        --print(i,r,g,b,a)
    end
    print('Score:',self.rs, self.bs)

    -- Restore state
    gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, fb[0])
end

return scene_rtt_quad
