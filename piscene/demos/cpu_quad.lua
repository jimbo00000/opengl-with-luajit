--[[ cpu_quad.lua

    Square bouncing around the screen. Position kept on CPU,
    rotation done on the GPU as a function of time.
]]

cpu_quad = {}
cpu_quad.__index = cpu_quad

local ffi = require "ffi"
local sf = require "util.shaderfunctions2"

function cpu_quad.new(...)
    local self = setmetatable({}, cpu_quad)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function cpu_quad:setDataDirectory(dir)
end

function cpu_quad:init()
    self.vbos = {}
    self.prog = 0
    self.pos = {0, 0}
    self.vel = {.3, .57}

    self.rot = 0
    self.avel = .11
end

cpu_quad.vs=[[
#version 100
#line 36
attribute vec2 vP;
uniform vec2 uOffset;
uniform float uRot;
varying vec3 vfC;

float sinf(float x)
{
  x*=0.159155;
  x-=floor(x);
  float xx=x*x;
  float y=-6.87897;
  y=y*xx+33.7755;
  y=y*xx-72.5257;
  y=y*xx+80.5874;
  y=y*xx-41.2408;
  y=y*xx+6.28077;
  return x*y;
}
float cosf(float x)
{
  return sinf(x+1.5708);
}

void main()
{
    vfC = vec3(vP/.25,0.);
    vec2 pt = vP;

    float sr = sinf(uRot);
    float cr = cosf(uRot);
    mat2 m = mat2(cr, -sr, sr, cr);

    gl_Position = vec4(m*pt+uOffset,0.,1.);
}
]]

cpu_quad.fs=[[
#version 100
#line 75

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
void main()
{
    vec3 col = .5*vfC+vec3(.5);
    gl_FragColor = vec4(col,1.);
}
]]

function cpu_quad:populateVBO()
    local s = .25
    local v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
end

function cpu_quad:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function cpu_quad:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function cpu_quad:initGL()
    self.prog, err = sf.make_shader_from_source({vsrc=self.vs,fsrc=self.fs})
    if self.prog == 0 then
        local dbg = debug.getinfo(1)
        local tag = dbg.short_src..':'..dbg.currentline
        err = err:sub(1,#err-1) -- strip newline
        err = tag..'\n    '..err
        print(err)
        return err
    end

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()
end

function cpu_quad:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
end

function cpu_quad:render()
    gl.glUseProgram(self.prog)

    local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffset")
    gl.glUniform2f(uoff_loc, self.pos[1], self.pos[2])
    local urot_loc = gl.glGetUniformLocation(self.prog, "uRot")
    gl.glUniform1f(urot_loc, self.rot)

    self:bindVBO()
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    self:unbindVBO()

    gl.glUseProgram(0)
end

function cpu_quad:timestep(absTime, dt)
    local v = .1
    for i=1,2 do
        self.pos[i] = self.pos[i] + v*self.vel[i]
        if math.abs(self.pos[i]) > 1 then
            self.vel[i] = -self.vel[i]
        end
    end
    self.rot = self.rot + self.avel
end

return cpu_quad
