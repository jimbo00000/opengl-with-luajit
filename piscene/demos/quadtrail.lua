--[[ quadtrail.lua

    Draws a bunch of rotating quads.
    Uses a pseudo-instancing technique, baking the instance ID
    into each vert's z coord.
]]

local DO_FONTS = true

quadtrail = {}
quadtrail.__index = quadtrail

local ffi = require "ffi"
local sf = require "util.shaderfunctions"
local glf = require "util.glfont"
local mm = require "util.matrixmath"
local lr_input = require 'util.libretro_input'

function quadtrail.new(...)
    local self = setmetatable({}, quadtrail)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function quadtrail:setDataDirectory(dir)
    self.dataDir = dir
end

function quadtrail:init()
    self.t = 0
    self.t_last = 0
    self.twist = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}

    self.n_quads = 2048
    self.chunkSz = 4
    self.draw_font = true
end

-- Fast trig thanks to Peter Knight
-- https://github.com/going-digital/tasty-pastry
local vs=[[
#version 100
uniform float uTime;
uniform float uTwist;
uniform int uNumQuads;

attribute vec3 vP;
varying vec3 vfC;

float sinf(float x)
{
  x*=0.159155;
  x-=floor(x);
  float xx=x*x;
  float y=-6.87897;
  y=y*xx+33.7755;
  y=y*xx-72.5257;
  y=y*xx+80.5874;
  y=y*xx-41.2408;
  y=y*xx+6.28077;
  return x*y;
}
float cosf(float x)
{
  return sinf(x+1.5708);
}

vec2 spiralOffset3(float fid, float posPhase)
{
    float sides = 3.;
    float loops = 12.;
    float twopi = 2. * 3.1415927;

    float phAdv = fract(.1*uTime);
    fid += phAdv;

    float domain = loops * twopi;
    float segs = loops * sides;
    float whichSeg = floor(fid*segs);

    {
        float plo = (whichSeg/segs) * domain;
        float phi = ((whichSeg+1.)/segs) * domain;
        float t = fract(fid*segs);

        float r0 = whichSeg + 1.;
        r0 -= phAdv*segs;
        r0 *= (1.+phAdv);

        float r1 = r0 + (1.+phAdv);

        vec2 loPt = r0 * vec2(sin(plo), cos(plo));
        vec2 hiPt = r1 * vec2(sin(phi), cos(phi));
        vec2 a =
             .035*mix(loPt, hiPt, t)
             * (1.+(1.-phAdv));
        a.x *= -1.;
        return a;
    }
}

void main()
{
    int id = int(vP.z) - 1;
    float fid = float(id) / float(uNumQuads);

    float posPhase = uTime;
    vec2 Offset = spiralOffset3(fid, posPhase);

    float rot = uTwist*fid;
    float sr = sinf(rot);
    float cr = cosf(rot);
    mat2 m = mat2(cr, -sr, sr, cr);
    vfC = vec3(vP.xy/.05,0.);
    vec2 pt = m
        //* (fid 
        * .6*(
            // .01+
            pow(fid,.3)
     * (2.*vP.xy));
    gl_Position = vec4(pt+Offset,0.,1.);
}
]]

local fs=[[
#version 100

#define BRIGHTNESS 1.2
#define GAMMA 1.4
#define SATURATION .65

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
void main()
{
    vec3 col = .5*vfC+vec3(.5);
    vec2 d = col.xy;
    col.xy = d;

    // Thanks Kali!
    col=pow(col,vec3(GAMMA))*BRIGHTNESS;
    col=mix(vec3(length(col)),col,SATURATION);

    gl_FragColor = vec4(normalize(col),1.);
}
]]

function quadtrail:init_quads()
    math.randomseed(1)
    self.offsets = {}
    for i=1,self.n_quads do
        table.insert(self.offsets,-1+2*math.random())
        table.insert(self.offsets,-1+2*math.random())
    end
    self:populateVBO()
end

function quadtrail:populateVBO()
    local s = .05
    local verts = {
        -s,-s,0,
        s,-s,0,
        s,s,0,
        -s,-s,0,
        s,s,0,
        -s,s,0,
    }

    local all = {}
    for i=1,self.n_quads do
        for j=1,6 do
            verts[3*j] = i
        end
        for j=1,#verts do
            table.insert(all, verts[j])
        end
    end
    local av = ffi.typeof('GLfloat[?]')(#all,all)

    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(av),av,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,3,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
end

function quadtrail:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function quadtrail:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function quadtrail:initGL()
    local unisz = ffi.typeof('GLint[?]')(0)
    gl.glGetIntegerv(GL.GL_MAX_VERTEX_UNIFORM_VECTORS, unisz)
    print('GL_MAX_VERTEX_UNIFORM_VECTORS',unisz[0])

    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:init_quads()

    if DO_FONTS then
        local fontname = 'segoe_ui128'
        self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
        self.glfont:setDataDirectory(self.dataDir.."/fonts")
        self.glfont:initGL()
    end
end

function quadtrail:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    if self.glfont then
        self.glfont:exitGL()
    end
end

function quadtrail:render()
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    gl.glUseProgram(self.prog)

    local utw_loc = gl.glGetUniformLocation(self.prog, "uTwist")
    gl.glUniform1f(utw_loc, self.twist)

    local ut_loc = gl.glGetUniformLocation(self.prog, "uTime")
    gl.glUniform1f(ut_loc, self.t)

    local un_loc = gl.glGetUniformLocation(self.prog, "uNumQuads")
    gl.glUniform1i(un_loc, self.n_quads)    

    self:bindVBO()
    local chunkSz = self.n_quads
    gl.glDrawArrays(GL.GL_TRIANGLES, 0, 6*chunkSz)
    self:unbindVBO()


    if self.glfont and self.draw_font then
        local m = {}
        local p = {}
        mm.make_identity_matrix(m)
        local s = .5
        mm.glh_scale(m, s, s, s)
        mm.glh_translate(m, 30, 30, 0)
        -- TODO getStringWidth and center text
        if not win_w then
            win_w,win_h = 800,600
        end
        mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
        gl.glDisable(GL.GL_DEPTH_TEST)
        local col = {1, 1, 1}
        self.glfont:render_string(m, p, col, "quadtrail3 "..self.n_quads)
        mm.glh_translate(m, 0, 120, 0)
    end
end

function quadtrail:timestep(absTime, dt)
    self.t = -absTime
    self.twist = 100+100*math.sin(.1*absTime)
end

function quadtrail:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function quadtrail:onButtonPressed(padIndex, button)
    if button == lr_input.JOYPAD_A then
        self.t_last = self.t
        self.chunkSz = self.chunkSz * 2
        self.chunkSz = math.min(self.chunkSz, 256)
    elseif button == lr_input.JOYPAD_B then
        self.t_last = self.t
        self.chunkSz = self.chunkSz * .5
        self.chunkSz = math.max(self.chunkSz, 1)
    elseif button == lr_input.JOYPAD_X then
        self.n_quads = self.n_quads * 2
        self.n_quads = math.min(self.n_quads, 4096*2*16)
        self:init_quads()
    elseif button == lr_input.JOYPAD_Y then
        self.n_quads = self.n_quads * .5
        self.n_quads = math.max(self.n_quads, 1)
        self:init_quads()
    elseif button == lr_input.JOYPAD_SELECT then
        self.draw_font = not self.draw_font
    end
end

return quadtrail
