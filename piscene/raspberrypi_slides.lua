--[[ raspberrypi_slides.lua

    A short slide deck with bullet points on the Raspberry Pi's features.
]]

raspberrypi_slides = {}
raspberrypi_slides.__index = raspberrypi_slides

local ffi = require "ffi"
local sf = require "util.shaderfunctions"
local glf = require "util.glfont"
local mm = require "util.matrixmath"
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'

require("util.slideshow")
local slides = {
    Slideshow.new({
        title="Khronos APIs",
        bullets={
        "    on Raspberry Pi",
        "",
        "",
        "    Jim Susinno",
        "    Khronos Boston Chapter",
        },
        shown_lines = 4,
    }),

    Slideshow.new({
        title="A cool platform",
        bullets={
            "- Cheap",
            "- Capable",
            "- Popular",
        },
    }),

    Slideshow.new({
        title="How cheap?",
        bullets={
            "- Pi 3                      $35",
            "- 2.5A Power Cable  $10",
            "- MicroSD Card      $10",
            "- Case  (optional)",
            "*Input devices not included"
        },
    }),

    Slideshow.new({
        title="How capable?",
        bullets={
            "- OpenGLES 2.0",
            "- OpenGL 2",
            "- OpenMAX, OpenVG",
            "- EGL   No OpenSL?",
            "- Debian: Python, Mathematica, etc.",
        },
    }),

    Slideshow.new({
        title="How popular?",
        bullets={
            "Model 2: 3 million",
            "Model 3: 5 million",
            "Model 0: 1-2 million",
            "Total: 15 million",
        },
    }),

    Slideshow.new({
        title="GL Extensions",
        bullets={
            "- GL_MAX_TEXTURE_SIZE = 2048",
            "- GL_MAX_TEX..._UNITS = 8",
            "- GL_OES_texture_npot",
            "- GL_OES_vertex_half_float",
            "- GL_OES_mapbuffer",
        },
    }),

    Slideshow.new({
        title="GLSL",
        bullets={
            "OpenGL ES GLSL ES 1.00",
            "#version 100",
            "#version 120 with OpenGL"
        },
    }),
    
    Slideshow.new({
        title="Broadcom VideoCore IV",
        bullets={
            "Docs released",
            "Graphics stack BSD 3-claused",
            "Firmware still proprietary",
            "Eric Anholt developed free driver",
            "Arch Ref Guide available online"
        },
    }),
    
    Slideshow.new({
        title="Linux Graphics Stack",
        bullets={
            "OpenGL on X11/dispmanx",
            "GLES on VT",
            "    $ chvt",
            "SDL - targets VT or X11",
            "Glfw works on X"
        },
    }),
    
    Slideshow.new({
        title="Boot configuration",
        bullets={
            "/boot/config.txt",
            "gpu mem",
            "arm/gpu/sdram freq",
            "over voltage, temp limit...",
            "Voids warranty!"
        },
    }),
    
    Slideshow.new({
        title="Manufacturing Variation",
        bullets={
            "Some overclock better than others",
            "Get the 2.5A power supply",
            "Stick-on heatsinks: do they help?",
        },
    }),

}

local slide_idx = 1
local slide = slides[1]


function raspberrypi_slides.new(...)
    local self = setmetatable({}, raspberrypi_slides)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function raspberrypi_slides:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function raspberrypi_slides:init()
    self.buttons = {}
end

function raspberrypi_slides:initGL()
    local fontname = 'segoe_ui128'
    self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
    self.glfont:setDataDirectory(self.dataDir.."/fonts")
    self.glfont:initGL()
    slide:initGL(self.dataDir)
end

function raspberrypi_slides:exitGL()
    slide:exitGL()
end

function raspberrypi_slides:render()
    gl.glClearColor(1,1,1,1)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    slide:draw_text(self.glfont)
end

function raspberrypi_slides:timestep(absTime, dt)
end

function raspberrypi_slides:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:on_button_pressed(j,b)
                end
            end
        end
    end
end

function raspberrypi_slides:increment_slide(incr)
    slide_idx = slide_idx + incr

    if slide_idx < 1 then slide_idx = 1 end
    if slide_idx > #slides then slide_idx = #slides end

    slide:exitGL()
    slide = slides[slide_idx]
    slide:initGL(dataDir)
end

function raspberrypi_slides:forward()
    if slide.shown_lines >= #slide.bullet_points then
        self:increment_slide(1)
        return true
    end
    slide:advance(1)
end

function raspberrypi_slides:back()
    if slide.shown_lines <= 0 then
        self:increment_slide(-1)
        return true
    end
    slide:advance(-1)
end

function raspberrypi_slides:on_button_pressed(padIndex, button)
    if button == lr_input.JOYPAD_A then
        snd.playSound("test.wav")
        self:forward()
    elseif button == lr_input.JOYPAD_B then
        snd.playSound("pop_drip.wav")
        self:back()
    elseif button == lr_input.JOYPAD_L then
        snd.playSound("pop_drip.wav")
        self:increment_slide(-1)
    elseif button == lr_input.JOYPAD_R then
        snd.playSound("test.wav")
        self:increment_slide(1)
    end
end

return raspberrypi_slides
