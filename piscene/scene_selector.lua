--[[ scene_selector.lua

    A minimal UI for selecting scenes from a directory.
    Press the A button to select a scene and view it,
    press select+start buttons to exit the scene and return to the selector.

    Inspired by the EmulationStation interface, a frontend for retroarch.
]]

scene_selector = {}
scene_selector.__index = scene_selector

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local SDL = require 'lib/sdl' -- For joystick list
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'

function scene_selector.new(...)
    local self = setmetatable({}, scene_selector)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function scene_selector:init(scenedir)
    self:populateSceneList(scenedir)
end

function scene_selector:populateSceneList(scenedir)
    self.scenedir = scenedir
    self.scene_modules = {}
    self.scene_idx = 1

    -- We really shouldn't be doing this...
    -- Getting a list of files using the shell is a bad idea.
    -- The alternative is building LuaFilesystem.
    local use_shell_hacks_to_get_scene_list = true
    if use_shell_hacks_to_get_scene_list then
        --print('populating from '..scenedir)
        self.scene_modules = getSceneList(scenedir)
    else
        self.scene_modules = {
            'simple_clear',
            'simple_clockface',
            'simple_mover',
        }
    end
end

-- Return a list of all files and a list of directories
-- inside the given path.
function getSceneList(dir)
    require("util.shell_hacks")
    modules = {}
    print("SHEELL HAHAH")
    local files = scandir_portable_sorta(dir)

    -- Poor man's isDirectory check
    local dirs = listdirs_portable_sorta(dir)
    local dirhash = {}
    for k,v in pairs(dirs) do
        print(dir,k,v)

        -- strip trailing /
        if string.sub(v,-1) == '/' then
            v = string.sub(v,0,#v-1)
        end

        --print(v..'/')
        if not dirhash[v] then
            dirhash[v] = true
        end
    end

    --print(ffi.os, #files)
    for k,v in pairs(files) do
        local s = v:gsub(".lua", "")
        --print(k,v,s)
        if s ~= 'scene_selector' then
            if dirhash[v] then
                s = s..'/'
            end
            table.insert(modules, s)
        end
    end
    return modules, dirs
end

function scene_selector:getSelectedSceneName()
    return self.scene_modules[self.scene_idx]
end

function scene_selector:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function scene_selector:initGL()
    local fontname = 'segoe_ui128'
    self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
    self.glfont:setDataDirectory(self.dataDir.."/fonts")
    self.glfont:initGL()
end

function scene_selector:exitGL()
    self.glfont:exitGL()
end

function scene_selector:render()
    gl.glClearColor(.2,.2,.2,1)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .4
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    self.glfont:render_string(m, p, col, "Scene Selector")

    mm.glh_translate(m, 120, 100, 0)
    self.glfont:render_string(m, p, col, "dir: "..self.scenedir)

    mm.glh_translate(m, 0, 150, 0)

    local nhood = 2
    for i=-nhood,nhood do
        local idx = self.scene_idx + i
        if idx >= 1 and idx <= #self.scene_modules then
            local name = self.scene_modules[idx]
            local str = tostring(idx)..'/'..tostring(#self.scene_modules)..')   '..name
            lum = .5
            if i == 0 then lum = 1 end
            self.glfont:render_string(m, p, {lum,lum,lum}, str)
        end
        mm.glh_translate(m, 0, 100, 0)
    end

    if self.lastErrorMessage then
        mm.glh_translate(m, 0, 100, 0)

        -- http://wiki.interfaceware.com/534.html
        local function string_split(s, d)
            local t,i,f = {}, 0, nil
            local match = '(.-)'..d..'()'
            if string.find(s, d) == nil then return {s} end
            for sub, j in string.gmatch(s, match) do
                i = i + 1
                t[i] = sub
                f = j
            end
            if i ~= 0 then t[i+1] = string.sub(s, f) end
            return t
        end

        local msg = self.lastErrorMessage
        local lines = string_split(msg,'\n')
        local s = .4
        mm.glh_scale(m, s, s, s)
        for _,l in pairs(lines) do
            mm.glh_translate(m, 0, 100, 0)
            self.glfont:render_string(m, p, col, l)
        end
    end

    self:renderJoysticksText()
end

-- Return a table of strings, each describing an SDL joystick
local function getJoystickDescriptions()
    local descrs = {}

    local nj = SDL.SDL_NumJoysticks()
    for i=0,nj-1 do
        local jname = SDL.SDL_JoystickNameForIndex(i)
        local jp = SDL.SDL_JoystickOpen(i)
        local nb = SDL.SDL_JoystickNumButtons(jp)
        local na = SDL.SDL_JoystickNumAxes(jp)
        local str = tostring(i)..') ['..ffi.string(jname)..'] '..nb..' buttons, '..na..' axes'

        -- Attempt to give some more info with guids, but they are kinda long and cumbersome
        if false then
            --local guid = SDL.SDL_JoystickGetDeviceGUID(i)
            local guid = SDL.SDL_JoystickGetGUID(jp)
            local guidbuf = ffi.new("char[64]")
            SDL.SDL_JoystickGetGUIDString(guid, guidbuf, 64)
            str = str..' GUID='..ffi.string(guidbuf)
        end
        table.insert(descrs, str)
    end

    return descrs
end

function scene_selector:renderJoysticksText()
    local descrs = getJoystickDescriptions()

    local m = {}
    local p = {}
    mm.make_identity_matrix(m)

    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)

    local lineh = 140
    mm.glh_translate(m, 10, win_h, 0)

    local s = .16
    mm.glh_scale(m, s, s, s)
    local col = {1, 1, 0}

    mm.glh_translate(m, 0, -((#descrs+1)*lineh + 50), 0)
    self.glfont:render_string(m, p, col, '#joysticks='..(#descrs)..':')
    for _,str in pairs(descrs) do    
        mm.glh_translate(m, 0, lineh, 0)
        self.glfont:render_string(m, p, col, str)
    end
end

function scene_selector:timestep(absTime, dt)
end

local last_states = {}
-- Initial joystick states
-- TODO: dynamically allocate
for j=1,8 do
    local last_state = {}
    -- TODO: dynamically allocate button count
    for i=1,14 do last_state[i]=0 end
    table.insert(last_states, last_state)
end
function scene_selector:update_retropad(joystates)
    for j=1,#joystates do
        local js = joystates[j]
        if js then
            for b=1,14 do
                if last_states[j][b] == 0 and js[b] == 1 then
                    self:on_button_pressed(b)
                end
                last_states[j][b] = js[b]
            end
        end
    end
end

function scene_selector:on_button_pressed(i)
    local skipSize = 5

    if i == lr_input.JOYPAD_A then
        snd.playSound("test.wav")
        -- TODO select scene
    elseif i == lr_input.JOYPAD_DOWN then
        self:incrementSceneIdx(1)
    elseif i == lr_input.JOYPAD_UP then
        self:incrementSceneIdx(-1)
    elseif i == lr_input.JOYPAD_L then
        self:incrementSceneIdx(-skipSize)
    elseif i == lr_input.JOYPAD_R then
        self:incrementSceneIdx(skipSize)
    end
end

function scene_selector:incrementSceneIdx(delta)
    snd.playSound("pop_drip.wav")
    self.scene_idx = self.scene_idx + delta

    if self.scene_idx < 1 then
        self.scene_idx = #self.scene_modules
    end

    if self.scene_idx > #self.scene_modules then
        self.scene_idx = 1
    end
end

return scene_selector
