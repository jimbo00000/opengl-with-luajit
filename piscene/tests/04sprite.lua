--[[ simple_sprite.lua

    Animated sprite sheet, frames tiled on the texture image.
    Takes controller input and plays sounds.
]]

simple_sprite = {}
simple_sprite.__index = simple_sprite

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'
local filesystem = require 'util.filesystem'

function simple_sprite.new(...)
    local self = setmetatable({}, simple_sprite)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_sprite:setDataDirectory(dir)
    self.dataDir = dir
    if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_sprite:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}
    self.firstTime = true
end

local vs=[[
#version 100
attribute vec2 vP;
attribute vec2 vT;

uniform vec2 texScale;
uniform vec2 texOffset;

varying vec2 vfC;

void main()
{
    vec2 texC = .5+.5*vT.xy;
    vfC = texC*texScale+texOffset;
    gl_Position = vec4(vP,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec2 vfC;
uniform sampler2D tex;

void main()
{
    vec2 tc = vfC;
    vec4 col = texture2D(tex, tc);
    gl_FragColor = col;
}
]]

function simple_sprite:loadTextures(texfilename, w, h)
    local data, size = filesystem.read('images/'..texfilename)
    print(size, 'bytes read from', texfilename)

    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    self.texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    --gl.glTexParameteri( GL.GL_TEXTURE_2D, GL.GL_DEPTH_TEXTURE_MODE, GL.GL_INTENSITY ); --deprecated, out in 3.1
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
                  w, h, 0,
                  GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end

function simple_sprite:populateVBO()
    local s = .95
    v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)

    s = 1.
    t = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    tl = gl.glGetAttribLocation(self.prog,"vT")
    local tvbo = self.vbos[2]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,tvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(t),t,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(tl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(tl)
end

function simple_sprite:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)

    local tl = gl.glGetAttribLocation(self.prog, "vT")
    local tvbo = self.vbos[2]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, tvbo[0])
    gl.glVertexAttribPointer(tl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(tl)
end

function simple_sprite:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local tl = gl.glGetAttribLocation(self.prog, "vT")
    gl.glDisableVertexAttribArray(vl)
    gl.glDisableVertexAttribArray(tl)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
end

function simple_sprite:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    local tvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,tvbo)
    table.insert(self.vbos, tvbo)
    self:populateVBO()

    local fontname = 'segoe_ui128'
    self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
    self.glfont:setDataDirectory(self.dataDir.."/fonts")
    self.glfont:initGL()

    self.texID = 0

    self:loadTextures('expl_spritesheet1.data', 900,900)
    self.spritex,self.spritey = 9,9
end

function simple_sprite:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    local texdel = ffi.new("GLuint[1]", self.texID)
    gl.glDeleteTextures(1,texdel)
end

function simple_sprite:render()
    gl.glEnable(GL.GL_BLEND)
    gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)

    gl.glUseProgram(self.prog)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    local stex_loc = gl.glGetUniformLocation(self.prog, "tex")
    gl.glUniform1i(stex_loc, 0)

    local dt = self.t - self.t_last
    local sx,sy = self.spritex, self.spritey
    local numSteps = sx*sy
    local durationSeconds = 1
    local frame = math.floor(numSteps * (dt / durationSeconds))
    local texOffx = math.fmod(frame, sx)
    local texOffy = math.floor(frame/sy)
    local texoff_loc = gl.glGetUniformLocation(self.prog, "texOffset")
    gl.glUniform2f(texoff_loc, texOffx/sx, texOffy/sy)
    local texsc_loc = gl.glGetUniformLocation(self.prog, "texScale")
    gl.glUniform2f(texsc_loc, 1/sx, 1/sy)

    self:bindVBO()
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    self:unbindVBO()

    if self.glfont then
        local m = {}
        local p = {}
        mm.make_identity_matrix(m)
        local s = .5
        mm.glh_scale(m, s, s, s)
        mm.glh_translate(m, 30, 30, 0)
        -- TODO getStringWidth and center text
        if not win_w then
            win_w,win_h = 800,600
        end
        mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
        gl.glDisable(GL.GL_DEPTH_TEST)
        local col = {1, 1, 1}
        self.glfont:render_string(m, p, col, "simple_sprite")
        mm.glh_translate(m, 0, 90, 0)
        local st = tostring(frame)..'  '..tostring(texOffx)..','..tostring(texOffy)
        self.glfont:render_string(m, p, col, st)
        mm.glh_translate(m, 0, 90, 0)
        self.glfont:render_string(m, p, col, 'Press any button')
    end

    gl.glDisable(GL.GL_BLEND)
end

function simple_sprite:timestep(absTime, dt)
    self.t = absTime

    if self.firstTime then
        self.t_last = self.t
    end
    self.firstTime = false
end

function simple_sprite:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:onButtonPressed(j,b)
                end
            end
        end
    end
end

function simple_sprite:onButtonPressed(padIndex, button)
    self:on_button_pressed(button)
end

function simple_sprite:on_button_pressed(i)
    self.t_last = self.t

    local soundmap = {
        [lr_input.JOYPAD_A] = "pop_drip.wav",
        [lr_input.JOYPAD_B] = "Jump5.wav",
        [lr_input.JOYPAD_X] = "Pickup_Coin7.wav",
        [lr_input.JOYPAD_Y] = "test.wav",
    }
    local s = soundmap[i]
    if s then snd.playSound(s) end
end

return simple_sprite
