--[[ simple_mover.lua

    Takes controller input and moves a square around the viewport.
]]

simple_mover = {}
simple_mover.__index = simple_mover

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions2'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require("util.glfont")

function simple_mover.new(...)
    local self = setmetatable({}, simple_mover)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_mover:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_mover:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0

    self.movers = {}
end

local vs=[[
#version 100

uniform vec2 uOffset;
uniform vec3 uColor;

attribute vec2 vP;
varying vec2 vfP;
varying vec3 vfC;

void main()
{
    vfP = vP;
    vfC = uColor;
    gl_Position = vec4(.2*vP+uOffset,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
varying vec2 vfP;

uniform float uBlue;

void main()
{
    vec3 col = .5*vfC+vec3(.5);
    if (max(abs(vfP.x), abs(vfP.y)) < .8)
    {
        col = vec3(vfP, 0.);
    }

    col += uBlue*col;
    gl_FragColor = vec4(col,1.);
}
]]

function simple_mover:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_mover:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function simple_mover:populateVBO()
    self:bindVBO()
    local s = 1
    local v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
end

function simple_mover:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()

    local fontname = 'segoe_ui128'
    self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
    self.glfont:setDataDirectory(self.dataDir.."/fonts")
    self.glfont:initGL()
end

function simple_mover:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
end

function simple_mover:render()
    gl.glUseProgram(self.prog)

    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")
    local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffset")
    local ucol_loc = gl.glGetUniformLocation(self.prog, "uColor")

    -- color table
    -- TODO: generate unique colors for as many players as possible
    -- TODO: fairness rules for draw order, blending? 
    local colors = {
        {0.2, 0.2, 1.0},
        {1.0, 0.2, 0.2},
        {0.2, 1.0, 0.2},
        {1.0, 1.0, 0.2},

        {0.2, 1.0, 1.0},
        {1.0, 0.2, 1.0},
        {0.2, 0.2, 0.2},
        {1.0, 1.0, 1.0},
    }
    self:bindVBO()
    for i=1,#self.movers do
        local targetMover = self.movers[i]
        if targetMover then
            local off = targetMover[1]
            local col = colors[i]
            gl.glUniform2f(uoff_loc, off[1], off[2])
            gl.glUniform3f(ucol_loc, col[1], col[2], col[3])

            local dt = self.t - targetMover[3]
            local b = math.max(0, 1-3*dt)
            gl.glUniform1f(u_loc, b)

            gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
        end
    end
    self:unbindVBO()

    if self.glfont then
        local m = {}
        local p = {}
        mm.make_identity_matrix(m)
        local s = .5
        mm.glh_scale(m, s, s, s)
        mm.glh_translate(m, 30, 30, 0)
        -- TODO getStringWidth and center text
        if not win_w then
            win_w,win_h = 800,600
        end
        mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
        gl.glDisable(GL.GL_DEPTH_TEST)
        local col = {1, 1, 1}
        self.glfont:render_string(m, p, col, 'simple_mover x'..tostring(#self.movers))
        mm.glh_translate(m, 0, 100, 0)
        self.glfont:render_string(m, p, col, 'Use Dpad to move quad around')
    end
end

function simple_mover:timestep(absTime, dt)
    self.t = absTime
    for j=1,#self.movers do

        local mov = self.movers[j]
        local pos = mov[1]
        local vel = mov[2]
        for i=1,2 do
            -- Apply velocity
            pos[i] = pos[i] + vel[i]

            -- Bounds check
            local b = .2
            pos[i] = math.min(math.max(-1+b,pos[i]),1-b)
        end
    end
end

function simple_mover:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:on_button_pressed(j,b)
                end
            end

            -- Create mover if not already there
            if not self.movers[j] then
                local offset = {0,0}
                local vel = {0,0}
                local tlast = 0
                self.movers[j] = {offset,vel, tlast}
            end

            local v = .01
            local vel = self.movers[j][2]
            vel[1] = 0
            vel[2] = 0
            if curstate[lr_input.JOYPAD_LEFT] ~= 0 then
                vel[1] = -v
            elseif curstate[lr_input.JOYPAD_RIGHT] ~= 0 then
                vel[1] = v
            end
            if curstate[lr_input.JOYPAD_UP] ~= 0 then
                vel[2] = v
            elseif curstate[lr_input.JOYPAD_DOWN] ~= 0 then
                vel[2] = -v
            end
        end
    end
end

function simple_mover:on_button_pressed(j,i)
    if not self.movers then return end

    local targetMover = self.movers[j]
    if targetMover then
        if i == lr_input.JOYPAD_A then
            snd.playSound("test.wav")
            targetMover[3] = self.t
        elseif i == lr_input.JOYPAD_B then
            snd.playSound("pop_drip.wav")
        end
    end
end

return simple_mover
