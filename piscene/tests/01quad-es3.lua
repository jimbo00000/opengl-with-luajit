--[[ simple_quad.lua

    The simplest scene that creates geometry and displays it.
    Requires a shader(code included) for drawing to screen.
]]

simple_quad = {}
simple_quad.__index = simple_quad

local ffi = require "ffi"
local sf = require "util.shaderfunctions"

function simple_quad.new(...)
    local self = setmetatable({}, simple_quad)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_quad:setDataDirectory(dir)
end

function simple_quad:init()
    self.vao = 0
    self.vbos = {}
    self.prog = 0
end

local vs=[[
#version 310 es

in vec2 vP;
out vec3 vfC;

void main()
{
    vfC = vec3(vP/.5, 0.);
    vec2 pt = vP;
    gl_Position = vec4(pt,0.,1.);
}
]]

local fs=[[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfC;
out vec4 fragColor;

void main()
{
    vec3 col = .5*vfC+vec3(.5);
    fragColor = vec4(col,1.);
}
]]

function simple_quad:populateVBO()
    local s = .5
    local v = ffi.typeof('GLfloat[?]')(8,{-s,-s, s,-s, s,s, -s,s,})
    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_quad:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()
    gl.glBindVertexArray(0)
end

function simple_quad:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
    gl.glBindVertexArray(0)
end

function simple_quad:render()
    gl.glUseProgram(self.prog)

    gl.glBindVertexArray(self.vao)
    gl.glDrawArrays(GL.GL_TRIANGLE_FAN,0,6)
    gl.glBindVertexArray(0)

    gl.glUseProgram(0)
end

function simple_quad:timestep(absTime, dt)
end

return simple_quad
