local simple_collide = {
    _DESCRIPTION = [[
        A simple example of CPU movement and collision detection.
        Mover quads are checked for hits against world quads.
    ]],
    _URL = 'https://bitbucket.org/jimbo00000/opengl-with-luajit/src/master/'
}
simple_collide.__index = simple_collide

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local mm = require 'util.matrixmath'
local lri = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'

function simple_collide.new(...)
    local self = setmetatable({}, simple_collide)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_collide:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_collide:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}

    self.n_quads = 8
    self.movers = {}
    self:randomize()
    self.moversz = .05
end

function simple_collide:randomize()
    self.targets = {}
    for i=1,self.n_quads do
        local off = {-1+2*math.random(), -1+2*math.random()}
        local s = .05 + .1*math.random()
        local t = {pos=off, sz=s}
        table.insert(self.targets, t)
    end
end

local vs = [[
#version 100
uniform float uTime;
uniform vec3 uOffsetSz;
attribute vec2 vP;
varying vec3 vfC;

void main()
{
    float rot = uTime;
    mat2 m = mat2(1.);
    vfC = vec3(vP,0.);
    vec2 pt = m * vP;
    gl_Position = vec4(uOffsetSz.z*pt+uOffsetSz.xy,0.,1.);
}
]]

local fs = [[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
uniform float uBlue;
void main()
{
    vec3 col = .5*vfC+vec3(.5);
    col.b = uBlue;
    gl_FragColor = vec4(col,1.);
}
]]

function simple_collide:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_collide:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function simple_collide:populateVBO()
    self:bindVBO()
    local s = 1
    local v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
end

function simple_collide:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})
    
    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()

    local fontname = 'segoe_ui128'
    self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
    self.glfont:setDataDirectory(self.dataDir.."/fonts")
    self.glfont:initGL()
end

function simple_collide:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
end

function simple_collide:render()

    -- Targets
    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")
    gl.glUniform1f(u_loc, b)

    local ut_loc = gl.glGetUniformLocation(self.prog, "uTime")
    gl.glUniform1f(ut_loc, self.t)

    self:bindVBO()
    -- TODO: pseudo-instancing
    for i=1,self.n_quads do
        local off = self.targets[i].pos
        local s = self.targets[i].sz
        local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffsetSz")
        gl.glUniform3f(uoff_loc, off[1], off[2], s)

        gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    end

    -- Collectors
    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")
    gl.glUniform1f(u_loc, b)
    local ut_loc = gl.glGetUniformLocation(self.prog, "uTime")
    gl.glUniform1f(ut_loc, 0)
    
    for i=1,#self.movers do
        local off = self.movers[i][1]
        local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffsetSz")
        gl.glUniform3f(uoff_loc, off[1], off[2], self.moversz)

        gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    end
    self:unbindVBO()


    -- Score/fps overlay
    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    local str = ''
    for i=1,#self.movers do
        str = str..tostring(self.movers[i].score)..'  '
    end
    self.glfont:render_string(m, p, col, str)

    if self.fps then
        mm.glh_translate(m, 0, 120, 0)
        self.glfont:render_string(m, p, col, 'simple_collide '..self.fps.." fps")
    end
end

function simple_collide:_createNewMover()
    local offset = {0,0}
    local vel = {0,0}
    return {
        offset,
        vel,
        ['score'] = 0,
        ['name'] = 'player '..#self.movers,
        ['sz'] = .05,
    }
end

------------------------------------------
-- Collision detection
------------------------------------------
local function moverHitsTargetSpherical(moversz, moverpos, target)
    local rad = target.sz + moversz
    local rad2 = rad*rad

    local dx,dy = moverpos[1]-target.pos[1], moverpos[2]-target.pos[2]
    local dist2 = dx*dx+dy*dy
    return dist2 < rad2
end

local function moverHitsTargetRect(moversz, moverpos, target)
    --[[
    local r1 = {
        x = moverpos[1] - moversz,
        y = moverpos[2] - moversz,
        w = 2*moversz,
        h = 2*moversz,
    }
    local r2 = {
        x = target.pos[1] - target.sz,
        y = target.pos[2] - target.sz,
        w = target.sz,
        h = target.sz,
    }]]

    local dx,dy = moverpos[1]-target.pos[1], moverpos[2]-target.pos[2]
    local rad = target.sz + moversz
    if math.abs(dx) > rad then return false end
    if math.abs(dy) > rad then return false end
    return true
end

function simple_collide:getNewPosition(mov, goalPos)
    local actualPos = {goalPos[1], goalPos[2]}
    local hits = {}
    local numHits = 0

    local pos = mov[1]
    -- Check all targets for hits
    for i=1,self.n_quads do
        local target = self.targets[i]
        if moverHitsTargetRect(self.moversz, goalPos, target) then
            for i=1,2 do
                actualPos[i] = pos[i] -- TODO: resolution(use bump)
            end
            numHits = numHits + 1
        end
    end

    return actualPos, hits, numHits
end

function simple_collide:moveInWorld(mov)
    local pos = mov[1]
    local vel = mov[2]
    local goalPos = {}
    for x=1,2 do
        goalPos[x] = pos[x] + vel[x]
    end

    local actualPos, hits, numHits = self:getNewPosition(mov, goalPos)
    if numHits > 0 then
        snd.playSound("test.wav")
    end

    -- Bounds check
    for i=1,2 do
        pos[i] = actualPos[i]
        local b = self.moversz
        pos[i] = math.min(math.max(-1+b,pos[i]),1-b)
    end
end

function simple_collide:timestep(absTime, dt)
    self.t = absTime
    for j=1,#self.movers do
        local mov = self.movers[j]
        if not mov then return end
        self:moveInWorld(mov)
    end
end

------------------------------------------
-- Player input
------------------------------------------
local function setVelocityFromInput(mov, curstate)
    local v = .01
    local vel = mov[2]
    vel[1] = 0
    vel[2] = 0
    if curstate[lri.JOYPAD_LEFT] ~= 0 then
        vel[1] = -v
    elseif curstate[lri.JOYPAD_RIGHT] ~= 0 then
        vel[1] = v
    end
    if curstate[lri.JOYPAD_UP] ~= 0 then
        vel[2] = v
    elseif curstate[lri.JOYPAD_DOWN] ~= 0 then
        vel[2] = -v
    end
end

function simple_collide:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:on_button_pressed(j,b)
                end
            end

            -- Create mover if not already there
            if not self.movers[j] then
                self.movers[j] = self:_createNewMover()
            end

            setVelocityFromInput(self.movers[j], curstate)
        end
    end
end

function simple_collide:on_button_pressed(padIndex, button)
    if button == lri.JOYPAD_A then
        snd.playSound("test.wav")
        self.t_last = self.t
    elseif button == lri.JOYPAD_B then
        snd.playSound("Pickup_Coin7.wav")
        self.t_last = self.t
    elseif button == lri.JOYPAD_START then
        -- Reset
        self:randomize()
   end
end

return simple_collide
