--[[ simple_clear.lua

    The quickest and easiest scene that:
     - draws something 
     - plays sounds
     - takes input
]]

simple_clear = {}
simple_clear.__index = simple_clear

local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'

function simple_clear.new(...)
    local self = setmetatable({}, simple_clear)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_clear:setDataDirectory(dir)
    self.dataDir = dir
	snd.setDataDirectory(dir)
end

function simple_clear:init()
    self.t = 0
end

function simple_clear:initGL()
end

function simple_clear:exitGL()
end

function simple_clear:render() --_for_one_eye(view, proj)
    local t = 3*self.t
    local function f(t) return .5+.5*math.sin(t) end
    gl.glClearColor(f(t), f(1.3*t), f(1.7*t), 0.5)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)
end

function simple_clear:timestep(absTime, dt)
    self.t = absTime
end

function simple_clear:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:on_button_pressed(j,b)
                end
            end
        end
    end
end

function simple_clear:on_button_pressed(padIndex, button)
    self.t_last = self.t

    print("  pressed",padIndex,button)
    local soundmap = {
        [lr_input.JOYPAD_A] = "pop_drip.wav",
        [lr_input.JOYPAD_B] = "Jump5.wav",
        [lr_input.JOYPAD_X] = "Pickup_Coin7.wav",
        [lr_input.JOYPAD_Y] = "test.wav",
    }
    local s = soundmap[button]
    if s then snd.playSound(s) end
end

return simple_clear
