local simple_collide = {
    _DESCRIPTION = [[
        A simple example of CPU movement and collision detection.
        Mover quads are checked for hits against world quads, and removed if hit.
        Target VBO is (optionally) updated on each removal.
    ]],
    _URL = 'https://bitbucket.org/jimbo00000/opengl-with-luajit/src/master/'
}
simple_collide.__index = simple_collide

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions'
local mm = require 'util.matrixmath'
local lri = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require 'util.glfont'
local bump = require 'thirdparty.bump'

function simple_collide.new(...)
    local self = setmetatable({}, simple_collide)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_collide:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_collide:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}

    self.draw_font = true
    self.refresh_vbo = true
    self.debug_mode = false
    self.do_collect = true
    self.coll_filter = 1 -- 1-4
    self.movspeed = .01
    self.n_quads = 32
    self.cellsz = 64

    self.movers = {}
    self.world = bump.newWorld(self.cellsz)
end

local vs = [[
#version 100

uniform vec3 uOffsetSz;
attribute vec2 vP;
varying vec3 vfC;

void main()
{
    vfC = vec3(vP,0.);
    vec2 pt = vP;
    gl_Position = vec4(uOffsetSz.z*pt + uOffsetSz.xy,0.,1.);
}
]]

local fs = [[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
uniform float uBlue;

void main()
{
    vec3 col = .5*vfC+vec3(.5);
    col.b = uBlue;
    gl_FragColor = vec4(col,1.);
}
]]

function simple_collide:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_collide:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function simple_collide:allocateVBO()
    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
end

function simple_collide:populateVBO()
    local all = self:makeTableOfTargetPositions()
    local av = ffi.typeof('GLfloat[?]')(#all,all)

    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(av),av,GL.GL_STATIC_DRAW)
end

-- Bake instanced data into an array of triangle coords.
-- Result will be made into a typed array with ffi and passed to GL.
function simple_collide:makeTableOfTargetPositions()
    local all = {}
    for _,t in pairs(self.targets) do
        local s = t.sz
        local x = t.pos[1]
        local y = t.pos[2]

        local verts = {
            0+x, 0+y,
            s+x, 0+y,
            s+x, s+y,
            0+x, 0+y,
            s+x, s+y,
            0+x, s+y,
        }
        for j=1,#verts do
            table.insert(all, verts[j])
        end
    end
    return all
end

-- Clears the existing set of targets and populates the world with a random set of new quads.
function simple_collide:randomizeTargets()
    if self.targets then
        for _,v in pairs(self.targets) do
            if v then self.world:remove(v.box) end
        end
    end

    self.targets = {}
    for i=1,self.n_quads do
        local x = -1+2*math.random()
        local y = -1+2*math.random()
        local s = .05 + .1*math.random()
        local w,h = s,s

        local box = {
            name='obj'..i,
            idx=i,
        }
        self.world:add(box,  x,y,  w,h)

        local t = {
            pos={x,y},
            sz=s,
            box=box
        }
        table.insert(self.targets, t)
    end
end

function simple_collide:randomizeAndPopulate()
    self:randomizeTargets()
    self:populateVBO()
end

function simple_collide:initGL()
    self.prog, err = sf.make_shader_from_source({vsrc=vs,fsrc=fs})
    if self.prog == 0 then
        local dbg = debug.getinfo(1)
        local tag = dbg.short_src..':'..dbg.currentline
        err = err:sub(1,#err-1) -- strip newline
        err = tag..'\n    '..err
        print(err)
        return err
    end

    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})
    local unis = {
        'uBlue',
        'uOffsetSz',
    }
    self.uniforms = {}
    local function uni(var) return gl.glGetUniformLocation(self.prog, var) end
    for k,v in pairs(unis) do
        self.uniforms[v] = uni(v)
    end

    self:allocateVBO()
    self:randomizeAndPopulate()

    -- Separate vbo for mover rect
    local mvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,mvbo)
    table.insert(self.vbos, mvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,mvbo[0])
    local s = 1
    local v = ffi.typeof('GLfloat[?]')(12,{0,0, s,0, s,s,    0,0, s,s, 0,s,})
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)

    local fontname = 'segoe_ui128'
    self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
    self.glfont:setDataDirectory(self.dataDir.."/fonts")
    self.glfont:initGL()
end

function simple_collide:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
end

function simple_collide:render()
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    
    -- Targets
    gl.glUseProgram(self.prog)
    gl.glUniform1f(self.uniforms.uBlue, b)
    gl.glUniform3f(self.uniforms.uOffsetSz, 0,0,1)

    self:bindVBO()
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6*#self.targets)

    -- Movers
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[2]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)

    -- TODO: coalesce draw calls
    for i=1,#self.movers do
        local mov = self.movers[i]
        local off = mov[1]
        gl.glUniform3f(self.uniforms.uOffsetSz, off[1], off[2], mov.sz)

        gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    end
    
    self:unbindVBO()

    if self.draw_font then
        self:drawOverlay()
    end
end

function simple_collide:drawOverlay()
    if not self.glfont then return end

    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .35
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    local str = 'bump/attribs: '
    str = str .. 'n='..tostring(#self.targets)
    str = str .. '/'..tostring(self.n_quads)
    str = str .. '  ['

    for i=1,#self.movers do
        str = str..tostring(self.movers[i].score)..'  '
    end
    str = str..']'
    self.glfont:render_string(m, p, col, str)

    if self.debug_mode then
        mm.glh_translate(m, 0, 120, 0)
        self.glfont:render_string(m, p, col,
            "DBG cellsz="..tostring(self.cellsz)
            ..' refr='..tostring(self.refresh_vbo)
            ..' coll='..tostring(self.do_collect)
            ..' filt='..tostring(self.coll_filter)
            )
    end
end

------------------------------------------
-- Collision detection
------------------------------------------
function simple_collide:timestep(absTime, dt)
    self.t = absTime
    for j=1,#self.movers do
        local mov = self.movers[j]
        if not mov then return end
        self:moveInWorld(mov)
    end
end

function simple_collide:moveInWorld(mov)
    local pos = mov[1]
    local vel = mov[2]
    local s = mov.sz
    local goalPos = {}
    for x=1,2 do
        local newpos = pos[x] + vel[x]
        -- VBO holds quad in [0,s] (centered at x+s/2)
        goalPos[x] = math.min(math.max(-1, newpos), 1-s)
    end

    --
    -- Bump physics
    --
    local filters = {
        'touch',
        'cross',
        'slide',
        'bounce',
    }
    local playerFilter = function(item, other)
      return filters[self.coll_filter]
    end

    -- Hit Test
    local actualX, actualY, cols, len =
        self.world:move(
            mov, goalPos[1], goalPos[2], playerFilter)

    -- Apply allowed movement
    mov[1] = {actualX, actualY}

    -- Consequences
    if len > 0 then
        snd.playSound("test.wav")
    end
    for i=1,len do
        local col = cols[i]

        if self.do_collect then
            -- Mover collects target
            if not col.other.score then
                self:removeTarget(col.other)
                local mov = self.movers[col.item.idx]
                mov.score = mov.score + 1
            end
        end
    end
end

function simple_collide:removeTarget(other)
    --print(other.name, other.idx)
    -- Don't take out other players
    if other.score then return end

    self.world:remove(other)
    -- This call shuffles all items above it down to close the array.
    table.remove(self.targets, other.idx)
    -- Adjust idxs in world tables to match new position in array.
    for i=other.idx, #self.targets do
        --print(i, self.targets[i].box.idx, self.targets[i].box.name)
        self.targets[i].box.idx = i
    end

    if self.refresh_vbo then
        self:populateVBO()
    end
end

function simple_collide:repopulateBumpWorld()
    -- Delete old world - shouldn't need to clean it out
    self.world = nil

    self.world = bump.newWorld(self.cellsz)

    for i=1,#self.movers do
        local mov = self.movers[i]
        local x,y = mov[1][1], mov[1][2]
        local sz = mov.sz
        local w,h = sz,sz
        self.world:add(mov,  x,y,  w,h)
    end

    for i=1,self.n_quads do
        local t = self.targets[i]
        if not t then return end

        local x = t.pos[1]
        local y = t.pos[2]
        local s = t.sz
        local w,h = s,s
        self.world:add(t.box,  x,y,  w,h)
    end
end

function simple_collide:_createNewMover()
    local idx = #self.movers + 1
    local offset = {.1,.1*idx}
    local vel = {0,0}
    return {
        offset,
        vel,
        ['score'] = 0,
        ['name'] = 'player '..idx,
        ['idx'] = idx,
        ['sz'] = .05,
    }
end

function simple_collide:AddNewMoverAtIndex(idx)
    local mov = self:_createNewMover()
    self.movers[idx] = mov

    local x,y = mov[1][1], mov[1][2]
    local sz = mov.sz
    local w,h = sz,sz
    self.world:add(mov,  x,y,  w,h)
    print(self.world:getRect(mov))
end

function simple_collide:resizeMover(mov, newsz)
    mov.sz = newsz
    local off = mov[1]
    local x,y = off[1], off[2]
    local w,h = newsz,newsz
    self.world:update(mov,  x,y,  w,h)
end

------------------------------------------
-- Player input
------------------------------------------
local function setVelocityFromInput(mov, curstate, v)
    local vel = mov[2]
    vel[1] = 0
    vel[2] = 0
    if curstate[lri.JOYPAD_LEFT] ~= 0 then
        vel[1] = -v
    elseif curstate[lri.JOYPAD_RIGHT] ~= 0 then
        vel[1] = v
    end
    if curstate[lri.JOYPAD_UP] ~= 0 then
        vel[2] = v
    elseif curstate[lri.JOYPAD_DOWN] ~= 0 then
        vel[2] = -v
    end
end

function simple_collide:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:on_button_pressed(j,b)
                end
            end

            -- Create mover if not already there
            if not self.movers[j] then
                self:AddNewMoverAtIndex(j)
            end

            setVelocityFromInput(self.movers[j], curstate, self.movspeed)
        end
    end
end

function simple_collide:on_button_pressed(padIndex, button)
    local mov = self.movers[padIndex]
    if not mov then return end

    local sz_factor = 1.5

    -- Use this to swap tables
    if button == lri.JOYPAD_START then
        self.debug_mode = not self.debug_mode
    elseif button == lri.JOYPAD_SELECT then
        snd.playSound("pop_drip.wav")
        self.draw_font = not self.draw_font
    end

    local func_table_normal = {
        [lri.JOYPAD_A] = function (x)
            snd.playSound("test.wav")
            self.t_last = self.t
            self:resizeMover(mov, mov.sz * sz_factor)
        end,
        [lri.JOYPAD_B] = function (x)
            snd.playSound("Pickup_Coin7.wav")
            self.t_last = self.t
            self:resizeMover(mov, mov.sz / sz_factor)
        end,

        [lri.JOYPAD_Y] = function (x)
            self.n_quads =  self.n_quads / 2
            self.n_quads = math.max(1,self.n_quads)
            self:allocateVBO()
            self:randomizeAndPopulate()
        end,

        [lri.JOYPAD_X] = function (x)
            self.n_quads =  self.n_quads * 2
            self:allocateVBO()
            self:randomizeAndPopulate()
        end,

        [lri.JOYPAD_L] = function (x)
            self.movspeed = self.movspeed / sz_factor
            snd.playSound("Pickup_Coin7.wav")
            self.t_last = self.t
        end,

        [lri.JOYPAD_R] = function (x)
            self.movspeed = self.movspeed * sz_factor
            snd.playSound("Pickup_Coin7.wav")
            self.t_last = self.t
        end,
    }

    local func_table_debug = {
        [lri.JOYPAD_A] = function (x)
            snd.playSound("test.wav")
            self.refresh_vbo = not self.refresh_vbo
        end,

        [lri.JOYPAD_Y] = function (x)
            self.cellsz =  self.cellsz / 2
            self.cellsz = math.max(1,self.cellsz)
            self:repopulateBumpWorld()
        end,

        [lri.JOYPAD_X] = function (x)
            self.cellsz =  self.cellsz * 2
            self:repopulateBumpWorld()
        end,

        [lri.JOYPAD_L] = function (x)
            snd.playSound("test.wav")
            self.do_collect = not self.do_collect
        end,

        [lri.JOYPAD_R] = function (x)
            snd.playSound("test.wav")
            self.coll_filter = self.coll_filter + 1
            if self.coll_filter == 5 then
                self.coll_filter = 1
            end
        end,
    }

    local func_table = func_table_normal
    if self.debug_mode then
        func_table = func_table_debug
    end
    local f = func_table[button]
    if f then f() end
end

return simple_collide
