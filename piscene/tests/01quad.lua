--[[ simple_quad.lua

    The simplest scene that creates geometry and displays it.
    Requires a shader(code included) for drawing to screen.
]]

simple_quad = {}
simple_quad.__index = simple_quad

local ffi = require "ffi"
local sf = require "util.shaderfunctions"

function simple_quad.new(...)
    local self = setmetatable({}, simple_quad)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_quad:setDataDirectory(dir)
end

function simple_quad:init()
    self.vbos = {}
    self.prog = 0
end

local vs=[[
#version 100
attribute vec2 vP;
varying vec3 vfC;

void main()
{
    vfC = vec3(vP/.05,0.);
    vec2 pt = vP;
    gl_Position = vec4(pt,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
void main()
{
    vec3 col = .5*vfC+vec3(.5);
    gl_FragColor = vec4(col,1.);
}
]]

function simple_quad:populateVBO()
    local s = .5
    local v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    local vl = gl.glGetAttribLocation(self.prog,"vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_quad:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_quad:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function simple_quad:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()
end

function simple_quad:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
end

function simple_quad:render()
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    gl.glUseProgram(self.prog)

    self:bindVBO()
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    self:unbindVBO()

    gl.glUseProgram(0)
end

function simple_quad:timestep(absTime, dt)
end

return simple_quad
