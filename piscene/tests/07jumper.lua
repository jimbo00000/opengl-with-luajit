--[[ simple_jumper.lua

    Similar to 05mover, but with velocity, acceleration and a floor.
]]

simple_jumper = {}
simple_jumper.__index = simple_jumper

local ffi = require 'ffi'
local sf = require 'util.shaderfunctions2'
local mm = require 'util.matrixmath'
local lr_input = require 'util.libretro_input'
local snd = require 'util.audio'
local glf = require("util.glfont")

function simple_jumper.new(...)
    local self = setmetatable({}, simple_jumper)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_jumper:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_jumper:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0

    self.movers = {}
end

local vs=[[
#version 100

uniform vec2 uOffset;
uniform vec3 uColor;

attribute vec2 vP;
varying vec2 vfP;
varying vec3 vfC;

void main()
{
    vfP = vP;
    vfC = uColor;
    gl_Position = vec4(.2*vP+uOffset,0.,1.);
}
]]

local fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
varying vec2 vfP;

uniform float uBlue;

void main()
{
    vec3 col = .5*vfC+vec3(.5);
    if (max(abs(vfP.x), abs(vfP.y)) < .8)
    {
        col = vec3(vfP, 0.);
    }

    col += uBlue*col;
    gl_FragColor = vec4(col,1.);
}
]]

function simple_jumper:bindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    local vvbo = self.vbos[1]
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glVertexAttribPointer(vl, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vl)
end

function simple_jumper:unbindVBO()
    local vl = gl.glGetAttribLocation(self.prog, "vP")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
    gl.glDisableVertexAttribArray(vl)
end

function simple_jumper:populateVBO()
    self:bindVBO()
    local s = 1
    local v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
end

function simple_jumper:initGL()
    -- Pre-load sound
    snd.playSound("test.wav")

    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})

    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    table.insert(self.vbos, vvbo)
    self:populateVBO()

    local fontname = 'segoe_ui128'
    self.glfont = glf.new(fontname..'.fnt', fontname..'_0.raw')
    self.glfont:setDataDirectory(self.dataDir.."/fonts")
    self.glfont:initGL()
end

function simple_jumper:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
end

function simple_jumper:render()
    gl.glUseProgram(self.prog)

    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")
    local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffset")
    local ucol_loc = gl.glGetUniformLocation(self.prog, "uColor")

    -- color table
    -- TODO: generate unique colors for as many players as possible
    -- TODO: fairness rules for draw order, blending? 
    local colors = {
        {0.2, 0.2, 1.0},
        {1.0, 0.2, 0.2},
        {0.2, 1.0, 0.2},
        {1.0, 1.0, 0.2},

        {0.2, 1.0, 1.0},
        {1.0, 0.2, 1.0},
        {0.2, 0.2, 0.2},
        {1.0, 1.0, 1.0},
    }
    self:bindVBO()
    for i=1,#self.movers do
        local targetMover = self.movers[i]
        if targetMover then
            local off = targetMover[1]
            local col = colors[i]
            gl.glUniform2f(uoff_loc, off[1], off[2])
            gl.glUniform3f(ucol_loc, col[1], col[2], col[3])

            local dt = self.t - targetMover.t_last
            local b = math.max(0, 1-3*dt)
            gl.glUniform1f(u_loc, b)

            gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
        end
    end
    self:unbindVBO()

    if self.glfont then
        local m = {}
        local p = {}
        mm.make_identity_matrix(m)
        local s = .5
        mm.glh_scale(m, s, s, s)
        mm.glh_translate(m, 30, 30, 0)
        -- TODO getStringWidth and center text
        if not win_w then
            win_w,win_h = 800,600
        end
        mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
        gl.glDisable(GL.GL_DEPTH_TEST)
        local col = {1, 1, 1}
        self.glfont:render_string(m, p, col, "simple_jumper x"..tostring(#self.movers))

        --[[
        local mov = self.movers[1]
        if mov then
            local pos = mov[1]
            local vel = mov[2]
            local vel = mov[3]

            mm.glh_translate(m, 0, 120, 0)
            local str = "1) "..(pos[1])..' '..(pos[2])
            self.glfont:render_string(m, p, col, str)

            mm.glh_translate(m, 0, 120, 0)
            local str = "2) "..(vel[1])..' '..(pos[2])
            self.glfont:render_string(m, p, col, str)
        end
        ]]
    end
end

function simple_jumper:timestep(absTime, dt)
    self.t = absTime
    local acc = {0,-.3} -- gravity
    for j=1,#self.movers do
        local mov = self.movers[j]
        local pos = mov[1]
        local vel = mov[2]
        for i=1,2 do
            -- Apply acceleration
            vel[i] = vel[i] + acc[i] * dt

            -- Apply velocity
            pos[i] = pos[i] + vel[i]

            -- Bounds check
            local b = .2
            pos[i] = math.min(math.max(-1+b,pos[i]),1-b)
        end

        -- Floor check(y only)
        if pos[2] < 0 then
            pos[2] = 0
            vel[2] = 0
        end
    end
end

function simple_jumper:update_retropad(joystates)
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    self:on_button_pressed(j,b)
                end
            end

            -- Create mover if not already there
            if not self.movers[j] then
                local pos = {0,0}
                local vel = {0,0}
                local acc = {0,0}
                self.movers[j] = {pos,vel,acc, t_last=0}
            end

            local v = .01
            local vel = self.movers[j][2]
            vel[1] = 0
            if curstate[lr_input.JOYPAD_LEFT] ~= 0 then
                vel[1] = -v
            elseif curstate[lr_input.JOYPAD_RIGHT] ~= 0 then
                vel[1] = v
            end

            --[[
            vel[2] = 0
            if curstate[lr_input.JOYPAD_UP] ~= 0 then
                vel[2] = v
            elseif curstate[lr_input.JOYPAD_DOWN] ~= 0 then
                vel[2] = -v
            end
            ]]
        end
    end
end

function simple_jumper:on_button_pressed(j,i)
    if not self.movers then return end

    local targetMover = self.movers[j]
    if targetMover then
        if i == lr_input.JOYPAD_A then
            snd.playSound("test.wav")
            targetMover.t_last = self.t

            -- jump!
            local pos = targetMover[1]
            local vel = targetMover[2]
            vel[2] = .05 -- initial vel

        elseif i == lr_input.JOYPAD_B then
            snd.playSound("pop_drip.wav")
        end
    end
end

return simple_jumper
