-- socket.lua

local ffi = require("ffi")

-- http://stackoverflow.com/questions/17877224/how-to-prevent-a-lua-script-from-failing-when-a-require-fails-to-find-the-scri
local function prequire(m)
  local ok, err = pcall(require, m)
  if not ok then return nil, err end
  return err
end

local socket = nil
if (ffi.os == "Windows") then
    package.cpath = package.cpath .. ';bin/windows/socket/core.dll'
    package.loadlib("socket/core.dll", "luaopen_socket_core")
    socket = require 'socket.core'
elseif (ffi.os == "Linux") then
    package.cpath = package.cpath .. ';bin/linux/'..jit.arch..'/socket/?.so'
    socket = prequire 'socket.core'
elseif (ffi.os == "OSX") then
    package.cpath = package.cpath .. ';bin/osx/socket/?.so'
    socket = require 'socket.core'
end

return socket
