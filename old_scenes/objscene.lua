-- objscene.lua

objscene = {}

local obj = require("util.obj")

local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

local vbos = {}
local vao = 0
local progs = {}
local dataDir = nil
local numTriangles = 0
local sceneTime = 0
local lastManualTimeAdjustment = 0

local basic_vert = [[
#version 310 es

layout(location = 0) in vec4 vPosition;
layout(location = 1) in vec4 vNormal;
layout(location = 2) in vec4 vOffset;
layout(location = 3) in vec4 vCentroid;
layout(location = 4) in vec4 vQuat;

out vec3 vVertCol;

layout(location = 0) uniform mat4 mvmtx;
layout(location = 1) uniform mat4 prmtx;

// Quaternion rotation of a vector
vec3 qtransform(vec4 q, vec3 v)
{
    return v + 2.0*cross(cross(v, q.xyz) + q.w*v, q.xyz);
}

void main()
{
    int vertID3 = gl_VertexID % 3;

    vec3 col = vec3(1.,0.,0.);
    if (vertID3 == 1)
        col = vec3(0.,1.,0.);
    if (vertID3 == 2)
        col = vec3(0.,0.,1.);

    vec3 norm = vNormal.xyz;
    norm = qtransform(vQuat, norm);
    vVertCol = norm;

    vec3 pos = vPosition.xyz;

    pos -= vCentroid.xyz;
    pos = qtransform(vQuat, pos);
    pos += vCentroid.xyz;

    pos += vOffset.xyz;

    gl_Position = prmtx * mvmtx * vec4(pos, 1.);
}
]]

local basic_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vVertCol;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vVertCol, 1.);
}
]]


function objscene.setDataDirectory(dir)
    dataDir = dir
end

local function loadmodel()
    local filename = "teapot.obj"
    if dataDir then filename = dataDir .. "/" .. filename end
    print("Loading obj "..filename)
    obj.loadmodel(filename)
    print(#obj.vertlist.." floats for vertices, "..#obj.idxlist.." triangle indices.")
end

local function init_model_attributes()
    if #obj.olist == 0 then return end

    local v = obj.vertlist
    local i = obj.idxlist

    local verts = glFloatv(#v,v)

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(0, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    vbos.originalTris = vvbo

    gl.glEnableVertexAttribArray(0)

    local lines = glUintv(#i,i)
    local ivbo = glIntv(0)
    gl.glGenBuffers(1, ivbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, ivbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(lines), lines, GL.GL_STATIC_DRAW)
    vbos.triIndices = ivbo
end

--[[
    Split up a mesh with shared vertices and an index array into separate
    triangle for drawing with glDrawArrays , and compute normals.
]]
local splittris_comp_src = [[
#version 310 es
layout(local_size_x=128) in;

layout(std430, binding=0) buffer iblock { int indices[]; };
layout(std430, binding=1) buffer vblock { vec4 origVerts[]; };
layout(std430, binding=2) buffer sblock { vec4 splitVerts[]; };
layout(std430, binding=3) buffer nblock { vec4 splitNorms[]; };
layout(std430, binding=4) buffer cblock { vec4 splitCentroids[]; };
layout(std430, binding=5) buffer qblock { vec4 splitQuats[]; };

layout(location = 0) uniform int uNumTris;

void main()
{
    int index = int(gl_GlobalInvocationID.x);
    if (index >= uNumTris)
        return;

    int i = indices[3*index];
    int j = indices[3*index+1];
    int k = indices[3*index+2];

    vec4 a = origVerts[i];
    vec4 b = origVerts[j];
    vec4 c = origVerts[k];

    vec3 a3 = a.xyz;
    vec3 b3 = b.xyz;
    vec3 c3 = c.xyz;
    vec4 mab = vec4(.5 * (a3 + b3), 1.);
    vec4 mbc = vec4(.5 * (c3 + b3), 1.);
    vec4 mac = vec4(.5 * (a3 + c3), 1.);


    //splitVerts[3*index] = a;
    //splitVerts[3*index+1] = b;
    //splitVerts[3*index+2] = c;

    splitVerts[4*3*index+0*3+0] = a;
    splitVerts[4*3*index+0*3+1] = mab;
    splitVerts[4*3*index+0*3+2] = mac;

    splitVerts[4*3*index+1*3+0] = b;
    splitVerts[4*3*index+1*3+1] = mbc;
    splitVerts[4*3*index+1*3+2] = mab;

    splitVerts[4*3*index+2*3+0] = c;
    splitVerts[4*3*index+2*3+1] = mac;
    splitVerts[4*3*index+2*3+2] = mbc;

    splitVerts[4*3*index+3*3+0] = mab;
    splitVerts[4*3*index+3*3+1] = mbc;
    splitVerts[4*3*index+3*3+2] = mac;

    // Calculate normals
    vec3 ba = (b-a).xyz;
    vec3 ca = (c-a).xyz;
    vec3 norm = normalize(cross(ba,ca));

    // Calculate centroid for triangle
    vec4 cent = (1./3.) * (a + b + c);

    vec4 q = vec4(0.,0.,0.,1.);

    for (int i=0; i<4; ++i)
    {
        for (int j=0; j<3; ++j)
        {
            int idx = 4*3*index + 3*i + j;
            splitNorms[idx] = vec4(norm, 0.);
            splitCentroids[idx] = cent;
            splitQuats[idx] = q;
        }
    }

#if 0
    splitVerts[3*index] = a;
    splitVerts[3*index+1] = b;
    splitVerts[3*index+2] = c;

    splitNorms[3*index] = vec4(norm, 0.);
    splitNorms[3*index+1] = vec4(norm, 0.);
    splitNorms[3*index+2] = vec4(norm, 0.);

    splitCentroids[3*index] = cent;
    splitCentroids[3*index+1] = cent;
    splitCentroids[3*index+2] = cent;

    splitQuats[3*index  ] = q;
    splitQuats[3*index+1] = q;
    splitQuats[3*index+2] = q;
#endif

}
]]

local function assembleTriangleBuffer()
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vbos.triIndices[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 1, vbos.originalTris[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 2, vbos.splitTriVerts[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 3, vbos.splitTriNorms[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 4, vbos.splitTriCentroids[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 5, vbos.triQuats[0])

    gl.glUseProgram(progs.splitTris)
    local n = #obj.idxlist / 3
    gl.glUniform1i(0, n)
    gl.glDispatchCompute(n/128+1, 1, 1)
    gl.glUseProgram(0)

    gl.glMemoryBarrier(GL.GL_SHADER_STORAGE_BARRIER_BIT)
end

-- Per-triangle info
function allocateSplitGeometryBuffers()
    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, triVertCount * ffi.sizeof('GLfloat'), nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(0, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    vbos.splitTriVerts = vvbo

    local nvbo = glIntv(0)
    gl.glGenBuffers(1, nvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, nvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, triVertCount * ffi.sizeof('GLfloat'), nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(1, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    vbos.splitTriNorms = nvbo

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, triVertCount * ffi.sizeof('GLfloat'), nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(3, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    vbos.splitTriCentroids = cvbo

    local ovbo = glIntv(0)
    gl.glGenBuffers(1, ovbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, ovbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, triVertCount * ffi.sizeof('GLfloat'), nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(2, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    vbos.triOffsets = ovbo

    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, triVertCount * ffi.sizeof('GLfloat'), nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(4, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    vbos.triQuats = qvbo
end


--[[
    Reset positions and orientations of individual triangles.
]]
local resettris_comp_src = [[
#version 310 es
layout(local_size_x=128) in;

layout(std430, binding=0) buffer oblock { vec4 triLocs[]; };
layout(std430, binding=1) buffer qblock { vec4 triOris[]; };

layout(location = 0) uniform int uNumTris;

void main()
{
    int index = int(gl_GlobalInvocationID.x);
    if (index >= uNumTris)
        return;

    vec4 o4 = vec4(0.,0.,0., 1.);
    triLocs[3*index] = o4;
    triLocs[3*index+1] = o4;
    triLocs[3*index+2] = o4;

    vec4 q = vec4(0.,0.,0.,1.);
    triOris[3*index] = q;
    triOris[3*index+1] = q;
    triOris[3*index+2] = q;
}
]]

function resetPerTriBuffer(t)
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vbos.triOffsets[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 1, vbos.triQuats[0])

    gl.glUseProgram(progs.resetTris)
    gl.glUniform1i(0, numTriangles)
    local n = (3*numTriangles)/128 + 1
    gl.glDispatchCompute(n, 1, 1)
    gl.glUseProgram(0)

    gl.glMemoryBarrier(GL.GL_SHADER_STORAGE_BARRIER_BIT)
end

--[[
    Timestep positions and orientations of individual triangles.
]]
local triattrs_comp_src = [[
#version 310 es
layout(local_size_x=128) in;

layout(std430, binding=0) buffer sblock { vec4 splitVerts[]; };
layout(std430, binding=1) buffer nblock { vec4 splitNorms[]; };
layout(std430, binding=2) buffer oblock { vec4 triLocs[]; };
layout(std430, binding=3) buffer qblock { vec4 triOris[]; };
layout(std430, binding=4) buffer cblock { vec4 triCentroids[]; };

layout(location = 0) uniform int uNumTris;
layout(location = 1) uniform float uMagnitude;

float hash( float n ) { return fract(sin(n)*43758.5453); }

vec3 getDirForIndex(int idx)
{
    vec3 v01 = vec3(
        hash(float(idx) * 3.),
        hash(float(idx) * 5.),
        hash(float(idx) * 7.)
        );
    return 2.*v01 - vec3(1.);
}

void main()
{
    int index = int(gl_GlobalInvocationID.x);
    if (index >= uNumTris)
        return;

    //vec4 p = normalize(splitVerts[3*index]);
    vec3 randDir = getDirForIndex(index);
    vec3 normDir = splitNorms[3*index].xyz;
    vec3 dir = mix(normDir, randDir, .5);

    // Outwards blast motion
    vec3 o = uMagnitude * dir;

    // Downward acceleration of gravity
    float t = uMagnitude;
    vec3 g = .01 * vec3(0., -9.8, 0.);
    o += t*t*g;

    // Rotation
    float rotSpeed = pow(hash(float(index) * 13.), 3.);
    vec3 axis = getDirForIndex(index/3);
    vec4 av = vec4(uMagnitude*rotSpeed*axis, 1.);
    vec4 q = normalize(av);

    // Floor
    vec3 cntr = triCentroids[3*index].xyz;
    float floor = -cntr.y + -2.;
    if (o.y < floor)
    {
        o.y = floor;
        vec3 up = vec3(0., 1., 0.);
        vec3 axis = cross(normDir, up);
        float dp = dot(normDir, up);
        q = normalize(vec4(axis, dp));
    }

    vec4 o4 = vec4(o, 1.);
    triLocs[3*index] = o4;
    triLocs[3*index+1] = o4;
    triLocs[3*index+2] = o4;

    triOris[3*index] = q;
    triOris[3*index+1] = q;
    triOris[3*index+2] = q;
}
]]

local function timestepPerTriBuffer(t)
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vbos.splitTriVerts[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 1, vbos.splitTriNorms[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 2, vbos.triOffsets[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 3, vbos.triQuats[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 4, vbos.splitTriCentroids[0])

    gl.glUseProgram(progs.triAttrs)
    gl.glUniform1i(0, numTriangles)
    gl.glUniform1f(1, 10*t)

    local n = 3*numTriangles/128 + 1
    gl.glDispatchCompute(n, 1, 1)
    gl.glUseProgram(0)
    gl.glMemoryBarrier(GL.GL_SHADER_STORAGE_BARRIER_BIT)
end

function objscene.initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.glBindVertexArray(vao)

    progs.drawTris = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    progs.splitTris = sf.make_shader_from_source({
        compsrc = splittris_comp_src,
        })

    progs.triAttrs = sf.make_shader_from_source({
        compsrc = triattrs_comp_src,
        })

    progs.resetTris = sf.make_shader_from_source({
        compsrc = resettris_comp_src,
        })

    loadmodel()
    init_model_attributes()

    numTriangles = (#obj.idxlist / 3) * 4
    print("numTriangles: "..numTriangles)
    triVertCount = numTriangles * 4 * 3

    gl.glEnableVertexAttribArray(0)
    gl.glEnableVertexAttribArray(1)
    gl.glEnableVertexAttribArray(2)
    gl.glEnableVertexAttribArray(3)
    gl.glEnableVertexAttribArray(4)

    allocateSplitGeometryBuffers()
    assembleTriangleBuffer()
    timestepPerTriBuffer(0)

    gl.glBindVertexArray(0)
    sceneTime = 0
end

function objscene.exitGL()
    gl.glBindVertexArray(vao)
    for _,v in pairs(vbos) do
        gl.glDeleteBuffers(1,v)
    end
    vbos = {}

    for _,p in pairs(progs) do
        gl.glDeleteProgram(p)
    end
    progs = {}

    local vaoId = ffi.new("GLuint[1]", vao)
    gl.glDeleteVertexArrays(1, vaoId)

    obj.olist = {}
    obj.vertlist = {}
    obj.idxlist = {}
end

function objscene.render_for_one_eye(mview, proj)
    gl.glUseProgram(progs.drawTris)
    gl.glUniformMatrix4fv(0, 1, GL.GL_FALSE, glFloatv(16, mview))
    gl.glUniformMatrix4fv(1, 1, GL.GL_FALSE, glFloatv(16, proj))

    gl.glBindVertexArray(vao)
    for k,v in pairs(obj.olist) do
        if v[1] > 0 then
            gl.glDrawArrays(GL.GL_TRIANGLES, 0, numTriangles * 3)
        end
    end
    gl.glBindVertexArray(0)

    gl.glUseProgram(0)
end

function objscene.timestep(absTime, dt)
    timestepPerTriBuffer(sceneTime*.0001)

    local dt = os.clock() - lastManualTimeAdjustment
    if dt < .1 then return end
    sceneTime = sceneTime + dt
end

function objscene.keypressed(ch)
    sceneTime = 0
    resetPerTriBuffer()
end

function objscene.setSimTime(t)
    sceneTime = (t-400)*.1
    if sceneTime < 0 then sceneTime = 0 end
    --timestepPerTriBuffer(sceneTime*.1)
    lastManualTimeAdjustment = os.clock()
end

return objscene
