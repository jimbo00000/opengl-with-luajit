-- subdivided_cube.lua
-- Draw a simple cube whose sides have been broken into n subdivisions.

scene = {}

local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

local vao = 0
local vbos = {}

local prog_meshvsfs = 0
local num_tri_idxs = 0

local basic_vert = [[
#version 330

in vec4 vPosition;

out vec3 vfColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

void main()
{
    vfColor = vPosition.xyz;
    gl_Position = prmtx * mvmtx * vPosition;
}
]]

local basic_frag = [[
#version 330

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfColor, 1.0);
}
]]


-- Create a VBO for six faces of a subdivided cube.
local function init_spheremesh_attributes(prog)
    local facets = 6

    -- Create a single subdivided square
    local v = {}
    function make_points(mtx)
        for j=0,facets do
            for i=0,facets do
                pt = {i/facets,j/facets,0,1} -- xywz
                pt =mm.transform(pt, mtx)
                table.insert(v, pt[1])
                table.insert(v, pt[2])
                table.insert(v, pt[3])
            end
        end
    end

    local f = {}
    function make_faces(first)
        for j=1,facets do
            for i=1,facets do
                xy = {i,j}
                local function getidx(i,j)
                    return (facets+1)*j + i + first
                end
                table.insert(f, getidx(i,j))
                table.insert(f, getidx(i-1,j-1))
                table.insert(f, getidx(i,j-1))

                table.insert(f, getidx(i,j))
                table.insert(f, getidx(i-1,j))
                table.insert(f, getidx(i-1,j-1))
            end
        end
    end

    -- Replicate the square 6 times and transform each
    -- to a different cube face.
    local mtx = {
        -1,0,0,0,
        0,1,0,0,
        0,0,1,0,
        1,0,0,1,
    }
    local mtx2 = {
        1,0,0,0,
        0,0,1,0,
        0,-1,0,0,
        0,0,0,1,
    }
    local mtx3 = {
        0,-1,0,0,
        0,0,1,0,
        -1,0,0,0,
        0,1,0,1,
    }
    local mtx4 = {
        1,0,0,0,
        0,1,0,0,
        0,0,1,0,
        0,0,1,1,
    }
    local mtx5 = {
        -1,0,0,0,
        0,0,1,0,
        0,-1,0,0,
        1,1,0,1,
    }
    local mtx6 = {
        0,1,0,0,
        0,0,1,0,
        -1,0,0,0,
        1,0,0,1,
    }
    local matrices = {mtx, mtx2, mtx3, mtx4, mtx5, mtx6}

    local fs = (facets+1)*(facets+1)
    for k,v in pairs(matrices) do
        make_faces((k-1)*fs)
        make_points(v)
    end

    num_tri_idxs = #f
    local verts = glFloatv(#v, v)
    local faces = glUintv(#f, f)

    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(prog, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(vbos, vvbo)

    gl.glEnableVertexAttribArray(vpos_loc)

    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(faces), faces, GL.GL_STATIC_DRAW)
    table.insert(vbos, qvbo)
end

function scene.initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.glBindVertexArray(vao)

    prog_meshvsfs = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })
    init_spheremesh_attributes(prog_meshvsfs)

    gl.glBindVertexArray(0)
end

function scene.exitGL()
    gl.glBindVertexArray(vao)
    for _,v in pairs(vbos) do
        gl.glDeleteBuffers(1,v)
    end
    vbos = {}

    gl.glDeleteProgram(prog_meshvsfs)

    local vaoId = ffi.new("GLuint[1]", vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

function scene.render_for_one_eye(mview, proj)
    local prog = prog_meshvsfs
    local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(prog, "prmtx")
    gl.glUseProgram(prog)
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    local m = {}
    for i=1,16 do m[i] = mview[i] end
    mm.glh_scale(m,10,10,10)
    mm.glh_translate(m, -.5, -.5, -.5)
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, m))
    gl.glBindVertexArray(vao)
    gl.glDrawElements(GL.GL_TRIANGLES, num_tri_idxs, GL.GL_UNSIGNED_INT, nil)
    gl.glBindVertexArray(0)

    gl.glUseProgram(0)
end

function scene.timestep(absTime, dt)
end

function scene.keypressed(ch)
end

return scene
