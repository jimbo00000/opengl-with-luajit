--[[ slide05_scene.lua

    Polar coordinates - angle
]]
slide05_scene = {}

require("util.slideshow")
local slide = Slideshow.new({
    title="Polar Coordinates",
    bullets={
        "Use atan2(y,x) to get theta",
        "Origin(0,0) at center",
        "Angle in range [-PI,PI]",
    }
})

local shad = require("util.fullscreen_shader")
local mm = require("util.matrixmath")

local dataDir = nil

function slide05_scene.setDataDirectory(dir)
    dataDir = dir
end

local frag_main = [[
#define PI 3.14159265359
void main()
{
    // atan range in [-PI,PI]
    vec2 uv_ = 2.*uv.yx;
    uv_.x = 2.-uv_.x;
    vec2 uv_11 = uv_ - vec2(1.); // uv in [0,1], remap to [-1,1]
    float angle_11 = atan(uv_11.y, uv_11.x) / PI;
    float angle01 = .5*(1. + angle_11);
    vec3 col = vec3(angle01,angle01,1.);
    fragColor = vec4(col, 1.0);
}
]]
function slide05_scene.initGL()
    slide:initGL(dataDir)

    shad = FullscreenShader.new(frag_main)
    shad:initGL()
end

function slide05_scene.exitGL()
    slide:exitGL()
    shad:exitGL()
end

function slide05_scene.timestep(absTime, dt)
end

function slide05_scene.keypressed(ch)
    return slide:keypressed(ch)
end

function slide05_scene.render_for_one_eye(view, proj)
    shad:render(view, proj)

    gl.glDisable(GL.GL_DEPTH_TEST)
    slide:draw_text()
end

return slide05_scene
