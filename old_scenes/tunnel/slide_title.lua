--[[ slide_title.lua

    A Powerpoint-similar presentation in OpenGL.
    Each keypress reveals a bullet point.
]]
slide_title = {}

require("util.slideshow")

local slide = Slideshow.new({
    title="Tunnel Effect in OpenGL",
    bullets={
        "",
        "",
        "    Jim Susinno",
        "    Khronos Boston Chapter",
    }
})

local dataDir = nil

-- Since data files must be loaded from disk, we have to know
-- where to find them. Set the directory with this standard entry point.
function slide_title.setDataDirectory(dir)
    dataDir = dir
end

function slide_title.initGL()
    slide:initGL(dataDir)
    slide.shown_lines = 4
end

function slide_title.exitGL()
    slide:exitGL()
end

function slide_title.render_for_one_eye(view, proj)
    gl.glClearColor(1,1,1,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    slide:draw_text()
end

function slide_title.timestep(absTime, dt)
end

function slide_title.keypressed(ch)
    return slide:keypressed(ch)
end

return slide_title
