--[[ slide09_scene.lua

    A Powerpoint-similar presentation in OpenGL.

    Each keypress reveals a bullet point.
]]
slide09_scene = {}

require("util.slideshow")

local slide = Slideshow.new({
    title="Geometry Approach",
    bullets={
        "- More work to set up",
        "- Requires large # of verts",
        "+ Change verts in shader",
        "+ Aspect ratio invariant",
    }
})

local dataDir = nil

-- Since data files must be loaded from disk, we have to know
-- where to find them. Set the directory with this standard entry point.
function slide09_scene.setDataDirectory(dir)
    dataDir = dir
end

function slide09_scene.initGL()
    slide:initGL(dataDir)
end

function slide09_scene.exitGL()
    slide:exitGL()
end

function slide09_scene.render_for_one_eye(view, proj)
    gl.glClearColor(1,1,1,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    slide:draw_text()
end

function slide09_scene.timestep(absTime, dt)
end

function slide09_scene.keypressed(ch)
    return slide:keypressed(ch)
end

return slide09_scene
