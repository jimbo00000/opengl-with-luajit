--[[ slide07_scene.lua

    Polar coordinates - angle
]]
slide07_scene = {}

require("util.slideshow")
local slide = Slideshow.new({
    title="Hiding Infinity",
    bullets={
        "Use fog",
        "(or something like it)",
        "mix(vec3(1.), col, length(uv_))"
    }
})

local shad = require("util.fullscreen_shader")
local mm = require("util.matrixmath")

local dataDir = nil

function slide07_scene.setDataDirectory(dir)
    dataDir = dir
end

local frag_main = [[
void main()
{
    vec2 uv_ = 2.*uv - vec2(1.);
    float radial = 1. / (uv_.x*uv_.x + uv_.y*uv_.y);
    float radwaves = 1.5 * sin(2.*radial);
    vec3 col = vec3(radwaves,1.,radwaves);

    // Put some fog/darkness at the end of the tunnel to
    // obscure a rather unsightly infinity.
    col = mix(vec3(1.), col, .5*length(uv_));

    fragColor = vec4(col, 1.0);
}
]]
function slide07_scene.initGL()
    slide:initGL(dataDir)

    shad = FullscreenShader.new(frag_main)
    shad:initGL()
end

function slide07_scene.exitGL()
    slide:exitGL()
    shad:exitGL()
end

function slide07_scene.timestep(absTime, dt)
end

function slide07_scene.keypressed(ch)
    return slide:keypressed(ch)
end

function slide07_scene.render_for_one_eye(view, proj)
    shad:render(view, proj)

    gl.glDisable(GL.GL_DEPTH_TEST)
    slide:draw_text()
end

return slide07_scene
