--[[ slide08_scene.lua
]]
slide08_scene = {}

require("util.slideshow")
local sTri = require("scene.vsfstri")
local sCyl = require("scene.cylinder_scene")
local mm = require("util.matrixmath")

local slide = Slideshow.new({
    title="Geometry Approach",
    bullets={
        "Start with vsfstri.lua",
        "Generate cylinder",
        "Place camera at start",
    }
})

local dataDir = nil

function slide08_scene.setDataDirectory(dir)
    dataDir = dir
end

function slide08_scene.initGL()
    slide:initGL(dataDir)
    sTri.initGL()
    sCyl.initGL()
end

function slide08_scene.exitGL()
    slide:exitGL()
    sTri.exitGL()
    sCyl.exitGL()

    sCyl = nil
    package.loaded['scene.cylinder_scene'] = nil
end

function slide08_scene.timestep(absTime, dt)
    t = absTime
end

function slide08_scene.keypressed(ch)
    return slide:keypressed(ch)
end

function slide08_scene.render_for_one_eye(view, proj)
    gl.glClearColor(1,1,1,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    local m = {}
    mm.make_identity_matrix(m)

    if slide.shown_lines == 2 then
        local m = {}
        mm.make_identity_matrix(m)
        mm.glh_translate(m,1,.5,-.5)
        mm.glh_rotate(m, 10*t, 0,1,0)
        mm.glh_translate(m,0,0,-.5)
        mm.post_multiply(view, m)
    end

    if slide.shown_lines <= 2 then
        mm.glh_translate(m, 0.5,-1,0)
        mm.pre_multiply(view, m)
    end

    if slide.shown_lines == 1 then
        sTri.render_for_one_eye(view, proj)
    elseif slide.shown_lines >= 2 then
        sCyl.render_for_one_eye(view, proj)
    end

    gl.glDisable(GL.GL_DEPTH_TEST)
    slide:draw_text()
end

return slide08_scene
