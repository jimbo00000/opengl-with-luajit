--[[ slide04_scene.lua
]]
slide04_scene = {}

require("util.slideshow")
local sTri = require("scene.vsfstri")
local sQuad = require("scene.vsfsquad")
local shad = require("util.fullscreen_shader")
local mm = require("util.matrixmath")

local slide = Slideshow.new({
    title="Pure Frag Shader",
    bullets={
        "Start with vsfstri.lua",
        "Add another vertex",
        "Ignore view matrices",
    }
})

local dataDir = nil

function slide04_scene.setDataDirectory(dir)
    dataDir = dir
end

function slide04_scene.initGL()
    slide:initGL(dataDir)
    sTri.initGL()
    sQuad.initGL()

    local frag_main = [[
    void main()
    {
        vec3 col = vec3(uv, 0.);
        fragColor = vec4(col, 1.0);
    }
    ]]
    shad = FullscreenShader.new(frag_main)
    shad:initGL()
end

function slide04_scene.exitGL()
    slide:exitGL()
    sTri.exitGL()
    sQuad.exitGL()
    shad:exitGL()
end

function slide04_scene.timestep(absTime, dt)
end

function slide04_scene.keypressed(ch)
    return slide:keypressed(ch)
end

function slide04_scene.render_for_one_eye(view, proj)
    gl.glClearColor(1,1,1,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    local m = {}
    mm.make_identity_matrix(m)
    mm.glh_translate(m, 0.5,-1,0)
    mm.pre_multiply(view, m)
    if slide.shown_lines == 1 then
        sTri.render_for_one_eye(view, proj)
    elseif slide.shown_lines == 2 then
        sQuad.render_for_one_eye(view, proj)
    elseif slide.shown_lines == 3 then
        shad:render(view, proj)
    end

    gl.glDisable(GL.GL_DEPTH_TEST)
    slide:draw_text()
end

return slide04_scene
