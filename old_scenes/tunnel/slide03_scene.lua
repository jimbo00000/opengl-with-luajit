--[[ slide03_scene.lua
]]
slide03_scene = {}

require("util.slideshow")

local slide = Slideshow.new({
    title="Two Methods",
    bullets={
        "1) Pure frag shader",
        "2) Cylinder Geometry",
    }
})

local dataDir = nil

function slide03_scene.setDataDirectory(dir)
    dataDir = dir
end

function slide03_scene.initGL()
    slide:initGL(dataDir)
end

function slide03_scene.exitGL()
    slide:exitGL()
end

function slide03_scene.timestep(absTime, dt)
end

function slide03_scene.keypressed(ch)
    return slide:keypressed(ch)
end

function slide03_scene.render_for_one_eye(view, proj)
    gl.glClearColor(1,1,1,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)
    slide:draw_text()
end

return slide03_scene
