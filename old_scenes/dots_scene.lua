-- dots_scene.lua
dots_scene = {}

local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

-- Types from https://github.com/nanoant/glua/blob/master/init.lua
local glIntv     = ffi.typeof('GLint[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

--local wirecube = require("scene.wirecubescene")
local vbos = {}
local vao = 0
local prog = 0
local pt_dim = 20

local vectorpoints_vert = [[
#version 330

in vec4 vPosition;

uniform mat4 mvmtx;
uniform mat4 prmtx;

void main()
{
    vec3 pos3 = vPosition.xyz;
    vec4 pos4 = vec4(pos3, 1.);
    gl_Position = pos4; // apply matrices in geom shader
}
]]

local vectorpoints_geom = [[
#version 330

layout(points) in;
layout(line_strip, max_vertices = 2) out;

out vec3 gfColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

vec3 vectorField(in vec3 pos)
{
    vec3 wc = pos - vec3(2.);
    float f = 1.;
    vec2 planeCenter = vec2(2.) + 1.*vec2(cos(f*wc.z), sin(f*wc.z));
    vec3 dp = vec3(wc.xy-planeCenter, 0.);

    float mag = 1. - smoothstep(0.,3.,length(dp));
    vec3 pull = -dp;
    pull.z = 1.;
    return mag * pull;

    //return normalize(pos - vec3(2.)); // radial
}

void main() {
    vec4 pos = gl_in[0].gl_Position;
    vec3 off = vectorField(pos.xyz);

    gl_Position = prmtx * mvmtx * pos;
    gfColor = abs(off);
    EmitVertex();

    gl_Position = prmtx * mvmtx * (pos + vec4(.5 * off,0.));
    gfColor = abs(off);
    EmitVertex();

    EndPrimitive();
}
]]

local vectorpoints_frag = [[
#version 330

in vec3 gfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(gfColor, 1.0);
}
]]

local function init_points_attributes()
    local pts = {}
    for k=0,pt_dim-1 do
        local kx = k*pt_dim*pt_dim*3
        for j=0,pt_dim-1 do
            local jx = kx + j*pt_dim*3
            for i=0,pt_dim-1 do
                local ix = jx + i*3
                local sz = 8
                pts[ix] = sz*(i-1)/pt_dim
                pts[ix+1] = sz*(j-1)/pt_dim
                pts[ix+2] = sz*(k-1)/pt_dim
            end
        end
    end
    local verts = glFloatv(3*pt_dim*pt_dim*pt_dim, pts)

    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(vbos, vvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
end

function dots_scene.initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.glBindVertexArray(vao)

    prog = sf.make_shader_from_source({
        vsrc = vectorpoints_vert,
        fsrc = vectorpoints_frag,
        gsrc = vectorpoints_geom,
        })

    init_points_attributes()
    gl.glBindVertexArray(0)
    --wirecube.initGL()
end

function dots_scene.exitGL()
    gl.glBindVertexArray(vao)
    for _,v in pairs(vbos) do
        gl.glDeleteBuffers(1,v)
    end
    vbos = {}
    gl.glDeleteProgram(prog)
    local vaoId = ffi.new("GLuint[1]", vao)
    gl.glDeleteVertexArrays(1, vaoId)
    
    --wirecube.exitGL()
end

local function draw_points()
    gl.glBindVertexArray(vao)
    gl.glDrawArrays(GL.GL_POINTS, 0, 3*pt_dim*pt_dim*pt_dim)
    gl.glBindVertexArray(0)
end

function dots_scene.render_for_one_eye(mview, proj)
    local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(prog, "prmtx")

    local m = {}
    for i=1,16 do m[i] = mview[i] end
    local t = -3
    mm.glh_translate(m, t,t,t)

    gl.glUseProgram(prog)
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, m))
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    draw_points()

    gl.glUseProgram(0)
    --wirecube.render_for_one_eye(m, proj)
end

function dots_scene.timestep(absTime, dt)
    --wirecube.timestep(dt)
end

return dots_scene
