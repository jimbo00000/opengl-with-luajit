-- wizard.lua
--
-- read in and display a point cloud
--
-- began as nbody07.lua
-- modified by Mark Stock (markjstock@gmail.com)

wizard = {}

local openGL = require("opengl")
local ffi = require("ffi")
local sf = require("util.shaderfunctions")
local mm = require("util.matrixmath")

-- Types from:
-- https://github.com/nanoant/glua/blob/master/init.lua
local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glCharv    = ffi.typeof('GLchar[?]')
local glSizeiv   = ffi.typeof('GLsizei[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')
local glConstCharpp = ffi.typeof('const GLchar *[1]')

local vbos = {}
local vao = 0
local prog_display = 0

local pt_vert = [[
#version 430
layout(location = 0) in vec4 vposition;
out float brite;
void main() {
   gl_Position = vec4(vposition.xyz, 1.f);
   brite = vposition.w;
}
]]

local bb_geom = [[
#version 430
layout(location = 0) uniform mat4 View;
layout(location = 1) uniform mat4 Projection;
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;
in float brite[];
out vec2 txcoord;
out float fbrite;
void main() {
   float rad = 0.0025f;
   fbrite = brite[0];
   vec4 pos = View*gl_in[0].gl_Position;

   vec4 ppos = Projection*pos;
   // their apparent radius
   //float fudge = rad * 0.02 * ppos.z;
   // minimum radius
   float fudge = 0.0006 * ppos.z;
   float newrad = max(rad, fudge);
   //brite = brite * rad / newrad;

   txcoord = vec2(-1,-1);
   gl_Position = Projection*(pos+newrad*vec4(txcoord,0,0));
   EmitVertex();
   txcoord = vec2( 1,-1);
   gl_Position = Projection*(pos+newrad*vec4(txcoord,0,0));
   EmitVertex();
   txcoord = vec2(-1, 1);
   gl_Position = Projection*(pos+newrad*vec4(txcoord,0,0));
   EmitVertex();
   txcoord = vec2( 1, 1);
   gl_Position = Projection*(pos+newrad*vec4(txcoord,0,0));
   EmitVertex();
}
]]

local bb_sept = [[
#version 430
layout(location = 0) uniform mat4 View;
layout(location = 1) uniform mat4 Projection;
layout (points) in;
layout (triangle_strip, max_vertices = 7) out;
in float brite[];
out vec2 txcoord;
out float fbrite;
void main() {
   float rad = 0.001;
   fbrite = brite[0];
   vec4 pos = View*gl_in[0].gl_Position;

   vec4 ppos = Projection*pos;
   // their apparent radius
   //float fudge = rad * 0.02 * ppos.z;
   // minimum radius
   float fudge = 0.0006 * ppos.z;
   float newrad = max(rad, fudge);
   //brite = brite * rad / newrad;

   txcoord = vec2(0,-1);
   gl_Position = Projection*(pos+newrad*vec4(txcoord,0,0));
   EmitVertex();
   txcoord = vec2(-0.781831447,-0.623489846);
   gl_Position = Projection*(pos+newrad*vec4(txcoord,0,0));
   EmitVertex();
   txcoord = vec2(0.781831476,-0.62348981);
   gl_Position = Projection*(pos+newrad*vec4(txcoord,0,0));
   EmitVertex();
   txcoord = vec2(-0.974927922,0.222520892);
   gl_Position = Projection*(pos+newrad*vec4(txcoord,0,0));
   EmitVertex();
   txcoord = vec2(0.974927911,0.222520937);
   gl_Position = Projection*(pos+newrad*vec4(txcoord,0,0));
   EmitVertex();
   txcoord = vec2(-0.433883766,0.900968855);
   gl_Position = Projection*(pos+newrad*vec4(txcoord,0,0));
   EmitVertex();
   txcoord = vec2(0.433883724,0.900968875);
   gl_Position = Projection*(pos+newrad*vec4(txcoord,0,0));
   EmitVertex();
}
]]

local rad_frag = [[
#version 330
in vec2 txcoord;
in float fbrite;
layout(location = 0) out vec4 FragColor;
void main() {
   FragColor = vec4(fbrite, fbrite, fbrite, 1.0);
}
]]

local particles = 94484

local function init_point_attributes()
    -- position array: x, y, z, brite
    pos_array = ffi.new("float[?]", particles*4)

    -- initialize particles from text file
    infile = "data/wizard_7m.bin"
    fh,err = io.open(infile, "rb")
    if err then print("Cannot open " .. infile); return; end

    -- read all of the binary data
    pos_array = fh:read("*all")
    assert(fh:close())

    -- set up buffers to contain this data
    local vboIds = ffi.new("int[1]")
    gl.glGenBuffers(1, vboIds)
    local vboP = vboIds[0]

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vboP)
    --gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(pos_array), pos_array, GL.GL_STATIC_DRAW)
    gl.glBufferData(GL.GL_ARRAY_BUFFER, particles*4*4, pos_array, GL.GL_STATIC_DRAW)

    gl.glEnableVertexAttribArray(0)
    gl.glVertexAttribPointer(0, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil);

    -- and get these ready for the compute shaders
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vboP)

    table.insert(vbos, vboP)
end

function wizard.initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.glBindVertexArray(vao)

    prog_display = sf.make_shader_from_source({
        vsrc = pt_vert,
        gsrc = bb_sept,
        fsrc = rad_frag,
        })

    init_point_attributes()
    gl.glBindVertexArray(0)
end

function wizard.exitGL()
    gl.glBindVertexArray(vao)
    for _,v in pairs(vbos) do
        local vboId = ffi.new("GLuint[1]", v)
        gl.glDeleteBuffers(1,vboId)
    end
    vbos = {}
    gl.glDeleteProgram(prog_display)
    local vaoId = ffi.new("GLuint[2]", vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

function wizard.render_for_one_eye(mview, proj)
    gl.glUseProgram(prog_display)

    gl.glUniformMatrix4fv(0, 1, GL.GL_FALSE, glFloatv(16, mview))
    gl.glUniformMatrix4fv(1, 1, GL.GL_FALSE, glFloatv(16, proj))

    -- this is the stuff that I just don't understand
    gl.glEnable(GL.GL_DEPTH_TEST)
    gl.glDisable(GL.GL_BLEND)
    --gl.glDisable(GL.GL_DEPTH_TEST)
    --gl.glEnable(GL.GL_BLEND)
    --gl.glBlendFunc(GL.GL_ONE, GL.GL_ONE)
    -- like this one
    --gl.glBlendFunc(GL.GL_ONE, GL.GL_ONE_MINUS_SRC_COLOR)
    --gl.glBlendFunc(GL.GL_SRC_COLOR, GL.GL_ONE_MINUS_SRC_COLOR)
    -- even better (will occlude)
    --gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)

    gl.glBindVertexArray(vao)
    gl.glDrawArrays(GL.GL_POINTS, 0, particles)
    gl.glBindVertexArray(0)
    gl.glEnable(GL.GL_DEPTH_TEST)
    gl.glDisable(GL.GL_BLEND)

    gl.glUseProgram(0)
end

function wizard.timestep(absTime, dt)
end

return wizard
