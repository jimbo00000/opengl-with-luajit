--[[ postprocessingslides_scene.lua

    A slideshow containing multiple slides in a single scene. 
]]

postprocessingslides_scene = {}

require("util.slideshow")
local mm = require("util.matrixmath")

local thumbnail = require("scene.filter_thumbnail")
local highlightQuad = require("util.fullscreen_shader")
local vsfsquad = require("scene.vsfsquad")
require("util.fullscreen_shader")
local shad

local slides = {
    Slideshow.new({
        title="Post Processing in GLSL",
        bullets={
        "",
        "",
        "    Jim Susinno",
        "    Khronos Boston Chapter",
        },
        shown_lines = 4,
    }),

    Slideshow.new({
        title="Motivation",
        bullets={
            "Make things look cool",
        },
    }),

    Slideshow.new({
        title="Without Post-Processing",
        bullets={
            "- Render to framebuffer",
            "  (aka the screen, or window)",
        },
    }),

    Slideshow.new({
        title="With Post-Processing",
        bullets={
            "- Render to texture",
            "- Render texture to framebuffer",
            "   (with processing applied)",
        },
    }),

    Slideshow.new({
        title="Rendering Overview",
        bullets={
            "- Bind FBO (render target)",
            "- Draw as usual",
            "- Unbind (bind FBO 0)",
            "- Use postprocess program",
            "- Bind RT as texture and render quad",
        },
    }),

    Slideshow.new({
        title="Initializing FBOs",
        bullets={
            "- GenFramebuffer; Bind",
            "- GenTexture for depth; attach",
            "- GenTexture for color; attach",
            "- CheckFramebufferStatus"
        },
    }),

    Slideshow.new({
        title="Presenting FBOs to screen",
        bullets={
            "Just as in tunnel demo:",
            "- Start with vsfsquad",
            "- Ignore view matrix",
            "- Draw over NDC [-1,1] in xy"
        },
        background = "fullshad",
    }),

    Slideshow.new({
        title='The trivial "filter"',
        bullets={
            "- Simply passes texture data through",
            "- Standard input, output variables",
        },
        codesnippet = {
            "uniform sampler2D tex;",
            "in vec2 uv;",
            "out vec4 fragColor;",
            "void main()",
            "{",
            "    fragColor = texture(tex, uv);",
            "}",
        },
        background = "highlight1",
    }),

    Slideshow.new({
        title="Programmable Shading",
        bullets={
            "- Don't have to pass along intact pixels",
            "- A processing opportunity...",
            "- Draw whatever you like!"
        },
    }),

    Slideshow.new({
        title="Color Inversion",
        bullets={
            "- Simple calculation",
            "- Shows the inverse of each pixel",
        },
        codesnippet = {
            "void main()",
            "{",
            "    fragColor = vec4(1.) - texture(tex, uv);",
            "}",
        },
        background = "invert",
    }),

    Slideshow.new({
        title="Luminance",
        bullets={
            "- Simple calculation",
            "- Shows only intensity of each pixel",
        },
        codesnippet = {
            "void main()",
            "{",
            "    fragColor = vec4(.3*length(texture(tex, uv)));",
            "}",
        },
        background = "black&white",
    }),

    Slideshow.new({
        title="Animation",
        bullets={
            "- Add a time variable",
            "- Displace read location",
            "- Notice texture repeat",
        },
        codesnippet = {
            "", -- Hack the code snippet downward
            "",
            "uniform float time;",
            "void main()",
            "{",
            "    vec2 tc = uv + .1*vec2(sin(time), cos(.7*time));",
            "    fragColor = texture(tex, tc);",
            "}",
        },
        background = "wiggle",
    }),

    Slideshow.new({
        title="Single Tap Filters",
        bullets={
            "- Perform one texture read per fragment",
            "- Can do more if you like - ",
            "    GLSL is Turing-complete",
            "- What does that cost for performance?",
        },
    }),

    Slideshow.new({
        title="Tiled Memory Formats",
        bullets={
            "- Textures are stored on GPU RAM",
            "- RAM is equipped with texture cache",
            "- Layout optimized for usage",
            "   - Space filling curve: Z-order/Morton",
            "- Load one texel, cache its neighbors",
        },
    }),

    Slideshow.new({
        title="Convolution",
        bullets={
            "9 weighted taps",
            "Yields several effects:",
            " - Blur, Sharpness",
            " - Edge Detection",
            " - Emboss",
        },
        background = "edgedetect",
    }),

    Slideshow.new({
        title="Buffer Resolution",
        bullets={
            "- texture() takes a vec2 (floats)",
            "- Where is the next pixel?",
            "- Need to pass in w,h parameters"
        },
        codesnippet = {
            "",
            "",
            "uniform int ResolutionX;",
            "uniform int ResolutionY;",
            "float step_x = 1./float(ResolutionX);",
            "float step_y = 1./float(ResolutionY);",
        },
    }),

    Slideshow.new({
        title="Separable Convolution",
        bullets={
            "- Larger kernels require wider access",
            "- Taps grow ~ n^2",
            "+ Blur only a horizontal line",
            "+ Swap horizontal & vertical",
            "+ Blur vertical line in next pass",
        },
    }),

    Slideshow.new({
        title="Filter Types",
        bullets={
            "- Lens Distortion",
            "- Lens Flare",
            "- Chromatic Aberration",
            "- Film Grain",
        },
    }),

    Slideshow.new({
        title="Filter Types...",
        bullets={
            "- Color Grading",
            "- Tone Mapping, HDR",
            "- Bloom, Glow",
            "- Gamma Correction",
        },
    }),

    Slideshow.new({
        title="Filter Types......",
        bullets={
            "Focus Effects",
            "- Radial Blur",
            "- Motion Blur",
            "- Depth of Field",
            "- Fog",
        },
    }),

    Slideshow.new({
        title="Filter Types.........",
        bullets={
            "Coloring Effects",
            "- Cel Shading",
            "- Sketch/Ink/Paint",
            "- Posterize",
            "- Sepia, Nightvision",
        },
    }),

    Slideshow.new({
        title="Filter Types............",
        bullets={
            "- Vignette",
            "- Scanlines",
            "- Dithering",
            "- Borders, Rotation",
            "- Invent your own!",
        },
    }),

    Slideshow.new({
        title="Filter Chaining",
        bullets={
            "- Output of one filter into another",
            "- As many times as hardware allows",
            "- Just like pipes on the command line"
        },
    }),

    Slideshow.new({
        title="Filter DAGs",
        bullets={
            "- Input multiple filters into another",
            "- Tree coalesces into one final output",
            "- Multiple render targets may help here",
            "- Node-based programming tools",
        },
    }),

    Slideshow.new({
        title="Processing Cost",
        bullets={
            "- Reducing resolution helps tremendously",
            "- 1/2 res in each dimension:",
            "     ==> 4x fewer pixels",
            "- 4x more filters at frame rate",
        },
    }),

    Slideshow.new({
        title="For Maximum Speed",
        bullets={
            "GPU has an internal work scheduler",
            "- How can we harness its full potential?",
            "- Mobile(tiled) vs. Desktop",
            "- Texture formats",
            "- Horizontal vs. vertical taps",
        },
    }),

    Slideshow.new({
        title="For Maximum Speed...",
        bullets={
            "- Surface synchronization",
            "- Dependent reads",
            "- Unordered Access Views",
            "- Compute Shaders",
            "- Ask a driver developer!",
        },
    }),

    Slideshow.new({
        title="Questions?",
        bullets={
        },
    }),

}

local highlight_fragsrc = [[
void main()
{
    if (uv.x < .1) discard;
    if (uv.y < .26) discard;
    if (uv.x > .5) discard;
    if (uv.y > .4) discard;
    fragColor = vec4(1.,.5,.5,1.);
}
]]

local slide_idx = 1
local slide = slides[1]

local dataDir = nil

-- Since data files must be loaded from disk, we have to know
-- where to find them. Set the directory with this standard entry point.
function postprocessingslides_scene.setDataDirectory(dir)
    dataDir = dir
    thumbnail.setDataDirectory(dir)
end

function postprocessingslides_scene.initGL()
    slide:initGL(dataDir)

    thumbnail.initGL()
    highlightQuad = FullscreenShader.new(highlight_fragsrc)
    highlightQuad:initGL()

    local frag_main = [[
    void main()
    {
        vec3 col = vec3(uv, 0.);
        fragColor = vec4(col, 1.0);
    }
    ]]
    shad = FullscreenShader.new(frag_main)
    shad:initGL()

    vsfsquad.initGL()

    local dir = "fonts"
    local fontname = "courier_512"
    if dataDir then dir = dataDir .. "/" .. dir end
    codefont = GLFont.new(fontname..'.fnt', fontname..'_0.raw')
    codefont:setDataDirectory(dir)
    codefont:initGL()
end

function postprocessingslides_scene.exitGL()
    slide:exitGL()
    thumbnail.exitGL()
    highlightQuad:exitGL()
    shad:exitGL()
    vsfsquad.exitGL()

    codefont:exitGL()
end

-- Some slides may contain some live graphics in addition to text.
-- Nothing but special cases in this function.
-- TODO: move these into their respective slide table
function postprocessingslides_scene.draw_ancillary_scenes(view, proj)
    if slide.background == "highlight1" then
        -- A highlight over a code snippet
        -- Rectangle is defined in the pure frag shader source.
        if slide.shown_lines > 1 then
            gl.glDepthMask(GL.GL_FALSE)
            highlightQuad:render(view, proj, nil)
            gl.glDepthMask(GL.GL_TRUE)
        end
    elseif slide.background == "invert" then
        -- An example of an invert shader
        if slide.shown_lines > 1 then
            thumbnail.render_for_one_eye(view, proj)
        end
    elseif slide.background == "black&white" then
        thumbnail.render_for_one_eye(view, proj)
    elseif slide.background == "wiggle" then
        thumbnail.render_for_one_eye(view, proj)
    elseif slide.background == "edgedetect" then
        thumbnail.render_for_one_eye(view, proj)
    elseif slide.background == "fullshad" then
        if slide.shown_lines == 2 then
            gl.glDepthMask(GL.GL_FALSE)
            vsfsquad.render_for_one_eye(view, proj)
            gl.glDepthMask(GL.GL_TRUE)
        elseif slide.shown_lines > 2 then
            gl.glDepthMask(GL.GL_FALSE)
            shad:render(view, proj)
            gl.glDepthMask(GL.GL_TRUE)
        end
    end
end

-- Draw slide number in the lower left of the window
function postprocessingslides_scene.draw_slide_number()
    local w,h = 2160/2,1440/2 -- guess a resolution
    local m = {}
    mm.make_identity_matrix(m)
    mm.glh_translate(m, 10,h-30,0)
    local s = .2
    mm.glh_scale(m,s,s,s)
    local p = {}
    mm.make_identity_matrix(p)
    mm.glh_ortho(p, 0, w, h, 0, -1, 1)
    local txt = tostring(slide_idx).."/"..tostring(#slides)
    slide.glfont:render_string(m, p, {.8,.8,.8}, txt)
end

function postprocessingslides_scene.draw_code_snippet(lines)
    if not codefont then return end
    if not lines then return end

    local m = {}
    mm.make_identity_matrix(m)
    mm.glh_translate(m, 120, 440, 0)
    local s = .3
    mm.glh_scale(m,s,s,s)
    local p = {}
    local w,h = 2160/2,1440/2 -- guess a resolution
    mm.glh_ortho(p, 0, w, h, 0, -1, 1)
    local col = {0,0,0}
    for _,line in pairs(lines) do
        codefont:render_string(m, p, col, line)
        mm.glh_translate(m, 0, 100, 0)
    end
end

function postprocessingslides_scene.render_for_one_eye(view, proj)
    gl.glClearColor(1,1,1,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    postprocessingslides_scene.draw_ancillary_scenes(view, proj)
    slide:draw_text()

    local lines = slide.codesnippet
    postprocessingslides_scene.draw_code_snippet(lines)

    postprocessingslides_scene.draw_slide_number()
end

function postprocessingslides_scene.timestep(absTime, dt)
    thumbnail.timestep(absTime, dt)
end

function postprocessingslides_scene.increment_scene(incr)
    slide_idx = slide_idx + incr

    if slide_idx < 1 then slide_idx = 1 end
    if slide_idx > #slides then slide_idx = #slides end

    slide:exitGL()
    slide = slides[slide_idx]
    slide:initGL(dataDir)

    -- Set up the right filters
    if slide.background == "invert" then
        thumbnail.set_filters({"invert", "passthrough"})
    elseif slide.background == "black&white" then
        thumbnail.set_filters({"black&white", "passthrough"})
    elseif slide.background == "wiggle" then
        thumbnail.set_filters({"wiggle", "passthrough"})
    elseif slide.background == "edgedetect" then
        thumbnail.set_filters({"convolve", "passthrough"})
    end
end

function postprocessingslides_scene.keypressed(ch)
    if ch == 257 --[[glfw.GLFW.KEY_ENTER]] then
        postprocessingslides_scene.increment_scene(1)
        return true
    end

    if ch == 259 --[[glfw.GLFW.KEY_BACKSPACE]] then
        postprocessingslides_scene.increment_scene(-1)
        return true
    end

    if ch == 262 then -- right arrow in glfw
        if slide.shown_lines >= #slide.bullet_points then
            postprocessingslides_scene.increment_scene(1)
            return true
        end
    end

    if ch == 263 then -- left arrow in glfw
        if slide.shown_lines <= 0 then
            postprocessingslides_scene.increment_scene(-1)
            return true
        end
    end

    return slide:keypressed(ch)
end

local action_types = {
  [0] = "Down",
  [1] = "Up",
  [2] = "Move",
  [3] = "Cancel",
  [4] = "Outside",
  [5] = "PointerDown",
  [6] = "PointerUp",
}

function postprocessingslides_scene.onSingleTouch(pointerid, action, x, y)
    local actionflag = action % 255
    local a = action_types[actionflag]
    if a == "Down" or a == "PointerDown" then
        postprocessingslides_scene.keypressed(262)
    end
end

return postprocessingslides_scene
