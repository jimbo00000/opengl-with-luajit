-- sdf_mesh_scene.lua
sdf_mesh_scene = {}

local Grid = require("scene.gridcube_scene")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")
--local origin = require("scene.origin")

local progs = {}

--[[
    Perturb all vertices with a function.
]]
local perturbverts_comp_src = [[
#version 430

layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };

vec3 displacePoint(vec3 p)
{
    vec3 center = vec3(.5);
    float radius = 1.;
    return center + radius*normalize(p-center);
}

void main()
{
    uint index = gl_GlobalInvocationID.x;
    vec4 p = positions[index];

    p.xyz = displacePoint(p.xyz);

    positions[index] = p;
}
]]

local function perturbVertexPositions()
    local vvbo = Grid.get_vertices_vbo()
    local num_verts = Grid.get_num_verts()
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vvbo[0])
    local prog = progs.perturbverts
    gl.glUseProgram(prog)

    gl.glDispatchCompute(num_verts/256+1, 1, 1)
    gl.glUseProgram(0)
end




local libiq_sdf_src = [[
//
// Distance field functions
//


float sdSphere( vec3 p, float s )
{
    return length(p)-s;
}

float udRoundBox( vec3 p, vec3 b, float r )
{
  return length(max(abs(p)-b,0.0))-r;
}

#if 0
vec3 calcNormal( in vec3 pos )
{
    vec3 eps = vec3( 0.001, 0.0, 0.0 );
    vec3 nor = vec3(
        map(pos+eps.xyy).x - map(pos-eps.xyy).x,
        map(pos+eps.yxy).x - map(pos-eps.yxy).x,
        map(pos+eps.yyx).x - map(pos-eps.yyx).x );
    return normalize(nor);
}
#endif
]]

local snakehead_src = [[

#line 82
#define U(a,b) (a.x*b.y-b.x*a.y)

vec2 P = vec2(1,.72), O = vec2(-1.16,.63);

vec2 A[15];
vec2 T1[5];
vec2 T2[5];

vec3 L = normalize(vec3(1,.72, 1)), Y = vec3(0,1,0), E = Y*.01;

float tMorph;
mat2 mat2Rot;

// Distance to Bezier
// inspired by [iq:https://www.shadertoy.com/view/ldj3Wh]
// calculate distance to 2D bezier curve on xy but without forgeting the z component of p
// total distance is corrected using pytagore just before return
vec2 B(vec2 m, vec2 n, vec2 o, vec3 p) {
    vec2 q = p.xy;
    m-= q; n-= q; o-= q;
    float x = U(m, o), y = 2. * U(n, m), z = 2. * U(o, n);
    vec2 i = o - m, j = o - n, k = n - m, 
         s = 2. * (x * i + y * j + z * k), 
         r = m + (y * z - x * x) * vec2(s.y, -s.x) / dot(s, s);
    float t = clamp((U(r, i) + 2. * U(k, r)) / (x + x + y + z), 0.,1.); // parametric position on curve
    r = m + t * (k + k + t * (j - k)); // distance on 2D xy space
    return vec2(sqrt(dot(r, r) + p.z * p.z), t); // distance on 3D space
}

            

float smin(float a, float b, float k){
    float h = clamp(.5+.5*(b-a)/k, 0., 1.);
    return mix(b,a,h)-k*h*(1.-h);
}

// Distance to scene
float M(vec3 p) {

// Distance to Teapot --------------------------------------------------- 
    // precalcul first part of teapot spout
    vec2 h = B(T1[2],T1[3],T1[4], p);
    float a = 99., 
    // distance to teapot handle (-.06 => make the thickness) 
        b = min(min(B(T2[0],T2[1],T2[2], p).x, B(T2[2],T2[3],T2[4], p).x) - .06, 
    // max p.y-.9 => cut the end of the spout 
                max(p.y - .9,
    // distance to second part of teapot spout (abs(dist,r1)-dr) => enable to make the spout hole 
                    min(abs(B(T1[0],T1[1],T1[2], p).x - .07) - .01, 
    // distance to first part of teapot spout (tickness incrase with pos on curve) 
                        h.x * (1. - .75 * h.y) - .08)));
    
    // distance to teapot body => use rotation symetry to simplify calculation to a distance to 2D bezier curve
    vec3 qq= vec3(sqrt(dot(p,p)-p.y*p.y), p.y, 0);
    // the substraction of .015 enable to generate a small thickness arround bezier to help convergance
    // the .8 factor help convergance  
    for(int i=0;i<13;i+=2) 
        a = min(a, (B(A[i], A[i + 1], A[i + 2], qq).x - .015) * .7); 
    // smooth minimum to improve quality at junction of handle and spout to the body
    float dTeapot = smin(a,b,.02);
    
    return dTeapot;
}

// Try raymarching towards origin
vec3 raymarchPoint(vec3 p)
{
    vec3 ro = p;
    vec3 rd = -p;
    vec3 step = strength * rd;
    for (int i=0; i<10; ++i)
    {
        float d = M(2.*ro - vec3(1.5));
        ro += step * d;
    }
    return ro;
}

vec3 displacePoint(vec3 p)
{
    // https://www.shadertoy.com/view/MslSDN

    // Teapot body profil (8 quadratic curves) 
    A[0]=vec2(0,0);A[1]=vec2(.64,0);A[2]=vec2(.64,.03);A[3]=vec2(.8,.12);A[4]=vec2(.8,.3);A[5]=vec2(.8,.48);A[6]=vec2(.64,.9);A[7]=vec2(.6,.93);
    A[8]=vec2(.56,.9);A[9]=vec2(.56,.96);A[10]=vec2(.12,1.02);A[11]=vec2(0,1.05);A[12]=vec2(.16,1.14);A[13]=vec2(.2,1.2);A[14]=vec2(0,1.2);
    // Teapot spout (2 quadratic curves)
    T1[0]=vec2(1.16, .96);T1[1]=vec2(1.04, .9);T1[2]=vec2(1,.72);T1[3]=vec2(.92, .48);T1[4]=vec2(.72, .42);
    // Teapot handle (2 quadratic curves)
    T2[0]=vec2(-.6, .78);T2[1]=vec2(-1.16, .84);T2[2]=vec2(-1.16,.63);T2[3]=vec2(-1.2, .42);;T2[4]=vec2(-.72, .24);


    return raymarchPoint(p);
}
]]


-- Modify each of the mesh's vertices in the buffer by a function.
-- Modeling with vector fields.
local comp_src = [[
#version 430

layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
uniform float strength;
uniform int uNumVerts;
vec3 displacePoint(vec3 p);

void main()
{
    int index = int(gl_GlobalInvocationID);
    if (index >= uNumVerts)
        return;
    vec4 p = positions[index];
    p.xyz = displacePoint(p.xyz);
    positions[index] = p;
}
]]

function sdf_mesh_scene.runComputeShader(strength)
    --Displaces the vertices in cubes' VBO by a vector field with scalar magnitude.
    local vvbo = Grid.get_vertices_vbo()
    local num_verts = Grid.get_num_verts()

    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vvbo[0])
    local prog = progs.mold_mesh
    gl.glUseProgram(prog)
    local ust_loc = gl.glGetUniformLocation(prog, "strength")
    if ust_loc > -1 then
        gl.glUniform1f(ust_loc, strength)
    end

    local unv_loc = gl.glGetUniformLocation(prog, "uNumVerts")
    gl.glUniform1i(unv_loc, num_verts)

    gl.glDispatchCompute(num_verts/256+1, 1, 1)
    gl.glUseProgram(0)
end



function sdf_mesh_scene.initGL()
    progs.perturbverts= sf.make_shader_from_source({
        compsrc = perturbverts_comp_src,
        })

    progs.mold_mesh= sf.make_shader_from_source({
        compsrc = comp_src..libiq_sdf_src..snakehead_src,
        })

    Grid.initGL()
    perturbVertexPositions()
    Grid.recalc_normals()
    --origin.initGL()
end

function sdf_mesh_scene.exitGL()
    Grid.exitGL()
    for _,v in pairs(progs) do
        gl.glDeleteProgram(v)
    end
    --origin.exitGL()
end

function sdf_mesh_scene.render_for_one_eye(view, proj)
    local m = {}
    mm.make_identity_matrix(m)
    local s = 1
    mm.glh_scale(m, s,s,s)
    --mm.glh_translate(m, 5,0,20)
    mm.pre_multiply(m, view)

    Grid.render_for_one_eye(m, proj)
    --origin.render_for_one_eye(m, proj)
end

function sdf_mesh_scene.timestep(absTime, dt)
    Grid.timestep(absTime, dt)
end

function sdf_mesh_scene.keypressed(ch)
    sdf_mesh_scene.runComputeShader(.1)
    gl.glMemoryBarrier(GL.GL_SHADER_STORAGE_BARRIER_BIT)
    Grid.recalc_normals()
    gl.glMemoryBarrier(GL.GL_SHADER_STORAGE_BARRIER_BIT)
end

return sdf_mesh_scene
