-- rottest.lua
rottest = {}

local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

local vbos = {}
local vao = 0
local prog = 0

local basic_vert = [[
#version 330

in vec4 vPosition;
in vec4 vColor;

out vec3 vfColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

void main()
{
    vfColor = vColor.xyz;
    gl_Position = prmtx * mvmtx * vPosition;
}
]]

local basic_frag = [[
#version 330

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfColor, 1.0);
}
]]


local function init_cube_attributes()
    local verts = glFloatv(3*3, {
        0,0,0,
        1,0,0,
        1,1,0,
        })

    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(prog, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)

    local quads = glUintv(6*6, {
        0,1,2,
    })
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(quads), quads, GL.GL_STATIC_DRAW)
    table.insert(vbos, qvbo)
end

function rottest.initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.glBindVertexArray(vao)

    prog = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    init_cube_attributes()
    gl.glBindVertexArray(0)
end

function rottest.exitGL()
    gl.glBindVertexArray(vao)
    for _,v in pairs(vbos) do
        gl.glDeleteBuffers(1,v)
    end
    vbos = {}
    gl.glDeleteProgram(prog)
    local vaoId = ffi.new("GLuint[1]", vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

local qtest = {1,0,0,0}

function rottest.render_for_one_eye(view, proj)
    local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(prog, "prmtx")
    gl.glUseProgram(prog)
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    -- concatenate quat rotation
    local m = {}
    for i=1,16 do m[i] = view[i] end
    local mq = mm.quat_to_matrix(qtest)
    mm.post_multiply(m, mq)

    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, m))
    gl.glBindVertexArray(vao)
    gl.glDrawElements(GL.GL_TRIANGLES, 3, GL.GL_UNSIGNED_INT, nil)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end

function rottest.timestep(absTime, dt)
end

function make_quat(theta, vec)
    local q = {
        math.cos(.5 * theta),
        vec[1] * math.sin(.5 * theta),
        vec[2] * math.sin(.5 * theta),
        vec[3] * math.sin(.5 * theta),
    }
    return q
end

local xr = 0
local yr = 0
local zr = 0

function rottest.keypressed(ch)
    if ch == 'x' then
        xr = xr + .1
        qtest = make_quat(xr, {1,0,0})
    end
    if ch == 'y' then
        yr = yr + .1
        qtest = make_quat(yr, {0,1,0})
    end
    if ch == 'z' then
        zr = zr + .1
        qtest = make_quat(zr, {0,0,1})
    end

    if ch == 'i' then
        xr = xr + .1
        local rm = {}
        mm.make_rotation_matrix(rm, xr, 1,0,0)
        qtest = mm.matrix_to_quat(rm)
        mm.quat_normalize(qtest)
    end
    if ch == 'o' then
        yr = yr + .1
        local rm = {}
        mm.make_rotation_matrix(rm, yr, 0,1,0)
        qtest = mm.matrix_to_quat(rm)
        mm.quat_normalize(qtest)
    end
    if ch == 'p' then
        zr = zr + .1
        local rm = {}
        mm.make_rotation_matrix(rm, zr, 0,0,1)
        qtest = mm.matrix_to_quat(rm)
        mm.quat_normalize(qtest)
    end
end

return rottest
