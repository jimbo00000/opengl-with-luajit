--[[ filter_thumbnail.lua

    Designed to fit in the whitespace of a presentation slide.
]]
filter_thumbnail = {}

--local openGL = require("opengl")
local ffi = require("ffi")
local sf = require("util.shaderfunctions")
local mm = require("util.matrixmath")

local subject = require("scene.hybrid_scene") -- any scene here
-- The copy is a hack to allow two copies of filter_chain
-- TODO: rewrite to use prototype "objects"
local PostFX = require("effect.effect_chain_copy")

local glIntv = ffi.typeof('GLint[?]')
local glUintv = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

-- Module-internal state: hold a list of VBOs for deletion on exitGL
local vbos = {}
local vao = 0
local prog = 0
local dataDir

local fw,fh=512,512

local basic_vert = [[
#version 310 es

in vec4 vPosition;
in vec4 vColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

out vec3 vfColor;

void main()
{
    vfColor = vColor.xyz;
    gl_Position = prmtx * mvmtx * vPosition;
}
]]

local basic_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

uniform sampler2D sTex;

void main()
{
    vec4 tc = texture(sTex, vfColor.xy);
    fragColor = vec4(tc.xyz, 1.);
}
]]

local function init_quad_attributes()
    local verts = glFloatv(4*2, {
        .3,.3,
        .9,.3,
        .9,.9,
        .3,.9,
        })

    local cols = glFloatv(4*2, {
        0,0,
        1,0,
        1,1,
        0,1,
        })

    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(prog, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(cols), cols, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)
end

function filter_thumbnail.setDataDirectory(dir)
    dataDir = dir
    if subject.setDataDirectory then subject.setDataDirectory(dir) end
end

function filter_thumbnail.initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.glBindVertexArray(vao)

    prog = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    init_quad_attributes()
    gl.glBindVertexArray(0)

    subject.initGL()
    if PostFX then PostFX.initGL(fw,fh) end
end

function filter_thumbnail.exitGL()
    gl.glBindVertexArray(vao)
    for _,v in pairs(vbos) do
        gl.glDeleteBuffers(1,v)
    end
    vbos = {}
    gl.glDeleteProgram(prog)
    local vaoId = ffi.new("GLuint[1]", vao)
    gl.glDeleteVertexArrays(1, vaoId)

    subject.exitGL()
    if PostFX then PostFX.exitGL() end
end

-- This is the "scene within a scene" that will appear in the FBO
function render_scene(view, proj)
    gl.glClearColor(0.3,0.3,0.3,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)
    subject.render_for_one_eye(view, proj)
end

-- Render to the internal buffer owned by this scene
-- View is determined by the scene's "internal camera" and
-- is set by the caller, render_for_one_eye.
function render_pre_pass(view, proj)
    -- Save viewport dimensions
    local vp = ffi.new("GLuint[4]", 0,0,0,0)
    gl.glGetIntegerv(GL.GL_VIEWPORT, vp)
    -- Save bound FBO
    local boundfbo = ffi.new("int[1]")
    gl.glGetIntegerv(GL.GL_FRAMEBUFFER_BINDING, boundfbo)

    -- Draw quad scene to this scene's first internal fbo
    if PostFX then PostFX.bind_fbo() end
    gl.glEnable(GL.GL_DEPTH_TEST)
    render_scene(view, proj)
    if PostFX then PostFX.unbind_fbo() end
    gl.glEnable(GL.GL_DEPTH_TEST)

    -- Restore viewport
    gl.glViewport(vp[0],vp[1],vp[2],vp[3])
    -- Restore FBO binding
    if boundfbo[0] ~= 0 then
        gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, boundfbo[0])
    end
end

function filter_thumbnail.render_for_one_eye(view, proj)
    -- Fixed camera view for the scene within a scene
    local cam = {}
    mm.make_identity_matrix(cam)
    mm.glh_translate(cam, 0,0,-1)
    render_pre_pass(cam, proj)

    local m={}
    local p={}
    mm.make_identity_matrix(m)
    mm.make_identity_matrix(p)
    gl.glUseProgram(prog)
    local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(prog, "prmtx")
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, m))
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, p))

    if PostFX.get_filters then
        local filts = PostFX.get_filters()
        local tex = filts[#filts].fbo.tex
        gl.glActiveTexture(GL.GL_TEXTURE0)
        gl.glBindTexture(GL.GL_TEXTURE_2D, tex)
        local tx_loc = gl.glGetUniformLocation(prog, "tex")
        gl.glUniform1i(tx_loc, 0)
    end

    gl.glBindVertexArray(vao)
    gl.glDrawArrays(GL.GL_TRIANGLE_FAN, 0, 4)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end

function filter_thumbnail.timestep(absTime, dt)
    subject.timestep(absTime, dt)
    if PostFX and PostFX.timestep then PostFX.timestep(absTime, dt) end
end

function filter_thumbnail.onSingleTouch(pointerid, action, x, y)
    --print("filter_thumbnail.onSingleTouch",pointerid, action, x, y)
end

function filter_thumbnail.set_filters(names)
    for _,v in pairs(names) do
        PostFX.remove_all_effects(index)
        PostFX.insert_effect_by_name(v)
    end
end

return filter_thumbnail
