-- grid_scene.lua

scene = {}

local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv     = ffi.typeof('GLint[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

local vao = 0
local vbos = {}
local progs = {}
local num_verts = 0
local num_tri_idxs = 0

local subdivs = 256
local global_time = 0

--[[
    Display the grid with basic lighting.
]]
local basic_vert = [[
#version 330

in vec4 vPosition;
in vec4 vNormal;

out vec3 vfPos;
out vec3 vfNormal;

uniform mat4 mvmtx;
uniform mat4 prmtx;

void main()
{
    vfPos = (mvmtx * vPosition).xyz;
    vfNormal = normalize(mat3(mvmtx) * vNormal.xyz);
    gl_Position = prmtx * mvmtx * vPosition;
}
]]

local basic_frag = [[
#version 330

in vec3 vfPos;
in vec3 vfNormal;
out vec4 fragColor;

vec3 lightPos = vec3(0., 2., 2.);
float shininess = 125.;
void main()
{
    //fragColor = vec4(normalize(abs(vfNormal)), 1.0);

    vec3 N = normalize(vfNormal);
    vec3 L = normalize(lightPos - vfPos); // direction *to* light
    vec3 E = -vfPos;

    // One-sided lighting
    vec3 spec = vec3(0.);
    float bright = max(dot(N,L), 0.);

    if (bright > 0.)
    {
        // Specular lighting
        vec3 H = normalize(L + E);
        float specBr = max(dot(H,N), 0.);
        spec = vec3(1.) * pow(specBr, shininess);
    }

    //fragColor = vec4(egNormal, 0.);
    vec3 basecol = vec3(.4);
    fragColor = vec4(abs(basecol) * bright + spec, 1.);
}
]]



--[[
    Set up initial vertex values - a subdivided square grid.
]]
local initverts_comp_src = [[
#version 430

#define THREADS_PER_BLOCK 256
layout(local_size_x=256) in;
layout(std430, binding=0) buffer nblock { vec4 positions[]; };
uniform int uFacets;

void main()
{
    uint index = gl_GlobalInvocationID.x;
    uint f_1 = uFacets + 1;
    uvec2 gridIdx = uvec2(index % f_1, index / f_1);
    vec4 v = vec4(
        float(gridIdx.x)/float(f_1),
        float(gridIdx.y)/float(f_1),
        0.,
        1.);
    positions[index] = v;
}
]]

local function initVertexPositions()
    local vvbo = vbos.vertices
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vvbo[0])
    local prog = progs.initverts
    gl.glUseProgram(prog)

    local uf_loc = gl.glGetUniformLocation(prog, "uFacets")
    gl.glUniform1i(uf_loc, subdivs)

    gl.glDispatchCompute(num_verts/256+1, 1, 1)
    gl.glUseProgram(0)
end

--[[
    Set up initial face values - a subdivided square grid.
]]
local initfaces_comp_src = [[
#version 430
#line 115
#define THREADS_PER_BLOCK 256
layout(local_size_x=256) in;

layout(std430, binding=0) buffer iblock { uint indices[]; };

uniform int uFacets;

uint getFaceFromXY(uint i, uint j)
{
    return (uFacets+1) * j + i;
}

void main()
{
    uint index = gl_GlobalInvocationID.x;
    uint gridIdx = index / 6;
    uint i = gridIdx % uFacets;
    uint j = gridIdx / uFacets;

    uint triIdx = index % 6;
    uint tris[] = {
        getFaceFromXY(i,j),
        getFaceFromXY(i+1,j),
        getFaceFromXY(i,j+1),
        getFaceFromXY(i,j+1),
        getFaceFromXY(i+1,j),
        getFaceFromXY(i+1,j+1),
    };

    indices[index] = tris[triIdx];
}
]]

local function setFaceIndices()
    local ivbo = vbos.elements
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, ivbo[0])
    local prog = progs.initfaces
    gl.glUseProgram(prog)

    local uf_loc = gl.glGetUniformLocation(prog, "uFacets")
    gl.glUniform1i(uf_loc, subdivs)

    gl.glDispatchCompute(num_tri_idxs/256+1, 1, 1)
    gl.glUseProgram(0)
    gl.glMemoryBarrier(GL.GL_SHADER_STORAGE_BARRIER_BIT)
end

--[[
    Perturb all vertices with an animated function.
]]
local perturbverts_comp_src = [[
#version 430

#define THREADS_PER_BLOCK 256
layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
uniform float uGlobalTime;

void main()
{
    uint index = gl_GlobalInvocationID.x;
    vec4 p = positions[index];
    float t = uGlobalTime * .1;
    p.z = .05 * (
        sin(20.*(t + p.x)) +
        sin(20.*(t + p.y))
        );
    positions[index] = p;
}
]]

local function perturbVertexPositions()
    local vvbo = vbos.vertices
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vvbo[0])
    local prog = progs.perturbverts
    gl.glUseProgram(prog)

    local ut_loc = gl.glGetUniformLocation(prog, "uGlobalTime")
    gl.glUniform1f(ut_loc, global_time)

    gl.glDispatchCompute(num_verts/256+1, 1, 1)
    gl.glUseProgram(0)
end

--[[
    Clear normals using compute shader to prepare for normal calculation
    which accumulates, averaging normals from each face adjacent to
    each vertex.
]]
local clear_normals_comp_src = [[
#version 430

#define THREADS_PER_BLOCK 256
layout(local_size_x=256) in;
layout(std430, binding=0) buffer nblock { vec4 normals[]; };

void main()
{
    uint index = gl_GlobalInvocationID.x;
    vec4 n = normals[index];
    n.xyz = vec3(0.);
    normals[index] = n;
}
]]

local function clearNormals()
    local nvbo = vbos.normals
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, nvbo[0])
    local prog = progs.clearnorms
    gl.glUseProgram(prog)
    gl.glDispatchCompute(num_verts/256+1, 1, 1)
    gl.glUseProgram(0)
end

--[[
    Calculate normals in compute shader
    Trigger shader on element indices, indexing into positions and
    normals attribute buffers. Sum up the contributions from each
    face including each vertex indexed.
]]
local calc_normals_comp_src = [[
#version 430

layout(local_size_x=256) in;
layout(std430, binding=0) buffer vblock { vec4 positions[]; };
layout(std430, binding=1) coherent buffer nblock { vec4 normals[]; };

struct faceData {
    int a;
    int b;
    int c;
};
layout(std430, binding=2) coherent buffer iblock { faceData indices[]; };

uniform int numTris;
uniform int triidx;
uniform int trimod;

void main()
{
    uint index = gl_GlobalInvocationID.x;
    if (index >= numTris)
        return;

    // Handle only one triangle per quad at a time to avoid data races
    if ((index % 2) != trimod)
        return;

    faceData fd = indices[index];

    vec3 pos = positions[fd.a].xyz;
    vec3 posx = positions[fd.b].xyz;
    vec3 posy = positions[fd.c].xyz;

    vec3 v1 = posx - pos;
    vec3 v2 = posy - pos;
    vec3 norm = cross(v1, v2);

    if (triidx == 0)
    {
        normals[fd.a].xyz += norm;
    }
    else if (triidx == 1)
    {
        normals[fd.b].xyz += norm;
    }
    else
    {
        normals[fd.c].xyz += norm;
    }
}
]]
local function recalculateNormals()
    clearNormals()

    local vvbo = vbos.vertices
    local nvbo = vbos.normals
    local fvbo = vbos.elements
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 1, nvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 2, fvbo[0])
    local prog = progs.calcnorms
    gl.glUseProgram(prog)

    local unt_loc = gl.glGetUniformLocation(prog, "numTris")
    local uti_loc = gl.glGetUniformLocation(prog, "triidx")
    local utm_loc = gl.glGetUniformLocation(prog, "trimod")
    gl.glUniform1i(unt_loc, subdivs*subdivs*2)
    for m=0,1 do
        gl.glUniform1i(utm_loc, m)
        for i=0,2 do
            gl.glUniform1i(uti_loc, i)
            gl.glDispatchCompute(num_tri_idxs/256+1, 1, 1)
            gl.glMemoryBarrier(GL.GL_ALL_BARRIER_BITS)
        end
    end
    gl.glUseProgram(0)
end

local function init_gridmesh_verts(prog, facets)
    num_verts = (subdivs+1)*(subdivs+1)
    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    local vnorm_loc = gl.glGetAttribLocation(prog, "vNormal")

    local sz = num_verts*4*ffi.sizeof('float')
    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(vpos_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    vbos.vertices = vvbo

    local nvbo = glIntv(0)
    gl.glGenBuffers(1, nvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, nvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(vnorm_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    vbos.normals = nvbo

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vnorm_loc)

    initVertexPositions()
end

local function init_gridmesh_faces(facets)
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)

    num_tri_idxs = 6 * facets * facets
    local sz = 4 * num_tri_idxs * ffi.sizeof('GLuint')

    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_COPY)
    vbos.elements = qvbo

    setFaceIndices()
end

function scene.initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.glBindVertexArray(vao)

    progs.meshvsfs = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    progs.initverts = sf.make_shader_from_source({
        compsrc = initverts_comp_src,
        })

    progs.initfaces = sf.make_shader_from_source({
        compsrc = initfaces_comp_src,
        })

    progs.clearnorms = sf.make_shader_from_source({
        compsrc = clear_normals_comp_src,
        })

    progs.calcnorms= sf.make_shader_from_source({
        compsrc = calc_normals_comp_src,
        })

    progs.perturbverts= sf.make_shader_from_source({
        compsrc = perturbverts_comp_src,
        })

    init_gridmesh_verts(progs.meshvsfs, subdivs)
    init_gridmesh_faces(subdivs)

    gl.glBindVertexArray(0)
end

function scene.exitGL()
    gl.glBindVertexArray(vao)
    for _,v in pairs(vbos) do
        gl.glDeleteBuffers(1,v)
    end
    vbos = {}

    for _,p in pairs(progs) do
        gl.glDeleteProgram(p)
    end
    progs = {}

    local vaoId = ffi.new("GLuint[1]", vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

function scene.render_for_one_eye(mview, proj)
    gl.glDisable(GL.GL_CULL_FACE)

    local prog = progs.meshvsfs
    local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(prog, "prmtx")
    gl.glUseProgram(prog)
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    local m = {}
    for i=1,16 do m[i] = mview[i] end
    mm.glh_scale(m,10,10,10)
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, m))
    gl.glBindVertexArray(vao)
    gl.glDrawElements(GL.GL_TRIANGLES,
        num_tri_idxs,
        GL.GL_UNSIGNED_INT, nil)
    gl.glBindVertexArray(0)

    gl.glUseProgram(0)
end

function scene.timestep(absTime, dt)
    global_time = absTime

    perturbVertexPositions()
    gl.glMemoryBarrier(GL.GL_ALL_BARRIER_BITS)
    recalculateNormals()
end

function scene.keypressed(ch)
    if ch == 'f' then return end
    if ch == 'l' then return end

    if ch ~= 'n' then
        perturbVertexPositions()
    end
    gl.glMemoryBarrier(GL.GL_ALL_BARRIER_BITS)

    recalculateNormals()
end

return scene
