-- compute_mesh.lua
-- Draw a simple cube whose sides have been broken into n subdivisions.

scene = {}

local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')
local glCharv    = ffi.typeof('GLchar[?]')
local glConstCharpp = ffi.typeof('const GLchar *[1]')

local vao = 0
local vbos = {}

local prog_meshvsfs = 0
local num_tri_idxs = 0
local comp_prog = 0

local basic_vert = [[
#version 330

in vec4 vPosition;
in vec4 vNormal;

out vec3 vfNormal;

uniform mat4 mvmtx;
uniform mat4 prmtx;

void main()
{
    vfNormal = vNormal.xyz;
    gl_Position = prmtx * mvmtx * vPosition;
}
]]

local basic_frag = [[
#version 330

in vec3 vfNormal;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfNormal, 1.0);
}
]]


-- Modify each of the mesh's vertices in the buffer by a function.
-- Modeling with vector fields.
local comp_src = [[
#version 430

#define THREADS_PER_BLOCK 256
layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
uniform float strength;

vec3 displacePoint(vec3 p);

void main()
{
    int index = int(gl_GlobalInvocationID);
    vec4 p = positions[index];
    p.xyz = displacePoint(p.xyz);
    positions[index] = p;
}
]]

local libiq_sdf_src = [[
//
// Distance field functions
//
float smin( float a, float b, float k )
{
    float h = clamp( 0.5+0.5*(b-a)/k, 0.0, 1.0 );
    return mix( b, a, h ) - k*h*(1.0-h);
}

float sdSphere( vec3 p, float s )
{
    return length(p)-s;
}

float udRoundBox( vec3 p, vec3 b, float r )
{
  return length(max(abs(p)-b,0.0))-r;
}

#if 0
vec3 calcNormal( in vec3 pos )
{
    vec3 eps = vec3( 0.001, 0.0, 0.0 );
    vec3 nor = vec3(
        map(pos+eps.xyy).x - map(pos-eps.xyy).x,
        map(pos+eps.yxy).x - map(pos-eps.yxy).x,
        map(pos+eps.yyx).x - map(pos-eps.yyx).x );
    return normalize(nor);
}
#endif
]]

local snakehead_src = [[
float sdHead( vec3 p )
{
    p *= .2; // scale *up*
    vec3 peye1 = p + vec3(.1, 0., .1);
    vec3 peye2 = p + vec3(-.1,0.,.1);
    vec3 psnout = p + vec3(0., 0., .3);
    float r = .2;
    float deyes = min(sdSphere(peye1, r), sdSphere(peye2, r));

    vec3 pbrow1 = p + vec3(.14, -.12, .32);
    vec3 pbrow2 = p + vec3(-.14, -.12, .32);
    deyes = min(deyes, sdSphere(pbrow1, r*.125));
    deyes = min(deyes, sdSphere(pbrow2, r*.125));
    deyes = smin(deyes, udRoundBox(psnout, vec3(.035, .015, .28), .1), .3);
    return deyes;
}

float DE(vec3 p)
{
    return sdHead(p);
}

// Try raymarching towards origin
vec3 raymarchPoint(vec3 p)
{
    vec3 ro = p;
    vec3 rd = -p;
    vec3 step = strength * rd;
    for (int i=0; i<100; ++i)
    {
        float d = DE(ro);
        ro += step * d;
    }
    return ro;
}

vec3 displacePoint(vec3 p)
{
    return raymarchPoint(p);
}
]]


local vecfield_src = [[
vec3 vectorField(in vec3 pos)
{
    vec3 wc = pos - vec3(2.);
    float f = 1.;
    vec2 planeCenter = vec2(2.) + 1.*vec2(cos(f*wc.z), sin(f*wc.z));
    vec3 dp = vec3(wc.xy-planeCenter, 0.);

    float mag = 1. - smoothstep(0.,3.,length(dp));
    vec3 pull = -dp;
    pull.z = 1.;
    return mag * pull;
}

vec3 distortPoint(vec3 p)
{
    return strength * vectorField((2.*p.xyz+vec3(2.)));
}

vec3 displacePoint(vec3 p)
{
    return p + distortPoint(p);
}
]]

local noise_src = [[
float hash( float n ) { return fract(sin(n)*753.5453123); }
float noise( in vec3 x )
{
    vec3 p = floor(x);
    vec3 f = fract(x);
    f = f*f*(3.0-2.0*f);
    
    float n = p.x + p.y*157.0 + 113.0*p.z;
    return mix(mix(mix( hash(n+  0.0), hash(n+  1.0),f.x),
                   mix( hash(n+157.0), hash(n+158.0),f.x),f.y),
               mix(mix( hash(n+113.0), hash(n+114.0),f.x),
                   mix( hash(n+270.0), hash(n+271.0),f.x),f.y),f.z);
}

float DE(vec3 p)
{
    // noise returns values in [0,1]
    float n11 = 2.*noise(10.*p) - 1.;
    return .01*n11;
}

vec3 displacePoint(vec3 p)
{
    return p + p*DE(p);
}
]]

local spherefield_src = [[
vec3 vectorField(in vec3 pos)
{
    vec3 center = vec3(0.);
    vec3 toc = pos - center;
    float len = length(toc);
    float d1 = len - 1.;
    if (len > 1.)
    {
        return -toc * d1;
    }
    return toc * -d1;
}

vec3 distortPoint(vec3 p)
{
    return strength * vectorField(p);
}

vec3 displacePoint(vec3 p)
{
    return p + distortPoint(p);
}
]]


local snap_to_sphere_src = [[
vec3 displacePoint(vec3 p)
{
    vec3 center = vec3(0.);
    float radius = 1.;
    return center + radius*normalize(p-center);
}
]]

local pushout_sphere_src = [[
#line 1
vec3 sphere_center = vec3(0.,0.,0.);

uniform vec3 crater_center;
uniform float radius;

// http://www.iquilezles.org/www/articles/functions/functions.htm
float cubicPulse( float c, float w, float x )
{
    x = abs(x - c);
    if( x>w ) return 0.0f;
    x /= w;
    return 1.0f - x*x*(3.0f-2.0f*x);
}

float profile_function(float r)
{
    float x = pow(r,2.);

    return mix(.75*smoothstep(0,1,x), -smoothstep(0,1,x), smoothstep(0.3,.6,x));
    //return mix(smoothstep(0,1,x), (-1.+smoothstep(0,1,x)), smoothstep(0.3,.7,x));
    //return cubicPulse(.5, .5, x);
    //return r;

    //return clamp(x-.5,0.,1.);
    //return x*x;
    //return sin(3.14159*x);

    //return .1*-2.*r*(r-.5);
    return .1* -sin(-2*3.14159*3/4*x);
}

vec3 displacePoint(vec3 p)
{
    float ltc = length(p - crater_center);
    if (ltc < radius)
    {
        float r_1 = 1. - (ltc/radius);
        vec3 disp = strength * profile_function(r_1) * normalize(p-sphere_center);
        return p + disp;
    }
    return p;
}
]]

local distortion_srcs = {
    snap_to_sphere_src,
    libiq_sdf_src .. snakehead_src,
    vecfield_src,
    noise_src,
    spherefield_src,
    pushout_sphere_src,
}

-- Create a VBO for six faces of a subdivided cube.
local function init_spheremesh_attributes(prog)
    local facets = 64

    -- Create a single subdivided square
    local v = {}
    function make_points(mtx)
        for j=0,facets do
            for i=0,facets do
                pt = {i/facets,j/facets,0,1} -- xyzw
                pt = mm.transform(pt, mtx)
                table.insert(v, pt[1])
                table.insert(v, pt[2])
                table.insert(v, pt[3])
                table.insert(v, pt[4])
            end
        end
    end

    local f = {}
    function make_faces(first)
        for j=1,facets do
            for i=1,facets do
                xy = {i,j}
                local function getidx(i,j)
                    return (facets+1)*j + i + first
                end
                table.insert(f, getidx(i,j))
                table.insert(f, getidx(i-1,j-1))
                table.insert(f, getidx(i,j-1))

                table.insert(f, getidx(i,j))
                table.insert(f, getidx(i-1,j))
                table.insert(f, getidx(i-1,j-1))
            end
        end
    end

    -- Replicate the square 6 times and transform each
    -- to a different cube face.
    local mtx = {
        1,0,0,0,
        0,1,0,0,
        0,0,1,0,
        0,0,0,1,
    }
    local mtx2 = {
        1,0,0,0,
        0,0,1,0,
        0,-1,0,0,
        0,0,0,1,
    }
    local mtx3 = {
        0,1,0,0,
        0,0,1,0,
        -1,0,0,0,
        0,0,0,1,
    }
    local mtx4 = {
        1,0,0,0,
        0,1,0,0,
        0,0,1,0,
        0,0,1,1,
    }
    local mtx5 = {
        1,0,0,0,
        0,0,1,0,
        0,-1,0,0,
        0,1,0,1,
    }
    local mtx6 = {
        0,1,0,0,
        0,0,1,0,
        -1,0,0,0,
        1,0,0,1,
    }
    local matrices = {mtx, mtx2, mtx3, mtx4, mtx5, mtx6}

    local fs = (facets+1)*(facets+1)
    for k,v in pairs(matrices) do
        make_faces((k-1)*fs)

        local m = {}
        mm.make_identity_matrix(m)
        mm.glh_scale(m, 2,2,2)
        mm.glh_translate(m, -.5,-.5,-.5)
        mm.post_multiply(m, v)
        make_points(m)
    end

    num_verts = #v
    num_tri_idxs = #f
    local verts = glFloatv(#v, v)
    local faces = glUintv(#f, f)

    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    local vnorm_loc = gl.glGetAttribLocation(prog, "vNormal")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(vpos_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(vbos, vvbo)

    local nvbo = glIntv(0)
    gl.glGenBuffers(1, nvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, nvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(vnorm_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(vbos, nvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vnorm_loc)

    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(faces), faces, GL.GL_STATIC_DRAW)
    table.insert(vbos, qvbo)
end

function scene.initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.glBindVertexArray(vao)

    prog_meshvsfs = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })
    init_spheremesh_attributes(prog_meshvsfs)

    scene.switchToField(1)
    scene.runComputeShader(.01)

    gl.glBindVertexArray(0)
end

function scene.exitGL()
    gl.glBindVertexArray(vao)
    for _,v in pairs(vbos) do
        gl.glDeleteBuffers(1,v)
    end
    vbos = {}

    gl.glDeleteProgram(prog_meshvsfs)
    gl.glDeleteProgram(comp_prog)

    local vaoId = ffi.new("GLuint[1]", vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

-- Compile a new compute shader
function scene.switchToField(x)
    local cs = distortion_srcs[x]
    if cs == nil then return end

    gl.glDeleteProgram(comp_prog)
    comp_prog = sf.make_shader_from_source({
        compsrc = comp_src..cs,
        })
end

function scene.runComputeShader(strength)
    --Displaces the vertices in cubes' VBO by a vector field with scalar magnitude.
    local vvbo = vbos[1]
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vvbo[0])
    gl.glUseProgram(comp_prog)
    local ust_loc = gl.glGetUniformLocation(comp_prog, "strength")
    if ust_loc > -1 then
        gl.glUniform1f(ust_loc, strength)
    end
    gl.glDispatchCompute(num_verts/256, 1, 1)
    gl.glUseProgram(0)
end


function scene.render_for_one_eye(mview, proj)
    local prog = prog_meshvsfs
    local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(prog, "prmtx")
    gl.glUseProgram(prog)
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    local m = {}
    for i=1,16 do m[i] = mview[i] end
    mm.glh_scale(m,10,10,10)
    --mm.glh_translate(m, -.5, -.5, -.5)
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, m))
    gl.glBindVertexArray(vao)
    gl.glDrawElements(GL.GL_TRIANGLES, num_tri_idxs, GL.GL_UNSIGNED_INT, nil)
    gl.glBindVertexArray(0)

    gl.glUseProgram(0)
end

function scene.timestep(absTime, dt)
end

function random_variance(center, variance)
    -- Return a random value centered on center, with += variance
    return center - variance + variance*2*math.random()
end

function create_random_crater()

    -- TODO: monte carlo random on sphere
    local cc = {random_variance(0,1), random_variance(0,1), random_variance(0,1), }
    local r = random_variance(.2, .1)

    -- Set uniforms for sphere locations - program must be active.
    gl.glUseProgram(comp_prog)
    local ucc_loc = gl.glGetUniformLocation(comp_prog, "crater_center")
    gl.glUniform3f(ucc_loc, cc[1], cc[2], cc[3])
    local ucr_loc = gl.glGetUniformLocation(comp_prog, "radius")
    gl.glUniform1f(ucr_loc, r)
    gl.glUseProgram(0)

    gl.glBindVertexArray(vao)
    scene.runComputeShader(.1)
    gl.glBindVertexArray(0)
end

function scene.keypressed(ch)
    scene.switchToField(string.byte(ch,1) - string.byte('0'))
    gl.glBindVertexArray(vao)
    scene.runComputeShader(.1)
    gl.glBindVertexArray(0)

    if ch == '6' then
        create_random_crater()
    end
    if ch == 'c' then
        scene.switchToField(6)
        for i=0,100 do create_random_crater() end
    end
end

return scene
