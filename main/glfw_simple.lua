-- glfw_simple.lua

package.path = package.path..';lib/?.lua'
local ffi = require("ffi")
local glfw = require "glfw"
GL = require "opengl"
GL.loader = glfw.glfw.GetProcAddress
GL:import()

package.path = package.path .. ';lib/?.lua'
package.path = package.path .. ';piscene/?.lua'

local socket = nil
if (ffi.os == "Windows") then
    package.cpath = package.cpath .. ';bin/windows/socket/core.dll'
    package.loadlib("socket/core.dll", "luaopen_socket_core")
    socket = require 'socket.core'
elseif (ffi.os == "Linux") then
    package.cpath = package.cpath .. ';bin/linux/'..jit.arch..'/socket/?.so'
    socket = require 'socket.core'
elseif (ffi.os == "OSX") then
    package.cpath = package.cpath .. ';bin/osx/socket/?.so'
    socket = require 'socket.core'
end

local clock = os.clock
if socket then
    print("Using socket.gettime in Luasocket version "..socket._VERSION)
    clock = socket.gettime
end

local win_w = 800
local win_h = 600

local scenedir = "piscene"
local Scene
function switch_to_scene(name)
    local fullname = scenedir.."."..name
    local status,err = pcall(function ()
        local SceneLibrary = require(fullname)
        Scene = SceneLibrary.new()
    end)
    if not status then
        print(err)
    end
    if Scene then
        if Scene.setDataDirectory then Scene:setDataDirectory("data") end
        if Scene.resizeViewport then Scene:resizeViewport(win_w,win_h) end
        Scene:initGL()
    end
end

function onkey(window, key, scancode, action, mods)
    if Scene and Scene.keypressed then
        local consumed = Scene:keypressed(key, scancode, action, mods)
        if consumed then return end
    end
end

function display()
    gl.glClearColor(.2,.2,.2,1)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    local status, err = pcall(function () Scene:render() end)
    if not status then print(err) end
end

function timestep(absTime, dt)
    if Scene and Scene.timestep then Scene:timestep(absTime, dt) end
    --handleJoystick()
end

function main()
    local fullscreen = false
    local startScene = nil
    for k,v in pairs(arg) do
        print(k,v)

        if k > 0 then
            if v == '-f' then
                fullscreen = true
            elseif k == 2 then
                startScene = v
            end
        end
    end

    glfw.glfw.Init()

    if jit.arch == "arm" then
        -- Guessing we're on the pi, do nothing
    elseif jit.os == "windows" then
        -- defaults are good here
    elseif jit.os == "OSX" then -- OSX old GL settings
        glfw.glfw.WindowHint(glfw.GLFW.CONTEXT_VERSION_MAJOR, 3)
        glfw.glfw.WindowHint(glfw.GLFW.CONTEXT_VERSION_MINOR, 3)
        glfw.glfw.WindowHint(glfw.GLFW.OPENGL_FORWARD_COMPAT, 1)
        glfw.glfw.WindowHint(glfw.GLFW.OPENGL_PROFILE, glfw.GLFW.OPENGL_CORE_PROFILE)
    end

    w = glfw.glfw.CreateWindow(win_w,win_h,"glfw_simple",nil,nil)
    glfw.glfw.MakeContextCurrent(w)
    glfw.glfw.SetKeyCallback(w, onkey)

    print(ffi.string(gl.glGetString(GL.GL_VENDOR)))
    print(ffi.string(gl.glGetString(GL.GL_RENDERER)))
    print(ffi.string(gl.glGetString(GL.GL_VERSION)))
    print(ffi.string(gl.glGetString(GL.GL_SHADING_LANGUAGE_VERSION)))

    if jit.os == "OSX" then
        -- Core profile requires a VAO
        local vaoId = ffi.new("int[1]")
        gl.glGenVertexArrays(1, vaoId)
        vao = vaoId[0]
        gl.glBindVertexArray(vao)
    end

    switch_to_scene(startScene or 'tests/02simple_clockface')

    local firstFrameTime = clock()
    local lastFrameTime = clock()
    while glfw.glfw.WindowShouldClose(w)==0 do
        if glfw.glfw.GetKey(w, 256) == 1 then glfw.glfw.SetWindowShouldClose(w, 1) end
        glfw.glfw.PollEvents()
        display()

        local now = clock()
        timestep(now - firstFrameTime, now - lastFrameTime)
        lastFrameTime = now

        if socket then socket.sleep(.01) end

        glfw.glfw.SwapBuffers(w)
    end
end

main()