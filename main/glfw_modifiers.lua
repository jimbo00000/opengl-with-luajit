-- A test of modifier key handling in glfw.

package.path = package.path .. ';lib/?.lua'
local ffi = require 'ffi'
local glfw = require 'glfw'
local openGL = require 'opengl'
openGL.loader = glfw.glfw.GetProcAddress
openGL:import()

local altdown = false
local ctrldown = false
local shiftdown = false
local window

-- X11 on Ubuntu 16 and Fedora 21 gives modifier flags that are out-of-date
-- at the time of key press. The key events themselves come through correctly,
-- so we can manually apply their effects to their bit fields.
-- NOTE: This does not account for the case of holding both sides of the
-- same modifier.
function workaroundX11ModifierState(key, action, mods)
    local key_mod_pairs = {
        [glfw.GLFW.KEY_LEFT_SHIFT] = glfw.GLFW.MOD_SHIFT,
        [glfw.GLFW.KEY_RIGHT_SHIFT] = glfw.GLFW.MOD_SHIFT,
        [glfw.GLFW.KEY_LEFT_CONTROL] = glfw.GLFW.MOD_CONTROL,
        [glfw.GLFW.KEY_RIGHT_CONTROL] = glfw.GLFW.MOD_CONTROL,
        [glfw.GLFW.KEY_LEFT_ALT] = glfw.GLFW.MOD_ALT,
        [glfw.GLFW.KEY_RIGHT_ALT] = glfw.GLFW.MOD_ALT,
    }
    local keymod = key_mod_pairs[key]
    if keymod then
        if action == glfw.GLFW.PRESS then
            mods = bit.bor(mods, keymod)
        elseif action == glfw.GLFW.RELEASE then
            mods = bit.bxor(mods, keymod)
        end
    end
    return mods
end

function onkey(window, key, scancode, action, mods)
    -- Hack around X11's broken modifier handling
    if (ffi.os == "Linux") then
        mods = workaroundX11ModifierState(key, action, mods)
    end

    altdown = 0 ~= bit.band(mods, glfw.GLFW.MOD_ALT)
    ctrldown = 0 ~= bit.band(mods, glfw.GLFW.MOD_CONTROL)
    shiftdown = 0 ~= bit.band(mods, glfw.GLFW.MOD_SHIFT)
    print(key,'action=',action, 'mods=',mods)

    if key == glfw.GLFW.KEY_ESCAPE then glfw.glfw.SetWindowShouldClose(window, 1) end
end

function main()
    local win_w = 800
    local win_h = 600
    glfw.glfw.Init()
    window = glfw.glfw.CreateWindow(win_w,win_h,"glfw modifier test",nil,nil)
    glfw.glfw.SetKeyCallback(window, onkey)
    glfw.glfw.MakeContextCurrent(window)
    gl.glViewport(0,0, win_w, win_h)

    print(ffi.string(glfw.glfw.GetVersionString()))
    
    while glfw.glfw.WindowShouldClose(window) == 0 do
        glfw.glfw.PollEvents()

        gl.glClearColor(
            altdown and 1 or 0,
            ctrldown and 1 or 0,
            shiftdown and 1 or 0,
            0.0)
        gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

        glfw.glfw.SwapBuffers(window)
    end
end

main()
