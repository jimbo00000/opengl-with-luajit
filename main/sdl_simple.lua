-- sdl_simple.lua

local DO_POSTFX_DOWNSCALE = false
local win_w = 800*1.5
local win_h = 600*1.5
local datadir = 'data'
local codedir = ''

print(jit.version, jit.os, jit.arch)

local ffi = require "ffi"
local SDL = require "lib/sdl"
local libretro_input = require 'util.libretro_input'

package.path = package.path .. ';lib/?.lua'
local openGL = require "opengl"
openGL.loader = SDL.SDL_GL_GetProcAddress
openGL:import()

local socket = nil
if (ffi.os == "Windows") then
    package.cpath = package.cpath .. ';bin/windows/socket/core.dll'
    package.loadlib("socket/core.dll", "luaopen_socket_core")
    socket = require 'socket.core'
elseif (ffi.os == "Linux") then
    package.cpath = package.cpath .. ';bin/linux/'..jit.arch..'/socket/?.so'
    socket = require 'socket.core'
elseif (ffi.os == "OSX") then
    package.cpath = package.cpath .. ';bin/osx/socket/?.so'
    socket = require 'socket.core'
end

local clock = os.clock
if socket then
    print("Using socket.gettime in Luasocket version "..socket._VERSION)
    clock = socket.gettime
end

local lr_input = require 'util.libretro_input'
local fpstimer = require 'util.fpstimer'
local g_ft = fpstimer.create()
local filesystem = require 'util.filesystem'

local window

-- Modular plugin setup
for k,v in pairs({'piscene','scene','effect'}) do
    package.path = package.path .. ';'..v..'/?.lua'
end

local Scene
local PostFX = nil

--[[
    Scene concerns:
    loading, switching between. Tries to catch any errors in the scene
    being switched to and prints them to screen.
    Ties into scene_selector and called on input events.
]]

-- http://wiki.interfaceware.com/534.html
function string_split(s, d)
    local t = {}
    local i = 0
    local f
    local match = '(.-)' .. d .. '()'
    
    if string.find(s, d) == nil then
        return {s}
    end
    
    for sub, j in string.gmatch(s, match) do
        i = i + 1
        t[i] = sub
        f = j
    end
    
    if i ~= 0 then
        t[i+1] = string.sub(s, f)
    end
    
    return t
end

function addDirToPackagePath(dir)
    local paths = {}
    local hash = {}

    -- Ensure uniqueness and order of entries
    local dirs = string_split(package.path, ';')
    table.insert(dirs, dir)
    for k,v in pairs(dirs) do
        if not hash[v] then
            table.insert(paths, v)
            hash[v] = true
        end
    end
    package.path = table.concat(paths, ';')

    -- Print new path
    for k,v in pairs(paths) do
        print('\t'..k..') '..v)
    end
    print('package.path=', package.path)
end

---TODO: add all scenes in a list to a "construct" scene if #>1
function switchToScene(namelist)
    local name = namelist[1]
    print("switchToScene: ",name)

    -- Handle directories
    --TODO: handle trailing slash correctly on git bash
    if string.sub(name,-1) == '/' then
        local dirname = string.sub(name,0,#name-1)
        --print('DIR', dirname, Scene.scenedir)
        local fulldir = Scene.scenedir..'/'..dirname
        
        addDirToPackagePath(fulldir..'/?.lua')

        -- Special case for scene selector
        -- TODO: Should scene selector handle multiple scenes?
        -- How could it interact with the scene container construct?
        Scene:populateSceneList(fulldir)
        print("  Dir added: ", name)
        return
    end

    local oldScene = Scene
    Scene = nil

    local status,err = pcall(function ()
        local SceneLibrary = require(name)

        -- The scene_selector's init takes a directory to scan for scenes.
        --TODO: nil defaults to piscene? (It seems preferable
        --for scene selector to not be specialized for any specific path)
        local param = nil
        if name == 'scene_selector' then
            param = 'piscene' -- where to scan for scenes
        end

        Scene = SceneLibrary.new(param)

        --if Scene then
        if Scene.setDataDirectory then Scene:setDataDirectory(datadir) end
        if Scene.resizeViewport then Scene:resizeViewport(win_w,win_h) end
        local err = Scene:initGL()
        if type(err)=='string' then
            Scene:exitGL()
            Scene = nil
            error(err)
        end
    end)

    local lasterror = nil
    if not status then
        lasterror = 'Error in '..name..':\n    '..err
        -- TODO: hold on to scene_selector
        if oldScene then
            oldScene.lastErrorMessage = lasterror
        end
        if Scene then
            Scene:exitGL()
        end
        Scene = nil
    else
        print(status,err,'    success.')
    end

    if not Scene then
        print(lasterror)
        if not oldScene then
            print("No such scene:", name, "- exiting.")
            os.exit(1)
        end
        print("Scene failed to initialize. Reverting to previous scene.")
        Scene = oldScene
        -- Do we need to unload the module?
        package.loaded[name] = nil
    else
        print('destroying old scene...')
        if oldScene and oldScene.exitGL then
            oldScene:exitGL()
        end
    end
end

function enterScene()
    if Scene and Scene.getSelectedSceneName then
        saved_idx = Scene.scene_idx -- save state
        local name = Scene:getSelectedSceneName()
        switchToScene({name})
    end
end

function exitScene()
    if Scene and Scene.getSelectedSceneName then
        -- Select+Start on selector exits app
        os.exit(0)
    end

    datadir = 'data'
    switchToScene({'scene_selector'})
    if saved_idx then
        --Scene.scene_idx = saved_idx -- restore state
    end
end

--[[
    Input Concerns:
    Handles keyboard and joystick events through SDL.
    Holds state globally and does joystick simulation using keyboard input.
    Also handles mouse motion.
]]
local cur_keys = {}
for i=1,255 do cur_keys[i] = 0 end
function onkey(key, scancode, action, mods)
    if true then
        if Scene and Scene.keypressed then
            local consumed = Scene:keypressed(key, scancode, action, mods)
            if consumed then return end
        end
    end

    if true then
        if action == 1 and key < 255 and string.char(key) == 'p' then
            if not PostFX then
                initPostFX()
            else
                PostFX = nil
                if Scene.resizeViewport then Scene:resizeViewport(win_w,win_h) end
            end
        end
    end

    -- Simulate retropad inputs with keys
    cur_keys[key] = action
end

function onmousemove(x,y)
    if Scene and Scene.mousemove then
        Scene:mousemove(x,y)
    end
end

function onmousebutton(type, which, button, state, x, y)
    if Scene and Scene.mousebutton then
        Scene:mousebutton(type, which, button, state, x, y)
    end
end

function onmousewheel(type, which, x, y, direction)
    if Scene and Scene.mousewheel then
        Scene:mousewheel(type, which, x, y, direction)
    end
end

local retropadStates = {}

function update_retropad(joystates) -- TODO add last_states parameter
    for j=1,#joystates do
        local curstate = joystates[j]
        local laststate = curstate.last
        if curstate and laststate then
            for b=1,14 do
                if laststate[b] == 0 and curstate[b] == 1 then
                    on_button_pressed(j,b)
                end
            end
        end
    end
end

-- TODO: release or event function
function on_button_pressed(padIndex, button)
    local holdingSelect = false
    local padState = retropadStates[padIndex]
    if padState[lr_input.JOYPAD_SELECT] ~= 0 then
        holdingSelect = true
    end

    -- TODO: isButtonPressed function for a given pad
    if holdingSelect and button == lr_input.JOYPAD_START then
        exitScene()
    elseif button == lr_input.JOYPAD_A then
        enterScene()
    end
end

-- Check here for button pressed state to initiate changes(load new scene)
-- TODO: move this out into another layer? Scene changer?
function updateRetropad(joyStates)
    update_retropad(joyStates)
    if Scene then
        if Scene.update_retropad then
            Scene:update_retropad(joyStates)
        end
    end
end

-- TODO: handle disconnection gracefully
function handleJoystick()
    local lastStates = {}
    for _,v in pairs(retropadStates) do
        local last = {}
        for i=1,14 do last[i] = v[i] end
        table.insert(lastStates, last)
    end
    retropadStates = {}
    populateRetropadStates(retropadStates)

    for i=1,#lastStates do
        retropadStates[i].last = lastStates[i]
    end

    updateRetropad(retropadStates)
end

function populateRetropadStates(joyStates)
    handleNativeSDLJoysticks(joyStates)

    local doSimJoysticks = false

    local nj = SDL.SDL_NumJoysticks()
    if nj == 0 then
        doSimJoysticks = true
    end
    if doSimJoysticks then
        handleKeyboardEmulatedJoysticks(cur_keys, joyStates)
    end
end

function handleNativeSDLJoysticks(joyStates)
    -- Get SDL joystick states into this table
    local pJoysticks = {}
    local nj = SDL.SDL_NumJoysticks()
    for i=0,nj-1 do
        local jname = SDL.SDL_JoystickNameForIndex(i)
        local jp = SDL.SDL_JoystickOpen(i)
        local nb = SDL.SDL_JoystickNumButtons(jp)
        local na = SDL.SDL_JoystickNumAxes(jp)
        --print(i,'['..ffi.string(jname)..']',nb..' buttons',na..' axes')
        table.insert(pJoysticks, jp)
    end

    for j=1,#pJoysticks do
        -- TODO: cache
        local pJoystick = pJoysticks[j]
        local status = tostring(j)..' '..tostring(pJoystick)..' '
        local axes = {}
        local buttons = {}

        local nb = SDL.SDL_JoystickNumButtons(pJoystick)
        for i=0,nb-1 do
            local bi = SDL.SDL_JoystickGetButton(pJoystick,i)
            status = status..bi
            table.insert(buttons, bi)
        end
        local na = SDL.SDL_JoystickNumAxes(pJoystick)
        for i=0,na-1 do
            local ai = SDL.SDL_JoystickGetAxis(pJoystick,i)
            status = status..' '..ai
            table.insert(axes, ai)
        end
        local name = ffi.string(SDL.SDL_JoystickName(pJoystick))
        local rp = libretro_input.getRetropadStateFromSDL(name, axes, buttons)
        table.insert(joyStates,rp)
        --print(status)
    end
end

-- Emulate retropad buttons with keyboard.
-- For those times when you don't have a USB gamepad handy,
-- or when you don't have 5 of them.
local function getRetropadStateFromKeyboardMapping(keystates, mapping)
    local rp = {}

    for i=1,14 do rp[i] = 0 end
    local map = libretro_input.mappings[mapping]
    for i=44,255 do
        local bi = string.char(i)
        local k = map[bi]
        if k and keystates[i] == 1 then rp[k] = 1 end
    end

    return rp
end

function handleKeyboardEmulatedJoysticks(keyStates, joyStates)
    local mappings = {
        'keyboard1',
        'keyboard2',
--[[
        'compact_keyboard1',
        'compact_keyboard2',
        'compact_keyboard3',
        'compact_keyboard4',
        'compact_keyboard5',
        ]]
    }
    for _,m in pairs(mappings) do
        local rp = getRetropadStateFromKeyboardMapping(keyStates, m)
        table.insert(joyStates,rp)
    end
end


--[[
    Graphics/main loop concerns
]]
function initPostFX()
    local PostFXLibrary = require('effect.effect_chain')
    local params = {}
    params.filter_names = {
        "vignette",
    }
    PostFX = PostFXLibrary.new(params)
    local w,h = 320,240 -- typical libretro resolution
    PostFX:initGL(w,h)
    if Scene.resizeViewport then Scene:resizeViewport(w,h) end
end

function initGL()
    if DO_POSTFX_DOWNSCALE then
        initPostFX()
    end
end

function display()
    gl.glClearColor(.2,.2,.2,1)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    local status, err = pcall(function () Scene:render() end)
    if not status then print(err) end
end

function timestep(absTime, dt)
    handleJoystick()
    if Scene and Scene.timestep then Scene:timestep(absTime, dt) end
end

function main()
    local fullscreen = false
    local startScenes = {}
    local HELP = [[
    ____Options:____
    -?  help
    --help
    -f  fullscreen
    -C  code directory
    -D  data directory
    -scene=<scene>
    -res=640x480  window size(resolution)
    ]]
    
    for k,v in pairs(arg) do
        --print('ARG',k,v)

        local function ends_with(str, ending)
           return ending == "" or str:sub(-#ending) == ending
        end

        if k > 0 then
            if v == '-?' or v == '--help' then
                print(HELP)
                os.exit(0)
            elseif v == '-f' then
                fullscreen = true
            elseif string.match(v, "^-C=[%./]*%a+") then
                codedir = string.sub(v,4)
                print("codedir",codedir)
            elseif string.match(v, "^-D=[%./]*%a+") then
                datadir = string.sub(v,4)
                print("datadir",datadir)
            elseif string.match(v, "^-scene=[%./]*%a+") then
                -- Jump directly to scene from cmd line arg
                local prefix = '-scene='
                local scene = string.sub(v,string.len(prefix)+1)
                if ends_with(scene, '.lua') then
                    scene = string.sub(scene,0,#scene-4)
                    print('Stripped off the .lua suffix for you...')
                end
                print("scene=", scene)

                local dirs = string_split(scene, '/')
                local x = table.remove(dirs,#dirs)
                table.insert(startScenes, x)

                for k,v in pairs(dirs) do
                    print(dirs,k,v)
                end
                local newpath = table.concat(dirs, '/')..'/?.lua'
                print(newpath)
                package.path = package.path..';'..newpath
                print(package.path)

            elseif string.match(v, "^-res=%d+x%d+") then
                local prefix = '-res='
                local modestr = string.sub(v,string.len(prefix)+1)
                local x,y = string.match(modestr, "^(%d+)x(%d+)")
                local w,h = tonumber(x), tonumber(y)
                if w and h then
                    print("Setting window resolution:", w,h)
                    win_w,win_h = w,h
                else
                    print("Invalid resolution: ", modestr)
                end

            elseif k == 2 then
                table.insert(startScenes, v)
            end
        end
    end

    filesystem.dataDirectory = datadir

    local linkedver = ffi.new("SDL_version[1]")
    SDL.SDL_GetVersion(linkedver)
    local revnum = SDL.SDL_GetRevisionNumber()
    local revstr = ffi.string(SDL.SDL_GetRevision())
    local v = linkedver[0]
    local vstr = v.major..'.'..v.minor..'.'..v.patch..' rev '..revstr
    print('SDL version '..vstr)

    SDL.SDL_Init(SDL.SDL_INIT_VIDEO + SDL.SDL_INIT_JOYSTICK)

    if fullscreen then
        local current = ffi.new("SDL_DisplayMode[1]")
        local should_be_zero = SDL.SDL_GetCurrentDisplayMode(0, current)
        if should_be_zero ~= 0 then
            print('error: ',should_be_zero)
        end
        local function printMode(str, dispmode)
            local mode = dispmode[0]
            if not mode then return end
            print(str,mode.w..'x'..mode.h..'@'..mode.refresh_rate..' Hz')
        end
        printMode('Current: ', current)
        should_be_zero = SDL.SDL_GetDesktopDisplayMode(0, current)
        if should_be_zero ~= 0 then print('error: ',should_be_zero) end
        printMode('Desktop: ', current)
        if current then
            local mode = current[0]
            if mode then
                win_w = mode.w
                win_h = mode.h
                print("Video mode: "..win_w.."x"..win_h)
            end
        end
    end

    local flags = 
        ffi.C.SDL_WINDOW_OPENGL +
        ffi.C.SDL_WINDOW_SHOWN
    if fullscreen then
        flags = flags + ffi.C.SDL_WINDOW_FULLSCREEN
        SDL.SDL_ShowCursor(0)
    else
        flags = flags + ffi.C.SDL_WINDOW_RESIZABLE
    end

    if jit.os == "OSX" then -- OSX old GL settings
        SDL.SDL_GL_SetAttribute(ffi.C.SDL_GL_CONTEXT_MAJOR_VERSION, 3)
        SDL.SDL_GL_SetAttribute(ffi.C.SDL_GL_CONTEXT_MINOR_VERSION, 3)
        SDL.SDL_GL_SetAttribute(ffi.C.SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG, 1)
        SDL.SDL_GL_SetAttribute(ffi.C.SDL_GL_CONTEXT_PROFILE_MASK, ffi.C.SDL_GL_CONTEXT_PROFILE_CORE)
    end

    SDL.SDL_GL_SetAttribute(ffi.C.SDL_GL_MULTISAMPLEBUFFERS, 1)
    SDL.SDL_GL_SetAttribute(ffi.C.SDL_GL_MULTISAMPLESAMPLES, 16)

    local windowTitle = "OpenGL with Luajit"
    window = SDL.SDL_CreateWindow(
        windowTitle,
        SDL.SDL_WINDOWPOS_UNDEFINED,
        SDL.SDL_WINDOWPOS_UNDEFINED,
        win_w,
        win_h,
        flags
    )

    if window == nil then
        print("Could not create window: " .. ffi.string( SDL.SDL_GetError() ))
        os.exit(1)
    end

    glContext = SDL.SDL_GL_CreateContext(window);
    if glContext == nil then
        print("There was an error creating the OpenGL context!\n")
        return 0
    end

    SDL.SDL_GL_MakeCurrent(window, glContext)

    if jit.os == "OSX" then
        -- Core profile requires a VAO
        local vaoId = ffi.new("int[1]")
        gl.glGenVertexArrays(1, vaoId)
        vao = vaoId[0]
        gl.glBindVertexArray(vao)
    end

    initGL()
    if #startScenes == 0 then
        table.insert(startScenes, 'scene_selector')
    end
    switchToScene(startScenes)
    --if Scene and Scene.resizeViewport then Scene:resizeViewport(win_w, win_h) end

    print(ffi.string(gl.glGetString(GL.GL_VENDOR)))
    print(ffi.string(gl.glGetString(GL.GL_RENDERER)))
    print(ffi.string(gl.glGetString(GL.GL_VERSION)))
    print(ffi.string(gl.glGetString(GL.GL_SHADING_LANGUAGE_VERSION)))

    local firstFrameTime = clock()
    local lastFrameTime = clock()
    local quit = false
    while not quit do
        local event = ffi.new("SDL_Event")
        while SDL.SDL_PollEvent(event) ~= 0 do
            if event.type == SDL.SDL_QUIT then
                quit = true
            elseif event.type == SDL.SDL_KEYDOWN then
                if event.key.keysym.sym == SDL.SDLK_ESCAPE then
                    quit = true
                end
                onkey(event.key.keysym.sym, 0, 1, 0)
            elseif event.type == SDL.SDL_KEYUP then
                onkey(event.key.keysym.sym, 0, 0, 0)
            elseif event.type == SDL.SDL_MOUSEMOTION then
                onmousemove(event.motion.x, event.motion.y)

            elseif event.type == SDL.SDL_MOUSEBUTTONUP or
                   event.type == SDL.SDL_MOUSEBUTTONDOWN then
                onmousebutton(
                    event.type,
                    event.button.which,
                    event.button.button,
                    event.button.state,
                    event.button.x,
                    event.button.y
                )
            elseif event.type == SDL.SDL_MOUSEWHEEL then
                onmousewheel(
                    event.type,
                    event.motion.which,
                    event.motion.x,
                    event.motion.y)
            end
        end


        if PostFX then PostFX:bind_fbo() end
        display()
        if PostFX then PostFX:unbind_fbo() end

        -- Apply post-processing and present
        if PostFX then
            gl.glDisable(GL.GL_DEPTH_TEST)
            gl.glViewport(0,0, win_w, win_h)
            PostFX:present(win_w, win_h)
        end


        g_ft:onFrame(lastFrameTime)
        Scene.fps = math.floor(g_ft:getFPS()) -- Just jam that in there

        local fpstitle = (ffi.os == "Windows" or ffi.os == "OSX")
        fpstitle = false
        if fpstitle then
            SDL.SDL_SetWindowTitle(window, windowTitle.." "..math.floor(g_ft:getFPS()).." fps")
        end

        local now = clock()
        timestep(now - firstFrameTime, now - lastFrameTime)
        lastFrameTime = now
        if socket then socket.sleep(.01) end

        SDL.SDL_GL_SwapWindow(window)
    end

    SDL.SDL_DestroyWindow(window)
    SDL.SDL_Quit()
end
main()
