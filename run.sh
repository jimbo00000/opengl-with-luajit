#!/bin/bash

# http://stackoverflow.com/questions/394230/detect-the-os-from-a-bash-script
if [[ "$OSTYPE" == "linux-gnu" ]]; then
    os='linux'
elif [[ "$OSTYPE" == "linux-gnueabihf" ]]; then
    os='linux'
elif [[ "$OSTYPE" == "darwin"* ]]; then
    os='osx'
elif [[ "$OSTYPE" == "cygwin" ]]; then
    os='windows'
elif [[ "$OSTYPE" == "msys" ]]; then
    os='windows'
elif [[ "$OSTYPE" == "freebsd"* ]]; then
    os='freebsd'
else
    os='solaris'
fi

if [[ "$HOSTTYPE" == "x86_64" ]]; then
    arch='x64'
elif [[ "$HOSTTYPE" == "arm" ]]; then
    arch='arm'
else
    arch=''
fi

if [[ "$os" == "windows" ]]; then
    arch=''
fi
if [[ "$os" == "osx" ]]; then
    arch=''
fi
echo "\$os="$os

# https://stackoverflow.com/questions/59895/
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo "\$dir="$dir

# set default driver
mainfile='main/glfw_main.lua'
args=()

# parse args
for i in "$@"; do
    if [[ $i == -* ]]; then
        args+=( $i )
    else
        mainfile=$i
    fi
done

# set up main
if [[ "$mainfile" != *lua ]]; then
    mainfile=$mainfile'.lua'
fi

# Infer a 'main/'
if [[ "$mainfile" != main* ]]; then
    mainfile='main/'$mainfile
fi
echo "\$mainfile="$mainfile
echo "\$args="${args[@]}

LD_LIBRARY_PATH=$dir/bin/$os/$arch $dir/bin/$os/$arch/luajit $dir/$mainfile ${args[@]}

