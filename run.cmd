@echo off

SET mainfile=glfw_main.lua

IF NOT [%1] == [] (SET mainfile=%1)

:: Pass all command line params on to the script with %*
bin\windows\luajit main\%mainfile% %*
